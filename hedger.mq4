//+------------------------------------------------------------------+
//|                                                       hedger.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                              http://jaltoh.6x.to |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      "http://jaltoh.6x.to"

//--- input parameters
extern string    Pair1="EURUSD";
extern string    Pair2="USDCHF";
extern int       MA_Period=32;
extern double    MA_Threshold=0.0001;
extern double    Lot_Size = 0.1;
extern int       Magic_Number = 43434343;
extern double    Profit_Threshold = 5.0;
extern int       Max_Order_Pairs = 1;

double neg_threshold;
int order_pairs = 0;

bool
close_all(int magic_number)
{
    bool rc = true;
    
    if (order_pairs < 1) return (true);
           
    for (int i = 0; i < OrdersTotal(); i++) {
        
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
        if (OrderSymbol() != Pair1 && OrderSymbol() != Pair2) continue;
        else if (OrderMagicNumber() != magic_number) continue;

        int ret = OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), 3, Gray);
            
        if (ret < 0) {
           Print("Close order failed with error #", GetLastError() );
           rc = false;
        }
        
    }
    
    if (rc == true) order_pairs = 0;
    
    return (rc);
}

double
calc_profit(int magic_number)
{
    double presult = 0.0;
     
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if (OrderSymbol() != Pair1 && OrderSymbol() != Pair2) continue;
        else if (OrderMagicNumber() != magic_number) continue;

        presult += OrderProfit();
        
    }

    return (presult);   
}

bool 
end_is_neigh()
{
  if (TimeCurrent() - Time[0] < Period() * 60.0 * 0.75) return (false);
  else return (true);
}  

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   neg_threshold = -1.0 * MA_Threshold;
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
  
  if ( calc_profit(Magic_Number) > Profit_Threshold) close_all(Magic_Number);
  
//----
   double ehl1 = iCustom(Pair1, Period(), "Ehlers ZeroLag MA Osc", MA_Period, 50, 2.0, 0, 0, Pair1, 2, 0);
   double ehl1_diff = iCustom(Pair1, Period(), "Ehlers ZeroLag MA Osc", MA_Period, 50, 2.0, 0, 0, Pair1, 0, 0);
   if (ehl1_diff < 0.0 && ehl1 > neg_threshold) return (0);
   if (ehl1_diff > 0.0 && ehl1 < MA_Threshold) return (0);
   
   double ehl2 = iCustom(Pair1, Period(), "Ehlers ZeroLag MA Osc", MA_Period, 50, 2.0, 0, 0, Pair2, 2, 0);
   double ehl2_diff = iCustom(Pair1, Period(), "Ehlers ZeroLag MA Osc", MA_Period, 50, 2.0, 0, 0, Pair2, 0, 0);
   if (ehl2_diff < 0.0 && ehl2 > neg_threshold) return (0);
   if (ehl2_diff > 0.0 && ehl2 < MA_Threshold) return (0);
   
   if (order_pairs() > Max_Order_Pairs) return(0);

   if (end_is_neigh() == false) return(0);
   
   if (ehl1 < neg_threshold && ehl2 > MA_Threshold) {
      OrderSend(Pair1, OP_SELL, Lot_Size, MarketInfo(Pair1, MODE_BID), 3, 0.0, 0.0, "Hedger", Magic_Number);
      OrderSend(Pair2, OP_BUY, Lot_Size, MarketInfo(Pair2, MODE_ASK), 3, 0.0, 0.0, "Hedger", Magic_Number);
      order_pairs++;
   } else if (ehl1 > MA_Threshold && ehl2 < neg_threshold) {
      OrderSend(Pair1, OP_BUY, Lot_Size, MarketInfo(Pair1, MODE_ASK), 3, 0.0, 0.0, "Hedger", Magic_Number);
      OrderSend(Pair2, OP_SELL, Lot_Size, MarketInfo(Pair2, MODE_BID), 3, 0.0, 0.0, "Hedger", Magic_Number);
      order_pairs++;
   }
   
   
//----
   return(0);
  }
//+------------------------------------------------------------------+