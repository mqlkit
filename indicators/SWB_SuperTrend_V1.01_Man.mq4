//+------------------------------------------------------------------+
//|                                         SWB_SuperTrend_V1.01.mq4 |
//|                                Copyright � 2010, Dennis Hamilton |
//|                                              ramble_32@yahoo.com |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2010, Dennis Hamilton"
#property link      "ramble_32@yahoo.com"
//----
extern string  Separator_01="----  Input  Settings  ----";
extern double   lot=0.05;
extern double   tp_1=25;
extern double   tp_2=25;
extern int      magic=123;
extern int      bars_closed=0;
extern string  Separator_02="----  Range  &  Offsets  ----";
extern double   range=40;
extern int      max_level=25;
extern int      curb_level=4;
extern int      cp_level=3;
extern double   p1_offset=0;
extern double   p2_offset=0;
extern int      pend_delete=0;
extern double   profit_target=2.5;
extern double   curb_target=2.5;
extern bool     stealth=true;
extern bool     tier2_sig=false;
extern string  Separator_03="----  Lot  Settings  ----";
extern bool     lot_multiplier=false;
extern double   multiplier=2.0;
extern double   increment=0.0;
extern bool     power_lots=true;
extern string  Separator_04="----  Manual  Trading  ----";
extern bool     auto_trading=false;
extern bool     manual_start=false;
extern bool     manual_buy=false;
extern bool     manual_sell=false;
extern bool     limit_order=false;
extern int      manual_tp=25;
extern double   manual_offset=10;
extern string  Separator_05="----  Additional  ----";
extern double   deviation_reset=0;
extern int      reset_level=3;
extern bool     closeall=false;
//----
double  pt;
double  p_off;
double  p1_off;
double  p2_off;
double  m_tp;
double  m_off;
double  std=0.1;
double  AE1;
double  p_lot;
double  lot2;
double  bal_2;
double  incr_1;
double  b_price;
double  s_price;
double  L;
double  d_reset;
int     t_cnt;
int     OT;
bool    deleteall;
bool    hedge=false;
bool    m2_set;
bool    tp_avg;
string  ID="L";
string  f_trade="NULL";
//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   pt=Point; if(Digits==3 || Digits==5) pt=10*Point;
   tp_1*=pt;
   tp_2*=pt;
   range*=pt;
   if(p1_offset<5) p1_offset=0;
   if(p2_offset<5) p2_offset=0;
   if(manual_offset<5) manual_offset=0;
   p1_off=p1_offset*pt;
   p2_off=p2_offset*pt;
   m_off=manual_offset*pt;
   m_tp=manual_tp*pt;
//----
   d_reset=deviation_reset;
   if(curb_level<4) curb_level=0;
   cp_level-=1;
//----
   AE1=AccountEquity(); p_lot=lot/AE1;
   if(StringLen(Symbol())>6) std=1.0;
   profit_target*=std;
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
   int total=0, p_cnt=0, b_cnt=0, s_cnt=0, b2_cnt=0, s2_cnt=0; t_cnt=0;
   int b3_cnt=0, s3_cnt=0;  double L2[], B_LOOP, S_LOOP;
   for(int i=0; i<OrdersTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      total++;
      if(OT2()!="H") L2[LCT()]=OOP();
//----
      if(cmd()< 2) t_cnt++;
      if(cmd()> 1) p_cnt++;
      if(cmd()==0){ b_cnt++; b3_cnt++; } if(cmd()==4){ b2_cnt++; b3_cnt++; }
      if(cmd()==1){ s_cnt++; s3_cnt++; } if(cmd()==5){ s2_cnt++; s3_cnt++; }
      if(b3_cnt==1 || (b3_cnt>1 && OOP()<B_LOOP)) B_LOOP=OOP();
      if(s3_cnt==1 || (s3_cnt>1 && OOP()>S_LOOP)) S_LOOP=OOP();
   }
//----
   string H=""; if(stealth) H="_S"; if(hedge) H="_H"; if(stealth && hedge) H="_SH";
   if(total==0) f_trade="Null"+H;
   if(t_cnt==0 && p_cnt==1)  f_trade="Pend"+H;
   if(t_cnt==1 && p_cnt==0){ f_trade="Buy"+H; if(s_cnt==1) f_trade="Sell"+H; }
//+------------------------------------------------------------------+
   double o_charts=GlobalVariableGet("open_charts"); o_charts=1; // Global o_charts by-passed
   double AB=AccountBalance(), lot2=lot/o_charts, plf=1/o_charts;
//----
   if(power_lots)
   {
      if(AB>AE1) lot2=NormalizeDouble((p_lot*AB)/o_charts,Digits);
      plf=(lot2/lot)/o_charts; double lot3=lot2;
   }
//----
   double p_targ=profit_target*plf, incr_1=increment*plf, b_lot, s_lot;
//+------------------------------------------------------------------+
   bool t_buy=false, t_sell=false, t2_buy=false, t2_sell=false;
   if(auto_trading && !manual_start)
   {
      if((p1_off==0 && t_cnt==0) || (p1_off>0 && b3_cnt==0 && (s_cnt==0 || f_trade=="Buy"+H)))  t_buy=true;
      if((p1_off==0 && t_cnt==0) || (p1_off>0 && s3_cnt==0 && (b_cnt==0 || f_trade=="Sell"+H))) t_sell=true;
   }
   if(auto_trading || manual_start)
   {
      if(f_trade=="Buy"+H  && b3_cnt>0 && b_cnt<max_level) t2_buy=true;
      if(f_trade=="Sell"+H && s3_cnt>0 && s_cnt<max_level) t2_sell=true;
   }
//----
   if(total==0){ closeall=false; }
   if(b2_cnt==0 && s2_cnt==0) deleteall=false;
   if(!closeall && !deleteall)
   {
      if(t_buy || (t2_buy && Ask<=B_LOOP-range-p2_off) || manual_buy) // BUY
      {
         if(signal()==0 || (b_cnt>0 && !tier2_sig) || manual_buy)
         {
            if(t_cnt>=8) bal_2+=1.0*plf;
            if(lot_multiplier)  b_lot=lot2*MathPow(multiplier,b_cnt);
            if(!lot_multiplier) b_lot=lot2+(incr_1*b_cnt);
            OT=0; p_off=p1_off;
            if(b_cnt>0) p_off=p2_off;
            if(manual_buy){ p_off=m_off; m2_set=true; manual_buy=false; }
            if(p_off>0){ OT=4; if(limit_order){ OT=2; p_off*=-1; limit_order=false; } }
            //----
            OrderSend(Symbol(),OT,b_lot,Ask+p_off,3,0,0,ID+(b_cnt+1),magic,0,Blue);
         }
      }
//----
      if(t_sell || (t2_sell && Bid>=S_LOOP+range+p2_off) || manual_sell) // SELL
      {
         if(signal()==1 || (s_cnt>0 && !tier2_sig) || manual_sell)
         {
            if(t_cnt>=8) bal_2+=1.0*plf;
            if(lot_multiplier)  s_lot=lot2*MathPow(multiplier,s_cnt);
            if(!lot_multiplier) s_lot=lot2+(incr_1*s_cnt);
            OT=1; p_off=p1_off;
            if(s_cnt>0) p_off=p2_off;
            if(manual_sell){ p_off=m_off; m2_set=true; manual_sell=false; }
            if(p_off>0){ OT=5; if(limit_order){ p_off*=-1; OT=3; limit_order=false; } }
            //----
            OrderSend(Symbol(),OT,s_lot,Bid-p_off,3,0,0,ID+(s_cnt+1),magic,0,Red);
         }
      }
   }
//+------------------------------------------------------------------+
   double profit=0, c_prof[10], p_curb, t_lot=0, b2_lot=0, s2_lot=0;
   for(i=0; i<OrdersTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      profit+=OrderProfit();
      if(t_cnt>=curb_level && curb_level>0 && OT2()!="H")
      {
         int p=t_cnt-LCT()+1;
         c_prof[p]=OrderProfit();
         p_curb=c_prof[1]+c_prof[2]+c_prof[3];
      }
      if(cmd()< 2){ t_lot+=OrderLots();  if(LCT()==t_cnt-1) L=OOP(); }
      if(cmd()==0){ b2_lot+=OrderLots(); if(LCT()==1) b_price=OOP(); }
      if(cmd()==1){ s2_lot+=OrderLots(); if(LCT()==1) s_price=OOP(); }
   }
//----
   double dev_b, dev_s, deviate=0;
   if(b_cnt>0){ dev_b=b_price-Ask; if(dev_b>0) deviate=dev_b/pt; }
   if(s_cnt>0){ dev_s=Bid-s_price; if(dev_s>0) deviate=dev_s/pt; }
//+------------------------------------------------------------------+
   int d_cnt=0; double d_OOP=0;
   for(i=0; i<OrdersTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
//----
      double otp=OTP(), tp_3=tp_1; bool curb2=false;
      if(cmd()==0 && LCT()==t_cnt && L-OOP()>range*1.5) curb2=true;
      if(cmd()==1 && LCT()==t_cnt && OOP()-L>range*1.5) curb2=true;
      if(otp!=0 && LCT()>1 && LCT()<t_cnt){ d_cnt=LCT(); d_OOP=OOP(); }
      if(stealth && t_cnt==0) tp_avg=false;
      if(stealth && t_cnt>1){ tp_avg=true; if(LCT()==1) otp=0; }
//----
      if(cmd() <2 && otp==0) // Order Modify
      {
         if(t_cnt>1) tp_3=tp_2;
         if(m2_set){ tp_3=m_tp; m2_set=false; } if(cmd()==1) tp_3*=-1;
         if(!stealth || (stealth&& LCT()==1 && !tp_avg ) || curb2) otp=OOP()+tp_3;
//----
         if(otp!=0 || (LCT()==1 && tp_avg))
         {
            OrderModify(OrderTicket(),0,0,otp,0,CLR_NONE);
            if(LCT()==1) deleteall=true;
         }
      }
//----
      if(t_cnt==0 && pend_delete>0) // Pending Timeout
      {
         datetime OOT=OrderOpenTime(), get_time=TimeCurrent();
         if(cmd()>1 && get_time-OOT>pend_delete*60) OrderDelete(OrderTicket());
      }
   }
//+------------------------------------------------------------------+
   for(i=OrdersTotal()-1; i>=0; i--)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      if(cmd()<2)
      {
         if(deviate>=d_reset && d_reset>0 && t_cnt>=reset_level)
         {
            if(LCT()==1)
            OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
         }
         if(d_cnt>0 && LCT()>d_cnt) // Secondary Curb
         {
            if(!Ask && (cmd()==0 && Ask>=d_OOP) || (cmd()==1 && Bid<=d_OOP))
            OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
            if(!Ask && LCT()==t_cnt)
            {
               if((cmd()==0 && Ask>=OOP()+(range*1.5)) || (cmd()==1 && Bid<=OOP()-(range*1.5)))
               OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
            }
         }
         if(t_cnt>=8 && curb_level>0)
         {
            if(LCT()==t_cnt)
            {
               if((cmd()==0 && Ask>=OOP()+(range*1)) || (cmd()==1 && Bid<=OOP()-(range*1)))
               OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
            }
         }
      }
   }
//+------------------------------------------------------------------+
   double balance=0;
   for(i=0; i<OrdersHistoryTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_HISTORY);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      balance+=OrderProfit();
   }
//----
   if(t_cnt==0) GlobalVariableSet("bal_2"+Symbol()+magic,balance);
   bal_2=GlobalVariableGet("bal_2"+Symbol()+magic);
//+------------------------------------------------------------------+
   bool curb=false;
   if(t_cnt>=curb_level && p_curb>=curb_target*plf){ curb=true; } // Primary Curb
   if(t_cnt>1) p_targ=p_targ+(p_targ*(t_cnt-1)*0.5);
   if(balance+profit>=bal_2+p_targ){ closeall=true; get_time=TimeCurrent(); }
//----
   if(closeall || deleteall || curb)
   {
      for(i=OrdersTotal()-1; i>=0; i--)
      {
         OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
         if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
         if(closeall) deleteall=true;
         if(cmd()>1)
         {
            if(deleteall || (curb && LCT()>=t_cnt-cp_level))
            OrderDelete(OrderTicket());
         }
         if(cmd()<2)
         {
            if(closeall  || (curb && LCT()>=t_cnt-cp_level))
            OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
         }
      }
   }
//+------------------------------------------------------------------+
   string multi="Multiplier = "+DoubleToStr(multiplier,2), tier2="Yes", dev_r="";
   if(!lot_multiplier) multi="Increment = "+DoubleToStr(incr_1,2);
   if(!tier2_sig) tier2="No"; if(d_reset>0) dev_r=" / "+DoubleToStr(d_reset,0)+" / "+t_cnt+" / "+reset_level;
//----
   string line_1="Balance = "+DoubleToStr(bal_2,2)+"  |  Profit = "+DoubleToStr(bal_2+profit,2)+"  |  Equity = "+DoubleToStr(AccountEquity(),2)+"  |  Target = "+DoubleToStr(profit_target,2)+" / "+DoubleToStr(p_targ,2)+" / "+DoubleToStr(profit,2)+"\n";
   string line_2="Start Lot = "+DoubleToStr(lot,2)+"  |  Power lots = "+DoubleToStr(lot3,2)+"  |  Open = "+b_cnt+" / "+s_cnt+"  |  Pending = "+b2_cnt+" / "+s2_cnt+"  |  Deviate = "+DoubleToStr(deviate,0)+dev_r+"\n";
   string line_3="OPT = "+f_trade+"  |  "+multi+"  |  Spread = "+DoubleToStr((Ask-Bid)/pt,2)+"  |  Total Lots = "+DoubleToStr(t_lot,2)+"  |  Tier 2 = "+tier2+"\n";
//----
   Comment(line_1, line_2, line_3);
//----
   return(0);
  }
//+------------------------------------------------------------------+
int signal()
  {
   int signal=2;
   if(iCustom(NULL,0,"SuperTrend",10,3.0,0,bars_closed)!=EMPTY_VALUE) signal=0;
   if(iCustom(NULL,0,"SuperTrend",10,3.0,1,bars_closed)!=EMPTY_VALUE) signal=1;
   return(signal);
  }
//+------------------------------------------------------------------+
//----
int cmd(){ int cmd=OrderType(); return(cmd); }
//----
double OOP(){ double OOP=OrderOpenPrice(); return(OOP); }
//----
double OTP(){ double OOP=OrderTakeProfit(); return(OOP); }
//----
string OT2(){ string OT2=StringSubstr(OrderComment(),0,1); return(OT2); }
//----
int LCT(){ int LCT=StrToInteger(StringSubstr(OrderComment(),1,3)); return(LCT); }
//----
//+------------------------------------------------------------------+

