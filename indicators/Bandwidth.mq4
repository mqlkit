//+------------------------------------------------------------------+
//|                                                    Bandwidth.mq4 |
//|                                   Copyright  2011, Zarko Asenov |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright  2011, Zarko Asenov"
#property link      ""

#property indicator_separate_window
#property indicator_buffers 3
#property indicator_color1 Red
#property indicator_color2 OrangeRed
#property indicator_color3 Gold

//--- input parameters
extern double  Deviations     = 4.0;
extern int     Back_Period    = 18;
extern int     Mode_Function  = 1;
extern string  _HINT_         = "0: Bollinger bands difference, 1: Candle height, 2: Wick-body ratio";
extern bool    Display_Delta  = false;
extern int     Price_Mode     = PRICE_WEIGHTED;
  
//--- buffers
double ExtMapBuffer1[];
double ExtMapBuffer2[];
double ExtMapBuffer3[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- indicators
   SetIndexStyle(0,DRAW_HISTOGRAM);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexStyle(1,DRAW_LINE);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexStyle(2,DRAW_LINE);
   SetIndexBuffer(2,ExtMapBuffer3);
//----
   return(0);
  }

//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }

double
wick_body_ratio(
     double hi, double lo, double close, double open)
{
     double res;
     if (close > open) res = (hi - close) + (open - lo);
     else res = (hi - open) + (close - lo);
     if (res != 0.0) res = res / (hi - lo);
     return (res);
}
  
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int limit = Bars - IndicatorCounted() - 1;
//----
     if (limit < 0) return (-1);

   for(int i = limit; i >= 0; i--)
   {
      switch (Mode_Function) {
          case 0:
               /* Bollinger Bands */
               double Bands_Diff = iBands(Symbol(), Period(),  Back_Period, Deviations, 0, Price_Mode, 1, i) - 
                    iBands(Symbol(), Period(),  Back_Period, Deviations, 0, Price_Mode, 0, i);
               if (Display_Delta) {
                    double Prev_Bands_Diff = iBands(Symbol(), Period(),  Back_Period, Deviations, 0, Price_Mode, 1, i+1) - 
                         iBands(Symbol(), Period(), Back_Period, Deviations, 0, Price_Mode, 0, i+1);
               }
               break;
          case 1:
               /* Hi Lo Difference */
               Bands_Diff = High[i] - Low[i];
               if (Display_Delta) {
                    Prev_Bands_Diff = High[i+1] - Low[i+1];
               }
               break;
          case 2:
               /* Wick body difference */
               Bands_Diff = wick_body_ratio(High[i], Low[i], Close[i], Open[i]);

               if (Display_Delta) {
                    Prev_Bands_Diff = wick_body_ratio(High[i+1], Low[i+1], Close[i+1], Open[i+1]);
               }
               break;
          default:
               Bands_Diff = 0.0;
      }         
      
      if (Display_Delta) Bands_Diff -= Prev_Bands_Diff;
      ExtMapBuffer1[i] = Bands_Diff;
      ExtMapBuffer2[i] = ExtMapBuffer1[i];
   }

   for (i = limit; i >= 0; i--) ExtMapBuffer3[i] = iMAOnArray(ExtMapBuffer1, Bars, Back_Period, 0, MODE_EMA, i);
   
//----
   return(0);
  }
//+------------------------------------------------------------------+