//+------------------------------------------------------------------+
//|                                                  LaguerreCCI.mq4 |
//|                                                                  |
//| made after the description found for                             |
//| dynamic market lab                                               |
//+------------------------------------------------------------------+

#property copyright "mladen"
#property link      "mladenfx@gmail.com"

#property indicator_separate_window
#property indicator_buffers 1
#property indicator_color1  DeepSkyBlue
#property indicator_width1  2

//
//
//
//
//

extern double LaguerreGamma = 0.7;
extern int    LaguerrePrice = 0; 
extern int    CCIPeriod     = 14;

//
//
//
//
//

double CCI[];
double L0[];
double L1[];
double L2[];
double L3[];
double LG[];

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//
//

int init()
{
   IndicatorBuffers(6);
	SetIndexBuffer(0, CCI); SetIndexLabel(0,"Laguerre CCI");
   SetIndexBuffer(1, L0);
   SetIndexBuffer(2, L1);
   SetIndexBuffer(3, L2);
   SetIndexBuffer(4, L3);
   SetIndexBuffer(5, LG);
   IndicatorShortName("LaguerreCCI(" + DoubleToStr(LaguerreGamma, 2) + "," + CCIPeriod+")");
   return(0);
}
int deinit() { return(0); }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//

int start()
{
   int counted_bars=IndicatorCounted();
   int i,limit;

   if (counted_bars<0) return(-1);
   if (counted_bars>0) counted_bars--;
          limit = MathMin(Bars-counted_bars,Bars-1);

   //
   //
   //
   //
   //
   
   for (i=limit; i>=0; i--)
   {
		double Price=iMA(NULL,0,1,0,MODE_SMA,LaguerrePrice,i);
		
		L0[i] = (1.0 - LaguerreGamma)*Price + LaguerreGamma*L0[i+1];
		L1[i] = -LaguerreGamma*L0[i] + L0[i+1] + LaguerreGamma*L1[i+1];
		L2[i] = -LaguerreGamma*L1[i] + L1[i+1] + LaguerreGamma*L2[i+1];
		L3[i] = -LaguerreGamma*L2[i] + L2[i+1] + LaguerreGamma*L3[i+1];
		LG[i] = (L0[i] + 2 * L1[i] + 2 * L2[i] + L3[i]) / 6.0;
	}
   for (i=limit; i>=0; i--) CCI[i] = iCCIOnArray(LG,0,CCIPeriod,i);
   
   //
   //
   //
   //
   //
   
   return(0);
}