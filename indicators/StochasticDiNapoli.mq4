//+------------------------------------------------------------------+
//|                                           StochasticDiNapoli.mq4 |
//|                           Copyright � 2006, TrendLaboratory Ltd. |
//|            http://finance.groups.yahoo.com/group/TrendLaboratory |
//|                                       E-mail: igorad2004@list.ru |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2005, TrendLaboratory Ltd."
#property link      "http://finance.groups.yahoo.com/group/TrendLaboratory"

#property indicator_separate_window

#property indicator_level1 50
#property indicator_level2 0
#property indicator_level3 -50

#property indicator_buffers 3
#property indicator_color1 DodgerBlue
#property indicator_color2 Blue
#property indicator_color3 OrangeRed
//---- input parameters
extern int FastK=5;
extern int SlowK=3;
extern int SlowD=3;
extern bool Auto_Range = false;
//---- buffers
double StoBuffer[];
double SigBuffer[];
double DeltaBuffer[];
//double MdBuffer[];


int lasthiix;
int lastloix;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   string short_name;
//---- indicator line
   SetIndexStyle(0,DRAW_LINE, STYLE_SOLID, 1);
   SetIndexStyle(1,DRAW_LINE, STYLE_DOT, 1);
   SetIndexStyle(2,DRAW_HISTOGRAM, STYLE_SOLID, 3);
   
   SetIndexBuffer(0,StoBuffer);
   SetIndexBuffer(1,SigBuffer);
   SetIndexBuffer(2,DeltaBuffer);
   
   SetIndexLabel(0,"Stoch");
   SetIndexLabel(1,"Signal");
   SetIndexLabel(2,"Stochdelta");
//----
   SetIndexDrawBegin(0,FastK);
   SetIndexDrawBegin(1,FastK);
//----
   lasthiix = 0;
   lastloix = 0;

   return(0);
  }

 
double
lowest_low(int index)
{
     double res = Low[index];
     for (int loix = index; loix < Bars; loix++) {
          if (High[loix] < Low[index] && High[loix+1] < High[loix] && Low[loix+1] < Low[loix]) break;
          
          if (Low[loix] < res && Low[loix+1] > Low[loix]) res = Low[loix];
     }
     lastloix = loix;
     
     return (res);
}

double
highest_high(int index)
{
     double res = High[index];
     for (int hiix = index; hiix < Bars; hiix++) {
          if (Low[hiix] > High[index] && Low[hiix+1] > Low[hiix] && High[hiix+1] > High[hiix]) break;
          
          if (High[hiix] > res && High[hiix+1] < High[hiix]) res = High[hiix];
     }
     lasthiix = hiix;
     
     return (res);
}
  
//+------------------------------------------------------------------+
//| Stochastic DiNapoli                                              |
//+------------------------------------------------------------------+
int start()
  {
   int i,counted_bars=IndicatorCounted();
   double high,low;
//----
   if(Bars<=FastK) return(0);
//---- initial zero
   if(counted_bars<1)
      for(i=1;i<=FastK;i++) 
      {StoBuffer[Bars-i]=0.0;SigBuffer[Bars-i]=0.0;}
//----
   i=Bars-FastK-1;
   if(counted_bars>=FastK) i=Bars-counted_bars-1;
   while(i>=0)
   {
       double price = (Close[i] * 2.0 + High[i] + Low[i]) / 4.0;

       if (Auto_Range == true) {
          high = highest_high(i);
          low = lowest_low(i);
       }
       
       if (Auto_Range == false) {
          lastloix = iLowest(NULL,0,MODE_LOW,FastK,i);
          lasthiix = iHighest(NULL,0,MODE_HIGH,FastK,i);
          low=Low[lastloix];
          high=High[lasthiix];
       }

       double Volratio;
       double mean_volume = (Volume[lastloix] + Volume[lasthiix]) / 2.0;
       if (Volume[i] > 0 && mean_volume > 0) Volratio = Volume[i] / mean_volume;
       else Volratio = 1.0;
       if (i == 0) Volratio *= ( Period() * 60.0 / (TimeCurrent() - iTime(Symbol(), Period(), i)) );

       double Fast=(price-low)/(high-low)-0.5;
       Fast *= Volratio * 100.0;
       StoBuffer[i]=StoBuffer[i+1]+(Fast-StoBuffer[i+1])/SlowK;
       SigBuffer[i]=SigBuffer[i+1]+(StoBuffer[i]-SigBuffer[i+1])/SlowD;
       DeltaBuffer[i]=StoBuffer[i]-StoBuffer[i+1];
       i--;
   }
   
   IndicatorShortName(
               "Stoch lobar:"+TimeToStr(Time[lastloix]) +
               "  hibar:"+TimeToStr(Time[lasthiix]) );
   
   return(0);
  }
//+------------------------------------------------------------------+