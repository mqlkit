//+------------------------------------------------------------------+
//|                                            jmarket_sentiment.mq4 |
//|                                    Copyright  2011, Zarko Asenov |
//|                                              http://jaltoh.6x.to |
//+------------------------------------------------------------------+
#property copyright "Copyright  2011, Zarko Asenov"
#property link      "http://jaltoh.6x.to"

#property indicator_separate_window
#property indicator_buffers 5
#property indicator_color1 Red
#property indicator_color2 OrangeRed
#property indicator_color3 Gold
#property indicator_color4 SteelBlue
#property indicator_color5 Lime
#property indicator_width1 1
#property indicator_width2 1
#property indicator_width3 1
#property indicator_width4 2
#property indicator_width5 1
#property indicator_level1 1.0
#property indicator_level2 -1.0

#define DEBUG 0


#define WEIGHT_MOVE 0
#define ABS_MOVE    1
#define ABS_PUSH    2
#define TRENDLINE_NAME "jnextbar_predict"
#define INVALID_VOLUME 50

#define NEUTRAL 0
#define BULLISH 1
#define BEARISH 2


extern int MA_Period = 16;
extern int MA2_Period = 8;
extern int Time_Frame_Mins = 0;
extern int Retracement_Look_Back = 12;
extern double Time_Factor = 0.2;
extern double Volume_Factor = 0.5;
extern int Mode_Function = 0;
extern string _Mode_0 = "Bar to bar weighted hi-lo difference aggregate to volume ratio"; // nakaj stiska
extern string _Mode_1 = "Bar to bar any tick max difference"; // grid
extern string _Mode_2 = "Bar to bar tick max difference"; // pletkanje
extern int Sentiment_Expectation = NEUTRAL;
extern string _Expectation_0 = "Neutral sentiment";
extern string _Expectation_1 = "Bullish sentiment";
extern string _Expectation_2 = "Bearish sentiment";

//--- buffers
double ExtMapBuffer1[];
double ExtMapBuffer2[];
double ExtMapBuffer3[];
double ExtMapBuffer4[];
double ExtMapBuffer5[];
double ExtMapBuffer6[];



int time_frame;
double index_factor = 1.0;
int hTrendLine = -1;
int ind_window_handle;
double prev_best_step;
datetime prev_calc_time;

void
create_trendline()
{
     ind_window_handle = WindowFind("jmarket_sentiment");
   
     if (hTrendLine == -1 && ind_window_handle > -1) {
     
          hTrendLine = ObjectCreate(
               TRENDLINE_NAME, 
               OBJ_TREND, 
               ind_window_handle, 
               iTime(NULL, time_frame, 0), 
               0.0, 
               iTime(NULL, time_frame, 0) + Period() * 60, 
               0.0);
               
          ObjectSet(TRENDLINE_NAME, OBJPROP_COLOR, indicator_color2);
          ObjectSet(TRENDLINE_NAME, OBJPROP_STYLE, STYLE_SOLID);
          ObjectSet(TRENDLINE_NAME, OBJPROP_WIDTH, indicator_width2);
     }
}

void
remove_trendline()
{
     ObjectDelete(TRENDLINE_NAME);
}

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   IndicatorBuffers(6);
  
//---- indicators
   SetIndexStyle(0,DRAW_HISTOGRAM);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexLabel(0,"Movement");
   SetIndexEmptyValue(0, EMPTY_VALUE);
   
   SetIndexStyle(1,DRAW_LINE);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexLabel(1,"Movement Sum");
   SetIndexEmptyValue(1, EMPTY_VALUE);
   
   SetIndexStyle(2,DRAW_LINE);
   SetIndexBuffer(2,ExtMapBuffer3);
   SetIndexLabel(2,"Smoothed "+MA_Period);
   SetIndexEmptyValue(2, EMPTY_VALUE);
   
   SetIndexStyle(3,DRAW_LINE);
   SetIndexBuffer(3,ExtMapBuffer4);
   SetIndexLabel(3,"Trend step");
   SetIndexEmptyValue(3, EMPTY_VALUE);

   SetIndexStyle(4,DRAW_LINE);
   SetIndexBuffer(4,ExtMapBuffer5);
   SetIndexLabel(4,"Smoothed "+MA2_Period);
   SetIndexEmptyValue(4, EMPTY_VALUE);
   
   SetIndexStyle(5,DRAW_LINE);
   SetIndexBuffer(5,ExtMapBuffer6);
   SetIndexLabel(5,"Absolute movement");
   SetIndexEmptyValue(5, EMPTY_VALUE);
   
   if (Time_Frame_Mins == 0)
     time_frame = Period();
   else
     time_frame = Time_Frame_Mins;

   if (time_frame < 1) time_frame = 1;

   index_factor = Period();
   index_factor = index_factor / time_frame;
   
   prev_calc_time = 0;
   prev_best_step = 0;
   
   if (Mode_Function != WEIGHT_MOVE) {
     remove_trendline();
     SetIndexStyle(2, DRAW_LINE, STYLE_SOLID, indicator_width3, indicator_color3);
     SetIndexStyle(4, DRAW_LINE, STYLE_SOLID, indicator_width5, indicator_color5);
   } else {
     create_trendline();   
     SetIndexStyle(2, DRAW_LINE, STYLE_DOT, indicator_width3, CLR_NONE);
     SetIndexStyle(4, DRAW_LINE, STYLE_DOT, indicator_width5, indicator_color5);
   }
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   remove_trendline();
//----
   return(0);
  }

double
get_delta_ratio(
     double &SignalBuffer[],
     int SignalBufferLen)
{
if (DEBUG == 1)  Print("Debug get_delta_ratio");

     double delta = 0;
     bool going_up = false;
   
     for (int i = 0; i < SignalBufferLen; i++) {
        if (SignalBuffer[i] == 0 || SignalBuffer[i+1] == 0) continue;
        else if (delta != 0 && going_up != (SignalBuffer[i] > SignalBuffer[i+1]) ) break;
        
        if (delta == 0) going_up = (SignalBuffer[i] > SignalBuffer[i+1]);
        
        delta = ( (delta * i) + (SignalBuffer[i] - SignalBuffer[i+1])) / (i + 1.);
     }
   
     return (delta);
}

double
get_time_ratio(bool last_bar)
{
if (DEBUG == 1)  Print("Debug get_time_ratio");
     return (1);

     double time_ratio;
     if (last_bar == false) {
          time_ratio = 1;
     } else {
          double frame_delta = TimeCurrent() - (60. * iTime(NULL, time_frame, 0));
          if (frame_delta > 0) time_ratio = (time_frame * 60.) / frame_delta;
          else time_ratio = 0;
     }
          
     return(time_ratio);
}


double
get_weighted_move(
     double &hi, 
     double &low, 
     double &close, 
     double &prevhi, 
     double &prevlow, 
     double &prevclose,     
     int ix, int prev_ix)
{ 
if (DEBUG == 1) Print("Debug get_weighted_move");

     if (hi < prevlow || low > prevhi) return (0); // disconnected bars

      //current weighted high minus previous weighted high
     double hidiff = ((3. * hi + close) / 4.) - ((3. * prevhi + prevclose) / 4.);
     double lowdiff = ((3. * low + close) / 4.) - ((3. * prevlow + prevclose) / 4.);
     double absdiff = hidiff + lowdiff;

     double volume_ratio;
     double vol = iVolume(NULL, time_frame, ix);
     double prevol = iVolume(NULL, time_frame, prev_ix);
     
     if (vol < INVALID_VOLUME) {
          volume_ratio = 0;
     } else if (prevol < INVALID_VOLUME) {
          volume_ratio = 1;
     } else {               
          volume_ratio = MathPow(vol / prevol, Volume_Factor);
     }

     double time_ratio = get_time_ratio( (ix == 0) );
     
     return (absdiff * time_ratio * volume_ratio);
}


double
get_absolute_diff(
     double &hi, 
     double &low, 
     double &prevhi, 
     double &prevlow, 
     int ix)
{ 
if (DEBUG == 1) Print("Debug get_absolute_diff");

      //current weighted high minus previous weighted high
     double hidiff = MathAbs(hi - prevhi);
     double lowdiff = MathAbs(low - prevlow);
     double absdiff = hidiff + lowdiff;

     double time_ratio = get_time_ratio( (ix == 0) );
     
     return (absdiff * time_ratio);
}

/* largest price difference between two bars */
double
get_absolute_move(
     double hi, 
     double low, 
     double close, 
     double prevhi, 
     double prevlow, 
     double prevclose)
{
if (DEBUG == 1)  Print("Debug get_absolute_move");

     double abs_move = 0.0;
     
     double cur_hlc[3];
     cur_hlc[0] = hi;
     cur_hlc[1] = low;
     cur_hlc[2] = close;
     
     double prev_hlc[3];
     prev_hlc[0] = prevhi;
     prev_hlc[1] = prevlow;
     prev_hlc[2] = prevclose;

     for (int i = 0; i < 3; i++) 
          for (int j = 0; j < 3; j++) 
               if ( MathAbs(cur_hlc[i] - prev_hlc[j]) > MathAbs(abs_move) )
                    abs_move = cur_hlc[i] - prev_hlc[j];
          
     if ( (hi - low) > abs_move ) {
          if (close > prevclose) abs_move = hi - low; 
               else abs_move = low - hi;
     }
     
     return (abs_move);
}

void
get_bar_move(
          int now_bar, 
          int ref_bar, 
          double &max_move)
{
if (DEBUG == 1) Print("Debug get_bar_move");

     for (int ix = now_bar - 1; ix >= ref_bar; ix--) {
          double now_move = get_absolute_move(
                                   iHigh(NULL, time_frame, ix),
                                   iLow(NULL, time_frame, ix),
                                   iClose(NULL, time_frame, ix),
                                   iHigh(NULL, time_frame, now_bar),
                                   iLow(NULL, time_frame, now_bar),
                                   iClose(NULL, time_frame, now_bar) );
                                   
          if ( MathAbs(now_move) > MathAbs(max_move) ) max_move = now_move;
     }
}

/* largest price difference since start_bar trend */
double
get_max_move(int ref_bar)
{
     if (DEBUG == 1) Print("Debug get_max_move");

     double max_move = 0;
     bool ref_bull = (ExtMapBuffer1[ref_bar] > 0);
     
     for (int now_bar = ref_bar + 1; 
          (ref_bull == (ExtMapBuffer1[now_bar] > 0)) && (now_bar < Bars); 
          now_bar++) {

          get_bar_move(now_bar, ref_bar, max_move);
     }

     get_bar_move(now_bar, ref_bar, max_move);

     return (max_move);
}

#define MAXLEN 1024

double bears_array[MAXLEN];
double bulls_array[MAXLEN];


/* sorted arrays expected ! */
double
calc_pips(
     double &trending[],
     int trending_len,
     double &retracements[],
     int retracements_len
     )
{
     if (DEBUG == 1) Print("Debug calc_pips");

     double best_ratio = 0;
     double best_step = 0;

     if ( (TimeCurrent() - prev_calc_time) < (time_frame * Period() * 5) ) return(prev_best_step);

     double step_base = 0;
     for (int ix = 1;
          (step_base == 0) && (ix < Bars);
          ix++) 
     {
          step_base = ExtMapBuffer6[ix];
     }
     if (step_base == 0) return (prev_best_step);
     
     double interpolation_step = step_base * 0.0312; // 1/16
     
          
     for (int z = 0; z < 64; z++) {
          
          double cur_step = step_base + (z * interpolation_step);
               
          double won_pips = 0;
          double lost_pips = 0;
                    
          for (int j = 0; j < retracements_len; j++) {
               won_pips += cur_step * MathFloor(retracements[j] / cur_step) / Point;
               if (cur_step >= retracements[j]) lost_pips += 2. * MathMod(retracements[j], cur_step) / Point;
          }
               
          for (j = 0; j < trending_len; j++) {
               won_pips += cur_step * MathFloor(trending[j] / cur_step) / Point;
               if (cur_step >= trending[j]) lost_pips += 1. * MathMod(trending[j], cur_step) / Point;
          }

          if (won_pips == 0) double cur_ratio = 0;
          else if (lost_pips == 0) cur_ratio = won_pips;
          else cur_ratio = won_pips / lost_pips;

          if (cur_ratio > best_ratio) {
               best_step = cur_step;
               best_ratio = cur_ratio;
          }

     }
     
     if (best_step == 0) return (prev_best_step);
     
     prev_best_step = best_step;
     prev_calc_time = TimeCurrent();

     if (DEBUG == 1) Print(
          "Recomended minimal pip step " + best_step + " past " + 
          trending_len + " trending blocks, " + retracements_len + " retracements blocks.");
     
     return (best_step);
}

int
gcd(int a, int b)
{
     if (b == 0) return (a);
     else return ( gcd(b, a % b) );
}

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int counted_bars = IndicatorCounted();
//----
   int limit = Bars - counted_bars - 1;
   if (limit < 0) return (0);
   
   for (int i = limit; i >= 0; i--) {
   
     int ix = MathFloor(index_factor * i);
     
     double hi = iHigh(NULL, time_frame, ix);
     double low = iLow(NULL, time_frame, ix);
     double close = iClose(NULL, time_frame, ix);

     double prevhi = iHigh(NULL, time_frame, ix+1);
     double prevlow = iLow(NULL, time_frame, ix+1);
     double prevclose = iClose(NULL, time_frame, ix+1);
     
     if (prevhi == 0 || prevlow == 0 || prevclose == 0) continue;
     if (hi == 0 || low == 0 || close == 0) continue;
     
          switch (Mode_Function) {
          case WEIGHT_MOVE:
               ExtMapBuffer1[i] = 0;
               for (int j = 1; j < MA_Period; j++) {
                    int prev_ix = ix+j;
                    prevhi = iHigh(NULL, time_frame, prev_ix);
                    prevlow = iLow(NULL, time_frame, prev_ix);
                    prevclose = iClose(NULL, time_frame, prev_ix);
                    
                    double time_coef = (1. * j) / MA_Period;
                    time_coef = MathPow(1. - time_coef, Time_Factor);
                    
                    ExtMapBuffer1[i] +=  time_coef * get_weighted_move(hi, low, close, prevhi, prevlow, prevclose, ix, prev_ix);
               }
               ExtMapBuffer2[i] = ExtMapBuffer1[i];
               break;
         
          case ABS_MOVE:
               ExtMapBuffer1[i] = get_absolute_move(hi, low, close, prevhi, prevlow, prevclose);
               ExtMapBuffer2[i] = get_max_move(i);
               ExtMapBuffer6[i] = MathAbs(ExtMapBuffer1[i]);
          
               // fixup previous bars
               for (j = i + 1;
                    ((ExtMapBuffer2[i] > 0) == (ExtMapBuffer2[j] > 0.0)) && (j < Bars);
                    j++)
                    ExtMapBuffer2[j] = ExtMapBuffer2[i];
               break;

          case ABS_PUSH:
               ExtMapBuffer1[i] = get_absolute_diff(hi, low, prevhi, prevlow, i);
               ExtMapBuffer2[i] = ExtMapBuffer1[i];
               break;
          }     

     }
   
     if (Mode_Function == WEIGHT_MOVE) {
          double ma_coef = 1;
          
          for (i = limit; i >= 0; i--) 
               ExtMapBuffer3[i] = MathAbs(ExtMapBuffer1[i]);
               
          for (i = limit; i >= 0; i--) 
               ExtMapBuffer5[i] = iMAOnArray(ExtMapBuffer3, Bars, MA2_Period, 0, MODE_EMA, i);
               
     } else { 
          for (i = limit; i >= 0; i--) {
               ExtMapBuffer3[i] = iMAOnArray(ExtMapBuffer6, Bars, MA_Period, 0, MODE_EMA, i);
               ExtMapBuffer5[i] = (ExtMapBuffer6[i+1] * (MA2_Period - 1) + ExtMapBuffer6[i]) / (1. * MA2_Period);
          }
     }

     if (Mode_Function == ABS_MOVE) {
   
          switch (Sentiment_Expectation) {
          case NEUTRAL:
               bool going_bull = (ExtMapBuffer2[0] > 0);
               break;
          
          case BULLISH:
               going_bull = true;
               break;
          
          case BEARISH:
               going_bull = false;
               break;
          }
                    
          int bulls_len = 0;
          int bears_len = 0;
          double preval = 0;

          /* get unique movements */
          for (i = 0; i < Retracement_Look_Back; i++) {
               if (ExtMapBuffer2[i] == preval) continue;
               preval = ExtMapBuffer2[i];
               
               if (preval > 0) { 
                    bulls_array[bulls_len] = preval;
                    bulls_len++;
               } else if (preval < 0) {
                    bears_array[bears_len] = MathAbs(preval);
                    bears_len++;
               }
          }

          if (going_bull == true)
               double pip_step = calc_pips(bulls_array, bulls_len, bears_array, bears_len);
          else
               pip_step = calc_pips(bears_array, bears_len, bulls_array, bulls_len);
          
          ExtMapBuffer4[0] = pip_step;
          SetLevelValue(0, pip_step);
          SetLevelValue(1, -1.0 * pip_step);
       

   } else {
          double delta_sign = get_delta_ratio(ExtMapBuffer1, Bars);
          double predicted_price = delta_sign;
          predicted_price += ExtMapBuffer1[0];
     
          if (hTrendLine != -1) {
               ObjectSet(TRENDLINE_NAME, OBJPROP_TIME1, iTime(NULL, time_frame, 0) );
               ObjectSet(TRENDLINE_NAME, OBJPROP_TIME2, iTime(NULL, time_frame, 0) + Period() * 60);
               ObjectSet(TRENDLINE_NAME, OBJPROP_PRICE1, ExtMapBuffer2[0]);
               ObjectSet(TRENDLINE_NAME, OBJPROP_PRICE2, predicted_price);
          }
   
   }
    
//----
   return(0);
  }
//+------------------------------------------------------------------+