//+------------------------------------------------------------------+
//|                                   		 Meta COT WILLCO.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+

//   WILLCO

#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

#property indicator_separate_window
#property  indicator_buffers 3
#property  indicator_color1  Green
#property  indicator_color2  Red
#property  indicator_color3  Blue

extern int  period=52;
int movement_index=6;
extern bool Show_iNoncomm=false;
extern bool Show_iOperators=true;
extern bool Show_iNonrep=false;

double i_willco_noncomm[];
double i_willco_operators[];
double i_willco_nonrep[];

#include <cotlib.mq4>

int init()
{
   if(init_data()==false)error=true;
   if(error==false)load_data();
   if(error==false)count_data();
   //if(error==false)count_index(period);
   SetParam();
}

int start()
{
   if(error==true)Print("   .   ");
   if(DrawData==true)build_data();
}

void SetParam()
{
   SetIndexStyle(0,DRAW_LINE);
   SetIndexStyle(1,DRAW_LINE);
   SetIndexStyle(2,DRAW_LINE);
   IndicatorDigits(2);
   SetIndexEmptyValue(0,EMPTY_VALUE);
   SetIndexEmptyValue(1,EMPTY_VALUE);
   SetIndexEmptyValue(2,EMPTY_VALUE);
   SetLevelValue(0,0.0);
   SetLevelValue(1,20.0);
   SetLevelValue(2,80.0);
   SetLevelValue(3,100.0);
   if(load_cot_file==true)IndicatorShortName(StringConcatenate("Meta COT WILLCO (",period,"): ",str_trim(cot_file)));
   else IndicatorShortName(StringConcatenate("Meta COT WILLCO (",period,"): ",name));
   if(Show_iNoncomm==true){
      SetIndexBuffer(0,i_willco_noncomm);
      SetIndexLabel(0,"WILLCO: Noncommercial Traders");
   }
   if(Show_iOperators==true){
      SetIndexBuffer(1,i_willco_operators);
      SetIndexLabel(1,"WILLCO: Operators Traders");
   } 
   if(Show_iNonrep==true){
      SetIndexBuffer(2,i_willco_nonrep);
      SetIndexLabel(2,"WILLCO: Nonrep Traders");
   }
}

void build_data()
{
   int end_data=get_lastdata();
   for(int i=0;i<end_data;i++){
      if(Show_iNoncomm){i_willco_noncomm[i]=get_data(WILLCO_NONCOMM, i);/*Print(get_data(OI_NET_NONCOMM, i));*/}
      if(Show_iOperators)i_willco_operators[i]=get_data(WILLCO_OPERATORS, i);
      if(Show_iNonrep)i_willco_nonrep[i]=get_data(WILLCO_NONREP,i);
   }
   DrawData=false;
}
