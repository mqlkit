//+------------------------------------------------------------------+
//|                              Meta COT Percent Position in OI.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+

//          .

#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

#property indicator_separate_window
#property  indicator_buffers 6
#property  indicator_color1  Blue
#property  indicator_color2  SkyBlue
#property  indicator_color3  IndianRed
#property  indicator_color4  Red
#property  indicator_color5  Silver
#property  indicator_color6  SlateGray

int  period=52;
int movement_index=6;
extern bool Show_iNoncomm_Long=false;
extern bool Show_iNoncomm_Short=false;
extern bool Show_iOperators_Long=false;
extern bool Show_iOperators_Short=false;
extern bool Show_iNonrep_Long=false;
extern bool Show_iNonrep_Short=false;

double i_noncomm_long[];
double i_noncomm_short[];
double i_operators_long[];
double i_operators_short[];
double i_nonrep_long[];
double i_nonrep_short[];

#include <cotlib.mq4>

int init()
{
   if(init_data()==false)error=true;
   if(error==false)load_data();
   if(error==false)count_data();
   //if(error==false)count_index(period);
   SetParam();
}

int start()
{
   if(error==true)Print("   .   ");
   if(DrawData==true)DrowData();
}

void SetParam()
{
   SetIndexStyle(0,DRAW_LINE);
   SetIndexStyle(1,DRAW_LINE);
   SetIndexStyle(2,DRAW_LINE);
   SetIndexStyle(3,DRAW_LINE);
   SetIndexStyle(4,DRAW_LINE);
   SetIndexStyle(5,DRAW_LINE);
   SetIndexStyle(6,DRAW_LINE);
   IndicatorDigits(2);
   SetIndexEmptyValue(0,EMPTY_VALUE);
   SetIndexEmptyValue(1,EMPTY_VALUE);
   SetIndexEmptyValue(2,EMPTY_VALUE);
   SetIndexEmptyValue(3,EMPTY_VALUE);
   SetIndexEmptyValue(4,EMPTY_VALUE);
   SetIndexEmptyValue(5,EMPTY_VALUE);
   SetIndexEmptyValue(6,EMPTY_VALUE);
   SetLevelValue(0,0.0);
   SetLevelValue(1,20.0);
   SetLevelValue(2,50.0);
   SetLevelValue(3,80.0);
   SetLevelValue(4,100.0);
   if(load_cot_file==true)IndicatorShortName(StringConcatenate("Meta COT % Pos. in OI: ", str_trim(cot_file)));
   else IndicatorShortName(StringConcatenate("Meta COT % Pos. in OI: ", name));
   if(Show_iNoncomm_Long==true){
      SetIndexBuffer(0,i_noncomm_long);
      SetIndexLabel(0, "% Noncomm Long in OI");
   }
   if(Show_iNoncomm_Short==true){
      SetIndexBuffer(1,i_noncomm_short);
      SetIndexLabel(1,"% Noncomm Short in OI");
   }
   if(Show_iOperators_Long==true){
      SetIndexBuffer(2,i_operators_long);
      SetIndexLabel(2,"% Operators Long in OI");
   } 
   if(Show_iOperators_Short==true){
      SetIndexBuffer(3,i_operators_short);
      SetIndexLabel(3,"% Operators Short in OI");
   }
   if(Show_iNonrep_Long==true){
      SetIndexBuffer(4,i_nonrep_long);
      SetIndexLabel(4,"% Nonrep Long in OI");
   }
   if(Show_iNonrep_Short==true){
      SetIndexBuffer(5,i_nonrep_short);
      SetIndexLabel(5,"% Nonrep Short in OI");
   }
}

void DrowData()
{
   int end_data=get_lastdata();
   for(int i=0;i<end_data;i++){
      if(Show_iNoncomm_Long==true)i_noncomm_long[i]=get_data(OI_NONCOMM_LONG,i);
      if(Show_iNoncomm_Short==true)i_noncomm_short[i]=get_data(OI_NONCOMM_SHORT,i);
      if(Show_iOperators_Long==true)i_operators_long[i]=get_data(OI_OPERATORS_LONG,i);
      if(Show_iOperators_Short==true)i_operators_short[i]=get_data(OI_OPERATORS_SHORT,i);
      if(Show_iNonrep_Long==true)i_nonrep_long[i]=get_data(OI_NONREP_LONG,i);
      if(Show_iNonrep_Short==true)i_nonrep_short[i]=get_data(OI_NONREP_SHORT,i);
   }
   DrawData=false;
}
