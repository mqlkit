//+------------------------------------------------------------------+
//|                                               Meta COT Index.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+

//   COT-Index

#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

#property indicator_separate_window
#property  indicator_buffers 4
#property  indicator_color1  Green
#property  indicator_color2  Blue
#property  indicator_color3  Red
#property  indicator_color4  Silver

extern int  period=156;
int movement_index=6;
extern bool Show_iNoncomm=false;
extern bool Show_iOperators=true;
extern bool Show_iNonrep=false;
extern bool Show_iOI=true;

double i_open_interest[];
double i_noncomm[];
double i_operators[];
double i_nonrep[];

#include <cotlib.mq4>

int init()
{
   if(init_data()==false)error=true;
   if(error==false)load_data();
   if(error==false)count_data();
   //if(error==false)count_index(period);
   SetParam();
}

int start()
{
   if(error==true)Print("   .   ");
   DrowData();
}

void SetParam()
{
   SetIndexStyle(0,DRAW_LINE,0,2);
   SetIndexStyle(1,DRAW_LINE,0,2);
   SetIndexStyle(2,DRAW_LINE,0,2);
   SetIndexStyle(3,DRAW_LINE,0,2);
   IndicatorDigits(2);
   SetIndexEmptyValue(0,EMPTY_VALUE);
   SetIndexEmptyValue(1,EMPTY_VALUE);
   SetIndexEmptyValue(2,EMPTY_VALUE);
   SetIndexEmptyValue(3,EMPTY_VALUE);
   SetLevelValue(0,0.0);
   SetLevelValue(1,20.0);
   SetLevelValue(2,80.0);
   SetLevelValue(3,100.0);
   SetLevelStyle(0,0,Black);
   //SetLevelStyle(0,1,Black);
   //SetLevelStyle(0,1,Black);
   //SetLevelStyle(0,1,Black);
   if(load_cot_file==true)IndicatorShortName(StringConcatenate("Meta COT Index (",period,"): ",str_trim(cot_file)));
   else IndicatorShortName(StringConcatenate("Meta COT Index (",period,"): ",name));
   if(Show_iOI==true){
      SetIndexBuffer(0,i_open_interest);
      SetIndexLabel(0, "Open Interest");
   }
   if(Show_iNoncomm==true){
      SetIndexBuffer(1,i_noncomm);
      SetIndexLabel(1,"Index Noncommercial");
   }
   if(Show_iOperators==true){
      SetIndexBuffer(2,i_operators);
      SetIndexLabel(2,"Index Operators");
   } 
   if(Show_iNonrep==true){
      SetIndexBuffer(3,i_nonrep);
      SetIndexLabel(3,"Index Nonreportable");
   }
}

void DrowData()
{
   int end_data=get_lastdata();
   for(int i=0;i<end_data;i++){
      if(Show_iOI)i_open_interest[i]=get_data(INDEX_OI, i);
      if(Show_iNoncomm)i_noncomm[i]=get_data(INDEX_NONCOMM, i);
      if(Show_iOperators)i_operators[i]=get_data(INDEX_OPERATORS, i);
      if(Show_iNonrep)i_nonrep[i]=get_data(INDEX_NONREP,i);
   }
}
