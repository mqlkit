//+------------------------------------------------------------------+
//|                                                AXDX Advanced.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      ""

#property indicator_separate_window
#property indicator_buffers 4
#property indicator_color1 Salmon
#property indicator_color2 Aqua
#property indicator_color3 Silver
#property indicator_color4 Gold
#property indicator_level1 50.0
#property indicator_level2 -50.0
#property indicator_maximum 100.0
#property indicator_minimum -100.0

//--- input parameters
extern int       ADX_Period = 13;
extern int       Time_Frame = 240;
//--- buffers
double ExtMapBuffer0[];
double ExtMapBuffer1[];
double ExtMapBuffer2[];
double ExtMapBuffer3[];

double tf_factor = 0.0;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- indicators
   SetIndexStyle(0,DRAW_HISTOGRAM, STYLE_DOT);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexStyle(1,DRAW_LINE, STYLE_DOT);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexStyle(2,DRAW_LINE, STYLE_DOT);
   SetIndexBuffer(2,ExtMapBuffer0);
   SetIndexStyle(3,DRAW_HISTOGRAM, STYLE_SOLID);
   SetIndexBuffer(3,ExtMapBuffer3);
   
   tf_factor = Period();
   tf_factor = tf_factor / Time_Frame;
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int counted_bars = IndicatorCounted();
   int ix;
//----
   for (ix = Bars - counted_bars;ix >= 0; ix--) {
     
     double adx_plus; 
     double adx_minus; 

     int timeframe_ix = MathFloor(tf_factor * ix);
     adx_plus = iADX(Symbol(), Time_Frame, ADX_Period, PRICE_TYPICAL, 1, timeframe_ix); 
     adx_minus = iADX(Symbol(), Time_Frame, ADX_Period, PRICE_TYPICAL, 2, timeframe_ix); 
     
     double adx_diff = adx_plus - adx_minus;
     double adx_weigh = 0.0;
     
     if (adx_diff < 0.0 && adx_minus != 0.0) {
          adx_weigh = adx_diff / adx_minus;
     } else if (adx_diff > 0.0 && adx_plus != 0.0) {
          adx_weigh = adx_diff / adx_plus;
     } else {
          adx_weigh = 0.0;
     }
     
     int num_bars_period = MathRound(1.0 / tf_factor);
     
     adx_weigh *= 100.0;
     
     ExtMapBuffer0[ix] = adx_weigh;
     ExtMapBuffer1[ix] = adx_weigh;
     ExtMapBuffer2[ix] = ExtMapBuffer1[ix] - ExtMapBuffer1[ix + num_bars_period];
     
     if (adx_weigh >= 0.0) 
          ExtMapBuffer3[ix] = MathMax(0.0, MathMin(ExtMapBuffer2[ix], ExtMapBuffer0[ix]));
     else if (adx_weigh < 0.0) 
          ExtMapBuffer3[ix] = MathMin(0.0, MathMax(ExtMapBuffer2[ix], ExtMapBuffer0[ix]));
   }
//----
   return(0);
  }
//+------------------------------------------------------------------+