//+------------------------------------------------------------------+
//|                                                    ATR ratio.mq4 |
//|                         Copyright � 2005, Luis Guilherme Damiani |
//|                                      http://www.damianifx.com.br |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2005, Luis Guilherme Damiani"
#property link      "http://www.damianifx.com.br"

#property indicator_separate_window
#property indicator_buffers 2
#property indicator_color1 Silver
#property indicator_color2 Violet
#property indicator_style1 DRAW_LINE

//---- input parameters
extern int       ATR_Basis = 7;
extern int       ATR_2 = 35;
extern int       ATR_3 = 0;
extern double    triglevel = 1.0;
extern int       Smoothing_period = 13;
extern bool      Display_Delta = false;

//---- buffers
double ExtMapBuffer1[];
double ExtMapBuffer2[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

double recent_factor = 0.2;

int init()
  {

//---- indicators
   if (Display_Delta == false) {
     IndicatorBuffers(2);
     SetIndexStyle(1,DRAW_LINE);
   } else {
     IndicatorBuffers(1);
     SetIndexStyle(1,DRAW_NONE);
   }

   SetIndexBuffer(0, ExtMapBuffer1);
   SetIndexBuffer(1, ExtMapBuffer2);

   SetIndexEmptyValue(0, 0.0);
   SetIndexEmptyValue(1, 0.0);
//----
   return(0);
  }
  
//+------------------------------------------------------------------+
//| Custor indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//---- 
   
//----
   return(0);
  }
  
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int limit = Bars - IndicatorCounted() - 1;
   if (limit < 0) return (0);
   
   for(int i = limit; i >= 0; i--)
   {
      ExtMapBuffer2[i] = iATR(Symbol(), Period(), ATR_Basis, i);
      
      if (ATR_2 != 0.0 && ExtMapBuffer2[i] != 0.0) {
          double atr2_val = iATR(Symbol(), Period(), ATR_2, i);
          if (atr2_val == 0.0) atr2_val = 1.0;
          ExtMapBuffer2[i] = ExtMapBuffer2[i] / atr2_val;
      }
      
      if (ATR_3 != 0.0 && ExtMapBuffer2[i] != 0.0) {
          double atr3_val = iATR(Symbol(), Period(), ATR_3, i);
          if (atr3_val == 0.0) atr3_val = 1.0;
          ExtMapBuffer2[i] = ExtMapBuffer2[i] / atr3_val;
      }
      
      if (Display_Delta == true) ExtMapBuffer1[i] = ExtMapBuffer2[i] - ExtMapBuffer2[i+1];
   }
   
   if (Display_Delta == false) 
     for (i = 1; i < limit; i++) 
         ExtMapBuffer1[i] = iMAOnArray(ExtMapBuffer2, Bars, Smoothing_period, -1, MODE_EMA, i);
   
//----

   return(0);
  }
//+------------------------------------------------------------------+