//+------------------------------------------------------------------+
//|                                ModifiedOptimumEllipticFilter.mq4 |
//|                                                                  |
//| Code based on article:                                           |
//|     Stocks & Commodities V. 18:7 (20-29):                        |
//|     Optimal Detrending by John F. Ehlers                         |
//|                                                                  |
//|                                              contact@mqlsoft.com |
//|                                          http://www.mqlsoft.com/ |
//+------------------------------------------------------------------+
#property copyright "Coded by Witold Wozniak"
#property link      "www.mqlsoft.com"

#property indicator_chart_window
#property indicator_buffers 1
#property indicator_color1 Red

double smooth[];

int buffers = 0;
int drawBegin = 0;

int init() {
    drawBegin = 16;
    initBuffer(smooth, "MOEF", DRAW_LINE);
    IndicatorBuffers(buffers);
    IndicatorShortName("ModifiedOptimumEllipticFilter");
    return (0);
}
  
int start() {    
    if (Bars <= drawBegin) return (0);
    int countedBars = IndicatorCounted();
    if (countedBars < 0) return (-1);
    if (countedBars > 0) countedBars--;
    int s, limit = Bars - countedBars - 1;
    for (s = limit; s >= 0; s--) {
        smooth[s] = 0.13785 * (2 * P(s) - P(s + 1)) 
                  + 0.0007  * (2 * P(s + 1) - P(s + 2)) 
                  + 0.13785 * (2 * P(s + 2) - P(s + 3)) 
                  + 1.2103  * smooth[s + 1] - 0.4867 * smooth[s + 2]; 
    }
    return (0);  
}

double P(int index) {
    return ((High[index] + Low[index]) / 2.0);
}

void initBuffer(double array[], string label = "", int type = DRAW_NONE, int arrow = 0, int style = EMPTY, int width = EMPTY, color clr = CLR_NONE) {
    SetIndexBuffer(buffers, array);
    SetIndexLabel(buffers, label);
    SetIndexEmptyValue(buffers, EMPTY_VALUE);
    SetIndexDrawBegin(buffers, drawBegin);
    SetIndexShift(buffers, 0);
    SetIndexStyle(buffers, type, style, width);
    SetIndexArrow(buffers, arrow);
    buffers++;
}