//+------------------------------------------------------------------+
//|                                      Meta COT Movement Index.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+

//   .       .

#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

#property indicator_separate_window
#property  indicator_buffers 4
#property  indicator_color1  Green
#property  indicator_color2  Blue
#property  indicator_color3  Red
#property  indicator_color4  Silver

extern int  period=52;
extern int movement_index=6;
extern bool Show_iNoncomm=false;
extern bool Show_iOperators=true;
extern bool Show_iNonrep=false;
extern bool Show_iOI=false;

double i_movement_noncomm[];
double i_movement_operators[];
double i_movement_nonrep[];
double i_movement_oi[];

#include <cotlib.mq4>

int init()
{
   if(init_data()==false)error=true;
   if(error==false)load_data();
   if(error==false)count_data();
   SetParam();
}

int start()
{
   if(error==true)Print("   .   ");
   if(DrawData==true)build_data();
}

void SetParam()
{
   SetIndexStyle(0,DRAW_HISTOGRAM);
   SetIndexStyle(1,DRAW_HISTOGRAM);
   SetIndexStyle(2,DRAW_HISTOGRAM);
   SetIndexStyle(3,DRAW_HISTOGRAM);
   IndicatorDigits(2);
   SetIndexEmptyValue(0,EMPTY_VALUE);
   SetIndexEmptyValue(1,EMPTY_VALUE);
   SetIndexEmptyValue(2,EMPTY_VALUE);
   SetIndexEmptyValue(3,EMPTY_VALUE);
   SetLevelValue(0,-40.0);
   SetLevelValue(1,0.0);
   SetLevelValue(2,40.0);
   SetLevelStyle(0,0,Black);
   if(load_cot_file==true)IndicatorShortName(StringConcatenate("Meta COT Movement Index (",period,";",movement_index,"): ",str_trim(cot_file)));
   else IndicatorShortName(StringConcatenate("Meta COT Movement Index (",period,";",movement_index,"): ",name));
   
   if(Show_iOI==true){
      SetIndexBuffer(0,i_movement_oi);
      SetIndexLabel(0,"Movement Index: Open Interest");
   }
   if(Show_iNoncomm==true){
      SetIndexBuffer(1,i_movement_noncomm);
      SetIndexLabel(1,"Movement Index: Noncommercial Traders");
   }
   if(Show_iOperators==true){
      SetIndexBuffer(2,i_movement_operators);
      SetIndexLabel(2,"Movement Index: Operators Traders");
   } 
   if(Show_iNonrep==true){
      SetIndexBuffer(3,i_movement_nonrep);
      SetIndexLabel(3,"Movement Index: Nonrep Traders");
   }
}

void build_data()
{
   int end_data=get_lastdata();
   for(int i=0;i<end_data;i++){
      if(Show_iOI)i_movement_oi[i]=get_data(MOVEMENT_OI, i);
      if(Show_iNoncomm){i_movement_noncomm[i]=get_data(MOVEMENT_NONCOMM, i);/*Print(get_data(OI_NET_NONCOMM, i));*/}
      if(Show_iOperators)i_movement_operators[i]=get_data(MOVEMENT_OPERATORS, i);
      if(Show_iNonrep)i_movement_nonrep[i]=get_data(MOVEMENT_NONREP,i);
   }
   DrawData=false;
}
