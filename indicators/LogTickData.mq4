//=============================================================================
// LogTickData.mq4
// modified from TickSave.mq4
// written by komposter at komposterius@mail.ru
// 04/09/08 modified for format
// logs date time and bid to \experts\files\TickData\<symbol>.csv
// (Example output: 2008.05.07 22:25:21;1.5316)
// runs as an indicator on 1 minute (or any) chart
// places the record count in a separate window 
//=============================================================================
#property copyright "Copyright"  //komposter
#property link      ""           //many thanks to the original author(s) 
#property indicator_separate_window
#property indicator_buffers 1
#property indicator_color1 Red
extern int i_1Override_2Append = 2;
int iRcdCnt = 0;
string sFileName = "";
string sComment = "";
bool bRtn = false;
int iRtn = 0;
int err = 0;
double ExtMapBuffer[];
bool bFirst=True;
bool bKilled = false;
int prevPeriod=0;
int initBars=0;
int prevBars=0;
//=============================================================================
// indicator initialization function
//=============================================================================
int init() {
   string short_name;
   short_name = "LogTickData(" + i_1Override_2Append + ")";
   IndicatorShortName(short_name);
   SetIndexStyle(0,DRAW_LINE,0,1);
   SetIndexBuffer(0,ExtMapBuffer);
   SetIndexLabel(0,short_name);
   return(0);
}
//=============================================================================
// deinitialization function
//=============================================================================
int deinit() {
   Comment("");
   return(0);
}
//=============================================================================
// start function
//=============================================================================
int start() {
   if ( bKilled == true ) return;
   if ((i_1Override_2Append == 1) || (i_1Override_2Append == 2)) {
      //valid parm entered
   } else {   
      sComment = "Please select valid Overwrite or Append parameter.";
      Print(sComment);
      Comment(sComment);
      bKilled = true;
      return(-1);
   }
   sFileName = StringConcatenate( "TickData\\", Symbol() + ".csv" );
   if(Period() != prevPeriod) {
      prevPeriod = Period();
      bFirst = True;
   }
   if(IndicatorCounted() == 0) bFirst = True;
   if(bFirst == True) {
      initBars = Bars;
      prevBars = Bars;
      if ( i_1Override_2Append == 1 ) {
         FileDelete(sFileName);      
      }
      bFirst = False;
   }
   //Initialize End
   //All bars (full and tick)
   if(Bars <= initBars) return;        //are we past initially loaded bars   
   iRtn = Write_Tick();
   prevBars = Bars;
   return(0);
} 

int Write_Tick() {
   iRcdCnt++;
   //Comment("writing rcd no: " , iRcdCnt);
   int handle;
   handle = FileOpen(sFileName, FILE_READ|FILE_WRITE|FILE_CSV, ";");
   bRtn = FileSeek(handle, 0, SEEK_END);
   if (bRtn == false) {
      err=GetLastError();
      Print("error of seek: ",err);
      return(-1);
   }
   FileWrite(handle, TimeToStr(CurTime(), TIME_DATE|TIME_SECONDS), Bid);
   FileClose(handle);
   ExtMapBuffer[0] = iRcdCnt;
   return(0);
}   
//--- end ---