//+------------------------------------------------------------------+
//|                                              SMA Band EA v 1.mq4 |
//|                                                          Kalenzo |
//|                                          http://www.fxservice.eu |
//+------------------------------------------------------------------+
#property copyright "Kalenzo"
#property link      "http://www.fxservice.eu"
#include <stdlib.mqh>
extern int 	Magic1             = 123451;
extern int 	Magic2             = 123452;
extern int 	Magic3             = 123453,
	
			   FakeStopLoss      = 430,//fake levels
			   FakeTakeProfit    = 390,//fake levels
			   //TakeProfit        = 100,//hidden tp
			   Slippage          = 1;
extern bool   MultipleContractEntry = false;
extern double LotStep         = 0.1;//accepted values are : 1, 0.1 or 0.01
extern double MaxAmountRisked = 250;//in account currency
extern double OnePipValue     = 10;//depends of account type, eg. standard account 1 lot trade (100k) pip value = 10;
extern double MinimumLotSize  = 0.1;

extern string n1 = "--- SIGNAL SETUP ---";
extern double  ma_shift       = 0.20;
extern int     ma_price       = PRICE_CLOSE;
extern int     ma_method      = MODE_SMA;
extern int     ma_period      = 21;
extern int     lookback_bars  = 25;//check bars condition
extern int     price_momentum = 20;//number of bars that signal will be valid
extern bool    isAgressiveMod = true;

extern int        TimezoneStartHour      =     6;    // Start Hour of Trade Session 
extern int        TimezoneStartMinute    =     0;    // Start Minute of Trade Session 
extern int        TimezoneEndHour        =     18;    // End Hour of Trade Session
extern int        TimezoneEndMinute      =     0;    // End Hour of Trade Session     

extern bool       EnableTimezones             =     false; 

extern int        TradingModeSelection = 0;//0 -long&short, 1-long, 2-short; TRADE DIRECTION
extern bool       TurnFridayCloseOn = true;
extern int        FridayEndHour      =     21;    
extern int        FridayEndMinute    =     50;    

extern bool       MailOn   = true;
extern bool       AudioOn  = true;
extern double     StopLossTolerance = 110;//in procents - this is a stop loss value multiplier 
extern int        MaxStopLoss =  100;//Maximum stop loss in pips  
			   
extern string     Version = "SMA Band EA v 1.3.4";//iname

string ExpiriationDate        = "2020.8.12 17:35";//yyyy.mm.dd hh:mi
int    AllowedAccountNumber   = 1384988; 			   
			   
//INTERNAL PARAMETERS OF THE EA - DO NOT MODIFY MANUALY			   
			   
int bm = 1; 
bool expired = false;
int lotRounding = 0; 
 double initialStop = 0;			
 double trailingstop = 0;   
//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   if(LotStep!=0.01 && LotStep!= 0.1 && LotStep!=1) Alert("WRONG LOT STEP, ACCEPTED VALUES : 1,0.1,0.01");
   else
   {
      if(LotStep == 0.1) lotRounding = 1;
      else if(LotStep == 0.01) lotRounding = 2;
   }
   
   Print(Version);
	if(MarketInfo(Symbol(),MODE_DIGITS) == 5 ||  MarketInfo(Symbol(),MODE_DIGITS) == 3)bm = 10;
	StopLossTolerance = StopLossTolerance/100;
	
	//TakeProfit    = TakeProfit   * bm;
	MaxStopLoss   = MaxStopLoss  * bm;
	
	FakeStopLoss = FakeStopLoss * bm;
	FakeTakeProfit = FakeTakeProfit * bm;
	if(TimeCurrent()>StrToTime(ExpiriationDate)) 
	{
	    expired = true;
	    Alert("ACCESS DATE EXPIRED");
	}    
	if(AllowedAccountNumber != AccountNumber())
	{
	    expired = true;
	    Alert("WRONG ACCOUNT NUMBER");
	}
	
	
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
  
	  ObjectDelete("TAKE PROFIT 1");
	  ObjectDelete("TAKE PROFIT 2");
	  ObjectDelete("STOP LOSS 1");
	  ObjectDelete("STOP LOSS 2");
	  ObjectDelete("STOP LOSS 3");
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
   //if(expired) return(0);
   
		   
   bool closedFriday = false;
   datetime fend = StrToTime( FridayEndHour+":"+ FridayEndMinute);
   if(DayOfWeek() == 5 && TimeCurrent() >= fend) closedFriday = true;
   
   
   if(!orderExists())
	{
	  ObjectDelete("TAKE PROFIT 1");
	  ObjectDelete("TAKE PROFIT 2");
	  ObjectDelete("STOP LOSS 1");
	  ObjectDelete("STOP LOSS 2");
	  ObjectDelete("STOP LOSS 3");
	  
	  if(isOrderAllowed() && tradeTime() && !closedFriday)
	  {
	     MathSrand(TimeCurrent());
        
        double random = NormalizeDouble(MathRand()/33,0);
	     if(bm == 1) random = NormalizeDouble(random /10,0); 
	     
	     
	     initialStop = 0;
	     double difference  = 0;  
	   
	   if(getSignal(OP_BUY) && (TradingModeSelection == 0 || TradingModeSelection == 1)) 
	   {
	     initialStop = getStopLossLevel(OP_BUY,0);
	     
	     trailingstop = 0;//init value is 0
	     
	     initialStop = initialStop - (((Ask-initialStop)/bm) * StopLossTolerance);
	     
	     difference = ((Ask - initialStop)/Point)/bm;  
	     
	     if(difference>MaxStopLoss)
	     {
	        Print("Long trade restricted - stop loss too high: "+(difference/bm));
	     }
	     else
	     {
	        double longLot = LotSize(Ask)/3;
	        if(longLot<MinimumLotSize) longLot = MinimumLotSize;
	        
	        
	        openOrder(longLot,Magic1,OP_BUY,Ask-(FakeStopLoss+random)*Point  ,Ask + (FakeTakeProfit+random)*Point);
	        openOrder(longLot,Magic2,OP_BUY,Ask-(FakeStopLoss+random)*Point  ,Ask + (FakeTakeProfit+random)*Point);
	        
	        if(MultipleContractEntry)
	        openOrder(longLot,Magic3,OP_BUY,Ask-(FakeStopLoss+random)*Point  ,Ask + (FakeTakeProfit+random)*Point);
	        
	        Print("Random fake number of pips added: "+(random/bm));
	     }   
	   }  
		if(getSignal(OP_SELL)&& (TradingModeSelection == 0 || TradingModeSelection == 2)) 
		{
		  initialStop = getStopLossLevel(OP_SELL,0);
		  initialStop =  initialStop + (((initialStop - Bid)/bm) * StopLossTolerance);
	     
	     trailingstop = 0;//init value is 0
	     
	     difference = ((initialStop - Bid)/Point)/bm;  
	     
	     if(difference>MaxStopLoss)
	     {
	        Print("Short trade restricted - stop loss too high: "+(difference/bm));
	     }
	     else
	     {
	       double shortLot = LotSize(Bid)/3;
	       if(shortLot<MinimumLotSize) shortLot = MinimumLotSize;
	         
		    openOrder(shortLot,Magic1,OP_SELL, Bid+(FakeStopLoss+random)*Point ,Bid - (FakeTakeProfit+random)*Point);
		    openOrder(shortLot,Magic2,OP_SELL, Bid+(FakeStopLoss+random)*Point ,Bid - (FakeTakeProfit+random)*Point);
		    
		    if(MultipleContractEntry)
		    openOrder(shortLot,Magic3,OP_SELL, Bid+(FakeStopLoss+random)*Point ,Bid - (FakeTakeProfit+random)*Point);
		    
		    Print("Random fake number of pips added: "+(random/bm));
		  }  
		}
	  }
	}
   else
   {
      
            
            
            int total = OrdersTotal();
            for(int cnt = 0 ;cnt<=total;cnt++)
            {
               OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
               if(OrderMagicNumber() == Magic1 || OrderMagicNumber() == Magic2 || OrderMagicNumber() == Magic3) 
               {
                  if(closedFriday)
                  {//FRIDAY CLOSE
                     closeOrder(OrderTicket(),9999);
                     continue;
                  }

                  //NORMAL CLOSE CONDITIONS TP/SL
                  //NEW ORDERS WILL BE OPENED BY TRADING MODE SELECTION
                  //ORDERS THAT ARE ON THE MARKET SHOULD BE CLOSED NORMALY
                 
                  int orderShift = iBarShift(Symbol(),0,OrderOpenTime(),false);
                  int ot = OrderType();
                  
                  double profit = 0;
                  double stop = 0;
                  if(ot == OP_BUY)
                  {
                     stop = getStopLossLevel(OP_BUY,orderShift);
                     stop = stop - (((OrderOpenPrice()-stop)/bm) * StopLossTolerance);
                     
                     if(OrderMagicNumber() == Magic1)
                     {
                     /*
                     For the 1st contract: 
                     stop loss (sell stop order) is Z pips away lower
                     profit stop (sell limit order) is Y/2 pips away higher. 
                     */
                        drawLine("STOP LOSS 1",Time[10],stop, 1, Red,1);
                     
                        if(Ask<stop)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT SL");    
                           Print("ORDER 1 CLOSE AT STOP");
                        }
                     
                        profit = OrderOpenPrice()+((OrderOpenPrice()-stop)/2);
                     
                        if(Ask>profit)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TP"); 
                           Print("ORDER 1 CLOSE AT TP");     
                        }
                     
                        drawLine("TAKE PROFIT 1",Time[10],profit, 1, Green,1);
                     }  
                     else if(OrderMagicNumber() == Magic2)
                     {
                     /*
                     for the 2nd contract: 
                     initial stop loss (sell stop order) is Z pips away lower, 
                     trailing at Y/2 step.  
                     profit stop (sell limit order) is Y pips away higher.   
                     */
                        
                        if(Ask < stop && trailingstop == 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT SL");    
                           Print("ORDER 2 CLOSE AT STOP");
                        }
                        
                        //MAGAGE TRAILING STOP
                        if( Ask > (OrderOpenPrice() + ((OrderOpenPrice()-stop)/2) ) && Ask>trailingstop)
                        {
                           double newTrailing = Ask - ((OrderOpenPrice()-stop) /2);
                           
                           if(newTrailing>trailingstop)
                           trailingstop = newTrailing;
                        }
                        //CLOSE AT TRAILING STOP
                        if(Ask< trailingstop && trailingstop != 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TLSL");    
                           Print("ORDER 2 CLOSE AT TRAILINGSTOP");
                        }
                        
                        if(trailingstop == 0)
                           drawLine("STOP LOSS 2",Time[10],stop, 1, Magenta,1);
                        else
                           drawLine("STOP LOSS 2",Time[10],trailingstop, 1, Magenta,1);
                        
                        
                        profit = OrderOpenPrice()+((OrderOpenPrice()-stop));
                     
                        if(Ask>profit)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TP"); 
                           Print("ORDER 2 CLOSE AT TP");     
                        }
                     
                        drawLine("TAKE PROFIT 2",Time[10],profit, 1, Lime,1);
                       // trailingstop
                        
                     }
                     else if(OrderMagicNumber() == Magic3)
                     {
                     /*
                     for the 3rd contract (if there is one): 
	                  initial stop loss (sell stop order) is Z pips away lower, 
	                  trailing at Y/2 step.
	                  profit stop: no limit. 
                     */
                        if(Close[0] < stop && trailingstop == 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT SL");    
                           Print("ORDER 3 CLOSE AT STOP");
                        }
                        
                        //MAGAGE TRAILING STOP
                        if( Ask > (OrderOpenPrice() + ((OrderOpenPrice()-stop)/2) ) && Ask>trailingstop)
                        {
                           double newTrailing3 = Ask - ((OrderOpenPrice()-stop) /2);
                           
                           if(newTrailing3>trailingstop)
                           trailingstop = newTrailing3;
                        }
                        //CLOSE AT TRAILING STOP
                        if(Close[0] < trailingstop && trailingstop != 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TLSL");    
                           Print("ORDER 3 CLOSE AT TRAILINGSTOP");
                        }
                        
                        if(trailingstop == 0)
                           drawLine("STOP LOSS 3",Time[10],stop, 0, Pink,1);
                        else
                           drawLine("STOP LOSS 3",Time[10],trailingstop, 0, Pink,1);
                        
                        
                       
                     }
                     
                  }
                  else if(ot == OP_SELL)
                  {
                     stop = getStopLossLevel(OP_SELL,orderShift);
                     stop = stop + (((stop - OrderOpenPrice())/bm) * StopLossTolerance);
                     
                     
                     if(OrderMagicNumber() == Magic1)
                     {
                        drawLine("STOP LOSS 1",Time[10],stop, 1, Red,1);
                     
                        if(Bid>stop)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT SL");
                            Print("CLOSE AT STOP");
                        }
                     
                        profit = OrderOpenPrice()-((stop - OrderOpenPrice())/2);
                     
                        if(Bid<profit)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TP");  
                            Print("CLOSE AT TP 1");  
                        }
                     
                        drawLine("TAKE PROFIT 1",Time[10],profit, 1, Green,1);
                     }
                     else if(OrderMagicNumber() == Magic2)
                     {
                        /*
                     for the 2nd contract: 
                     initial stop loss (sell stop order) is Z pips away lower, 
                     trailing at Y/2 step.  
                     profit stop (sell limit order) is Y pips away higher.   
                     */
                        
                        if(Bid > stop && trailingstop == 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT SL");    
                           Print("ORDER 2 CLOSE AT STOP");
                        }
                        
                        //MAGAGE TRAILING STOP
                        if( Bid < (OrderOpenPrice() - ((stop - OrderOpenPrice())/2)) && (Bid < trailingstop || trailingstop == 0))
                        {
                           double newTrailings = Bid + ((stop-OrderOpenPrice()) /2);
                           
                           if(newTrailings<trailingstop || trailingstop == 0)
                           trailingstop = newTrailings;
                        }
                        
                        //CLOSE AT TRAILING STOP
                        if(Bid > trailingstop && trailingstop != 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TLSL");    
                           Print("ORDER 2 CLOSE AT TRAILINGSTOP");
                        }
                        
                        if(trailingstop == 0)
                           drawLine("STOP LOSS 2",Time[10],stop, 1, Magenta,1);
                        else
                           drawLine("STOP LOSS 2",Time[10],trailingstop, 1, Magenta,1);
                        
                        
                        profit = OrderOpenPrice()-((stop - OrderOpenPrice()));
                     
                        if(Bid<profit)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TP");  
                            Print("ORDER 2 CLOSE AT TP");  
                        }
                     
                        drawLine("TAKE PROFIT 2",Time[10],profit, 1, Lime,1);
                       // trailingstop
                     }
                     else if(OrderMagicNumber() == Magic3)
                     {
                        if(Bid > stop && trailingstop == 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT SL");    
                           Print("ORDER 2 CLOSE AT STOP");
                        }
                        
                        //MAGAGE TRAILING STOP
                        if( Bid < (OrderOpenPrice() - ((stop - OrderOpenPrice())/2)) && (Bid < trailingstop || trailingstop == 0))
                        {
                           double newTrailings2 = Bid + ((stop-OrderOpenPrice()) /2);
                           
                           if(newTrailings2<trailingstop || trailingstop == 0)
                           trailingstop = newTrailings2;
                        }
                        //CLOSE AT TRAILING STOP
                        if(Bid > trailingstop && trailingstop != 0)
                        {
                           closeOrder(OrderTicket(),9999);
                           if(MailOn) SendMail("SMA BAND EA MESSAGE","ORDER CLOSED AT TLSL");    
                           Print("ORDER 2 CLOSE AT TRAILINGSTOP");
                        }
                        
                        if(trailingstop == 0)
                           drawLine("STOP LOSS 3",Time[10],stop, 0, Pink,1);
                        else
                           drawLine("STOP LOSS 3",Time[10],trailingstop, 0, Pink,1);
                     }
                     
                     
                  }
               } 
                
            }
         
            if(closedFriday && MailOn) SendMail("SMA BAND EA MESSAGE","ORDERS CLOSED BY FRIDAY END");    
            if(closedFriday && AudioOn) SendMail("SMA BAND EA MESSAGE","ORDERS CLOSED BY FRIDAY END");    
             
            
              
   }
//----
   return(0);
  }
//+------------------------------------------------------------------+
void drawLine(string name,datetime tfrom, double pfrom , int width, color Col,int type)
{
         if(ObjectFind(name) != 0)
         {
            ObjectCreate(name, OBJ_HLINE, 0, tfrom, pfrom);
            
            if(type == 1) 
            ObjectSet(name, OBJPROP_STYLE, STYLE_SOLID);
            else if(type == 2)
            ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
            else
            ObjectSet(name, OBJPROP_STYLE, STYLE_DOT);
            
            ObjectSet(name, OBJPROP_COLOR, Col);
            ObjectSet(name,OBJPROP_WIDTH,width);
             
         }
         else
         {
            ObjectDelete(name);
            ObjectCreate(name, OBJ_HLINE, 0, tfrom, pfrom);
            
            if(type == 1) 
            ObjectSet(name, OBJPROP_STYLE, STYLE_SOLID);
            else if(type == 2)
            ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
            else
            ObjectSet(name, OBJPROP_STYLE, STYLE_DOT);
            
            ObjectSet(name, OBJPROP_COLOR, Col);
            ObjectSet(name,OBJPROP_WIDTH,width);
          
         }
}  
//+------------------------------------------------------------------+
bool getSignal(int mode)
{

   double longLevel = 0;
   double shortLevel = 0;
   
   if(isAgressiveMod)
   {
      longLevel = iCustom(Symbol(),0,"SMA_BAND_INDI_v2",ma_shift,ma_price,ma_method,ma_period,lookback_bars,price_momentum,isAgressiveMod,2,0);
      shortLevel = iCustom(Symbol(),0,"SMA_BAND_INDI_v2",ma_shift,ma_price,ma_method,ma_period,lookback_bars,price_momentum,isAgressiveMod,3,0);

      if(mode == OP_BUY && Ask > longLevel && longLevel != 0.0) return(true);
      else if(mode == OP_SELL && Bid < shortLevel && shortLevel != 0.0) return(true);
      else return(false);
   }
   else
   {
      longLevel = iCustom(Symbol(),0,"SMA_BAND_INDI_v2",ma_shift,ma_price,ma_method,ma_period,lookback_bars,price_momentum,isAgressiveMod,2,1);
      shortLevel = iCustom(Symbol(),0,"SMA_BAND_INDI_v2",ma_shift,ma_price,ma_method,ma_period,lookback_bars,price_momentum,isAgressiveMod,3,1);
      
      if(mode == OP_BUY && Close[1] > longLevel && longLevel != 0.0) return(true);
      else if(mode == OP_SELL && Close[1] < shortLevel && shortLevel != 0.0) return(true);
      else return(false);  
   }
   
   return(false);
}
//+------------------------------------------------------------------+
bool closeOrder(int ticket,double size)
{
   if(!IsTradeAllowed() )
   return (false);
   
   int MAXRETRIES = 5;
   int retries = 0;
   
   OrderSelect(ticket,SELECT_BY_TICKET,MODE_TRADES);
   
   double lots = OrderLots();
   if(size < lots)
   {
      lots = size;
   }
    
   int error;
   if(OrderType() == OP_BUY)
   {
      while(!OrderClose(ticket,lots, Bid, Slippage, Red))
      {
      
            error = GetLastError();
            
            if(error>1)
            {
               Print("OrderClose failed with error #",ErrorDescription(error));
               if(MailOn)
               SendMail("SMA BAND EA ERROR",ErrorDescription(error));   
            }
            Sleep(1000);
            
            RefreshRates();
         
            if(retries >= MAXRETRIES) 
              return(false);
            else
              retries++;
        }

   }
   else
   {
         while(!OrderClose(ticket, lots, Ask, Slippage, Red))
         {
            error = GetLastError();
            
            if(error>1)
            {
               Print("OrderClose failed with error #",ErrorDescription(error));
                if(MailOn)
               SendMail("SMA BAND EA ERROR",ErrorDescription(error));
            }
            Sleep(1000);
            
            RefreshRates();
         
            if(retries >= MAXRETRIES) 
              return(false);
            else
              retries++;
        }

   }
   
   return (true);
}
//+------------------------------------------------------------------++
bool tradeTime()
{  
   if(!EnableTimezones) return(true);
    
   bool result = false;
   
   
   datetime SessionStart = StrToTime( TimezoneStartHour+":"+ TimezoneStartMinute);
   datetime SessionEnd   = StrToTime( TimezoneEndHour+":"+ TimezoneEndMinute);
   
   if ( TimezoneStartHour <  TimezoneEndHour)
   result = TimeCurrent() >= SessionStart && TimeCurrent() < SessionEnd;      
   else
   if ( TimezoneStartHour >  TimezoneEndHour)
   result = (TimeCurrent() > SessionStart && TimeHour(TimeCurrent()) < 24)
          ||(TimeHour(TimeCurrent()) >= 0 && TimeCurrent() < SessionEnd);
   
   return(result);
}
//+------------------------------------------------------------------++
double getStopLossLevel(int mode,int shift)
{ 
   if(mode == OP_BUY )  return(Low[iLowest(Symbol(),0,MODE_LOW,lookback_bars,shift)]);
   else if(mode == OP_SELL )  return(High[iHighest(Symbol(),0,MODE_HIGH,lookback_bars,shift)]);
   else return(0);
}
//+------------------------------------------------------------------+
bool orderExists()
{
    int total = OrdersTotal();
    for(int cnt = 0 ;cnt<=total;cnt++)
    {
      OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
      if(OrderMagicNumber() == Magic1 || OrderMagicNumber() == Magic2 || OrderMagicNumber() == Magic3 ) 
      return(true);
    } 
    return(false); 
}
//+------------------------------------------------------------------+
 bool isOrderAllowed()
{
    int totalH = OrdersHistoryTotal();
    
    for(int cntH = 0 ; cntH<=totalH; cntH++)
    {
		OrderSelect(cntH, SELECT_BY_POS, MODE_HISTORY);
      if(OrderMagicNumber() == Magic1 || OrderMagicNumber() == Magic2 || OrderMagicNumber() == Magic3  ) 
		{
			int orderOShiftH = iBarShift(Symbol(),0,OrderOpenTime(),false);
		 	
			if(orderOShiftH == 0 ) 
			return(false);	
		}	
    }
    
    
    int total  = OrdersTotal();
    
    for(int cnt = 0 ; cnt <= total; cnt++)
    {
		OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
		
      if(OrderMagicNumber() == Magic1 || OrderMagicNumber() == Magic2 || OrderMagicNumber() == Magic3 ) 
		{
			int orderOShift = iBarShift(Symbol(),0,OrderOpenTime(),false);
		 	
			if(orderOShift == 0 ) 
			return(false);	
		}	
    }
    
    
	return(true);
}
 
//+------------------------------------------------------------------+
double LotSize(double oprice)
{   
   double lots = MinimumLotSize;
   double slpoints = (MathAbs(oprice-initialStop)/Point)/bm;
   
   //(dolars/stop los INT)/pipValue
   //Print(slpoints+" "+MaxAmountRisked+" "+OnePipValue);
    
   lots = NormalizeDouble((MaxAmountRisked/slpoints)/OnePipValue,lotRounding);
   if(lots<MinimumLotSize) lots = MinimumLotSize;
   return(lots);
}
//+------------------------------------------------------------------+
bool openOrder(double lots,int magic,int type, double sl,double tp, string description = "" )
{
   if(!IsTradeAllowed())
	{
		return (-1);
	}
   
	int error = 0;
	int ticket = 0;
  
 
   	
	if( type == OP_SELL )
	{
		while(true)
		{
		   RefreshRates();
		    
		 	ticket = OrderSend(Symbol(),OP_SELL,lots,MarketInfo(Symbol(),MODE_BID),Slippage,sl,tp,StringConcatenate(Version," ",description),magic,0,Pink);
         
			if(ticket<=0)
			{
				error=GetLastError();
			 
               Print("SELL ORDER ERROR:", ErrorDescription(error));
                
            
            if(!ErrorBlock(error)) break;
            
			}
			else
			{
			
			   OrderSelect(ticket,SELECT_BY_TICKET);
			   if(MailOn) SendMail("SMA BAND EA ORDER",OrderPrint());
			   break;
			}
		} 	
   }
   else if( type == OP_BUY )
   {
		
		while(true)
		{
		   RefreshRates();
		   
			ticket = OrderSend(Symbol(),OP_BUY,lots,MarketInfo(Symbol(),MODE_ASK),Slippage,sl,tp,StringConcatenate(Version," ",description),magic,0,Lime);
         
			if(ticket<=0)
			{
				error=GetLastError();
				
            Print("BUY ORDER ERROR:", ErrorDescription(error));
             
            
            if(!ErrorBlock(error)) break;
            
			}
			else
			{
			    
			   OrderSelect(ticket,SELECT_BY_TICKET);
			   if(MailOn) SendMail("SMA BAND EA ORDER",OrderPrint());
			   break;
			}
			
		}
   }
    
   return (0);
}
//+------------------------------------------------------------------+
bool ErrorBlock(int error = 0)
{
   
   switch(error)
   {
       case 0: 
       {
         //no error - exit from loop
         Print("NO ERROR");
         return(false);
       }
       case 2:
       {
           Print("System failure. Reboot the computer/check the server");
            if(MailOn)SendMail("SMA BAND EA ERROR","System failure. Reboot the computer/check the server");
           return(false);  
       }
       case 3:
       {
           Print("Error of the logic of the EA");
            if(MailOn)SendMail("SMA BAND EA ERROR","Error of the logic of the EA");
           return(false);  
       }
       case 4:
       {
           Print("Trading server is busy. Wait for 2 minutes.");
            if(MailOn)SendMail("SMA BAND EA ERROR","Trading server is busy. Wait for 2 minutes.");
           Sleep(120000);
           return(true);   
       }
       case 6:
       { 
           bool connect = false;
           int iteration = 0;
           Print("Disconnect ");
           while((!connect) || (iteration > 60))
           {
               Sleep(10000);
               Print("Connection not restored", iteration*10,"  seconds passed");
               connect = IsConnected();
               if(connect)
               {
                   Print("Connection restored");
               }
               iteration++;
           }
           Print("Connection problems");
            if(MailOn)SendMail("SMA BAND EA ERROR","Connection problems");
           return(false);  
       }
       case 8:
       {
           Print("Frequent requests");
            if(MailOn)SendMail("SMA BAND EA ERROR","Frequent requests");
           return(false);  
       }
       case 64:
       {
           Print("Account is blocked!");
            if(MailOn)SendMail("SMA BAND EA ERROR","Account is blocked!");
           return(false);  
       }
       case 65:
       {
           Print("Wrong account number???");
            if(MailOn)SendMail("SMA BAND EA ERROR","Wrong account number???");
           return(false);  
       }
       case 128:
       {//????
           Print("Waiting of transaction timed out");
            if(MailOn)SendMail("SMA BAND EA ERROR","Waiting of transaction timed out");
           Sleep(10000);//10 seconds
           RefreshRates();
           return(false);  
       }
       case 129:
       {
           Print("Wrong price");
            if(MailOn)SendMail("SMA BAND EA ERROR","Wrong price");
           RefreshRates();
           return(false);  
       }
       case 130:
       {
           Print("Wrong stop SLEVEL"+MarketInfo(Symbol(),MODE_STOPLEVEL)+" FZLVL "+MarketInfo(Symbol(),MODE_FREEZELEVEL)+" FZLVL "+MarketInfo(Symbol(),MODE_SPREAD));
            if(MailOn)SendMail("SMA BAND EA ERROR","Wrong stop SLEVEL"+MarketInfo(Symbol(),MODE_STOPLEVEL)+" FZLVL "+MarketInfo(Symbol(),MODE_FREEZELEVEL)+" FZLVL "+MarketInfo(Symbol(),MODE_SPREAD));
           RefreshRates();
           return(false);   
       }
       case 131:
       {
           Print("Wrong calculation of trade volume");
            if(MailOn)SendMail("SMA BAND EA ERROR","Wrong calculation of trade volume");
           return(false);  
       }
       case 132:
       {
           Print("Market closed");
            if(MailOn)SendMail("SMA BAND EA ERROR","Market closed");
           return(false);  
       }
       case 134:
       {//NOT ENOUGH CASH?
           Print("Lack of margin for performing operation, margin: "+AccountFreeMargin());
            if(MailOn)SendMail("SMA BAND EA ERROR","Lack of margin for performing operation, margin: "+AccountFreeMargin());
           return(false);  
       }
       case 135:
         {
           Print("Prices changed");
            if(MailOn)SendMail("SMA BAND EA ERROR","Prices changed");
           RefreshRates();
           return(true);  
         }
       case 136:
         {
           Print("No price!");
            if(MailOn)SendMail("SMA BAND EA ERROR","No price!");
           return(false);  
         }
       case 138:
         {
           Print("Requote again!");
            if(MailOn)SendMail("SMA BAND EA ERROR","Requote again!");
           RefreshRates();
           return(true);  
         }
       case 139:
         {
           Print("The order is in process. Program glitch");
            if(MailOn)SendMail("SMA BAND EA ERROR","The order is in process. Program glitch");
           Sleep(10000);//10 seconds
           return(true);  
         }
       case 141:
         {
           Print("Too many requests");
            if(MailOn)SendMail("SMA BAND EA ERROR","Too many requests");
           Sleep(10000);//10 seconds 
           return(true);  
         }
       case 148:
         {
           Print("Transaction volume too large");
            if(MailOn)SendMail("SMA BAND EA ERROR","Transaction volume too large");
           return(false);  
         }                                          
         default:
         {  
            Print("Unhandeled exception code:",error," stoplevel ",MarketInfo( Symbol(), MODE_STOPLEVEL) ," spread ",MarketInfo( Symbol(), MODE_SPREAD));
             if(MailOn)SendMail("SMA BAND EA ERROR","Unhandeled exception code:"+error);
            return(false);
         }
     }
   
  }