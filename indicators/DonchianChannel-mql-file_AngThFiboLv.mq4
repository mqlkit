//+------------------------------------------------------------------+
//|Based on              Donchian Channels - Generalized version.mq4 |
//|                         Copyright � 2005, Luis Guilherme Damiani |
//|                                      http://www.damianifx.com.br |
//+------------------------------------------------------------------+
//mod 2008 ForexFactory:   
//EMAAngle; AngleTreshold  jpkfox
//midLine ands FiboLevels edded

#property indicator_chart_window
#property indicator_buffers 7
#property indicator_color1 SlateGray
#property indicator_color2 SlateGray
#property indicator_color3 DarkSlateGray
#property indicator_color4 LightSlateGray
#property indicator_color5 LightSlateGray

#property indicator_color6 LimeGreen
#property indicator_color7 Red

#property indicator_style3 4
#property indicator_style4 2
#property indicator_style5 2



extern int ChannelPeriod=28;
extern int EMAPeriod=120;
extern int StartEMAShift=6;
extern int EndEMAShift=0;
extern double AngleTreshold=0.32;

extern bool ShowFiboLevels = true;
extern double FiboLevel1= 23.6;       //Fibo levels 23,6 38,2; 61,8; 76.4; 50
extern double FiboLevel2= 76.4;   
extern bool ShowMidLine = true;

double UpperLine[];
double LowerLine[];
double MidLine[];
double FibLevel1[];
double FibLevel2[];

double BuyBuffer[];
double SellBuffer[];

int init()
  {
   SetIndexStyle(0,DRAW_LINE);
   SetIndexBuffer(0,UpperLine);
   SetIndexLabel(0,"DonchianChnl UpL "+ChannelPeriod+"");

   SetIndexStyle(1,DRAW_LINE);
   SetIndexBuffer(1,LowerLine);
   SetIndexLabel(1,"DonchianChnl LowL "+ChannelPeriod+"");

   SetIndexStyle(2,DRAW_NONE);
   SetIndexBuffer(2,MidLine);
   SetIndexLabel(2,"DonchChnl MidLine");

   SetIndexStyle(3,DRAW_NONE);
   SetIndexBuffer(3,FibLevel1);
   SetIndexLabel(3,"FiboLevel1 "+FiboLevel1+"");

   SetIndexStyle(4,DRAW_NONE);
   SetIndexBuffer(4,FibLevel2);
   SetIndexLabel(4,"FiboLevel2 "+FiboLevel2+"");

if (ShowMidLine)     SetIndexStyle(2,DRAW_LINE);
if (ShowFiboLevels) {SetIndexStyle(3,DRAW_LINE); SetIndexStyle(4,DRAW_LINE);}



   SetIndexStyle(5,DRAW_ARROW,EMPTY);
   SetIndexArrow(5,241);
   SetIndexBuffer(5,BuyBuffer);
   SetIndexLabel(5,"Buy");

   SetIndexStyle(6,DRAW_ARROW,EMPTY);
   SetIndexArrow(6,242);
   SetIndexBuffer(6,SellBuffer);
   SetIndexLabel(6,"Sell");
   


   IndicatorShortName("DonchianChannel("+ChannelPeriod+")");

   for(int i=0;i<indicator_buffers;i++) SetIndexDrawBegin(i,ChannelPeriod); 

   return(0);
  }
//+------------------------------------------------------------------+
//| Price Channel                                                         |
//+------------------------------------------------------------------+
int start()
  {
   int i, start ,counted_bars=IndicatorCounted();
   int    k;
   double high,low,price, fEndMA, fStartMA, fAngle;

   if(Bars<=ChannelPeriod) return(0);
   
   if(counted_bars>=ChannelPeriod) {
      start=Bars-counted_bars-1;
   } else {
      start=Bars-ChannelPeriod-1;
   }
   
   BuyBuffer[0]=0;
   SellBuffer[0]=0;
   
   for(i=start;i>=0;i--) {

      UpperLine[i]=High[Highest(NULL, 0, MODE_HIGH, ChannelPeriod, i)];
      LowerLine[i]=Low[Lowest(NULL, 0, MODE_LOW, ChannelPeriod, i)];

      MidLine[i]  =(UpperLine[i]+LowerLine[i])/2;

      FibLevel1[i]  =LowerLine[i]+(UpperLine[i]-LowerLine[i])*FiboLevel1/100;
      FibLevel2[i]  =LowerLine[i]+(UpperLine[i]-LowerLine[i])*FiboLevel2/100;


      // next 3 lines from jpkfox, EMAAngle.mq4 

      fEndMA=iMA(NULL,0,EMAPeriod,0,MODE_EMA,PRICE_MEDIAN,i+EndEMAShift);
      fStartMA=iMA(NULL,0,EMAPeriod,0,MODE_EMA,PRICE_MEDIAN,i+StartEMAShift);
      fAngle = 10000.0 * (fEndMA - fStartMA)/(StartEMAShift-EndEMAShift);

      
      if(UpperLine[i+1]<High[i] && fAngle > AngleTreshold) {
         BuyBuffer[i]=High[i];
      }
      if(LowerLine[i+1]>Low[i] && fAngle < -AngleTreshold) {
         SellBuffer[i]=Low[i];
      }
      
      if (BuyBuffer[0]!=0) {
         Print("Donchian Buy Signal at " + BuyBuffer[0] + " -> " + Symbol() + "/" + Period());
         // PlaySound("expert.wav");
      }

      if (SellBuffer[0]!=0) {
         Print("Donchian Sell Signal at " + SellBuffer[0] + " -> " + Symbol() + "/" + Period());
         // PlaySound("expert.wav");
      }
      
   }
  }
//+------------------------------------------------------------------+