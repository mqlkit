//+------------------------------------------------------------------+
//|                                                      jkalman.mq4 |
//|                                   Copyright  2011, Zarko Asenov |
//|                                              http://jaltoh.6x.to |
//+------------------------------------------------------------------+
#property copyright "Copyright  2011, Zarko Asenov"
#property link      "http://jaltoh.6x.to"

#property indicator_chart_window
#property indicator_buffers 3
#property indicator_color1 DeepSkyBlue
#property indicator_color2 DeepSkyBlue
#property indicator_color3 DeepSkyBlue
#property indicator_width1 2
#property indicator_width2 1
#property indicator_width3 1
#property indicator_style2 STYLE_SOLID

//--- input parameters
extern double    Q = 0.022;
extern double    R = 0.617;
extern int       Num_Bars = 25;
extern int       Num_Sub_Bars = 64;

extern int       Sub_Time_Frame = PERIOD_M1;
extern double    Error_Weight = 2.0;

extern bool      Self_Correct = true;
extern int       Num_Cycles_Bar = 4;
extern double    Scatter = 33.3;

extern bool      Verbose = false;


//--- buffers
double ExtMapBuffer1[];
double ExtMapBuffer2[];
double ExtMapBuffer3[];



double x_est_last;
double P_last;
//the noise in the system
    
double K;
double P;
double P_temp;
double x_temp_est;
double x_est;

int iter_ctr;
double sum_error_kalman;
double median_error_kalman;
datetime prev_subbar_time;
double prev_sum_error_kalman = 0.0;

double used_q;
double used_r;
double next_q;
double next_r;
double adj_scatter;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

double subbars_period;
double inv_subbars_period;

int init()
  {
//---- indicators
   SetIndexStyle(0,DRAW_LINE);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexStyle(1,DRAW_LINE);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexStyle(2,DRAW_LINE);
   SetIndexBuffer(2,ExtMapBuffer3);
   SetIndexEmptyValue(0, 0.0);
   SetIndexEmptyValue(1, 0.0);
   SetIndexEmptyValue(2, 0.0);
//----
   prev_subbar_time = 0;
 
   used_q = Q;
   used_r = R;
   next_q = Q;
   next_r = R;
  
   // init_kalman( iClose(Symbol(), Period(), Num_Bars * Period() / Sub_Time_Frame) );

   adj_scatter = Scatter;
   
   subbars_period = Period() / Sub_Time_Frame;
   inv_subbars_period = 1.0 / subbars_period;
   
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
     Comment("");
//---Commen
   return(0);
  }
  

void
init_kalman(
     double init_value)
{
    //initial values for the kalman filter
    
    //initialize with a measurement
    x_est_last = init_value;
    P_last = 0.0;
    
    sum_error_kalman = 0.0;
    median_error_kalman = 0.0;
    iter_ctr = 0; // for median
}

double
do_kalman(
     double value, 
     double train_val,
     double q, 
     double r)
{
        //do a prediction
        P_temp = P_last + q;
        
        //calculate the Kalman gain
        K = P_temp / (P_temp + r); 

        //correct
        x_est = x_est_last + K * (value - x_est_last); 
        P = (1.0 - K) * P_temp;
        //we have our new system

        if (train_val != 0.0) {
          double abserror = MathAbs(train_val - x_est);
          sum_error_kalman += abserror;
          
          median_error_kalman = median_error_kalman * iter_ctr + Error_Weight * abserror;
          median_error_kalman /= (iter_ctr + Error_Weight);
          iter_ctr++;
        }
        
        //update our last's
        P_last = P;
        x_est_last = x_est;

        return (x_est);
}

double
do_cycle(int start_bar, int end_bar, double q, double r, double reference_value)
{
     double est = 0.0;
     sum_error_kalman = 0.0;
     
     for (int i = start_bar; i >= end_bar; i--) {
          double hist_val = iClose(NULL, Sub_Time_Frame, i);
          est = do_kalman(hist_val, reference_value, q, r);
     }
     
     return (est);
}


void
incr_coef(
     double &qr_coef, 
     double &prev_qr_coef)
{
     if (prev_qr_coef == 0.0) return;
     
     qr_coef = prev_qr_coef + (prev_qr_coef / adj_scatter);
}

void
decr_coef(
     double &qr_coef, 
     double &prev_qr_coef)
{
     if (prev_qr_coef == 0.0) return;

     qr_coef = prev_qr_coef - (prev_qr_coef / adj_scatter);
}

#define INCR_R_COEF 0
#define DECR_R_COEF 1
#define INCR_Q_COEF 0
#define DECR_Q_COEF 1

int last_r_adj = INCR_R_COEF;
int last_q_adj = INCR_Q_COEF;


void
filter_bar(int index)
{
   double est = 0.0;
   double ref_value = Close[index];

   used_q = next_q;
   used_r = next_r;
   
   est = do_cycle( Num_Sub_Bars + index * subbars_period, index * subbars_period, used_q, used_r, ref_value);
     
   if (Self_Correct == true) {

     if (sum_error_kalman < prev_sum_error_kalman) {
     
          if (last_q_adj == INCR_Q_COEF) incr_coef(next_q, used_q);
          else decr_coef(next_q, used_q);
          
          if (last_r_adj == INCR_R_COEF ) incr_coef(next_r, used_r);
          else decr_coef(next_r, used_r);
          
     } else if (prev_sum_error_kalman < sum_error_kalman) {
     
          if (last_q_adj == INCR_Q_COEF) decr_coef(next_q, used_q);
          else incr_coef(next_q, used_q);
          
          if (last_r_adj == INCR_R_COEF) decr_coef(next_r, used_r);
          else incr_coef(next_r, used_r);
          
     } else {
          next_q = used_q;
          next_r = used_r;
     }
     
     prev_sum_error_kalman = sum_error_kalman;
     
   } 
   
   if (est > ref_value) {
     ExtMapBuffer1[index] = est;
     ExtMapBuffer2[index] = est - median_error_kalman;
     ExtMapBuffer3[index] = 0.0;

     if (ExtMapBuffer2[index + 1] == 0.0) {
          double prev_err = ExtMapBuffer3[index + 1] - ExtMapBuffer1[index + 1];
          ExtMapBuffer2[index + 1] = ExtMapBuffer1[index + 1] - prev_err;
     }

   } else {
     ExtMapBuffer1[index] = est;
     ExtMapBuffer2[index] = 0.0;
     ExtMapBuffer3[index] = est + median_error_kalman;
     
     if (ExtMapBuffer3[index + 1] == 0.0) {
          prev_err = ExtMapBuffer1[index + 1] - ExtMapBuffer2[index + 1];
          ExtMapBuffer3[index + 1] = ExtMapBuffer1[index + 1] + prev_err;
     }

   }
   
}

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
//----
  int counted_bars = IndicatorCounted();
  int limit = Bars - counted_bars - 1;
  if (limit < 0) return (0);
  if (limit > Num_Bars) limit = Num_Bars;
 
//----
  if (limit == 0) { 
     if (prev_subbar_time == iTime(NULL, Sub_Time_Frame, 0)) return (0);
     else prev_subbar_time = iTime(NULL, Sub_Time_Frame, 0);
  }

   for (int i = limit; i >= 0; i--) {
     init_kalman( iClose(Symbol(), Sub_Time_Frame, (i + 1) * subbars_period - 1) );
     
     for (int j = 0;j < Num_Cycles_Bar; j++) filter_bar(i);
   }

   if (Verbose == true) {
     Comment(
           "\n\nTotal error this bar: ", sum_error_kalman, " (", iter_ctr," bars) \n",
           "Last bar median weighted Kalman error: ", median_error_kalman, "\n",
           "Q:",used_q," R:",used_r," \n",
           "Prediction: ", x_est, "\n");
   }     

   return(0);
  }
//+------------------------------------------------------------------+