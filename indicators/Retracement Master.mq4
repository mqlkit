//+------------------------------------------------------------------+
//|                                           Retracto.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                              http://jaltoh.6x.to |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      "http://jaltoh.6x.to"


#property indicator_separate_window

#property indicator_buffers 5

#property indicator_color1 Green
#property indicator_color2 DarkGray

#property indicator_color3 White

#property indicator_color4 Blue
#property indicator_color5 Red

#define AVERAGE_RETRACEMENT_PIPS        0
#define AVERAGE_RETRACEMENT_PERCENT     1
#define NUMBER_OF_RETRACEMENTS          2
#define NUMBER_OF_RETRACEMENTS_RATIO    3
#define STRONGEST_PUSH_PIPS             4
#define STRONGEST_PUSH_PERCENT          5
#define PRICE_STABILITY_HISTOGRAM       6

extern double Delta_Coefficient = 0.5;

extern bool Log_Prices_Spread = false;
extern bool Display_Price_Persistence = false;
extern bool Opposite_Retracement_Only = true;
extern double Minimum_Change_PIPs = 2.5;

extern int Mode_of_Function = AVERAGE_RETRACEMENT_PIPS;
extern string Mode_of_Function_0 = "Average Retracement Pips";
extern string Mode_of_Function_1 = "Average Retracement Ratio";
extern string Mode_of_Function_2 = "Retracements Count";
extern string Mode_of_Function_3 = "Retracements Count Ratio";
extern string Mode_of_Function_4 = "Price Push in Pips";
extern string Mode_of_Function_5 = "Price Push Ratio";

extern int Used_Price = PRICE_CLOSE;


//---- buffers
double Retracement_Range_Buffer[];
double Range_Delta_Buffer[];
double Spread_Monitor_Buffer[];
double Bulls_Buffer[];
double Bears_Buffer[];


//--- globals
#define NO_DIRECTION 0
#define DIRECTION_UP 1
#define DIRECTION_DOWN 2

#define NO_PRICE -1.0



int Last_Direction = NO_DIRECTION;
double Last_Direction_Change_Price = NO_PRICE;
double Last_Price = NO_PRICE;
double Average_Range;
int Ranges_Added;
double Average_Range_2;
int Ranges_Added_2;

datetime Last_Tick_Time;

double Previous_Price = NO_PRICE;
double Current_Price;

double Tick_Overall_Movement;
double Max_Tick_Bears;
double Max_Tick_Bulls;
double Minimum_Bullish_Movement;
double Minimum_Bearish_Movement;

int Up_Changes_This_Tick;
int Down_Changes_This_Tick;


// Used for price accounting
#define PRICE_VALUE           0
#define TOTAL_TIME_ACTIVE     1
#define LAST_LEFT_TIME        2
#define LAST_VISIT_TIME       3
#define NUM_PROPERTIES        4

int Tick_Price_Time[][NUM_PROPERTIES];



void
reset_tick_counters()
{
     Up_Changes_This_Tick = 0;
     Down_Changes_This_Tick = 0;
     Average_Range = NO_PRICE;
     Ranges_Added = 0;
     Average_Range_2 = NO_PRICE;
     Ranges_Added_2 = 0;

     Last_Price = NO_PRICE;
     Last_Direction = NO_DIRECTION;
     Last_Direction_Change_Price = NO_PRICE;

     Tick_Overall_Movement = 0.0;
     Max_Tick_Bulls = 0.0;
     Max_Tick_Bears = 0.0;

     Up_Changes_This_Tick = 0;
     Down_Changes_This_Tick = 0;
     
     ArrayResize(Tick_Price_Time, 0);
}

int
get_accounted_price_index(double requested_price)
{
     if (requested_price < 0.0) return (-1);
     
     int requested_price_int = requested_price / Point;
     int price_ix = 0;
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     
     while (price_ix < price_ct) {
          if (Tick_Price_Time[price_ix][PRICE_VALUE] == requested_price_int) break;
          price_ix++;
     }

     if (price_ix < price_ct) return (price_ix);
     else return (-1);
}

int
add_accounted_price(
     double price_value, 
     datetime total_time, 
     datetime last_left, 
     datetime last_visit)
{
     int price_value_int = price_value / Point;
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     int price_ix = price_ct;

     price_ct++;
     price_ct = ArrayResize(Tick_Price_Time, price_ct);
     
     if (price_ct < 1 || price_ix < 0) return(-1);

     Tick_Price_Time[price_ix][PRICE_VALUE] = price_value_int;     
     Tick_Price_Time[price_ix][TOTAL_TIME_ACTIVE] = total_time;     
     Tick_Price_Time[price_ix][LAST_LEFT_TIME] = last_left;     
     Tick_Price_Time[price_ix][LAST_VISIT_TIME] = last_visit;
     
     return(price_ix);
}

datetime
retime_accounted_price(int price_ix, datetime last_left)
{
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     if (price_ix < 0 || price_ix >= price_ct) return (-1);

     datetime last_visit = Tick_Price_Time[price_ix][LAST_VISIT_TIME];
     if (last_visit > last_left) return (-1);
     
     datetime total_time = last_left - last_visit;
     
     Tick_Price_Time[price_ix][TOTAL_TIME_ACTIVE] += total_time;
     Tick_Price_Time[price_ix][LAST_LEFT_TIME] = last_left;
     
     return (total_time);
}

datetime
visit_accounted_price(int price_ix, datetime last_visit)
{
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     if (price_ix < 0 || price_ix >= price_ct) return (-1);

     Tick_Price_Time[price_ix][LAST_VISIT_TIME] = last_visit;
     Tick_Price_Time[price_ix][LAST_LEFT_TIME] = 0;
     
     return (Tick_Price_Time[price_ix][TOTAL_TIME_ACTIVE]);
}

void
update_price_stats(double current_price, double previous_price)
{
     int price_ix = 0;
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     datetime current_time = TimeCurrent();

// Update price just left
     price_ix = get_accounted_price_index(previous_price);
     if (price_ix < 0) add_accounted_price(previous_price, 0, current_time, current_time);
     else retime_accounted_price(price_ix, current_time);

// Update newly visited price     
     price_ix = get_accounted_price_index(current_price);
     if (price_ix < 0) add_accounted_price(current_price, 0, 0, current_time);
     else visit_accounted_price(price_ix, current_time);
}

int
get_current_direction()
{
// Detect movement
    int direction;
    double price_difference;
    
    if (Last_Price == NO_PRICE) Last_Price = Current_Price;
    
    price_difference = Current_Price - Last_Price;
    
    if (price_difference > Minimum_Bullish_Movement) direction = DIRECTION_UP;
    else if (price_difference < Minimum_Bearish_Movement) direction = DIRECTION_DOWN;
    else return(NO_DIRECTION);

    update_price_stats(Current_Price, Last_Price);
        
    Last_Price = Current_Price;
    
    return (direction);
}

bool
direction_changed()
{
     int current_direction = get_current_direction();
     if (current_direction == NO_DIRECTION || current_direction == Last_Direction) return (false);
     
     Last_Direction = current_direction;
     
     if (current_direction == DIRECTION_UP) Up_Changes_This_Tick++;
     else if (current_direction == DIRECTION_DOWN) Down_Changes_This_Tick++;
     
     return (true);
}

double
get_current_change_range()
{
     if (direction_changed() == false) return (NO_PRICE);
     
     // else direction did change
     if (Last_Direction_Change_Price == NO_PRICE) Last_Direction_Change_Price = Current_Price;
     double current_range = (Current_Price - Last_Direction_Change_Price) / Point;
     Last_Direction_Change_Price = Current_Price;
     
     return (current_range);
}

void
update_price_stability()
{
}

void
update_strongest_push_percent(double current_range)
{
  // Find strongest retracement
  Tick_Overall_Movement += current_range;
  if (Tick_Overall_Movement > Max_Tick_Bulls) Max_Tick_Bulls = Tick_Overall_Movement;
  else if (Tick_Overall_Movement < Max_Tick_Bears) Max_Tick_Bears = Tick_Overall_Movement;
     
  double Max_Push_Difference = Max_Tick_Bulls + Max_Tick_Bears;
     
  // Calculate bull bear ratio
  if (Max_Push_Difference < 0.0) {
     Bears_Buffer[0] = -100.0 * Max_Push_Difference / Max_Tick_Bears;
     Bulls_Buffer[0] = 0.0;
     Range_Delta_Buffer[0] = Bears_Buffer[0];
  } else if (Max_Push_Difference > 0.0) {
     Bulls_Buffer[0] = 100.0 * Max_Push_Difference / Max_Tick_Bulls;
     Bears_Buffer[0] = 0.0;
     Range_Delta_Buffer[0] = Bulls_Buffer[0];
  }
}

void
update_strongest_push_pips(double current_range)
{
   // Find strongest retracement
   Tick_Overall_Movement += current_range;
   if (Tick_Overall_Movement > Max_Tick_Bulls) Max_Tick_Bulls = Tick_Overall_Movement;
   else if (Tick_Overall_Movement < Max_Tick_Bears) Max_Tick_Bears = Tick_Overall_Movement;
     
   double Max_Push_Difference = Max_Tick_Bulls + Max_Tick_Bears;
     
   // Calculate bull bear push ratio
   Bears_Buffer[0] = Max_Tick_Bears;
   Bulls_Buffer[0] = Max_Tick_Bulls;
}

void
update_retracements_count()
{
      Bulls_Buffer[0] = Up_Changes_This_Tick;
      Bears_Buffer[0] = -1.0 * Down_Changes_This_Tick;
}

void
update_retracements_ratio()
{
  double retracement_ratio;
  if (Up_Changes_This_Tick > Down_Changes_This_Tick) {

     retracement_ratio = 1.0 - Down_Changes_This_Tick / Up_Changes_This_Tick;
     retracement_ratio *= 100.0;

     Bulls_Buffer[0] = retracement_ratio;
     Bears_Buffer[0] = 0.0;
     Range_Delta_Buffer[0] = retracement_ratio;

  } else if (Up_Changes_This_Tick < Down_Changes_This_Tick) {
  
     retracement_ratio = 1.0 - Up_Changes_This_Tick / Down_Changes_This_Tick;
     retracement_ratio *= -100.0;

     Bears_Buffer[0] = retracement_ratio;
     Bulls_Buffer[0] = 0.0;
     Range_Delta_Buffer[0] = retracement_ratio;

  } else {

     Bears_Buffer[0] = 0.0;
     Bulls_Buffer[0] = 0.0;
     Range_Delta_Buffer[0] = 0.0;

  }
}

double
get_period_average_range(double current_range)
{
     // Calculate weighted average movement range
     if (Average_Range == NO_PRICE || Ranges_Added == 0)
          Average_Range = current_range;
     else
          Average_Range = (Average_Range * Ranges_Added + current_range) / (Ranges_Added + 1);

     Ranges_Added++;

     return (Average_Range);
}

double
get_period_average_range_2(double current_range)
{
     // Calculate weighted average movement range
     if (Average_Range_2 == NO_PRICE || Ranges_Added_2 == 0)
          Average_Range_2 = current_range;
     else
          Average_Range_2 = (Average_Range_2 * Ranges_Added_2 + current_range) / (Ranges_Added_2 + 1);

     Ranges_Added_2++;

     return (Average_Range_2);
}

bool
get_current_candle_bullish()
{
     return (Open[0] < Close[0]);
}

void
update_average_range_pips(double current_range)
{
     // Calculate average range
     if (Opposite_Retracement_Only == true && current_range > 0.0)
         Average_Range = get_period_average_range(current_range);         
     else if (Opposite_Retracement_Only == true && current_range < 0.0)
         Average_Range_2 = get_period_average_range_2(MathAbs(current_range));
     else if (Opposite_Retracement_Only == false)
         Average_Range = get_period_average_range(MathAbs(current_range));

     // Update graph
     if (Opposite_Retracement_Only == true && get_current_candle_bullish() == true) {
          Retracement_Range_Buffer[0] = Average_Range_2;
          Range_Delta_Buffer[0] = Average_Range_2;
     } else if (Opposite_Retracement_Only == true && get_current_candle_bullish() == false) {
          Retracement_Range_Buffer[0] = Average_Range;
          Range_Delta_Buffer[0] = Average_Range;
     } else if (Opposite_Retracement_Only == false) {
          Retracement_Range_Buffer[0] = Average_Range;
          Range_Delta_Buffer[0] = Average_Range;
     }
}

void
update_average_range_ratio(double current_range)
{
     double candle_length = (High[0] - Low[0]) / Point;
     if (candle_length <= 0.0) return;

     update_average_range_pips(current_range);

     Retracement_Range_Buffer[0] = 100.0 * Retracement_Range_Buffer[0] / candle_length;
     Range_Delta_Buffer[0] = Retracement_Range_Buffer[0];
}

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----

   IndicatorShortName("JRetracto"); 
   
   if (Log_Prices_Spread == true) {
          SetIndexStyle(2, DRAW_HISTOGRAM); 
          SetIndexLabel(2, "Spread History"); 
          SetIndexBuffer(2, Spread_Monitor_Buffer); 
   } else {
          SetIndexStyle(2, DRAW_NONE); 
          SetIndexLabel(2, ""); 
          SetIndexBuffer(2, NULL); 
   }

   switch (Mode_of_Function) {
     case STRONGEST_PUSH_PERCENT:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexStyle(1, DRAW_LINE); 

          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexLabel(1, "Push Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, "Bullish Pressure in Percents"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 

          SetIndexLabel(4, "Bearish Pressure in Percents"); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelStyle(STYLE_DASHDOT, 2, DodgerBlue); 
          SetLevelValue(1, 50.0);
          SetLevelValue(2, -50.0);
          break;

     case STRONGEST_PUSH_PIPS:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexStyle(1, DRAW_LINE); 

          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexLabel(1, "Push Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, "Bullish Absolute Movement in Pips"); 
          SetIndexLabel(4, "Bearish Absolute Movement in Pips"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(1, 0.0);
          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(2, 0.0);
          break;
          
     case NUMBER_OF_RETRACEMENTS:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexStyle(1, DRAW_NONE); 
          SetIndexLabel(1, ""); 
          SetIndexBuffer(1, NULL); 

          SetIndexLabel(3, "Bullish Retracements Count"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 

          SetIndexLabel(4, "Bearish Retracements Count"); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(1, 0.0);
          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(2, 0.0);
          break;

     case NUMBER_OF_RETRACEMENTS_RATIO:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexStyle(1, DRAW_LINE); 
          SetIndexLabel(1, "Retracements Ratio Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, "Bullish Retracements Ratio"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 

          SetIndexLabel(4, "Bearish Retracements Ratio"); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelStyle(STYLE_DASHDOT, 2, DodgerBlue); 
          SetLevelValue(1, 50.0);
          SetLevelValue(2, -50.0);
          break;
          
     case AVERAGE_RETRACEMENT_PIPS:
          SetIndexStyle(0, DRAW_HISTOGRAM); 
          SetIndexLabel(0, "Average Retracement Range in Pips"); 
          SetIndexBuffer(0, Retracement_Range_Buffer); 

          SetIndexStyle(1, DRAW_LINE); 
          SetIndexLabel(1, "Range Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, ""); 
          SetIndexStyle(3, DRAW_NONE); 
          SetIndexBuffer(3, NULL); 

          SetIndexLabel(4, ""); 
          SetIndexStyle(4, DRAW_NONE); 
          SetIndexBuffer(4, NULL); 

          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(1, 0.0);
          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(2, 0.0);
          break;
          
     case AVERAGE_RETRACEMENT_PERCENT:
          SetIndexStyle(0, DRAW_HISTOGRAM); 
          SetIndexLabel(0, "Average Retracement Range in Percents"); 
          SetIndexBuffer(0, Retracement_Range_Buffer); 

          SetIndexStyle(1, DRAW_LINE); 
          SetIndexLabel(1, "Range Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, ""); 
          SetIndexStyle(3, DRAW_NONE); 
          SetIndexBuffer(3, NULL); 

          SetIndexLabel(4, ""); 
          SetIndexStyle(4, DRAW_NONE); 
          SetIndexBuffer(4, NULL); 

          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelValue(1, 50.0);
          SetLevelValue(2, -50.0);
          break;
   }
   
   Minimum_Bullish_Movement = Minimum_Change_PIPs * Point;
   Minimum_Bearish_Movement = -1.0 * Minimum_Change_PIPs * Point;
//----

   reset_tick_counters();

   return(0);
  }
  
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }

int start()
  {
//----
  RefreshRates();
  
  // Chech if price moved
  Current_Price = Close[0];
  if (Current_Price == Previous_Price) return (0);
  Previous_Price = Current_Price;
  
  // New period
  if (Time[0] != Last_Tick_Time) reset_tick_counters();
  Last_Tick_Time = Time[0];

  // Show price spread graph
  if (Log_Prices_Spread == true) Spread_Monitor_Buffer[0] = (Ask - Bid) / Point;


  // Check for movement in range
  double current_range = get_current_change_range();

  switch (Mode_of_Function) {
    case STRONGEST_PUSH_PERCENT:
      if (current_range == NO_PRICE) return (0);
      else update_strongest_push_percent(current_range);
      break;

    case STRONGEST_PUSH_PIPS:
      if (current_range == NO_PRICE) return (0);
      else update_strongest_push_pips(current_range);
      break;
      
    case NUMBER_OF_RETRACEMENTS:
      update_retracements_count();
      break;

    case NUMBER_OF_RETRACEMENTS_RATIO:
      update_retracements_ratio();
      break;
      
    case AVERAGE_RETRACEMENT_PIPS:
      if (current_range == NO_PRICE) return (0);
      else update_average_range_pips(current_range);
      break;

    case AVERAGE_RETRACEMENT_PERCENT:
      if (current_range == NO_PRICE) return (0);
      else update_average_range_ratio(current_range);
      break;

    case PRICE_STABILITY_HISTOGRAM:
//TODO      if (current_range == NO_PRICE) return (0);
//      else update_price_stability(current_range);
      break;

    default:
      Print("Please choose a valid operating mode!");
  }
  
//----
   return(0);
  }
//+------------------------------------------------------------------+

