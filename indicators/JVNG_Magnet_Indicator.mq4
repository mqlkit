//+------------------------------------------------------------------+
//|                                  JVNG Magnet Index Indicator.mq4 |
//|                                  Copyright � 2007 Matthias Truxa |
//|                                              usmail4matt@gmx.net |
//+------------------------------------------------------------------+

#include "..\include\stdlib.mqh"
#property copyright "Copyright � 2007 Matthias Truxa"
#property link      "mailto:usmail4matt@gmx.net"

#property indicator_chart_window

double hi, lo; 
int zones[]; 

int init()
{
   Analyze(); 

   IndicatorShortName("Jvng Magnet");
   return (0);
}

int start()
{
      
   return (0);
}

void Analyze()
{
   ObjectsDeleteAll(0, OBJ_HLINE); 
   ObjectCreate("wait", OBJ_LABEL, 0, 0, 0); 
   ObjectSet("wait", OBJPROP_XDISTANCE, 10);
   ObjectSet("wait", OBJPROP_YDISTANCE, 100);
   ObjectSetText("wait", "Calculating Resistance and Support Levels from past data...", 10, "Arial", Yellow); 
   ObjectCreate("status", OBJ_LABEL, 0, 0, 0); 
   ObjectSet("status", OBJPROP_XDISTANCE, 10);
   ObjectSet("status", OBJPROP_YDISTANCE, 120);
   ObjectSetText("status", "Initializing...", 8, "Arial", Yellow); 
   
   ObjectSetText("status", "Locating all-time High and Low from " + Bars + "...", 8); 
   double h, l; 
   hi = -1; lo = -1; 
   for (int i=1; i<Bars; i++)
   {
      h = High[i]; 
      l = Low[i]; 
      if ((h > hi) || (hi < 0))
         hi = h; 
      if ((lo > l) || (lo < 0))
         lo = l; 
   }
   hi = NormalizeDouble(hi, Digits-1) + Point*10; 
   lo = NormalizeDouble(lo, Digits-1) - Point*10; 
   ObjectCreate("R_High", OBJ_HLINE, 0, 0, hi); 
   ObjectSet("R_High", OBJPROP_COLOR, Red); 
   ObjectSet("R_High", OBJPROP_WIDTH, 10); 
   ObjectCreate("R_Low", OBJ_HLINE, 0, 0, lo); 
   ObjectSet("R_Low", OBJPROP_COLOR, Red); 
   ObjectSet("R_Low", OBJPROP_WIDTH, 10); 
   
   ObjectSetText("status", "Zone Setup...", 8); 
   ArrayResize(zones, MathRound(hi - lo) * MathPow(10, Digits-1) + 16); 
   ArrayInitialize(zones, 0); 
   ObjectSetText("status", "Zone Setup - counting spikes within " + Bars + " Bars...", 8); 
   for (i=1; i<Bars-1; i++)
   {
      if ( ((Open[i] < Close[i]) && (Open[i+1] > Close[i+1])) && ((MathAbs(Close[i] - Open[i+1]) < 10) || (MathAbs(High[i] - High[i+1]) < 10)) )  // if up-spike
         zones[Translate(Close[i])]++; 
      else if ( ((Open[i] > Close[i]) && (Open[i+1] < Close[i+1])) && ((MathAbs(Open[i+1] - Close[i]) < 10) || (MathAbs(Low[i] - Low[i+1]) < 10)) )  // if down-spike
         zones[Translate(Open[i])]++; 
   }

   ObjectSetText("status", "Zone Setup - finding Max and Min number of hits...", 8); 
   int hhi = -1, hlo = -1; 
   for (i=0; i<ArraySize(zones); i++)
   {
      if ((zones[i] > hhi) || (hhi < 0))
         hhi = zones[i]; 
      if ( ((hlo > zones[i]) || (hlo < 0)) && (zones[i] != 0) )
         hlo = zones[i]; 
   }

   ObjectSetText("status", "Zone Setup - creating resistance and support line objects...", 8); 
   double price; 
   for (i=0; i<ArraySize(zones); i++)
   {
      if (zones[i] == 0)
         continue; 
      price = Revert(i); 
      ObjectCreate("RS_" + i, OBJ_HLINE, 0, 0, price); 
      ObjectSet("RS_" + i, OBJPROP_COLOR, RGB(50 + 200.0 / (hhi-hlo) * zones[i], 0, 0)); 
      ObjectSet("RS_" + i, OBJPROP_STYLE, STYLE_SOLID); 
      ObjectSet("RS_" + i, OBJPROP_WIDTH, 3); 
      ObjectSet("RS_" + i, OBJPROP_BACK, true); 
      ObjectSetText("RS_" + i, DoubleToStr(zones[i], 0) + "x Resistance/Support Line", 8); 
   }
   
   ObjectDelete("status"); 
   ObjectDelete("wait"); 
}

int Translate(double price)
{
   return ( MathRound((price - lo) * MathPow(10, Digits-1)) ); 
}

double Revert(int index)
{
   return (index / MathPow(10, Digits-1) + lo + Point*5); 
}

