//+------------------------------------------------------------------+
// PostTickData.mq4
// modified from TickSave.mq4 and other ea-s
// written by komposter at komposterius@mail.ru
// 04/09/08 modified for format
// reads \experts\files\TickData\<symbol>.csv
// runs as an indicator on 1 minute (or any) chart
// places the record count in a separate window
// note that the time stamp can not be preserved since MT does not chart below the 1 minute level
// so all bars are numbered sequentially from Jan 1, 1970
//=============================================================================
#property copyright "Copyright"  //komposter
#property link      ""           //many thanks to the original author(s) 
#property indicator_separate_window
#property indicator_buffers 1
#property indicator_color1 Red
#include <WinUser32.mqh>
extern int TicksInBar = 5;
double ExtMapBuffer[];
bool bFirst=True;
bool bInitDone = false;
bool bKilled = false;
int prevPeriod=0;
int initBars=0;
int prevBars=0;
int iRtn = 0;
int iLineCtr = 0;
int HistoryHandle = -1, HistoryFilePos = 0, hwnd = 0, TicksFilePos = 0, now_time;
int cnt_bars = 0;
double now_close, now_open, now_low, now_high, now_volume;
string _Symbol;
string ticks_file_name = "";
string sComment = "";
 
int _Period, _PeriodSec, pre_time, last_fpos = 0;
double pre_close;
//=============================================================================
// indicator initialization function
//=============================================================================
int init() {
    string short_name;
    short_name = "TickDataB(" + TicksInBar + ")";
    IndicatorShortName(short_name);
    SetIndexStyle(0,DRAW_LINE,0,1);
    SetIndexBuffer(0,ExtMapBuffer);
    SetIndexLabel(0,short_name);
    return(0);
}

double prices_array[];
double volumes_cache[];
int arr_size = 0;

void
calculate_volumes()
{
     int i = 0;
     while (i < arr_size) {
          i++;
     }
}

int
get_price_volume(
     double price, 
     double gridres)
{
}

void
reset_prices()
{
     ArrayResize(prices_array, 0);
}

void
add_price(double price)
{
     arr_size++;
     ArrayResize(prices_array, arr_size);
     prices_array[arr_size-1] = price;
}

extern double Resolution = 2.5;

double
get_low(double threshold)
{
     for (int i = 0; i < arr_size; i++) {
          if (get_price_volume(prices_array[i], Resolution) > threshold) 
               return (prices_array[i]);
     }
}

double
get_high(double threshold)
{
}

double
get_open(double threshold)
{
}

double
get_close(double threshold)
{
}

double
get_volume(double threshold)
{
}

int DoInit() {
    int    _GetLastError = 0, cnt_ticks = 0, temp[13];
    _Symbol = Symbol();
    hwnd = 0;
    string file_name = StringConcatenate( "!Tick", _Symbol, TicksInBar, ".hst" );
    HistoryHandle = FileOpenHistory( file_name, FILE_BIN | FILE_WRITE );
    //Print("doinit entered, hist hndl: ", HistoryHandle);
    if ( HistoryHandle < 0 ) {
        _GetLastError = GetLastError();
        sComment = "Err on FileOpenHistory" + file_name + ", Error #" + _GetLastError;
        Alert(sComment);
        //Comment(sComment);
        return(-1);
    }
    //Print("wrt hdr");
    FileWriteInteger ( HistoryHandle, 400, LONG_VALUE );
    FileWriteString  ( HistoryHandle, "Copyright � 2008, komposter", 64 );
    FileWriteString  ( HistoryHandle, StringConcatenate( "!Tick", _Symbol ), 12 );
    FileWriteInteger ( HistoryHandle, TicksInBar, LONG_VALUE );
    FileWriteInteger ( HistoryHandle, Digits, LONG_VALUE );
    FileWriteInteger ( HistoryHandle, 0, LONG_VALUE );       //timesign
    FileWriteInteger ( HistoryHandle, 0, LONG_VALUE );       //last_sync
    FileWriteArray   ( HistoryHandle, temp, 0, 13 );
 
    //+------------------------------------------------------------------+
    int year = 2008, month = 1;
    int cur_date = year*100+month, end_date = Year()*100+Month();
    datetime time; double bid; string tmp_str1, tmp_str2;
    now_time = 60; now_close = 0; now_open = 0; now_low = 0; now_high = 0; now_volume = 0;
 
    //while ( cur_date <= end_date ) {
    //while ( true == true ) {
        ticks_file_name = "";
        ticks_file_name = StringConcatenate( "TickData\\", Symbol(), ".csv" );
        //Print("file name is: " , ticks_file_name);
        int ticks_file_handle = FileOpen( ticks_file_name, FILE_READ | FILE_CSV );
        if ( ticks_file_handle < 0 ) {
            sComment = "Error on file open: " + ticks_file_name + ", #" +  GetLastError() + "!";
            Alert(sComment);
            //Comment(sComment);
            return(-1);
        } else {
            while ( !FileIsEnding( ticks_file_handle ) ) {
                if ( GetLastError() == 4099 ) break;
                tmp_str1 = FileReadString( ticks_file_handle );
                if ( StringLen( tmp_str1 ) < 19 ) continue;
                tmp_str2 = FileReadString( ticks_file_handle );
                if ( StringLen( tmp_str2 ) < Digits+2 ) continue;
                time = StrToTime  ( tmp_str1 );
                bid  = StrToDouble( tmp_str2 );
                iLineCtr ++;
                //Print("line ctr: ", iLineCtr);
                if ( now_volume >= TicksInBar || now_volume < 1 ) {
                    if ( now_volume >= TicksInBar ) {
                        FileWriteInteger    ( HistoryHandle, now_time,        LONG_VALUE    );
                        FileWriteDouble    ( HistoryHandle, now_open,        DOUBLE_VALUE);
                        FileWriteDouble    ( HistoryHandle, now_low,        DOUBLE_VALUE);
                        FileWriteDouble    ( HistoryHandle, now_high,        DOUBLE_VALUE);
                        FileWriteDouble    ( HistoryHandle, now_close,    DOUBLE_VALUE);
                        FileWriteDouble    ( HistoryHandle, now_volume,    DOUBLE_VALUE);
                        FileFlush            ( HistoryHandle );
                        cnt_bars ++;
                    }
                    now_time   += 60;
                    now_open   = bid;
                    now_low    = bid;
                    now_high   = bid;
                    now_close  = bid;
                    now_volume = 1;
                } else {
                    if ( bid < now_low  ) now_low  = bid;
                    if ( bid > now_high ) now_high = bid;
                    now_close = bid;
                    now_volume ++;
                }
                cnt_ticks ++;
            }
            FileClose( ticks_file_handle );
            GetLastError();
        }
 
        month ++;
        if ( month > 12 ) {
            month = 1;
            year ++;
        }
        cur_date = year*100+month;
    //}
    HistoryFilePos = FileTell( HistoryHandle);
    FileWriteInteger    ( HistoryHandle, now_time,        LONG_VALUE    );
    FileWriteDouble    ( HistoryHandle, now_open,        DOUBLE_VALUE);
    FileWriteDouble    ( HistoryHandle, now_low,        DOUBLE_VALUE);
    FileWriteDouble    ( HistoryHandle, now_high,        DOUBLE_VALUE);
    FileWriteDouble    ( HistoryHandle, now_close,    DOUBLE_VALUE);
    FileWriteDouble    ( HistoryHandle, now_volume,    DOUBLE_VALUE);
    FileFlush            ( HistoryHandle );
 
    Print("lines counted: ", cnt_ticks, ", bars built: ", cnt_bars, ", last bid: " ,now_close);
    sComment = "History file built: !Tick" + _Symbol + ",M" + TicksInBar + ", with bar count: " + cnt_bars;
    Print(sComment);
    //Comment(sComment);
    
    RefreshWindow();
 
    return(0);
}
 
void start() {
    if ( bKilled == true ) return;
    if (bInitDone == false) {
        iRtn = DoInit();
        if (iRtn != 0) {
            bKilled = true;
            sComment = sComment + ", Disabled.";
            Print(sComment);
            //Comment(sComment);
            return;
        }
        bInitDone = true;
    }
    //Print("start new tick, hist handle: ",HistoryHandle);
    if (bInitDone == false) return;
    if ( HistoryHandle < 0 ){
        //Print("start, hist handle lt 0 - exit now - err");
        return;
    }
    if(Period() != prevPeriod) {
       prevPeriod = Period();
       bFirst = True;
    }
    if(IndicatorCounted() == 0) bFirst = True;
    if(bFirst == True) {
       initBars = Bars;
       prevBars = Bars;
       bFirst = False;
    }
    //Initialize End
    //All bars (full and tick)
    if(Bars <= initBars) return;        //are we past initially loaded bars   
    prevBars = Bars;
    FileSeek( HistoryHandle, HistoryFilePos, SEEK_SET );
    now_volume ++;
    if ( now_volume <= TicksInBar ) {
        if ( Bid < now_low  ) now_low  = Bid;
        if ( Bid > now_high ) now_high = Bid;
        now_close = Bid;
        FileWriteInteger    ( HistoryHandle, now_time,        LONG_VALUE    );
        FileWriteDouble    ( HistoryHandle, now_open,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_low,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_high,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_close,    DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_volume,    DOUBLE_VALUE);
        FileFlush            ( HistoryHandle );
    } else {
        FileWriteInteger    ( HistoryHandle, now_time,        LONG_VALUE    );
        FileWriteDouble    ( HistoryHandle, now_open,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_low,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_high,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_close,    DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_volume-1,DOUBLE_VALUE);
        FileFlush            ( HistoryHandle );
        now_time        += 60;
        now_open        = Bid;
        now_low        = Bid;
        now_high        = Bid;
        now_close    = Bid;
        now_volume    = 1;
        HistoryFilePos = FileTell( HistoryHandle);
        FileWriteInteger    ( HistoryHandle, now_time,        LONG_VALUE    );
        FileWriteDouble    ( HistoryHandle, now_open,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_low,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_high,        DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_close,    DOUBLE_VALUE);
        FileWriteDouble    ( HistoryHandle, now_volume,    DOUBLE_VALUE);
        FileFlush            ( HistoryHandle );
        cnt_bars ++;
    }
    RefreshWindow();
    ExtMapBuffer[0] = cnt_bars;
    return;
}
 
void RefreshWindow() {
    //Print("doing refresh window:", hwnd, "last bid: " ,Bid);
    if ( hwnd == 0 ) {
        hwnd = WindowHandle( StringConcatenate( "!Tick", _Symbol ), TicksInBar );
        //if ( hwnd != 0 ) { Print( "err: ", "!Tick", _Symbol, TicksInBar, "?" ); }
    }
    //Print("doing refresh window:", hwnd, "last bid: " ,Bid);
    if ( hwnd != 0 ) { PostMessageA( hwnd, WM_COMMAND, 33324, 0 ); }
}
 
int deinit() {
    if ( HistoryHandle > 0 ) {
        FileClose( HistoryHandle );
        HistoryHandle = -1;
    }
    //Comment("");
    return(0);
}
 

