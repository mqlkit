//+------------------------------------------------------------------+
//|                                                    ma of cci.mq4 |
//|                      Copyright � 2009, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property indicator_separate_window
#property indicator_buffers 2
#property indicator_color1 Red
#property indicator_color2 Lime
#property indicator_level1 -150000
#property indicator_level2 150000
 
//---- input parameters
extern double lag_p =0.70;
extern int MA_Period=13;
extern int MA_Shift=0;
extern int MA_Method=0;
extern int NumberOfBarsToCalculate = 10000;
 
//---- indicator buffers
double Buffer0[];
double Buffer1[];
double Ma[];
double Lag[];
 
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
  int init()
  {
   Comment("");
   IndicatorBuffers(4);
//---- indicator line
   SetIndexStyle(0,DRAW_LINE,STYLE_SOLID,2);
   SetIndexStyle(1,DRAW_LINE,STYLE_SOLID,2);
   SetIndexStyle(2,DRAW_NONE);
   SetIndexStyle(3,DRAW_NONE);
 
   SetIndexBuffer(0,Buffer0);
   SetIndexBuffer(1,Buffer1);
   SetIndexBuffer(2,Ma);
   SetIndexBuffer(3,Lag);
   
 
   IndicatorDigits(MarketInfo(Symbol(),MODE_DIGITS));
//----
 
   SetIndexEmptyValue(0,0);
   SetIndexEmptyValue(1,0);
   SetIndexEmptyValue(2,0);
   SetIndexEmptyValue(3,0);
//----
   return(0);
  }
 
 
int start() {
      string short_name;
      short_name = "Laguerre["+lag_p+"] \ MA => Max bars to count: |"+(Bars-1)+"| ";
      IndicatorShortName(short_name);
 
      int shift;
      double lag = 0;
      for(shift=NumberOfBarsToCalculate-1;shift>=0;shift--){
         Lag[shift] = iCustom(NULL,0,"Laguerre RSI",lag_p,PRICE_CLOSE,shift);    
         }
      for(shift=NumberOfBarsToCalculate-1;shift>=0;shift--){
         Ma[shift] = iMAOnArray(Lag,0,MA_Period,MA_Shift,MA_Method,shift);
         Buffer0[shift] = Lag[shift];
         Buffer1[shift] = Ma[shift];
         }

      return(0);
}