//+------------------------------------------------------------------+
//|                                                   StochDelta.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      ""

#property indicator_separate_window
#property indicator_buffers 3
#property indicator_style1 STYLE_SOLID
#property indicator_style2 STYLE_SOLID
#property indicator_style3 STYLE_DOT
#property indicator_color1 Gold
#property indicator_color2 Salmon
#property indicator_color3 Red
#property indicator_width1 3
#property indicator_width2 1
#property indicator_width3 1


extern int Stochastic_K = 5;
extern int Stochastic_D = 3;
extern int Slowing = 3;
extern int Stoch_MA_mode = MODE_EMA;
extern int Stoch_Price_Mode = 0;
extern bool Display_Delta = true;
extern bool Live_Mode = false;
extern double Hamming_Const = 0.5;
  

double Delta[];
double Diff[];
double PriceOffset[];


double pi = 3.14159265;


//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- indicators
//----
   SetIndexBuffer(0,Diff); 
   SetIndexBuffer(1,Delta); 
   SetIndexBuffer(2,PriceOffset); 

   SetIndexStyle(0, DRAW_HISTOGRAM);
   SetIndexStyle(1, DRAW_LINE);
   SetIndexStyle(2, DRAW_HISTOGRAM, STYLE_DOT, 1);

   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
  
double 
inverse_hamming(
     double value, 
     double const)
{
     return ( 1.0 - const - const * MathCos( 2.0 * pi * value) );
}

double
get_stoch(int index, int bar)
{
     return ( 
               iStochastic(
                    Symbol(), 
                    Period(), 
                    Stochastic_K, 
                    Stochastic_D, 
                    Slowing, 
                    Stoch_MA_mode, 
                    Stoch_Price_Mode, 
                    index, 
                    bar) 
              );
}

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int limit = Bars - 1 - IndicatorCounted();
   if (limit < 0) return (-1);
//----

   for(int i = limit; i >= 0; i--)
    { 
      double stoch_lead = get_stoch(0, i);
      double stoch_slow = get_stoch(1, i);

      Diff[i] = stoch_lead - stoch_slow;
      Diff[i] *= inverse_hamming(stoch_lead / 100.0, Hamming_Const);
      
      if (Display_Delta == true) {
          Delta[i] = stoch_lead - get_stoch(0, i + 1);
      } else {
          Delta[i] = stoch_lead - 50.0; 
      }
      
      double mid_price = (High[i] + Low[i]) / 2.0;
      double candle_length = High[i] - Low[i];
      double compared_price;
      
      if (Live_Mode != false && Close[i] > mid_price && i == 0) {
          compared_price = Bid;
      } else if (Live_Mode != false && Close[i] < mid_price && i == 0) {
          compared_price = Ask;
      } else {
          compared_price = Close[i];
      }
      
      if (candle_length != 0.0) 
          PriceOffset[i] = 100.0 * (compared_price - mid_price) / candle_length;
      else
          PriceOffset[i] = 0.0;
          
      
   }
//----
   return(0);
  }
//+------------------------------------------------------------------+

