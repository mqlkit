//+------------------------------------------------------------------+
//|                                        SuperTrend volty like.mq4 |
//|                                                           Mladen |
//| initial idea by jntrader                                         |
//+------------------------------------------------------------------+
#property copyright "mladen"
#property link      "mladenfx@gmail.com"

#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Lime
#property indicator_color2 Lime
#property indicator_color3 Red
#property indicator_color4 Red
#property indicator_width2 1
#property indicator_width4 1


extern int    period       = 10;
extern int    appliedPrice = PRICE_CLOSE;
extern double multiplier   = 3.0;

//
//
//
//
//

double TrendU[];
double TrendUA[];
double TrendD[];
double TrendDA[];
double Direction[];
double Up[];
double Dn[];

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//
//

int init()
{
   IndicatorBuffers(7);
      SetIndexBuffer(0, TrendU);
      SetIndexBuffer(1, TrendUA); SetIndexStyle(1,DRAW_ARROW); SetIndexArrow(1,159);
      SetIndexBuffer(2, TrendD);
      SetIndexBuffer(3, TrendDA); SetIndexStyle(3,DRAW_ARROW); SetIndexArrow(3,159);
      SetIndexBuffer(4, Direction);
      SetIndexBuffer(5, Up);
      SetIndexBuffer(6, Dn);
      
      SetIndexLabel(0,"SuperTrend up");
      SetIndexLabel(1,"SuperTrend start up");
      SetIndexLabel(2,"SuperTrend down");
      SetIndexLabel(3,"SuperTrend start down");
   IndicatorShortName("SuperTrend");
   period = MathMax(1,period);
}
int deinit()
{
   return(0);
}




//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//
//

int start()
{
   int counted_bars = IndicatorCounted();
   int limit,i;

   if(counted_bars < 0) return(-1);
   if(counted_bars > 0) counted_bars--;
           limit = Bars-counted_bars;

   //
   //
   //
   //
   //
      
   for(i = limit; i >= 0; i--)
   {
      double atr    = iATR(NULL,0,period,i);
      double cprice = iMA(NULL,0,1,0,MODE_SMA,appliedPrice,i);
      double mprice = (High[i]+Low[i])/2;
             Up[i]  = mprice+multiplier*atr;
             Dn[i]  = mprice-multiplier*atr;
         
      //
      //
      //
      //
      //
         
      Direction[i] = Direction[i+1];
         if (cprice > Up[i+1]) Direction[i] =  1;
         if (cprice < Dn[i+1]) Direction[i] = -1;

      TrendU[i]  = EMPTY_VALUE;
      TrendUA[i] = EMPTY_VALUE;
      TrendD[i]  = EMPTY_VALUE;
      TrendDA[i] = EMPTY_VALUE;
         
         if (Direction[i] > 0) { Dn[i] = MathMax(Dn[i],Dn[i+1]); TrendU[i] = Dn[i]; if (Direction[i] != Direction[i+1]) TrendUA[i] = TrendU[i];}
         else                  { Up[i] = MathMin(Up[i],Up[i+1]); TrendD[i] = Up[i]; if (Direction[i] != Direction[i+1]) TrendDA[i] = TrendD[i];}
   }
   return(0);
}