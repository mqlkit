//+------------------------------------------------------------------+
//|                                            jmarket_sentiment.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                              http://jaltoh.6x.to |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      "http://jaltoh.6x.to"

#property indicator_separate_window
#property indicator_buffers 3
#property indicator_color1 Red
#property indicator_color2 OrangeRed
#property indicator_color3 Gold
#property indicator_width1 1
#property indicator_width2 2
#property indicator_width3 1

extern int MA_Period = 13;
extern double MA_Coef = 2.5;
extern int Time_Frame_Mins = 0;
extern string Symbol2 = "USDCHF";
extern double Symbol2_Coef = -0.5;
extern string Symbol3 = "USDJPY";
extern double Symbol3_Coef = -0.4;
extern string Symbol4 = "USDCAD";
extern double Symbol4_Coef = -0.25;

//--- buffers
double ExtMapBuffer1[];
double ExtMapBuffer2[];
double ExtMapBuffer3[];

int time_frame;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- indicators
   SetIndexStyle(0,DRAW_HISTOGRAM);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexStyle(1,DRAW_LINE);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexStyle(2,DRAW_LINE, 1, STYLE_DOT);
   SetIndexBuffer(2,ExtMapBuffer3);
   
   if (Time_Frame_Mins == 0)
     time_frame = Period();
   else
     time_frame = Time_Frame_Mins;
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int counted_bars = IndicatorCounted();
//----
   int limit = Bars - counted_bars - 1;
   if (limit < 0) return (0);
   
   for (int i = limit; i >= 0; i--) {
     double push1 = iCustom(Symbol(), time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 0, i);
     double filter1 = iCustom(Symbol(), time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 2, i);
     
     if (Symbol2 != "") {
          double push2 = Symbol2_Coef * iCustom(Symbol2, time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 0, i);
          double filter2 = Symbol2_Coef * iCustom(Symbol2, time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 2, i);
     }
     
     if (Symbol2 != "") {
          double push3 = Symbol3_Coef * iCustom(Symbol3, time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 0, i);
          double filter3 = Symbol3_Coef * iCustom(Symbol3, time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 2, i);
     }
     
     if (Symbol2 != "") {
          double push4 = Symbol4_Coef * iCustom(Symbol4, time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 0, i);
          double filter4 = Symbol4_Coef * iCustom(Symbol4, time_frame, "jmarket_sentiment", MA_Period, MA_Coef, time_frame, 2, i);
     }
     
     ExtMapBuffer1[i] = push1 + push2 + push3 + push4;
     ExtMapBuffer2[i] = ExtMapBuffer1[i];
     ExtMapBuffer3[i] = filter1 + filter2 + filter3 + filter4;
   }

//----
   return(0);
  }
//+------------------------------------------------------------------+