//+------------------------------------------------------------------+
//|                                                SMA_BAND_INDI.mq4 |
//|                                                          Kalenzo |
//|                                          http://www.fxservice.eu |
//+------------------------------------------------------------------+
#property copyright "Kalenzo"
#property link      "http://www.fxservice.eu"
#property indicator_buffers 4
#property indicator_color1 Red
#property indicator_color2 Lime
#property indicator_color3 DodgerBlue
#property indicator_color4 Magenta
#property indicator_width3 2
#property indicator_width4 2


double bup[];
double bdn[];

double bup1[];
double bdn1[];

extern double  ma_shift       = 0.20;
extern int     ma_price       = PRICE_CLOSE;
extern int     ma_method      = MODE_SMA;
extern int     ma_period      = 21;
extern int     lookback_bars  = 25;//check bars condition
extern int     price_momentum = 20;//number of bars that signal will be valid
extern bool    isAgressiveMod = true;

double upper = 0; 
double lower = 0; 
      

#property indicator_chart_window
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- indicators
   SetIndexBuffer(0,bup);
   SetIndexBuffer(1,bdn);

   SetIndexStyle(0,DRAW_ARROW);
   SetIndexStyle(1,DRAW_ARROW);
   
   SetIndexArrow(0,108);//167
   SetIndexArrow(1,108);
   
   
   SetIndexBuffer(2,bup1);
   SetIndexBuffer(3,bdn1);

   SetIndexStyle(2,DRAW_ARROW);
   SetIndexStyle(3,DRAW_ARROW);
   
   SetIndexArrow(2,167);//
   SetIndexArrow(3,167);
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
     int limit = 100+ma_period+price_momentum+lookback_bars;
     /*
     int counted_bars=IndicatorCounted();
     if(counted_bars<0) return(-1);
     if(counted_bars>0) counted_bars--;
     limit=Bars-counted_bars;
     */
     for(int i=limit; i>=0; i--)
     {
         
         //FIND LONG / SHORT BAR
         upper = iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_UPPER, i); 
         lower = iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_LOWER, i); 
      
         if(High[i] < lower/*bar bellow red line*/)
         {
            bup[i] = High[i];    
         }
         else bup[i] = 0.0;
         
         if(Low[i] > upper/*bar above green line*/)
         {
            bdn[i] = Low[i]; 
         }
         else bdn[i] = 0.0;
      }
      
         
      for(i=0; i<limit; i++)
      {
         bool validLong = true;
         bool validShort = true;
      
         
         bup1[i] = 0;
         bdn1[i] = 0;
         
         if(i>=lookback_bars)
         {
            if(bup[i]!=0.0)//red dots
            {
               
            
               for(int z = i;z>i-lookback_bars;z--)
               {
                   upper = iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_UPPER, z); 
                  
                  if(High[z] > upper)
                  {
                      validShort = false;
                      break;
                  }
               }
               
               if(validShort)
               {
                   for(int k=i-lookback_bars;k>i-lookback_bars-price_momentum;k--)
                   {
                       bup1[k] = iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_UPPER, k);
                       
                       //signal triggered - stop drawing
                       if(isAgressiveMod)
                       {
                           if(High[k] > iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_UPPER, k)) break; 
                       }
                       else
                       {
                           if(Close[k] > iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_UPPER, k)) break; 
                           
                       }    
                   }
               }
                
            }
            
            
            if(bdn[i]!=0.0)//green dots
            {
               for(z = i;z>=i-lookback_bars;z--)
               {
                  lower = iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_LOWER, z); 
                  
                  if(Low[z]<lower)
                  {
                     validLong = false;
                     break;
                  }   
               }
            
               if(validLong)
               {
                   for(k=i-lookback_bars;k>i-lookback_bars-price_momentum;k--)
                   {
                       bdn1[k] = iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_LOWER, k); 
                       
                       //signal triggered - stop drawing
                       if(isAgressiveMod)
                       {
                           if(Low[k] < iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_LOWER, k)) break;
                       }
                       else
                       {
                           if(Close[k] < iEnvelopes(Symbol(), 0, ma_period, ma_method, 0, ma_price, ma_shift, MODE_LOWER, k)) break;
                       }    
                   }
               }
                
            }
            
         }
      } 
       
                 
     return(0);
  }
//+------------------------------------------------------------------+

