//+------------------------------------------------------------------+
//|                                               Market_Outlook.mq4 |
//|                                   Copyright © 2011, Zarko Asenov |
//|                                              http://jaltoh.6x.to |
//+------------------------------------------------------------------+
#property copyright "Copyright © 2011, Zarko Asenov"
#property link      "http://jaltoh.6x.to"

#property indicator_chart_window
#property indicator_buffers 6
#property indicator_color1 Goldenrod
#property indicator_color2 Goldenrod
#property indicator_color3 Lime
#property indicator_color4 Lime
#property indicator_color5 MediumSlateBlue
#property indicator_color6 DarkOrange
#property indicator_width1 3
#property indicator_width2 3
#property indicator_width3 4
#property indicator_width4 4
#property indicator_width5 5
#property indicator_width6 5
#property indicator_style1 STYLE_SOLID
#property indicator_style2 STYLE_SOLID
#property indicator_style3 STYLE_SOLID
#property indicator_style4 STYLE_SOLID
#property indicator_style5 STYLE_SOLID
#property indicator_style6 STYLE_SOLID


//--- input parameters
extern int       Maximum_Resolution = PERIOD_M1;
<<<<<<< HEAD
extern int       Time_Frame = PERIOD_H1;
=======
extern int       Time_Frame = 0;
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
extern int       Bars_Computed = 10;
extern double    Time_Weight = 2.0;
extern bool      Rough_Estimates = false;
extern string    _ = ""; // separator
extern int       Price_Mode = PRICE_CLOSE;
extern int       Hint_Price_Close = PRICE_CLOSE;
extern int       Hint_Price_High = PRICE_HIGH;
extern int       Hint_Price_Low = PRICE_LOW;
extern int       Hint_Price_Median = PRICE_MEDIAN;
extern int       Hint_Price_Typical = PRICE_TYPICAL;
extern int       Hint_Price_Weighted = PRICE_WEIGHTED;


int max_resolution_secs;
int sub_periods_count;
double stop_level;
double time_factor;
double display_time_ratio;
int time_frame;

double avg_hi[];
double avg_lo[];
double avg_hi_weighted[];
double avg_lo_weighted[];
double avg_price[];
double avg_weighted[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- indicators
//----
     if (Time_Frame == 0) time_frame = Period();
     else time_frame = Time_Frame;
     
     if (Maximum_Resolution == 0) Maximum_Resolution = PERIOD_M1;
     
     if (Time_Weight == 0.0) time_factor = 1.0 + 1.0 * Maximum_Resolution / time_frame;
     else time_factor = Time_Weight;
     
     max_resolution_secs = Maximum_Resolution * 60;
     sub_periods_count = time_frame / Maximum_Resolution;

     stop_level = MarketInfo(Symbol(), MODE_STOPLEVEL);
     display_time_ratio = 1.0 * time_frame / Period();
     
     SetIndexBuffer(0, avg_hi);
     SetIndexBuffer(1, avg_lo);
     SetIndexBuffer(2, avg_hi_weighted);
     SetIndexBuffer(3, avg_lo_weighted);
     SetIndexBuffer(4, avg_price);
     SetIndexBuffer(5, avg_weighted);
     
     SetIndexStyle(0, DRAW_HISTOGRAM);
     SetIndexStyle(1, DRAW_HISTOGRAM);
     SetIndexStyle(2, DRAW_HISTOGRAM);
     SetIndexStyle(3, DRAW_HISTOGRAM);
     SetIndexStyle(4, DRAW_HISTOGRAM);
     SetIndexStyle(5, DRAW_HISTOGRAM);
     
     SetIndexLabel(0, "Average High Price");
     SetIndexLabel(1, "Average Low Price");
     SetIndexLabel(2, "Average Weighted High Price");
     SetIndexLabel(3, "Average Weighted Low Price");
     SetIndexLabel(4, "Average Price");
     SetIndexLabel(5, "Average Weighted Price");
     
     return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
     Comment("");
//----
     return(0);
  }

double
get_price(
     int price_mode,
     double close,
     double hi,
     double lo)
{
     double price;
     
     switch (price_mode) {
          case PRICE_CLOSE:
               price = close;
               break;
          case PRICE_HIGH:
               price = hi;
               break;
          case PRICE_LOW:
               price = lo;
               break;
          case PRICE_MEDIAN:
               price = (hi + lo) / 2.0;
               break;
          case PRICE_TYPICAL:
               price = (hi + lo + close) / 3.0;
               break;
          case PRICE_WEIGHTED:
               price = (hi + lo + close + close) / 4.0;
               break;
          default:
               price = 0.0;
               Print("Invalid price mode: " + Price_Mode);
     }
     
     return (price);
}
  
bool
get_weighted_hilo(
     datetime start_time, 
     datetime end_time,
     double &high,
     double &low,
     double &high_weight,
     double &low_weight,
     double &flat,
     double &weighted)
{
     int sub_period_start;
     int sub_period_end;
    
     sub_period_start = iBarShift(Symbol(), Maximum_Resolution, start_time);
     sub_period_end = iBarShift(Symbol(), Maximum_Resolution, end_time);

     int pos_ctr = 0;
     for (int ix = sub_period_start; ix >= sub_period_end; ix--) {
     
          double sub_high = iHigh(Symbol(), Maximum_Resolution, ix);
          double sub_low = iLow(Symbol(), Maximum_Resolution, ix);
          double sub_close = iClose(Symbol(), Maximum_Resolution, ix);
     
          high = (pos_ctr * high + sub_high) / (pos_ctr + 1.0);
          low = (pos_ctr * low + sub_low) / (pos_ctr + 1.0);
          high_weight = (pos_ctr * high_weight + time_factor * sub_high) / (pos_ctr + time_factor);
          low_weight = (pos_ctr * low_weight + time_factor * sub_low) / (pos_ctr + time_factor);
          
          double price = get_price(
                              Price_Mode, 
                              sub_close,
                              sub_high,
                              sub_low);
               
          weighted = (pos_ctr * weighted + time_factor * price) / (pos_ctr + time_factor);
          flat = (pos_ctr * flat + price) / (pos_ctr + 1.0);
          
          pos_ctr++;
     }
}

string
get_diff_string(
     double price1,
     double price2)
{
     return (
          " ( " + DoubleToStr( (price1 - price2) / Point, 0) + " ) "
          );
}

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int 
start()
  {
     int draw_bars  = Bars - IndicatorCounted() - 1;
     if (draw_bars < 0) return (0);
     
//----

     int compute_start_bar;
     if (draw_bars == 0) compute_start_bar = 0;
     else compute_start_bar = draw_bars / display_time_ratio;
          
     for (int ix = compute_start_bar; ix >= 0; ix--) {
          if (ix < Bars_Computed) {
               double current_avg_hi;
               double current_avg_lo;
               double current_avg_hi_weigh;
               double current_avg_lo_weigh;
               double current_avg_flat;
               double current_avg_weighted;
               
               datetime open_time = iTime(Symbol(), time_frame, ix);
               datetime close_time;
               
               if (ix > 0) 
                    close_time = iTime(Symbol(), time_frame, ix - 1) - max_resolution_secs;
               else 
                    close_time = TimeCurrent();
          
               get_weighted_hilo(
                    open_time, 
                    close_time, 
                    current_avg_hi, 
                    current_avg_lo, 
                    current_avg_hi_weigh, 
                    current_avg_lo_weigh, 
                    current_avg_flat,
                    current_avg_weighted);

          } else if (Rough_Estimates == true) {

               // Fake averages
               current_avg_hi = High[ix];
               current_avg_lo = Low[ix];
               current_avg_hi_weigh = (High[ix] + High[ix] + High[ix + 1]) / 3.0;
               current_avg_lo_weigh = (Low[ix] + Low[ix] + Low[ix + 1]) / 3.0;
               current_avg_weighted = get_price(PRICE_WEIGHTED, Close[ix], avg_hi_weighted[ix], avg_lo_weighted[ix]);
               current_avg_flat = get_price(PRICE_MEDIAN, Close[ix], avg_hi[ix], avg_lo[ix]);
          } else {
               continue;
          }
          
          for (int display_ix = ix * display_time_ratio; 
               display_ix < (ix + 1) * display_time_ratio; 
               display_ix++) {
               avg_hi[display_ix] = current_avg_hi;
               avg_lo[display_ix] = current_avg_lo;
               avg_hi_weighted[display_ix] = current_avg_hi_weigh;
               avg_lo_weighted[display_ix] = current_avg_lo_weigh;
               avg_weighted[display_ix] = current_avg_flat;
               avg_price[display_ix] = current_avg_weighted;
          }

     }

     double spread = MarketInfo(Symbol(), MODE_SPREAD);
     double mid_price = (High[0] + Low[0]) / 2.0;
     double typical_price = (High[0] + Low[0] + Close[0]) / 3.0;
     double weighted_price = (High[0] + Low[0] + Close[0] + Close[0]) / 4.0;

     double minutes_left = ( time_frame * 60.0 - (TimeCurrent() - iTime(Symbol(), time_frame, 0)) ) / 60.0;

     ix = 0;

     Comment( 
          "Symbol: " + Symbol() + "   " +
          "Calculated period: " + time_frame + " min\n" +
          "Spread: " + DoubleToStr(spread, 0) + "  " +
          "Min Stop: " + DoubleToStr(stop_level, 0) + "\n" +
          "Bar in T-" + DoubleToStr(minutes_left, 2) + " min\n" +
          "Time weight: " + DoubleToStr(time_factor, 2) + "  " +
          "Subperiod: " + Maximum_Resolution + " min\n\n" +

          "Midprice HL: " + DoubleToStr(mid_price, Digits) + get_diff_string(Close[0], mid_price) + "\n" +
          "Typical HLC: " + DoubleToStr(typical_price, Digits) + get_diff_string(Close[0], typical_price) + "\n" +
          "Weighted HLCC: " + DoubleToStr(weighted_price, Digits) + get_diff_string(Close[0], weighted_price) + "\n\n" +
           
          "Average Price: " + DoubleToStr(avg_price[ix], Digits) + get_diff_string(Close[0], avg_price[ix]) + "\n" +
          "Average Weighted: " + DoubleToStr(avg_weighted[ix], Digits) + get_diff_string(Close[0], avg_weighted[ix]) + "\n" +
          "Average High: " + DoubleToStr(avg_hi[ix], Digits) + get_diff_string(Close[0], avg_hi[ix]) + "\n" +
          "Average Low: " + DoubleToStr(avg_lo[ix], Digits) + get_diff_string(Close[0], avg_lo[ix]) + "\n\n"
          );
//----
     return(0);
  }
//+------------------------------------------------------------------+