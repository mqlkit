/*
���  ������  ����������  �������  ��������  �����  
JJMASeries.mqh 
JurXSeries.mqh 
PriceSeries.mqh 
� ����� (����������): MetaTrader\experts\include\
Heiken Ashi#.mq4
� ����� (����������): MetaTrader\indicators\
*/
//+------------------------------------------------------------------+ 
//|                                                    3c_JJRSX1.mq4 |
//|                           Copyright � 2006,     Nikolay Kositsin | 
//|                              Khabarovsk,   farria@mail.redcom.ru | 
//+------------------------------------------------------------------+ 
#property copyright "Copyright � 2006, Nikolay Kositsin"
#property link      "farria@mail.redcom.ru" 
//---- ��������� ���������� � ��������� ����
#property indicator_separate_window
//---- ���������� ������������ �������
#property indicator_buffers  3
//---- ����� ����������
#property indicator_color1  BlueViolet
#property indicator_color2  Magenta
#property indicator_color3  Gray
//---- ������� ������������ �����
#property indicator_width1 3
#property indicator_width2 3
#property indicator_width3 3
//---- ��������� �������������� ������� ����������
#property indicator_level1  0.5
#property indicator_level2 -0.5
#property indicator_level3  0.0
#property indicator_levelcolor MediumBlue
#property indicator_levelstyle 4
//---- ������� ��������� ���������� 
extern int        Length = 8;  // ������� ����������� ����������
extern int        Smooth = 3; // ������� ��������������� JMA �����������
extern int  Smooth_Phase = 100; // ��������������� JMA �����������, ������������ � �������� -100 ... +100, 
                                                                 //������ �� �������� ����������� ��������;
extern int Input_Price_Customs = 0;/* ����� ���, �� ������� ������������ ������ ���������� 
(0-CLOSE, 1-OPEN, 2-HIGH, 3-LOW, 4-MEDIAN, 5-TYPICAL, 6-WEIGHTED, 7-Heiken Ashi Close, 
8-SIMPL, 9-TRENDFOLLOW, 10-0.5*TRENDFOLLOW,11-Heiken Ashi Low, 12-Heiken Ashi High,  
13-Heiken Ashi Open, 14-Heiken Ashi Close.) */
//---- 
//---- ������������ ������
double Ind_Buffer1[];
double Ind_Buffer2[];
double Ind_Buffer3[]; 
//+------------------------------------------------------------------+  
//| JJRSX initialization function                                    |
//+------------------------------------------------------------------+ 
int init()
  {
//---- ����� ����������� ����������
   SetIndexStyle(0,DRAW_HISTOGRAM, STYLE_SOLID);
   SetIndexStyle(1,DRAW_HISTOGRAM, STYLE_SOLID);
   SetIndexStyle(2,DRAW_HISTOGRAM, STYLE_SOLID);
//---- 4 ������������ ������ ������������ ��� ����� 
   SetIndexBuffer(0,Ind_Buffer1);
   SetIndexBuffer(1,Ind_Buffer2);
   SetIndexBuffer(2,Ind_Buffer3);
//---- ��������� �������� ����������, ������� �� ����� ������ �� �������
   SetIndexEmptyValue(0,0); 
   SetIndexEmptyValue(1,0);
   SetIndexEmptyValue(2,0);
//---- ����� ��� ���� ������ � ����� ��� ��������
   SetIndexLabel(0,"Up_Trend");
   SetIndexLabel(1,"Down_Trend");
   SetIndexLabel(2,"Straight_Trend");
   IndicatorShortName("JJRSX(Length="+Length+")");
//---- ��������� ������� �������� (���������� ������ ����� ���������� �����) 
//��� ������������ �������� ����������  
   IndicatorDigits(0);
//---- �������� ������������� �������� ��������� Length
  if(Length<1)Length=1; 
//---- ��������� ������ ����, ������� � �������� ����� �������������� ���������  
   int draw_begin=3*Length+30+1; 
   SetIndexDrawBegin(0,draw_begin);
   SetIndexDrawBegin(1,draw_begin);
   SetIndexDrawBegin(2,draw_begin);   
//---- ���������� �������������
return(0);
  }
//+------------------------------------------------------------------+ 
//| JJRSX iteration function                                         |
//+------------------------------------------------------------------+ 
int start()
  {
   //----+ �������� ����� ���������� ������
   static int time2;
   //----+ �������� ���������� ������ � ��������� ������ 
   static double VelueM;
	//----+ �������� ���������� � ��������� ������   
	double Velue0,Velue1,trend; 
	//----+ �������� ����� ���������� � ��������� ��� ����������� �����
	int bar,limit,MaxBar,Tnew,counted_bars=IndicatorCounted();
	//---- �������� �� ��������� ������
	if (counted_bars<0)return(-1);
	//---- ��������� ����������� ��� ������ ���� ���������� 
	if (counted_bars>0) counted_bars--;
	//---- ����������� ������ ������ ������� ����, 
	//������� � �������� ����� �������� �������� ���� �����
	MaxBar=Bars-3*Length-30; 
	//---- ����������� ������ ������ ������� ����, 
	//������� � �������� ����� �������� �������� ����� �����
	limit=Bars-counted_bars-1; 
	//+--- ������������� ����
	if (limit>=MaxBar)
	  for(bar=Bars-1;bar>=MaxBar;bar--)
	   {
	    limit=MaxBar;
	    Ind_Buffer1[bar]=0.0;
	    Ind_Buffer2[bar]=0.0;
	    Ind_Buffer3[bar]=0.0;
	   }
	 //+--- �������������� �������� ���������� +========================+
    Tnew=Time[limit+1];
    if (limit<MaxBar)
    if (Tnew==time2)
     {
      Velue1=VelueM;
     }
    else 
     {
      if (Tnew>time2)
           Print("Error to restor the variables!!! Tnew>time2");
      else Print("Error to restor the variables!!! Tnew<time2");
      Print("Indicator will be re-calculated for all the bars!");
      return(-1);  
     }
    //+--- +===========================================================+
	 
	//----+ �������� ���� ���������� ����������
	while (bar>=0)
 	 {
 	  //+--- ���������� �������� ���������� +====+ 
     if (bar==1)
      {
       if(((limit==1)&&(time2==Time[2]))||(limit>1))
         {
          VelueM=Velue1;
          time2=Time[bar];
         }
      }
     //+---+====================================+     	 
		Velue0=iCustom( NULL,0,"JJRSX",Length,Smooth,Smooth_Phase,Input_Price_Customs,0,bar);
		if (bar==MaxBar)
		  {
		   Velue1=Velue0;
		   continue;
		  }		
		//---- ���������� ��� ���������� 
		trend=Velue0-Velue1;     
		if(trend>0)     {Ind_Buffer1[bar]=Velue0; Ind_Buffer2[bar]=0;      Ind_Buffer3[bar]=0;}
		else{if(trend<0){Ind_Buffer1[bar]=0;      Ind_Buffer2[bar]=Velue0; Ind_Buffer3[bar]=0;}
		else            {Ind_Buffer1[bar]=0;      Ind_Buffer2[bar]=0;      Ind_Buffer3[bar]=Velue0;}}    
		//---- 
		Velue1=Velue0;
		//----+
		bar--;
	 } 
	//---- ���������� ���������� �������� ����������
	return(0);
  }
//+-------------------------------------------------------------------------------+





