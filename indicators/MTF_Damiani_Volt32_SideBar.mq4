//+------------------------------------------------------------------------+
//|          MTF_ DamVolt32Sidebar                Damiani_volatmeter.mq4 ik|
//| fxtsd.com                     Copyright � 2006, Luis Guilherme Damiani |
//|  works with Damiani_volatmeter v3.2        http://www.damianifx.com.br |
//+------------------------------------------------------------------------+
//When the green line is BELOW the gray line - DON'T trade
#property copyright "Copyright � 2006, Luis Guilherme Damiani"
#property link      "http://www.damianifx.com.br"
//---- indicator settings 
#property  indicator_separate_window
#property  indicator_buffers 2
#property  indicator_color1  ForestGreen      
#property  indicator_color2  DarkSlateBlue    
#property  indicator_maximum 77
#property  indicator_minimum 1

extern int TimeFrame=0;
extern int DVsbLevel = 5;
extern int       Vis_atr=13;
extern int       Vis_std=20;
extern int       Sed_atr=40;
extern int       Sed_std=100;
extern double    Threshold_level=1.4;
extern bool      lag_supressor=true;
extern int       max_bars=2000;

//---- indicator buffers
double ExtMapBuffer1[];
double ExtMapBuffer2[];
double threshold[];
double vol_t[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   
//---- drawing settings

   SetIndexStyle(0,DRAW_ARROW);
   SetIndexArrow(0,59);//110;167;159;250;249;59
   SetIndexBuffer(0,ExtMapBuffer1);
    SetIndexLabel(0,"DV_TradeZone TF["+TimeFrame+"]");
  
   SetIndexStyle(1,DRAW_ARROW);
   SetIndexArrow(1,59);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexLabel(1,"DV_DO NOT Trade TF["+TimeFrame+"]");
   IndicatorDigits(0);
  
//---- name for DataWindow and indicator subwindow label
   switch(TimeFrame)
   {
      case 1 : string TimeFrameStr="Period_M1"; break;
      case 5 : TimeFrameStr="Period_M5"; break;
      case 15 : TimeFrameStr="Period_M15"; break;
      case 30 : TimeFrameStr="Period_M30"; break;
      case 60 : TimeFrameStr="Period_H1"; break;
      case 240 : TimeFrameStr="Period_H4"; break;
      case 1440 : TimeFrameStr="Period_D1"; break;
      case 10080 : TimeFrameStr="Period_W1"; break;
      case 43200 : TimeFrameStr="Period_MN1"; break;
      default : TimeFrameStr="Current Timeframe";
   } 
   IndicatorShortName("DV_Sb["+TimeFrameStr+"]");   

//---- initialization done
   return(0);
  }
//+------------------------------------------------------------------+
//| Calculations                                    |
//+------------------------------------------------------------------+
int start()
  {
   datetime TimeArray[];
   int    i,shift,limit,y=0,counted_bars=IndicatorCounted();
    
// Plot defined timeframe on to current timeframe   
   ArrayCopySeries(TimeArray,MODE_TIME,Symbol(),TimeFrame); 
   
   limit=Bars-counted_bars+TimeFrame/Period();
   for(i=0,y=0;i<limit;i++)
   {
   if (Time[i]<TimeArray[y]) y++;  
//---- main loop
   
ExtMapBuffer1[i] = EMPTY_VALUE; ExtMapBuffer2[i] = EMPTY_VALUE;
double threshold = iCustom(NULL, 0,"Damiani_volatmeter v3.2",Vis_atr,Vis_std,Sed_atr,Sed_std,Threshold_level, true, max_bars, 0,y );
double vol_t = iCustom(NULL, 0,"Damiani_volatmeter v3.2",Vis_atr,Vis_std,Sed_atr,Sed_std,Threshold_level, true, max_bars, 2,y );

if (vol_t > threshold )ExtMapBuffer1[i]=DVsbLevel;
if (vol_t < threshold)ExtMapBuffer2[i]=DVsbLevel;

}
       
  // Refresh buffers
//++++++++++++++++++++++++++++++++++++++   upgrade by Raff 
  
  if (TimeFrame < Period()) TimeFrame = Period();
  if (TimeFrame>Period()) {
     int PerINT=TimeFrame/Period()+1;
     datetime TimeArr[]; ArrayResize(TimeArr,PerINT);
     ArrayCopySeries(TimeArr,MODE_TIME,Symbol(),Period()); 
     for(i=0;i<PerINT+1;i++) {if (TimeArr[i]>=TimeArray[0]) {
//----
 /******************************************************** 
    Refresh buffers:         buffer[i] = buffer[0];
 ********************************************************/  

   ExtMapBuffer1[i]=ExtMapBuffer1[0]; 
   ExtMapBuffer2[i]=ExtMapBuffer2[0]; 
//----
   } } }
//+++++++++++++++++++++++++++++++++++++++++++++++   Raff

//-----

return(0);
  }
//+------------------------------------------------------------------+

