#property indicator_separate_window
#property indicator_buffers 4
#property indicator_color1 Red
#property indicator_color2 White
#property indicator_color3 OrangeRed
#property indicator_color4 Silver
#property indicator_width4 2

extern int MAPeriod    = 60;
extern int MAGainLimit = 50;
extern double DeltaCoef = 2.0;
extern int TypeDelta = 0;
extern string Type_Delta_Hint = "0 Delta EC; 1 EC; 2 Slow";
extern string Currency_Pair = "";


double DiffBuffer[];
double DeltaDiffBuffer[];
double DeltaDeltaBuffer[];
double DeltaDiffHist[];
double EMABuffer[];
double ECBuffer[];


double gdAlpha;
string currency;


int init()
{
    IndicatorBuffers(6);
    SetIndexStyle(0, DRAW_HISTOGRAM);
    SetIndexBuffer(0, DiffBuffer);
    SetIndexStyle(1, DRAW_LINE);
    SetIndexBuffer(1, DeltaDiffBuffer);
    SetIndexStyle(2, DRAW_LINE, STYLE_DOT, 1);
    SetIndexBuffer(2, DeltaDeltaBuffer);
    SetIndexStyle(3, DRAW_HISTOGRAM);
    SetIndexBuffer(3, DeltaDiffHist);
    SetIndexBuffer(4, EMABuffer);
    SetIndexBuffer(5, ECBuffer);

    SetIndexLabel(0, "EMA Difference");
    SetIndexLabel(1, "Difference Delta");
    SetIndexLabel(2, "Delta of Delta");
    SetIndexLabel(3, "Delta Difference Histo");

    gdAlpha = 2.0 / (MAPeriod + 1.0);
    if (Currency_Pair == "") currency = Symbol();
    else currency = Currency_Pair;
    
    RefreshRates();
    IndicatorShortName("ZeroLagMAOsc "+currency+" ("+MAPeriod+")");
    
    return(0);
}

int start()
{
    int    i, j, iCounted, iLimit;
    double dGain, dBestGain, dError, dLeastError, dEC;
    
    iCounted = IndicatorCounted();
    if(iCounted > 0) 
      iCounted--;
    iLimit = Bars - iCounted;
    for(i = iLimit-1; i >= 0; i--)
    {
        double currentPrice = ( iHigh(currency, Period(), i) + iLow(currency, Period(), i) + 2.0 * iClose(currency, Period(), i) ) / 4.0;
        EMABuffer[i] = gdAlpha * currentPrice + (1.0 - gdAlpha) * EMABuffer[i+1];
        dLeastError = 1000000.0;
        for(j = -MAGainLimit; j <= MAGainLimit; j++)
        {
            dGain = j / 10.0;
            dEC = gdAlpha * (EMABuffer[i] + dGain*(currentPrice - ECBuffer[i+1])) + (1.0 - gdAlpha) * ECBuffer[i+1];
            dError = currentPrice - dEC;
            if(MathAbs(dError) < dLeastError)
            {
                dLeastError = MathAbs(dError);
                dBestGain = dGain;
            }
        }
        ECBuffer[i] = gdAlpha * (EMABuffer[i] + dBestGain * (currentPrice - ECBuffer[i+1])) + (1.0 - gdAlpha) * ECBuffer[i+1];
        
        DiffBuffer[i] = ECBuffer[i] - EMABuffer[i];
        switch (TypeDelta) {
        case 0: DeltaDiffBuffer[i] = DiffBuffer[i] - DiffBuffer[i+1];
          break;
        case 1: DeltaDiffBuffer[i] = ECBuffer[i] - ECBuffer[i+1];
          break;
        case 2: DeltaDiffBuffer[i] = EMABuffer[i] - EMABuffer[i+1];
          break;
        }
        DeltaDiffBuffer[i] *= DeltaCoef;
        DeltaDeltaBuffer[i] = DeltaCoef * (DeltaDiffBuffer[i] - DeltaDiffBuffer[i+1]);
        
        if ((DeltaDeltaBuffer[i] < 0.0) != (DeltaDiffBuffer[i] < 0.0)) DeltaDiffHist[i] = 0.0;
        else if (DeltaDeltaBuffer[i] < 0.0) DeltaDiffHist[i] = MathMax(DeltaDeltaBuffer[i], DeltaDiffBuffer[i]);
        else if (DeltaDeltaBuffer[i] > 0.0) DeltaDiffHist[i] = MathMin(DeltaDeltaBuffer[i], DeltaDiffBuffer[i]);
        else DeltaDiffHist[i] = 0.0;
    }
    
    return(0);
}

