//Notes for the simple Kalman filter.
//
//How I use this:
// the indicators are currently set up for a 1 hour type chart, to produce
// the trend, 1 deviation on either side, 2 deviations on either side, and 2 deviations 
// on either side calculated off the highs and lows.
//
//Input variables:
// Samples sets the 
// die off of the exponentially averaged deviations. Bigger numbers give
// smoother edges. 120 (5 days on the hourlies) works pretty well, but feel free to change it. It
// can be set to a non-integer number, but use numbers greater than 2.
//
// DevLevel (1 & 2) - deviation levels - By setting
// the deviation levels to zero, they don't get plotted. Otherwise they m
//
// Suppression_dB sets the level that noise will be suppressed. 0dB is usually appropriate
// 20dB gives the equivalent of long term averages, -20 gives a fast line. The only pairs 
// I don't use this setting at 0 on are JPY crosses, in which I usuallly increase the suppression by 6
// dB or so.
//
// PredictBars - gives predictions for the center of trend in bars steps. For hourlies,
// I use 24 and 48. The information is printed on the "Experts" log. This gives
// an expected center and (IIRC) an 95% expected confidence interval. This will be fine tuned later. 
//
// PrintCurrentState - if true, print the trend value and the speed of the trend (in pips/bar).
//
// Comments and questions - brobeck@ns.sympatico.ca . I really appreciate feed back. Thanks!
// CB
// 

+-----------------------------------------------------+