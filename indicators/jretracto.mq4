//+------------------------------------------------------------------+
//|                                           Retracto.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                              http://jaltoh.6x.to |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      "http://jaltoh.6x.to"


#property indicator_separate_window
#property indicator_buffers 5
#property indicator_color1 Green
#property indicator_color2 DarkGray
#property indicator_color3 White
#property indicator_color4 Blue
#property indicator_color5 Red

#define AVERAGE_RETRACEMENT_PIPS        0
#define AVERAGE_RETRACEMENT_PERCENT     1
#define MOVEMENT_COUNT                  2
#define MOVEMENT_COUNT_RATIO            3
#define STRONGEST_PUSH_PIPS             4
#define STRONGEST_PUSH_PERCENT          5

extern double Delta_Coefficient = 0.5;

extern bool Log_Prices_Spread = false;
extern bool Display_Price_Persistence = false;
extern bool Opposite_Retracement_Only = true;
extern double Minimum_Movement_PIPs = 5.1;
extern double Recentness_Factor = 0.9;

extern int Mode_of_Function = STRONGEST_PUSH_PERCENT;

extern string Mode_of_Function_0 = "Average Retracement Pips";
extern string Mode_of_Function_1 = "Average Retracement Ratio";
extern string Mode_of_Function_2 = "Movement Count";
extern string Mode_of_Function_3 = "Movement Count Ratio";
extern string Mode_of_Function_4 = "Price Push in Pips";
extern string Mode_of_Function_5 = "Price Push Ratio";

extern int Used_Price = PRICE_CLOSE;

extern int Use_Close_Price = PRICE_CLOSE;
extern int Use_Median_Price = PRICE_MEDIAN;
extern int Use_Typical_Price = PRICE_TYPICAL;
extern int Use_Weighted_Price = PRICE_WEIGHTED;


//---- buffers
double Retracement_Range_Buffer[];
double Range_Delta_Buffer[];
double Spread_Monitor_Buffer[];
double Bulls_Buffer[];
double Bears_Buffer[];


//--- globals
#define NO_DIRECTION 0
#define DIRECTION_UP 1
#define DIRECTION_DOWN 2

#define NO_PRICE -1.0



int Last_Direction = NO_DIRECTION;
double Last_Direction_Change_Price = NO_PRICE;
double Last_Tick_Price = NO_PRICE;
double Average_Range;
int Ranges_Added;
double Average_Range_2;
int Ranges_Added_2;

datetime Last_Tick_Time;

double Tick_Overall_Movement;
double Max_Tick_Bears;
double Max_Tick_Bulls;
double Minimum_Bullish_Movement;
double Minimum_Bearish_Movement;
int Bullish_Movements;
int Bearish_Movements;

int subperiods_ct;

// Copies
double _Tick_Overall_Movement;
double _Max_Tick_Bears;
double _Max_Tick_Bulls;
double _Minimum_Bullish_Movement;
double _Minimum_Bearish_Movement;
int _Bullish_Movements;
int _Bearish_Movements;
int _Last_Direction;
double _Last_Direction_Change_Price;
double _Last_Tick_Price;
double _Average_Range;
int _Ranges_Added;
double _Average_Range_2;
int _Ranges_Added_2;



// Used for price accounting
#define PRICE_VALUE           0
#define TOTAL_TIME_ACTIVE     1
#define LAST_LEFT_TIME        2
#define LAST_VISIT_TIME       3
#define NUM_PROPERTIES        4

int Tick_Price_Time[][NUM_PROPERTIES];
double spread = 0.0;
double stop_level = 0.0;


void
reset_counters()
{
     spread = MarketInfo(Symbol(), MODE_SPREAD);
     stop_level = MarketInfo(Symbol(), MODE_STOPLEVEL);

     RefreshRates();

     Bullish_Movements = 0;
     Bearish_Movements = 0;
     Average_Range = NO_PRICE;
     Ranges_Added = 0;
     Average_Range_2 = NO_PRICE;
     Ranges_Added_2 = 0;

     Last_Direction = NO_DIRECTION;
     Last_Direction_Change_Price = NO_PRICE;

     Tick_Overall_Movement = 0.0;
     Max_Tick_Bulls = 0.0;
     Max_Tick_Bears = 0.0;

     Bullish_Movements = 0;
     Bearish_Movements = 0;
     
     ArrayResize(Tick_Price_Time, 0);
}

void
save_counters()
{
     _Bullish_Movements = Bullish_Movements;
     _Bearish_Movements = Bearish_Movements;
     _Average_Range = Average_Range;
     _Ranges_Added = Ranges_Added;
     _Average_Range_2 = Average_Range_2;
     _Ranges_Added_2 = Ranges_Added_2;

     _Last_Direction = Last_Direction;
     _Last_Direction_Change_Price = Last_Direction_Change_Price;

     _Tick_Overall_Movement = Tick_Overall_Movement;
     _Max_Tick_Bulls = Max_Tick_Bulls;
     _Max_Tick_Bears = Max_Tick_Bears;

     _Bullish_Movements = Bullish_Movements;
     _Bearish_Movements = Bearish_Movements;
}

void
restore_counters()
{
     Bullish_Movements = _Bullish_Movements;
     Bearish_Movements = _Bearish_Movements;
     Average_Range = _Average_Range;
     Ranges_Added = _Ranges_Added;
     Average_Range_2 = _Average_Range_2;
     Ranges_Added_2 = _Ranges_Added_2;

     Last_Direction = _Last_Direction;
     Last_Direction_Change_Price = _Last_Direction_Change_Price;

     Tick_Overall_Movement = _Tick_Overall_Movement;
     Max_Tick_Bulls = _Max_Tick_Bulls;
     Max_Tick_Bears = _Max_Tick_Bears;

     Bullish_Movements = _Bullish_Movements;
     Bearish_Movements = _Bearish_Movements;
}

int
get_accounted_price_index(double requested_price)
{
     if (requested_price < 0.0) return (-1);
     
     int requested_price_int = requested_price / Point;
     int price_ix = 0;
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     
     while (price_ix < price_ct) {
          if (Tick_Price_Time[price_ix][PRICE_VALUE] == requested_price_int) break;
          price_ix++;
     }

     if (price_ix < price_ct) return (price_ix);
     else return (-1);
}

int
add_accounted_price(
     double price_value, 
     datetime total_time, 
     datetime last_left, 
     datetime last_visit)
{
     int price_value_int = price_value / Point;
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     int price_ix = price_ct;

     price_ct++;
     price_ct = ArrayResize(Tick_Price_Time, price_ct);
     
     if (price_ct < 1 || price_ix < 0) return(-1);

     Tick_Price_Time[price_ix][PRICE_VALUE] = price_value_int;     
     Tick_Price_Time[price_ix][TOTAL_TIME_ACTIVE] = total_time;     
     Tick_Price_Time[price_ix][LAST_LEFT_TIME] = last_left;     
     Tick_Price_Time[price_ix][LAST_VISIT_TIME] = last_visit;
     
     return(price_ix);
}

datetime
retime_accounted_price(int price_ix, datetime last_left)
{
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     if (price_ix < 0 || price_ix >= price_ct) return (-1);

     datetime last_visit = Tick_Price_Time[price_ix][LAST_VISIT_TIME];
     if (last_visit > last_left) return (-1);
     
     datetime total_time = last_left - last_visit;
     
     Tick_Price_Time[price_ix][TOTAL_TIME_ACTIVE] += total_time;
     Tick_Price_Time[price_ix][LAST_LEFT_TIME] = last_left;
     
     return (total_time);
}

datetime
visit_accounted_price(int price_ix, datetime last_visit)
{
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     if (price_ix < 0 || price_ix >= price_ct) return (-1);

     Tick_Price_Time[price_ix][LAST_VISIT_TIME] = last_visit;
     Tick_Price_Time[price_ix][LAST_LEFT_TIME] = 0;
     
     return (Tick_Price_Time[price_ix][TOTAL_TIME_ACTIVE]);
}

void
update_price_stats(double current_price, double previous_price)
{
     int price_ix = 0;
     int price_ct = ArrayRange(Tick_Price_Time, 0);
     datetime current_time = TimeCurrent();

// Update price just left
     price_ix = get_accounted_price_index(previous_price);
     if (price_ix < 0) add_accounted_price(previous_price, 0, current_time, current_time);
     else retime_accounted_price(price_ix, current_time);

// Update newly visited price     
     price_ix = get_accounted_price_index(current_price);
     if (price_ix < 0) add_accounted_price(current_price, 0, 0, current_time);
     else visit_accounted_price(price_ix, current_time);
}

double
get_current_change_range(double current_price)
{
// Detect movement
     int direction;
     double price_difference;
    
     if (Last_Direction_Change_Price == NO_PRICE) 
          Last_Direction_Change_Price = current_price;
    
     price_difference = current_price - Last_Direction_Change_Price;
    
     if (price_difference > Minimum_Bullish_Movement) {
          direction = DIRECTION_UP;
          Bullish_Movements++;
     } else if (price_difference < Minimum_Bearish_Movement) {
          direction = DIRECTION_DOWN;
          Bearish_Movements++;
     } else {
          direction = NO_DIRECTION;
     }
     // update_price_stats(Current_Price, Last_Price);

     // no retracement here nothing to see        
     if (direction == NO_DIRECTION || direction == Last_Direction) 
          return (NO_PRICE);
          
     double current_range = (current_price - Last_Direction_Change_Price) / Point;
     Last_Direction = direction;
     Last_Direction_Change_Price = current_price;
          
     return (current_range);
}

void
update_retracement_pct(double current_range, int period_index)
{
  // Find strongest retracement
  Tick_Overall_Movement = Recentness_Factor * Tick_Overall_Movement + current_range;
  if (Tick_Overall_Movement > Max_Tick_Bulls) 
     Max_Tick_Bulls = Tick_Overall_Movement;
  else if (Tick_Overall_Movement < Max_Tick_Bears) 
     Max_Tick_Bears = Tick_Overall_Movement;
     
  double Max_Push_Difference = Max_Tick_Bulls + Max_Tick_Bears;
     
  // Calculate bull bear ratio
  if (Max_Push_Difference < 0.0) {
     Bears_Buffer[period_index] = -100.0 * Max_Push_Difference / Max_Tick_Bears;
     Bulls_Buffer[period_index] = 0.0;
     Range_Delta_Buffer[period_index] = Bears_Buffer[period_index];
  } else if (Max_Push_Difference > 0.0) {
     Bulls_Buffer[period_index] = 100.0 * Max_Push_Difference / Max_Tick_Bulls;
     Bears_Buffer[period_index] = 0.0;
     Range_Delta_Buffer[period_index] = Bulls_Buffer[period_index];
  }
  
}

void
update_retracement_pips(double current_range, int period_index)
{
   // Find strongest retracement
   Tick_Overall_Movement = Recentness_Factor * Tick_Overall_Movement + current_range;
   if (Tick_Overall_Movement > Max_Tick_Bulls) Max_Tick_Bulls = Tick_Overall_Movement;
   else if (Tick_Overall_Movement < Max_Tick_Bears) Max_Tick_Bears = Tick_Overall_Movement;
     
   double Max_Push_Difference = Max_Tick_Bulls + Max_Tick_Bears;
     
   // Calculate bull bear push ratio
   Bears_Buffer[period_index] = Max_Tick_Bears;
   Bulls_Buffer[period_index] = Max_Tick_Bulls;
}

#define MAX_HISTORY 256

double Bulls_Buffer_History[MAX_HISTORY];
double Bears_Buffer_History[MAX_HISTORY];

void
update_movement_count(int period_index)
{
      Bulls_Buffer[period_index] = Bullish_Movements;
      Bears_Buffer[period_index] = -1.0 * Bearish_Movements;
}

void
update_movement_ratio(int period_index)
{
  double retracement_ratio;
  
  if (Bullish_Movements > Bearish_Movements) {

     retracement_ratio = 1.0 - (1.0 * Bearish_Movements) / Bullish_Movements;
     retracement_ratio *= 100.0;

     Bulls_Buffer[period_index] = retracement_ratio;
     Bears_Buffer[period_index] = 0.0;
     Range_Delta_Buffer[period_index] = retracement_ratio;

  } else if (Bullish_Movements < Bearish_Movements) {
  
     retracement_ratio = 1.0 - (1.0 * Bullish_Movements) / Bearish_Movements;
     retracement_ratio *= -100.0;

     Bears_Buffer[period_index] = retracement_ratio;
     Bulls_Buffer[period_index] = 0.0;
     Range_Delta_Buffer[period_index] = retracement_ratio;

  } else {

     Bears_Buffer[period_index] = 0.0;
     Bulls_Buffer[period_index] = 0.0;
     Range_Delta_Buffer[period_index] = 0.0;

  }
  
}

double
get_period_average_range(double current_range)
{
     // Calculate weighted average movement range
     if (Average_Range == NO_PRICE || Ranges_Added == 0)
          Average_Range = current_range;
     else
          Average_Range = (Average_Range * Ranges_Added + current_range) / (Ranges_Added + 1);

     Ranges_Added++;

     return (Average_Range);
}

double
get_period_average_range_2(double current_range)
{
     // Calculate weighted average movement range
     if (Average_Range_2 == NO_PRICE || Ranges_Added_2 == 0)
          Average_Range_2 = current_range;
     else
          Average_Range_2 = (Average_Range_2 * Ranges_Added_2 + current_range) / (Ranges_Added_2 + 1);

     Ranges_Added_2++;

     return (Average_Range_2);
}

bool
get_candle_bullish(int period_index)
{
     return (Open[period_index] < Close[period_index]);
}

void
update_average_range_pips(double current_range, int period_index)
{
     // Calculate average range
     if (Opposite_Retracement_Only == true && current_range > 0.0)
         Average_Range = get_period_average_range(current_range);         
     else if (Opposite_Retracement_Only == true && current_range < 0.0)
         Average_Range_2 = get_period_average_range_2(MathAbs(current_range));
     else if (Opposite_Retracement_Only == false)
         Average_Range = get_period_average_range(MathAbs(current_range));

     // Update graph
     if (Opposite_Retracement_Only == true && get_candle_bullish(period_index) == true) {
          Retracement_Range_Buffer[period_index] = Average_Range_2;
          Range_Delta_Buffer[period_index] = Average_Range_2;
     } else if (Opposite_Retracement_Only == true && get_candle_bullish(period_index) == false) {
          Retracement_Range_Buffer[period_index] = Average_Range;
          Range_Delta_Buffer[period_index] = Average_Range;
     } else if (Opposite_Retracement_Only == false) {
          Retracement_Range_Buffer[period_index] = Average_Range;
          Range_Delta_Buffer[period_index] = Average_Range;
     }
}

void
update_average_range_ratio(double current_range, int period_index)
{
     double candle_length = (High[0] - Low[0]) / Point;
     if (candle_length <= 0.0) return;

     update_average_range_pips(current_range, period_index);

     Retracement_Range_Buffer[period_index] = 100.0 * Retracement_Range_Buffer[period_index] / candle_length;
     Range_Delta_Buffer[period_index] = Retracement_Range_Buffer[period_index];
}

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----

   IndicatorShortName("JRetracto"); 
   
   if (Log_Prices_Spread == true) {
          SetIndexStyle(2, DRAW_HISTOGRAM); 
          SetIndexLabel(2, "Spread History"); 
          SetIndexBuffer(2, Spread_Monitor_Buffer); 
   } else {
          SetIndexStyle(2, DRAW_NONE); 
          SetIndexLabel(2, ""); 
          SetIndexBuffer(2, NULL); 
   }

   switch (Mode_of_Function) {
     case STRONGEST_PUSH_PERCENT:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexStyle(1, DRAW_LINE); 

          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexLabel(1, "Push Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, "Bullish Pressure in Percents"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 

          SetIndexLabel(4, "Bearish Pressure in Percents"); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelStyle(STYLE_DASHDOT, 2, DodgerBlue); 
          SetLevelValue(1, 50.0);
          SetLevelValue(2, -50.0);
          break;

     case STRONGEST_PUSH_PIPS:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexStyle(1, DRAW_LINE); 

          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexLabel(1, "Push Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, "Bullish Absolute Movement in Pips"); 
          SetIndexLabel(4, "Bearish Absolute Movement in Pips"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(1, 0.0);
          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(2, 0.0);
          break;
          
     case MOVEMENT_COUNT:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexStyle(1, DRAW_NONE); 
          SetIndexLabel(1, ""); 
          SetIndexBuffer(1, NULL); 

          SetIndexLabel(3, "Bullish Retracements Count"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 

          SetIndexLabel(4, "Bearish Retracements Count"); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(1, 0.0);
          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(2, 0.0);
          break;

     case MOVEMENT_COUNT_RATIO:
          SetIndexStyle(0, DRAW_NONE); 
          SetIndexLabel(0, ""); 
          SetIndexBuffer(0, NULL); 

          SetIndexStyle(1, DRAW_LINE); 
          SetIndexLabel(1, "Retracements Ratio Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, "Bullish Retracements Ratio"); 
          SetIndexStyle(3, DRAW_HISTOGRAM); 
          SetIndexBuffer(3, Bulls_Buffer); 

          SetIndexLabel(4, "Bearish Retracements Ratio"); 
          SetIndexStyle(4, DRAW_HISTOGRAM); 
          SetIndexBuffer(4, Bears_Buffer); 

          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelStyle(STYLE_DASHDOT, 2, DodgerBlue); 
          SetLevelValue(1, 50.0);
          SetLevelValue(2, -50.0);
          break;
          
     case AVERAGE_RETRACEMENT_PIPS:
          SetIndexStyle(0, DRAW_HISTOGRAM); 
          SetIndexLabel(0, "Average Retracement Range in Pips"); 
          SetIndexBuffer(0, Retracement_Range_Buffer); 

          SetIndexStyle(1, DRAW_LINE); 
          SetIndexLabel(1, "Range Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, ""); 
          SetIndexStyle(3, DRAW_NONE); 
          SetIndexBuffer(3, NULL); 

          SetIndexLabel(4, ""); 
          SetIndexStyle(4, DRAW_NONE); 
          SetIndexBuffer(4, NULL); 

          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(1, 0.0);
          SetLevelStyle(STYLE_SOLID, 0, CLR_NONE);
          SetLevelValue(2, 0.0);
          break;
          
     case AVERAGE_RETRACEMENT_PERCENT:
          SetIndexStyle(0, DRAW_HISTOGRAM); 
          SetIndexLabel(0, "Average Retracement Range in Percents"); 
          SetIndexBuffer(0, Retracement_Range_Buffer); 

          SetIndexStyle(1, DRAW_LINE); 
          SetIndexLabel(1, "Range Delta"); 
          SetIndexBuffer(1, Range_Delta_Buffer); 

          SetIndexLabel(3, ""); 
          SetIndexStyle(3, DRAW_NONE); 
          SetIndexBuffer(3, NULL); 

          SetIndexLabel(4, ""); 
          SetIndexStyle(4, DRAW_NONE); 
          SetIndexBuffer(4, NULL); 

          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelStyle(STYLE_DASHDOT, 1, DodgerBlue); 
          SetLevelValue(1, 50.0);
          SetLevelValue(2, -50.0);
          break;
   }
   
   Minimum_Bullish_Movement = Minimum_Movement_PIPs * Point;
   Minimum_Bearish_Movement = -1.0 * Minimum_Movement_PIPs * Point;
   
   subperiods_ct = Period() * PERIOD_M1;
   
//----

   reset_counters();

   return(0);
  }

double
get_current_price(int price_type)
{
     switch (price_type) {
          case PRICE_CLOSE:
               return (Close[0]);

          case PRICE_MEDIAN:
               return ((Low[0] + High[0]) / 2.0);

          case PRICE_TYPICAL:
               return ((Low[0] + High[0] + Close[0]) / 3.0);

          case PRICE_WEIGHTED:
               return ((Low[0] + High[0] + Close[0] + Close[0]) / 4.0);
          
          default:
               return (0.0);
     }
}

void
do_your_thang(double range, int period_index)
{
  switch (Mode_of_Function) {
    case STRONGEST_PUSH_PERCENT:
      if (range == NO_PRICE) return (0);
      else update_retracement_pct(range, period_index);
      break;

    case STRONGEST_PUSH_PIPS:
      if (range == NO_PRICE) return (0);
      else update_retracement_pips(range, period_index);
      break;
      
    case MOVEMENT_COUNT:
      update_movement_count(period_index);
      break;

    case MOVEMENT_COUNT_RATIO:
      update_movement_ratio(period_index);
      break;
      
    case AVERAGE_RETRACEMENT_PIPS:
      if (range == NO_PRICE) return (0);
      else update_average_range_pips(range, period_index);
      break;

    case AVERAGE_RETRACEMENT_PERCENT:
      if (range == NO_PRICE) return (0);
      else update_average_range_ratio(range, period_index);
      break;
  }
}

 
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }

int start()
  {
  int limit = Bars - IndicatorCounted() - 1;
  if (limit < 0) return (0);
//----
  
  save_counters();
  
  for (int ix = limit; ix > 0; ix--) {
     if (ix > 5) continue;
     
     reset_counters();

     for (int subix = subperiods_ct * (ix + 1); subix >= ix * subperiods_ct; subix--) {
          double range;
          range = get_current_change_range(Open[subix]);
          do_your_thang(range, ix);
          range = get_current_change_range(Low[subix]);
          do_your_thang(range, ix);
          range = get_current_change_range(High[subix]);
          do_your_thang(range, ix);
          range = get_current_change_range(Close[subix]);
          do_your_thang(range, ix);
     }
     
  }

  ////   Now do current tick   ////
  
  // Chech if price moved
  double current_price = get_current_price(Used_Price);
  if (current_price == Last_Tick_Price) return (0);
  Last_Tick_Price = current_price;
  
  // New period
  if (Time[0] != Last_Tick_Time) reset_counters();
  else restore_counters();  
  Last_Tick_Time = Time[0];

  // Show price spread graph
  if (Log_Prices_Spread == true) Spread_Monitor_Buffer[0] = spread;


  // Check for movement in range
  double current_range = get_current_change_range(current_price);
  do_your_thang(current_range, 0);
  
//----
   return(0);
  }
//+------------------------------------------------------------------+

