//+------------------------------------------------------------------+
//|                                                 ZeroLag Tema.mq4 |
//|                                                           mladen |
//|                                                                  |
//| original ZeroLag Heiken-ashi Tema and                            |
//|          Zero-lag Typical price Tema for metatrader              |
//|          developed by Kalenzo                                    |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "www.forex-tsd.com"
#property link      "www.forex-tsd.com"

#property indicator_chart_window
#property indicator_buffers 2
#property indicator_color1 SteelBlue
#property indicator_color2 Magenta
#property indicator_width1 2

//
//
//
//
//

#define PRICE_HA 7
   extern string _        = "Parameters";
   extern int    iPerioda = 144;
   extern int    iPricea  = PRICE_TYPICAL;
   extern int    iPeriodb =  21;
   extern int    iPriceb  = PRICE_HA;

//
//
//
//
//

double buffer1[];
double buffer2[];
double buffer3[];
double buffer4[];
double buffer5[];
double buffer6[];
double buffer7[];
double buffer8[];

string IndicatorFileName; 
bool   calcTEMA;


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//
//

int init()
{
   calcTEMA = ( _ == "calculateTEMA" );

   IndicatorBuffers(8);
      SetIndexBuffer(0,buffer1);
      SetIndexBuffer(1,buffer2);
      SetIndexBuffer(2,buffer3);
      SetIndexBuffer(3,buffer4);
      SetIndexBuffer(4,buffer5);
      SetIndexBuffer(5,buffer6);
      SetIndexBuffer(6,buffer7);
      SetIndexBuffer(7,buffer8);
   if (calcTEMA) return(0);

   //
   //
   //
   //
   //
            
   IndicatorShortName("ZeroLag TEMA ("+iPerioda+","+iPeriodb+")");
      SetIndexLabel(0,"ZL TEMA "+iPerioda);
      SetIndexLabel(1,"ZL TEMA "+iPeriodb);
   IndicatorFileName = WindowExpertName();   
   return(0);
}
int deinit()
{
   return(0);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//
//

int start()
{
   double alphaa = 2.0 / (1.0 + iPerioda);
   double alphab = 2.0 / (1.0 + iPeriodb);
   int    counted_bars = IndicatorCounted();
   int    i,limit;
   
   if(counted_bars < 0) return(-1);
   if(counted_bars > 0) counted_bars--;
           limit = Bars-counted_bars;
   
   //
   //
   //
   //
   //
   
      if (calcTEMA) { calculateTEMA(limit); return(0);}
      
   //
   //
   //
   //
   //

   for (i=limit; i>=0; i--)
   {  
      if (iPerioda > 1)
         {
            double tema1 = iCustom(NULL,0,IndicatorFileName,"calculateTEMA",iPerioda,iPricea,0,i);
               buffer8[i] = buffer8[i+1]+alphaa*(tema1     -buffer8[i+1]);
               buffer7[i] = buffer7[i+1]+alphaa*(buffer8[i]-buffer7[i+1]);
               buffer6[i] = buffer6[i+1]+alphaa*(buffer7[i]-buffer6[i+1]);
               buffer1[i] = tema1+(tema1-(3*buffer8[i]-3*buffer7[i]+buffer6[i])); 
         }               
         
      //
      //
      //
      //
      //
               
      if (iPeriodb > 1)
         {
            double tema2 = iCustom(NULL,0,IndicatorFileName,"calculateTEMA",iPeriodb,iPriceb,0,i);
               buffer5[i] = buffer5[i+1]+alphab*(tema2     -buffer5[i+1]);
               buffer4[i] = buffer4[i+1]+alphab*(buffer5[i]-buffer4[i+1]);
               buffer3[i] = buffer3[i+1]+alphab*(buffer4[i]-buffer3[i+1]);
               buffer2[i] = tema2+(tema2-(3*buffer5[i]-3*buffer4[i]+buffer3[i])); 
         }               
   }         
   return(0);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//
//

void calculateTEMA(int limit)
{
   double alpha = 2.0 / (1.0 + iPerioda);
   double price;

   //
   //
   //
   //
   //
      
   for (int i=limit; i>= 0; i--)
   {
      if (iPricea==PRICE_HA)
      {   
         double haOpen  = (buffer7[i+1] + buffer8[i+1]) / 2;
         double haClose = (Open[i] + High[i] + Low[i] + Close[i]) / 4;
         double haHigh  = MathMax(High[i], MathMax(haOpen,haClose));
         double haLow   = MathMin(Low[i] , MathMin(haOpen,haClose));
      
         if(haOpen  <haClose) { buffer5[i] = haLow;  buffer6[i] = haHigh; } 
         else                 { buffer5[i] = haHigh; buffer6[i] = haLow;  } 
                                buffer7[i] = haOpen;
                                buffer8[i] = haClose;
          price = (buffer5[i] + buffer6[i] + buffer7[i] + buffer8[i])/4;
      }

      //
      //
      //
      //
      //
      
      else price = iMA(NULL,0,iPerioda,0,MODE_EMA,iPricea,i);
               
      //
      //
      //
      //
      //
              
      buffer4[i] = buffer4[i+1]+alpha*(price     -buffer4[i+1]);
      buffer3[i] = buffer3[i+1]+alpha*(buffer4[i]-buffer3[i+1]);
      buffer2[i] = buffer2[i+1]+alpha*(buffer3[i]-buffer2[i+1]);
      buffer1[i] = 3*buffer4[i]-3*buffer3[i]+buffer2[i];
   }
}