//+------------------------------------------------------------------+
//|                                              SpreadGenerator.mq4 |
//|                                           Copyright ｩ 2009, fai. |
//|                                    http://www.himitsukessya.com/ |
//+------------------------------------------------------------------+
// How to Use..
//
// 1. Install "SpreadGenerator.mq4" in \experts\scripts folder.
// 2. Attach "SpreadGenerator" on Target CurrencyPair Chart.
// 3. Input your favorite spread value.
//    ( -10 ? OK....No problem.. )
// 4. This script makes "symbols.sel" in \experts\files folder.
// 5. Try Online-Backtesting with Normal Current Spread 
//    and downloading Historical data. 
//
// 6. Take several long breaths to calm yourself down.
//
//
// 7. Disconnect from the Internet and Shutdown MetaTrader.
// 8. Copy your "symbols.sel" and Overwrite original "symbols.sel" 
//    in \history\YOUR DEMO SERVER Accounts folder.
// 9. Restart MetaTrader.( DO NOT CONNECT INTERNET !!! )
//10. Try Offline-Backtesting with YourOwnSpread.
//11. Say " It's crazy!! "
//
// MetaTrader always overwites  "symbols.sel" when connecting Server.
// Don't worry about your sweet-"symbols.sel".
//
// This script is NOT perfect.
// Because there is no data about the file-format of "symbols.sel".
//
// If you find out "unknown" parameter in this script,Please tell me in forum.

// 誰か　この英語を添削してください。＞＜


#property copyright "coded by fai.."
#property link      "http://www.himitsukessya.com/"
#property show_inputs


extern int spread = 5;

void init()
  {
  
   int    version=400;
   string c_symbol=Symbol();
   int    i_digits=Digits;
   int    i_period=Period();
         
   int ExtHandle;
   ExtHandle=FileOpen("symbols.sel",FILE_BIN|FILE_WRITE);
   if(ExtHandle<1)
   {
      Alert("Can't write symbols.sel !!!");
      return(false);
   }
   FileSeek(ExtHandle, 0, SEEK_SET);
   FileWriteInteger(ExtHandle, version, LONG_VALUE);
   FileWriteString (ExtHandle, c_symbol, 12);
   FileWriteInteger(ExtHandle, i_digits, LONG_VALUE);
   FileWriteInteger(ExtHandle, 27, LONG_VALUE);// unknown (maybe wrong value..)
   FileWriteInteger(ExtHandle, 1, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteDouble (ExtHandle, Point,DOUBLE_VALUE);
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 1, LONG_VALUE);// UP=0,DOWN=1,Gray=2
   FileWriteInteger(ExtHandle, 256, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, TimeCurrent(), LONG_VALUE);      
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown

   FileWriteDouble(ExtHandle, Bid,DOUBLE_VALUE);
   FileWriteDouble(ExtHandle, Bid + spread*Point,DOUBLE_VALUE);      
   FileWriteDouble(ExtHandle, High[0],DOUBLE_VALUE);   
   FileWriteDouble(ExtHandle, Low[0],DOUBLE_VALUE);  
   FileWriteDouble(ExtHandle, 0,DOUBLE_VALUE);// unknown
   FileWriteDouble(ExtHandle, 0,DOUBLE_VALUE); // unknown  
   FileWriteDouble(ExtHandle, Bid,DOUBLE_VALUE);
   FileWriteDouble(ExtHandle, Bid + spread*Point,DOUBLE_VALUE);
   if(StringFind(Symbol(), "USD", 0) == -1){
      Alert("Cross Pair...");
      AddSymbol(ExtHandle,GetUSDPairName(StringSubstr(Symbol(), 0, 3)));
      AddSymbol(ExtHandle,GetUSDPairName(StringSubstr(Symbol(), 3, 3)));  
   }  
   FileClose(ExtHandle);
   Alert(Symbol()+" [spread="+ spread+ "]  experts\\files\\symbols.sel OK");
   
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string GetUSDPairName(string target){
   string postfix = "";
   if(StringLen(Symbol())>6){postfix = StringSubstr(Symbol(),6,StringLen(Symbol())-1);}
   if(target == "EUR") return("EURUSD"+postfix);
   if(target == "GBP") return("GBPUSD"+postfix);
   if(target == "CAD") return("USDCAD"+postfix);
   if(target == "CHF") return("USDCHF"+postfix);
   if(target == "JPY") return("USDJPY"+postfix);
   if(target == "AUD") return("AUDUSD"+postfix);
   if(target == "SEK") return("USDSEK"+postfix);
   if(target == "NZD") return("NZDUSD"+postfix); 
   if(target == "ZAR") return("USDZAR"+postfix); 
   if(target == "NOK") return("USDNOK"+postfix); 
   if(target == "SGD") return("USDSGD"+postfix); 
   if(target == "DKK") return("USDDKK"+postfix); 
   if(target == "MXN") return("USDMXN"+postfix); 
   if(target == "HKD") return("USDHKD"+postfix);
   Alert("ERROR!! Can\'t find USDPairName...");
}
//-----------------------------------------------------------+
void AddSymbol(int ExtHandle,string currency)
{
   FileWriteString (ExtHandle, currency, 12);
   FileWriteInteger(ExtHandle, MarketInfo(currency,MODE_DIGITS), LONG_VALUE);
   FileWriteInteger(ExtHandle, 27, LONG_VALUE);// unknown (maybe wrong value..)
   FileWriteInteger(ExtHandle, 1, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteDouble (ExtHandle, MarketInfo(currency,MODE_POINT),DOUBLE_VALUE);
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, 1, LONG_VALUE);// UP=0,DOWN=1,Gray=2
   FileWriteInteger(ExtHandle, 256, LONG_VALUE);// unknown
   FileWriteInteger(ExtHandle, TimeCurrent(), LONG_VALUE);      
   FileWriteInteger(ExtHandle, 0, LONG_VALUE);// unknown

   FileWriteDouble(ExtHandle, 1.000,DOUBLE_VALUE);
   FileWriteDouble(ExtHandle, 1.000,DOUBLE_VALUE);      
   FileWriteDouble(ExtHandle, 1.000,DOUBLE_VALUE);   
   FileWriteDouble(ExtHandle, 1.000,DOUBLE_VALUE);  
   FileWriteDouble(ExtHandle, 0,DOUBLE_VALUE);// unknown
   FileWriteDouble(ExtHandle, 0,DOUBLE_VALUE); // unknown  
   FileWriteDouble(ExtHandle, 1.000,DOUBLE_VALUE);
   FileWriteDouble(ExtHandle, 1.000,DOUBLE_VALUE);     
   return;
}
int start(){}



