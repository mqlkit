//+------------------------------------------------------------------+
//|                                   Meta COT Script Agregation.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+

//                  .
//           S&P 500  E-Mini S&P 500,     
//       .
// ( -            )
//#property show_inputs
#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

#define EQU_NO           0
#define EQU_YES          1

#define DATA            1
#define OI              2
#define NONCOMM_LONG    3
#define NONCOMM_SHORT   4
#define SPREADING       5
#define OPERATORS_LONG  6
#define OPERATORS_SHORT 7
#define ALL_LONG        8
#define ALL_SHORT       9
#define NONREP_LONG     10
#define NONREP_SHORT    11

extern string s_list="AGREGATION.ini";

int start()
{
   int      h_list;                    //  - (    -)
   //string   s_list;                    //  -.
   
   int      h_enumeration;             //   
   string   s_enumeration;             //    
   
   int      h_rezult;                  //  -
   string   s_rezult;                  //  -
   
   int      h_temp;                    //     (  ),    - 
   string   s_temp;                    //  -   (  ),    - 
   
   int      h_file[];                  //     .                   
   string   s_file[];                  //    
   
   datetime data[], data_all;
   int      oi[],             oi_all,
            noncomm_long[],   noncomm_long_all,
            noncomm_short[],  noncomm_short_all,
            spreading[],      spreading_all,
            operators_long[], operators_long_all,
            operators_short[],operators_short_all,
            all_long[],       all_long_all,
            all_short[],      all_short_all,
            nonrep_long[],    nonrep_long_all,
            nonrep_short[],   nonrep_short_all;
   
   int      str;                       //    
   int      n;                         //     
   int      column;                    //   
   int      frnz_str[];                //  ""  *
   double   count;                     //       
   string   tmp;                       //   -    
         
   h_list=FileOpen(s_list,FILE_READ|FILE_CSV);                    //   
   if(h_list==-1){Print("   ");return(0);}
   while(FileIsEnding(h_list)==false){                            //   
      s_enumeration=FileReadString(h_list);                       //    -
      Print("---------------<<",s_enumeration,">>---------------");
      h_enumeration=FileOpen(s_enumeration,FILE_READ|FILE_CSV);   //   -
      if(h_enumeration==-1){
         Print("    ", s_enumeration);
         continue;                                                //  -   -   
      }
      
      s_rezult=StringConcatenate(ConvertName(s_enumeration),".csv");    
      h_rezult=FileOpen(s_rezult,FILE_WRITE|FILE_CSV);
      if(h_rezult==-1){
         Print("    : ", s_rezult,"; ", GetLastError());
         continue;
      }
      while(FileIsEnding(h_enumeration)==false){
         s_temp=FileReadString(h_enumeration);                    //   -,   -   
         h_temp=FileOpen(s_temp,FILE_READ|FILE_CSV);              //    
         if(h_temp==-1)continue;        //      -   
         str++;                                                   //       1.
         //Print("--> ",s_temp);
         while(FileIsEnding(h_temp)==false){
            tmp=FileReadString(h_temp);
            if(tmp!="")count++;
         }
         Print("--> ",s_temp," ",count/12);
         count=0;
         FileClose(h_temp);                                      //   
      }//End While ( -)
      FileSeek(h_enumeration,0,SEEK_SET);
      
      ArrayResize(h_file,str);
      ArrayResize(s_file,str);
      ArrayResize(data,str);
      ArrayResize(oi,str);
      ArrayResize(noncomm_long,str);
      ArrayResize(noncomm_short,str);
      ArrayResize(spreading,str);
      ArrayResize(operators_long,str);
      ArrayResize(operators_short,str);
      ArrayResize(all_long,str);
      ArrayResize(all_short,str);
      ArrayResize(nonrep_long,str);
      ArrayResize(nonrep_short,str);
      ArrayResize(frnz_str,str);
      while(FileIsEnding(h_enumeration)==false){
         s_file[n]=FileReadString(h_enumeration);                    //   -,   -   
         //Print(str,"; ",s_file[n]);
         h_file[n]=FileOpen(s_file[n],FILE_READ|FILE_CSV);           //    
         if(h_file[n]==-1){Print("  ");continue;}        //      -   
         n++;  
      }// End While( -)   
      //for(n=0;n<str;n++){
      //   Print(n,"; ",s_file[n]);
      //}
      //        (s_file[])    (h_file[]) 
      //     .      :
      while(FileIsEnding(h_file[0])==false){                         //          (,   )
         for(int i=0;i<str;i++){                                     //         -.
            if(frnz_str[i]==1)continue;
            tmp=FileReadString(h_file[i]);
            switch (column){
               case DATA:            data[i]=StrToTime(tmp);
               case OI:              oi[i]=StrToInteger(tmp);
               case NONCOMM_LONG:    noncomm_long[i]=StrToInteger(tmp);
               case NONCOMM_SHORT:   noncomm_short[i]=StrToInteger(tmp);
               case SPREADING:       spreading[i]=StrToInteger(tmp);
               case OPERATORS_LONG:  operators_long[i]=StrToInteger(tmp);
               case OPERATORS_SHORT: operators_short[i]=StrToInteger(tmp);
               case ALL_LONG:        all_long[i]=StrToInteger(tmp);
               case ALL_SHORT:       all_short[i]=StrToInteger(tmp);
               case NONREP_LONG:     nonrep_long[i]=StrToInteger(tmp);
               case NONREP_SHORT:    nonrep_short[i]=StrToInteger(tmp);
            }
         }// End For()
         //Print(h_file[0], ";  ",s_file[0]);
         column++;
         if(FileIsLineEnding(h_file[0])==true||FileIsEnding(h_file[0])==true){  //    -     ,  ,   
            column=0;
            if(tmp=="")continue;
            for(int k=0;k<str;k++){
               if(data[0]>data[k])
                  frnz_str[k]=1;
               if(data[0]<data[k])
                  frnz_str[0]=1;
               if(data[0]==data[k])frnz_str[k]=0;
               if(data[0]!=data[k]){
                  Print(" : ", TimeToStr(data[0],TIME_DATE),"; ",TimeToStr(data[k],TIME_DATE));
                  continue;
               }
            }
               
            for(k=0;k<str;k++){
               data_all=data[0];
               oi_all+=oi[k];
               noncomm_long_all+=noncomm_long[k];
               noncomm_short_all+=noncomm_short[k];
               spreading_all+=spreading[k];
               operators_long_all+=operators_long[k];
               operators_short_all+=operators_short[k];
               all_long_all+=all_long[k];
               all_short_all+=all_short[k];
               nonrep_long_all+=nonrep_long[k];
               nonrep_short_all+=nonrep_short[k];
            }
            FileWrite(h_rezult,ConvertName(s_enumeration),
                               TimeToStr(data_all,TIME_DATE),
                               oi_all,
                               noncomm_long_all,
                               noncomm_short_all,
                               spreading_all,
                               operators_long_all,
                               operators_short_all,
                               all_long_all,
                               all_short_all,
                               nonrep_long_all,
                               nonrep_short_all);
            //Print(TimeToStr(data_all,TIME_DATE),";  ",oi_all);
            data_all=0;
            oi_all=0;
            noncomm_long_all=0;
            noncomm_short_all=0;
            spreading_all=0;
            operators_long_all=0;
            operators_short_all=0;
            all_long_all=0;
            all_short_all=0;
            nonrep_long_all=0;
            nonrep_short_all=0;
         }
         
      }// End While()
      
      //  
      FileClose(h_enumeration);
      FileClose(h_rezult);
      /*for(int j=0;j<str;j++){
         FileClose(h_file[j]);
         noncomm_long[j]=0;
         noncomm_short[j]=0;
         spreading[j]=0;
         operators_long[j]=0;
         operators_short[j]=0;
         all_long[j]=0;
         all_short[j]=0;
         nonrep_long[j]=0;
         nonrep_short[j]=0;
      }*/
      
      n=0;str=0;
   } //End While(  )
   
}// End Programm   

string ConvertName(string str)
{
   string str_cnv;
   int strlen=StringLen(str);
   int char;
   for(int i=0;i<strlen-4;i++){
      char=StringGetChar(str,i);
      str_cnv=StringConcatenate(str_cnv,CharToStr(char));
   }
   return(str_cnv);
}

/*
--------------------
  *            .        2004.09.28
   2004.09.07. ..   2004.09.21  2004.09.14 ,      .     2004.09.28  
    2004.09.07   , ..  ,     .      "", -
         ,         .
*/
