//+------------------------------------------------------------------+
//|                                       Meta COT Script Report.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+

//          -.   
//      .
#property show_inputs
#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

extern int period=52;
extern int movement_index=6;
#include <cotlib.mq4>
int start()
{
   string cot_report;
   int handle, k;
   if(init_data()==false)error=true;
   if(error==false)load_data();
   if(error==false)count_data();
   Print(cot_file);
   cot_report="Meta COT Report "+cot_file;
   Print(cot_report);
   handle=FileOpen(cot_report,FILE_WRITE|FILE_CSV,';');
   FileWrite(handle,
   "Data",                       //     
   "Open Interest",              // 0.   
   "Noncomm Long",               // 1.     
   "Noncomm Short",              // 2.     
   "Operators Long",             // 3.    
   "Operators Short",            // 4.    
   "Nonrep Long",                // 5.     
   "Nonrep Short",               // 6.     
   "% Noncomm Long in OI",       // 7.  %       
   "% Noncomm Short in OI",      // 8.  %       
   "% Operators Long in OI",     // 9.  %      
   "% Operators Short in OI",    // 10. %      
   "% Nonrep Long in OI",        // 11. %       
   "% Nonrep Short in OI",       // 12. %       
   "Net Noncomm",                // 13.    
   "Net Operators",              // 14.   
   "Net Nonrep",                 // 15.    
   "Index OI",                   // 16.   
   "Index Noncomm",              // 17.     
   "Index Operators",            // 18.    
   "Index Nonrep",               // 19.     
   "WILLCO Noncomm",             // 20.  WILLCO    
   "WILLCO Operators",           // 21.  WILLCO  
   "WIllCO Nonrep",              // 22.  WILLCO   
   "Movement Index OI",          // 23.    OI
   "Movement Index Noncomm",     // 24.     
   "Movement Index Operators",   // 25.    
   "Movement Index Nonrep"       // 26.     
   );
   for(int i=0;i<n_str;i++){
      FileWrite(handle,
      TimeToStr(realize_data[i],TIME_DATE ),
      DoubleToStr(open_interest[i],0),
      DoubleToStr(noncomm_long[i],0),
      DoubleToStr(noncomm_short[i],0),
      DoubleToStr(operators_long[i],0),
      DoubleToStr(operators_short[i],0),
      DoubleToStr(nonrep_long[i],0),
      DoubleToStr(nonrep_short[i],0),
      DoubleToStr(oi_noncomm_long[i],2),
      DoubleToStr(oi_noncomm_short[i],2),
      DoubleToStr(oi_operators_long[i],2),
      DoubleToStr(oi_operators_short[i],2),
      DoubleToStr(oi_nonrep_long[i],2),
      DoubleToStr(oi_nonrep_short[i],2),
      DoubleToStr(net_noncomm[i],0),
      DoubleToStr(net_operators[i],0),
      DoubleToStr(net_nonrep[i],0),
      DoubleToStr(index_oi[i],2),
      DoubleToStr(index_ncomm[i],2),
      DoubleToStr(index_operators[i],2),
      DoubleToStr(index_nonrep[i],2),
      DoubleToStr(willco_ncomm[i],2),
      DoubleToStr(willco_operators[i],2),
      DoubleToStr(willco_nonrep[i],2),
      DoubleToStr(movement_oi[i],0),
      DoubleToStr(movement_ncomm[i],0),
      DoubleToStr(movement_operators[i],0),
      DoubleToStr(movement_nonrep[i],0)
      );
   }
   FileClose(handle);
   return(0);
}

