//+------------------------------------------------------------------+
//|                                  Meta COT Script Concatenate.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+

//           
//#property show_inputs

#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

extern string FilesList="CONCATENATE.ini";

int start()
{
   int    h_list, h_file, h_cont, h_rezult;                     //  
   int    column;
   string s_list, s_file, s_cont, s_rezult,                     // ,   
          string_cont, string_cont_f;
   h_list=FileOpen(FilesList,FILE_READ|FILE_CSV);      //   
   if(h_list==-1){Print("  ("+FilesList+")  ");return(0);}
   while(FileIsEnding(h_list)==false){                  //   
      s_list=FileReadString(h_list);                      //    
      Print(s_list);
      h_file=FileOpen(s_list,FILE_READ|FILE_CSV);         //      
      if(h_list==-1){Print(" " +s_list+"  ");continue;}    //        -   
      //s_rezult="COT - CONT - "+s_list;
      s_rezult=get_rezultname(s_list);
      h_rezult=FileOpen(s_rezult,FILE_WRITE|FILE_READ|FILE_CSV);   
      while(FileIsEnding(h_file)==false){
         s_file=FileReadString(h_file);
         //Print(s_file);
         h_cont=FileOpen(s_file,FILE_READ|FILE_CSV,';');
         if(h_cont==-1){Print("  "+s_file+ "  ");continue;}
         else Print("   "+s_file+"...");
         
         while(FileIsEnding(h_cont)==false){
            string_cont=FileReadString(h_cont);         
            if(string_cont=="")continue;
            if(FileIsLineEnding(h_cont)==true){
               string_cont_f=string_cont_f+";"+string_cont;
               FileWrite(h_rezult,string_cont_f);
               string_cont_f="";
               column=0;
            }
            else{ 
               //if(string_cont=="")continue;
               if(column==0)string_cont_f=string_cont_f+string_cont;
               else string_cont_f=string_cont_f+";"+string_cont;
               column++;
            }
         }
         FileClose(h_cont);
      }
      FileClose(h_file);
      FileClose(h_rezult); 
   }
   return(0);
}

string get_rezultname(string fname)
{
   int strlen=StringLen(fname)-1;
   int char;
   string str;
   for(int i=strlen;i>0;i--){
      char=StringGetChar(fname,i);
      if(char=='.'){
         for(int b=0;b<i;b++)
            str=str+CharToStr(StringGetChar(fname,b));
         str=str+".csv";
         return(str);
      }
   }
   return("COT - CONT - "+fname);
}
