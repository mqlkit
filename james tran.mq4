//+------------------------------------------------------------------+
//|                                            James tran_EA.mq4 |
//|                                       Copyright 2010, james tran. |
//|                                               |
//+------------------------------------------------------------------+

 
#property copyright "Copyright 2010, James tran."    


//+------------------------------------------------------------------+
//| Universal Constants                                              |
//+------------------------------------------------------------------+
#define TRADEPEND  -1
#define TRADEALL   0
#define TRADETRADE 1
#define TRADEBUY   1
#define TRADESELL  -1

#define CANDLE_BULL 1
#define CANDLE_BEAR -1
#define CANDLE_NONE 0

//+------------------------------------------------------------------+
//| User input variables                                             |
//+------------------------------------------------------------------+
extern string     AdvisorName = "KellyL_Ranging_EA";          // For Logging
extern string  AdvisorVersion = "1.0.2";            // The version number of this script
extern string     ProjectPage = "http://kolier.li/project/kellyl-ranging-ea";            // The project landing page
// Trade Time Filter - @see http://kolier.li/example/script-function-trade-time-filter
extern string Trade_Time_Filter = "http://kolier.li/example/script-function-trade-time-filter";
extern bool   TradeTimeFilter = false;          // Whether to use trade time filter
extern string      TradeBegin = "07:00";       // Don't trade before this time
extern string        TradeEnd = "19:00";       // Don't trade after this time
extern bool     EndInTomorrow = false;         // Whether TradeEnd is the next day, like you want to trade the period: 18:00~06:00, the 06:00 is the next am
// Order Operation - Open: @see http://kolier.li/example/script-function-order-operation-open
extern string Order_Operation_Open = "http://kolier.li/example/script-function-order-operation-open";
extern double    ProfitTarget = 10;            // In account currency, usually in dollar.
extern bool         ECNBroker = false;         // Whether using ECN broker
extern int              Magic = 222222;        // For identify trades belong to this EA, set to unique on different chart when using at the same time
extern int           Slippage = 3;             // Allow slippage
extern double            LotsInit = 0.1;           // Trade lots
extern bool       StealthMode = false;         // Whether TakeProfit and StopLoss in Stealth Mode, hiding from the broker
extern double      TakeProfit1 = 100;             // In Pips. Take Profit, Change to 0 if you don't want to use.
extern double        StopLoss1 = 100;             // In Pips. Stop Loss, Change to 0 if you don't want to use.
// Secondary Trade							
extern bool    UseTrade2 = true;
extern bool    BuySell2 = true;		
extern double   LastLoss2 = 40;		           // In Pips.
extern double     TakeProfit2 = 100;			  // In Pips.
extern double       StopLoss2 = 100;		     // In Pips.
extern double        LotMult2 = 2.0;		
// Third Trade							
extern bool     UseTrade3 = true;	
extern bool    BuySell3 = true;	
extern double   LastLoss3 = 40;		           // In Pips.
extern double     TakeProfit3 = 100;
extern double       StopLoss3 = 100;
extern double        LotMult3 = 3.0;
// Forth Trade							
extern bool     UseTrade4 = true;	
extern bool    BuySell4 = true;	
extern double   LastLoss4 = 40;		           // In Pips.
extern double     TakeProfit4 = 100;
extern double       StopLoss4 = 100;
extern double        LotMult4 = 4.0;
// Fifth Trade							
extern bool     UseTrade5 = true;
extern bool    BuySell5 = true;		
extern double   LastLoss5 = 40;		           // In Pips.
extern double     TakeProfit5 = 100;
extern double       StopLoss5 = 100;
extern double        LotMult5 = 5.0;
// six Trade							
extern bool     UseTrade6 = true;	
extern bool    BuySell6 = true;	
extern double   LastLoss6 = 100;		           // In Pips.
extern double     TakeProfit6 = 100;
extern double       StopLoss6 = 100;
extern double        LotMult6 = 6.0;
// Order Operation - Break Even: @see http://kolier.li/example/script-function-order-operation-breakeven
extern string Order_Operation_BreakEven = "http://kolier.li/example/script-function-order-operation-breakeven";
extern bool      UseBreakEven = false;          // Whether using BreakEven
extern bool    BE_StealthMode = false;         // Whether using BreakEven in Stealth Mode
extern double   BreakEvenPips = 20;            // In Pips. BreakEven Pips.
extern bool BreakEvenWithProfit = false;       // Different from normal BreakEven, you can make a little in BreakEven, like 1~2 pips
extern double BreakEvenProfit = 3;             // In Pips. BreakEven Pips take profit.
// Order Operation - Trailing Stop: @see http://kolier.li/example/script-function-order-operation-trailing-stop
extern string Order_Operation_Trailing_Stop = "http://kolier.li/example/script-function-order-operation-trailing-stop";
extern bool      UseTrailStop = false;          // Whether using TrailStop
extern bool    TrailAfterEven = true;          // Whether using TrailStop after the range between current price and the OrderOpenPrice >= TrailStopPips
extern bool    TS_StealthMode = false;         // Whether using TrailStop in Stealth Mode
extern double   TrailStopPips = 20;            // In Pips. TrailStop Pips
extern bool  TS_MovingInChunk = false;         // Whether wait for price move a range to set new SL
extern double        TS_Chunk = 10;            // In Pips. Moving pips as chunk


//+------------------------------------------------------------------+
//| Universal variables                                              |
//+------------------------------------------------------------------+
int TradeOn_n[6], num_trades=0, BuySell_n[6];
double Lots_n[6], Lots, TP_n[6], TakeProfit, SL_n[6], StopLoss, LastLoss_n[6];
datetime time_check;
int tickets[6]; 

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
   Lots_n[0] = LotsInit;
   Lots_n[1] = LotsInit*LotMult2;
   Lots_n[2] = LotsInit*LotMult3;
   Lots_n[3] = LotsInit*LotMult4;
   Lots_n[4] = LotsInit*LotMult5;
   Lots_n[5] = LotsInit*LotMult6;
   TP_n[0] = TakeProfit1;
   TP_n[1] = TakeProfit2;
   TP_n[2] = TakeProfit3;
   TP_n[3] = TakeProfit4;
   TP_n[4] = TakeProfit5;
   TP_n[5] = TakeProfit6;
   SL_n[0] = StopLoss1;
   SL_n[1] = StopLoss2;
   SL_n[2] = StopLoss3;
   SL_n[3] = StopLoss4;
   SL_n[4] = StopLoss5;
   SL_n[5] = StopLoss6;
   LastLoss_n[0] = 0;
   LastLoss_n[1] = LastLoss2;
   LastLoss_n[2] = LastLoss3;
   LastLoss_n[3] = LastLoss4;
   LastLoss_n[4] = LastLoss5;
   LastLoss_n[5] = LastLoss6;
   
   ArrayInitialize(TradeOn_n, 0);
   TradeOn_n[0] = 1;
   if(UseTrade2) TradeOn_n[1] = 1;
   if(UseTrade3) TradeOn_n[2] = 1;
   if(UseTrade4) TradeOn_n[3] = 1;
   if(UseTrade5) TradeOn_n[4] = 1;
   if(UseTrade6) TradeOn_n[5] = 1;
   ArrayInitialize(BuySell_n, 0);
   if(BuySell2) BuySell_n[1] = 1;
   if(BuySell3) BuySell_n[2] = 1;
   if(BuySell4) BuySell_n[3] = 1;
   if(BuySell5) BuySell_n[4] = 1;
   if(BuySell6) BuySell_n[5] = 1;
   
   
  
   return(0);
  }
  
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
   // Clear the un-used Global Variables
   GlobalVariablesDeleteAll(AdvisorName);

   return(0);
  }
  
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
   // BreakEven && TrailStop && StealthMode
   if(scanTrades(TRADETRADE)>0) {
      if(UseBreakEven) breakEven();
      if(UseTrailStop) trailStop();
      if(StealthMode) stealthMode();
   }
   
   // buy pattern = bear candle  bear candle bull candle bear candle
   // sell pattern = bear candle  bull candle bear candle bear candle
   int cond_buy[3], cond_sell[3];
 
   cond_buy[0] = CANDLE_BULL;
   cond_buy[1] = CANDLE_BULL;
   cond_buy[2] = CANDLE_BEAR;

   cond_sell[0] = CANDLE_BEAR;
   cond_sell[1] = CANDLE_BEAR;
   cond_sell[2] = CANDLE_BULL;
 
   
   bool cond_buy_fit[3], cond_sell_fit[3];
   ArrayInitialize(cond_buy_fit, false);
   ArrayInitialize(cond_sell_fit, false);
   
   int i, j, candle[3];
   int candle_limit = ArraySize(candle);
   ArrayInitialize(candle, CANDLE_NONE);
   for(i=1,j=0; i<20 && j<candle_limit; i++) {
      if(Close[i]-Open[i]>0) {
         candle[j] = CANDLE_BULL;
         j++;
      }
      else if(Close[i]-Open[i]<0) {
         candle[j] = CANDLE_BEAR;
         j++;
      }
      else if(Close[i]-Open[i]==0) {
         candle[j] = 2;
         j++;
        // continue;
      }
   }
   
   if(time_check<Time[0]) {
      time_check = Time[0];
      Print("Candle:",DoubleToStr(candle[0],0),"-",DoubleToStr(candle[1],0),"-",DoubleToStr(candle[2],0),"-",DoubleToStr(candle[3],0));
      Print("Buy:",DoubleToStr(cond_buy[0],0),"-",DoubleToStr(cond_buy[1],0),"-",DoubleToStr(cond_buy[2],0),"-",DoubleToStr(cond_buy[3],0));
      Print("Sell:",DoubleToStr(cond_sell[0],0),"-",DoubleToStr(cond_sell[1],0),"-",DoubleToStr(cond_sell[2],0),"-",DoubleToStr(cond_sell[3],0));  
   }
   
   for(i=0; i<candle_limit; i++) {
      if(candle[i]==cond_buy[i]) {
         cond_buy_fit[i] = true;
      }
      if(candle[i]==cond_sell[i]) {
         cond_sell_fit[i] = true;
      }
   }
   
   bool cond_buy_ready=true, cond_sell_ready=true;
   for(i=0; i<candle_limit; i++) {
      if(cond_buy_fit[i]==false) {
         cond_buy_ready=false;
      }
      if(cond_sell_fit[i]==false) {
         cond_sell_ready=false;
      }
   }
   
   // Trade Time Filter
   if(!TradeTimeFilter || (TradeTimeFilter && tradeTime())) {
      Lots = Lots_n[0];
      TakeProfit = TP_n[0];
      StopLoss = SL_n[0];
      if(!tradedInBar() && scanTrades(TRADETRADE)==0 && cond_buy_ready) {
         ArrayInitialize(tickets, 0);
         num_trades = 0;
         tradeBuy();
      
      }
      if(!tradedInBar() && scanTrades(TRADETRADE)==0 && cond_sell_ready) {
         ArrayInitialize(tickets, 0);
         num_trades = 0;
         tradeSell();
      }
      
      // Open More
      if(num_trades>0) {
         OrderSelect(tickets[num_trades-1], SELECT_BY_TICKET);
         if(TradeOn_n[num_trades]>0 && profit2Pips(OrderSymbol(),OrderProfit(),OrderLots())>=LastLoss_n[num_trades] && OrderProfit()>0) {
            Lots = Lots_n[num_trades];
            TakeProfit = TP_n[num_trades];
            StopLoss = SL_n[num_trades];
            if(BuySell_n[num_trades]>0) {
               tradeBuy();
            }
            else {
               tradeSell();
            }
         }
      }
   }
   
  // Close
   if(num_trades>0 && scanTrades(TRADETRADE)>0) {
      OrderSelect(tickets[num_trades -1], SELECT_BY_TICKET);
      if(OrderStopLoss()>0 && OrderCloseTime()>0) {
         while(scanTrades(TRADETRADE)>0) {
            closeTrades();
         }
      }
      
      if(ordersProfit()>ProfitTarget) {
         while(scanTrades(TRADETRADE)>0) {
            closeTrades();
         }
      }
   }

   return(0);
  }
  
//+------------------------------------------------------------------+
//| Total Profit of sepcific type of orders @http://kolier.li        |
//+------------------------------------------------------------------+
double ordersProfit(int order_dir=TRADEALL)
  {
   double profit = 0;
   for(int i=0; i<OrdersTotal(); i++) {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==Magic) {
         switch(order_dir) {
            case TRADEALL:
               profit += OrderProfit();
               break;
            case TRADEBUY:
               if(OrderType()==OP_BUY) {
                  profit += OrderProfit();
               }
               break;
            case TRADESELL:
               if(OrderType()==OP_SELL) {
                  profit += OrderProfit();
               }
               break;
         }// switch
      }
   }
  
   return(profit);
  }


//+------------------------------------------------------------------+
//| Trade Time Filter @http://kolier.li                              |
//|   @see http://kolier.li/example/script-function-trade-time-filter|
//+------------------------------------------------------------------+
bool tradeTime()
  {
   if(TradeTimeFilter) {
      datetime time_now, time_begin, time_end, time_end_today;
      time_now = TimeCurrent();
      time_begin = StrToTime(TradeBegin);
      if(!EndInTomorrow) {
         time_end = StrToTime(TradeEnd);
      }
      else if(EndInTomorrow) {
         time_end = StrToTime(TradeEnd) + 86400;
         time_end_today = StrToTime(TradeEnd);
      }
      if(!EndInTomorrow && (time_now<time_begin || time_now>time_end)) {
         return(false);
      }
      else if(EndInTomorrow && time_now>time_end_today && time_now<time_begin) {
         return(false);
      }
   }
   return(true);  
  }
  
//+------------------------------------------------------------------+
//| Trade Buy @http://kolier.li @v1.0                                |
//| @import                                                          |
//|   bool ECNBroker                                                 |
//+------------------------------------------------------------------+
void tradeBuy()
  {
   double price_now, price_profit, price_stop, lots;
   double take_profit = pips2Points(TakeProfit)*Point;
   double stop_loss = pips2Points(StopLoss)*Point;
   int ticket;
   price_now = Ask;
   if(TakeProfit > 0) {
      price_profit = price_now + take_profit;
   }
   else {
      price_profit = 0;
   }
   if(StopLoss > 0) {
      price_stop = price_now - stop_loss;
   }
   else {
      price_stop = 0;
   }
   lots = Lots;
   /*
   if(MoneyManagement) {
      lots = mmLots(stop_loss);
   }
   else {
      lots = Lots;
   }
   */
   
   if(!ECNBroker && !StealthMode) {
      ticket = OrderSend(Symbol(), OP_BUY, lots, price_now, Slippage, price_stop, price_profit, NULL, Magic, 0, Lime);
   }
   else {
      ticket = OrderSend(Symbol(), OP_BUY, lots, price_now, Slippage, 0, 0, NULL, Magic, 0, Lime);
   }
   
   if(ticket < 0) {
      //tradeBuy();
      Print(AdvisorName, "- tradeBuy() error #", GetLastError());
      Print("Price Buy: ", price_now, " TP: ", price_profit, " SL: ", price_stop, " Lots: ", lots);
   }
   else {
      OrderSelect(ticket, SELECT_BY_TICKET);
      if(ECNBroker) { 
         OrderModify(OrderTicket(), OrderOpenPrice(), price_stop, price_profit, 0, CLR_NONE);
      }
      tickets[num_trades] = ticket;
      num_trades++;
   }
  }
  
//+------------------------------------------------------------------+
//| Trade Sell @http://kolier.li @v1.0                               |
//| @import                                                          |
//|   bool ECNBroker                                                 |
//+------------------------------------------------------------------+
void tradeSell()
  {
   double price_now, price_profit, price_stop, lots;
   double take_profit = pips2Points(TakeProfit)*Point;
   double stop_loss = pips2Points(StopLoss)*Point;
   int ticket;
   price_now = Bid;
   if(TakeProfit > 0) {
      price_profit = price_now - take_profit;
   }
   else {
      price_profit = 0;
   }
   if(StopLoss > 0) {
      price_stop = price_now + stop_loss;
   }
   else {
      price_stop = 0;
   }
   lots = Lots;
   /*
   if(MoneyManagement) {
      lots = mmLots(stop_loss);
   }
   else {
      lots = Lots;
   }
   */

   if(!ECNBroker && !StealthMode) {
      ticket = OrderSend(Symbol(), OP_SELL, lots, price_now, Slippage, price_stop, price_profit, NULL, Magic, 0, Red);
   }
   else {
      ticket = OrderSend(Symbol(), OP_SELL, lots, price_now, Slippage, 0, 0, NULL, Magic, 0, Red);
   }
   
   if(ticket < 0) {
      //tradeBuy();
      Print(AdvisorName, "- tradeSell() error #", GetLastError());
      Print("Price Sell: ", price_now, " TP: ", price_profit, " SL: ", price_stop, " Lots: ", lots);
   }
   else {
      OrderSelect(ticket, SELECT_BY_TICKET);
      if(ECNBroker) {  
         OrderModify(OrderTicket(), OrderOpenPrice(), price_stop, price_profit, 0, CLR_NONE);
      }
      tickets[num_trades] = ticket;
      num_trades++;
   }
  }
  
//+------------------------------------------------------------------+
//| Close All Trades @http://kolier.li                               |
//+------------------------------------------------------------------+
void closeTrades()
  {
   for(int i=0; i<OrdersTotal(); i++) {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==OP_BUY) {
         OrderClose(OrderTicket(), OrderLots(), MarketInfo(OrderSymbol(),MODE_BID), Slippage, White);
      }
      else if(OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==OP_SELL) {
         OrderClose(OrderTicket(), OrderLots(), MarketInfo(OrderSymbol(),MODE_ASK), Slippage, White);
      }
   }
  }
  
//+------------------------------------------------------------------+
//| TP and SL stealthly, hide from the trade server                  |
//| Place at the front part of start()                               |
//+------------------------------------------------------------------+
void stealthMode()
  {
   double takeprofit_stealth = pips2Points(TakeProfit)*Point;
   double stoploss_stealth = pips2Points(StopLoss)*Point;
      
   for(int i=0; i<OrdersTotal(); i++) {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==Magic) {
         if(OrderType()==OP_BUY
            && ( (MarketInfo(OrderSymbol(),MODE_BID)-OrderOpenPrice()>=takeprofit_stealth && takeprofit_stealth>0)
               || (OrderOpenPrice()-MarketInfo(OrderSymbol(),MODE_BID)>=stoploss_stealth && stoploss_stealth>0) )) {
            OrderClose(OrderTicket(), OrderLots(), Bid, Slippage, White);
         }
         else if(OrderType()==OP_SELL
            && ( (OrderOpenPrice()-MarketInfo(OrderSymbol(),MODE_ASK)>=takeprofit_stealth && takeprofit_stealth>0)
               || (MarketInfo(OrderSymbol(),MODE_ASK)-OrderOpenPrice()>=stoploss_stealth && stoploss_stealth>0) )) {
            OrderClose(OrderTicket(), OrderLots(), Ask, Slippage, White);
         }
      }
   }
  }
  
//+------------------------------------------------------------------+
//| pips2Points() @http://kolier.li                                  |
//+------------------------------------------------------------------+
double pips2Points(double pips)
  {
   double points;
   if(Close[0]<10) {
      points = pips*MathPow(10,Digits-4);
   }
   else if(Close[0]>10) {
      points = pips*MathPow(10,Digits-2);  
   }
   
   return(points);
  }

//+------------------------------------------------------------------+
//| trailStop Target Points                                          |
//| Scan Trades @http://kolier.li @v1.0                              |
//| @params                                                          |
//|   string order_type TRADEALL|TRADETRADE|TRADEPEND                |
//|   string order_dir TRADEALL|TRADEBUY|TRADESELL                   |
//| @return                                                          |
//|   int orders_number                                              |
//+------------------------------------------------------------------+
int scanTrades(int order_type=TRADEALL, int order_dir=TRADEALL)
  {
   int orders_total = OrdersTotal();
   int orders_number = 0;
   for(int i=0; i<orders_total; i++)
   {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      if(OrderMagicNumber() == Magic && OrderSymbol() == Symbol()) {
         switch(order_type) {
            case TRADEALL:
               orders_number++;
               break;
            case TRADETRADE:
               if(OrderType()<=OP_SELL) {
                  orders_number = _scanTradesDir(order_dir, orders_number);
               }
               break;
            case TRADEPEND:
               if(OrderType()>OP_SELL) {
                  orders_number = _scanTradesDir(order_dir, orders_number);
               }
               break;
         }// switch
      }
   }
   return(orders_number);   
  }
  
int _scanTradesDir(int order_dir, int orders_number)
  {
      switch(order_dir) {
         case TRADEALL:
            orders_number++;
            break;
         case TRADEBUY:
            if(OrderType()==OP_BUY || OrderType()==OP_BUYLIMIT || OrderType()==OP_BUYSTOP) {
               orders_number++;
            }
            break;
         case TRADESELL:
            if(OrderType()==OP_SELL || OrderType()==OP_SELLLIMIT || OrderType()==OP_SELLSTOP) {
               orders_number++;
            }
            break;
      }// switch
      return(orders_number);
  }
  
//+------------------------------------------------------------------+
//| Break Even @http://kolier.li                                     |
//+------------------------------------------------------------------+
void breakEven()
  {
   int orders_total = OrdersTotal();
   int i, ticket;
   double break_even_points = pips2Points(BreakEvenPips);
   double break_even_with_profit = pips2Points(BreakEvenWithProfit);
   double stoploss_buy, stoploss_sell;
   string gv_name;
   double break_even_able;
   
   for(i=0; i<orders_total; i++) {
      break_even_able = -1;
      if(!BreakEvenWithProfit) {
         stoploss_buy = OrderOpenPrice();
         stoploss_sell = OrderOpenPrice();
      }
      else {
         stoploss_buy = OrderOpenPrice() + break_even_with_profit*Point;
         stoploss_sell = OrderOpenPrice() - break_even_with_profit*Point;
      }
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderProfit()>0) {
         if(BE_StealthMode) {
            break_even_able = GlobalVariableGet(AdvisorName+"_be_able_"+OrderTicket());
            if(OrderType()==OP_BUY && break_even_able>0 && Bid<=stoploss_buy) {
               OrderClose(OrderTicket(), OrderLots(), Bid, Slippage, White);
            }
            else if(OrderType()==OP_SELL && break_even_able>0 && Ask>=stoploss_sell) {
               OrderClose(OrderTicket(), OrderLots(), Ask, Slippage, White);
            }
         }
      
         if(OrderType()==OP_BUY && (OrderStopLoss()<stoploss_buy || OrderStopLoss()==0)
            && Bid-OrderOpenPrice()>=break_even_points*Point) {
            if(!BE_StealthMode) {
               OrderModify(OrderTicket(), OrderOpenPrice(), stoploss_buy, OrderTakeProfit(), 0, Green);
            }
            else {
               GlobalVariableSet(AdvisorName+"_be_able_"+OrderTicket(), 1);
            }
         }
         else if(OrderType()==OP_SELL && (OrderStopLoss()>stoploss_sell || OrderStopLoss()==0)
            && OrderOpenPrice()-Ask>=break_even_points*Point) {
            if(!BE_StealthMode) {
               OrderModify(OrderTicket(), OrderOpenPrice(), stoploss_sell, OrderTakeProfit(), 0, Magenta);
            }
            else {
               GlobalVariableSet(AdvisorName+"_be_able_"+OrderTicket(), 1);
            }
         }
      }
   }
   
   // Clear the un-used Global Variables
   for(i=0; i<GlobalVariablesTotal(); i++) {
      gv_name = GlobalVariableName(i);
      if(StringSubstr(gv_name, 0, 8+StringLen(AdvisorName))==AdvisorName+"_be_able") {
         ticket = StrToInteger(StringSubstr(gv_name, 8+StringLen(AdvisorName)));
         OrderSelect(ticket, SELECT_BY_TICKET);
         if(OrderCloseTime()>0) {
            GlobalVariableDel(gv_name);
         }
      }
   }
  }

//+------------------------------------------------------------------+
//| Trail Stop @http://kolier.li                                     |
//+------------------------------------------------------------------+
void trailStop()
  {
   int ticket;
   string gv_name;
   double trail_stop_points = pips2Points(TrailStopPips)*Point;
   double ts_chunk = pips2Points(TS_Chunk)*Point;
   double stoploss_order;
   
   for(int i=0; i<OrdersTotal(); i++) {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==Magic) {
         if(!TS_StealthMode) {
            stoploss_order = OrderStopLoss();
         }
         else {
            stoploss_order = GlobalVariableGet("ts_stoploss_"+OrderTicket());
            
            if(stoploss_order>0) {
               if(OrderType()==OP_BUY && Bid<=stoploss_order) {
                  OrderClose(OrderTicket(), OrderLots(), Bid, Slippage, White);
                  GlobalVariableDel(AdvisorName+"_ts_stoploss_"+OrderTicket());
                  continue;
               }
               else if(OrderType()==OP_SELL && Ask>=stoploss_order) {
                  OrderClose(OrderTicket(), OrderLots(), Ask, Slippage, White);
                  GlobalVariableDel(AdvisorName+"_ts_stoploss_"+OrderTicket());
                  continue;
               }
            }
         }

         if(OrderType()==OP_BUY
            && ((Bid-trail_stop_points>stoploss_order && stoploss_order>0) || stoploss_order==0)
            && (!TrailAfterEven || (TrailAfterEven && Bid-trail_stop_points>=OrderOpenPrice()))
            && (!TS_MovingInChunk || (TS_MovingInChunk && ((Bid-stoploss_order>trail_stop_points+ts_chunk && stoploss_order>0) || stoploss_order==0)))) {
            if(!TS_StealthMode) {
               OrderModify(OrderTicket(), OrderOpenPrice(), Bid-trail_stop_points, OrderTakeProfit(), 0, Blue);
            }
            else {
               GlobalVariableSet(AdvisorName+"_ts_stoploss_"+OrderTicket(), Bid-trail_stop_points);
            }
         }
         else if(OrderType()==OP_SELL
            && ((Ask+trail_stop_points<stoploss_order && stoploss_order>0) || stoploss_order==0)
            && (!TrailAfterEven || (TrailAfterEven && Ask+trail_stop_points<=OrderOpenPrice()))
            && (!TS_MovingInChunk || (TS_MovingInChunk && ((stoploss_order-Ask>trail_stop_points+ts_chunk && stoploss_order>0) || stoploss_order==0)))) {
            if(!TS_StealthMode) {
               OrderModify(OrderTicket(), OrderOpenPrice(), Ask+trail_stop_points, OrderTakeProfit(), 0, Orange);
            }
            else {
               GlobalVariableSet(AdvisorName+"_ts_stoploss_"+OrderTicket(), Ask+trail_stop_points);
            }
         }
      }
   }
   
   // Clear the un-used Global Variables
   for(i=0; i<GlobalVariablesTotal(); i++) {
      gv_name = GlobalVariableName(i);
      if(StringSubstr(gv_name, 0, 12+StringLen(AdvisorName))==AdvisorName+"_ts_stoploss") {
         ticket = StrToInteger(StringSubstr(gv_name, 12+StringLen(AdvisorName)));
         OrderSelect(ticket, SELECT_BY_TICKET);
         if(OrderCloseTime()>0) {
            GlobalVariableDel(gv_name);
         }
      }
   }
  }
  
//+------------------------------------------------------------------+
//| Whether traded in that bar, default to current bar               |
//| @http://kolier.li                                                |
//+------------------------------------------------------------------+
bool tradedInBar(int shift=0)
  {
   datetime time_begin = Time[shift];
   datetime time_end = time_begin + Period()*60;
   int i;
   for(i=0; i<OrdersTotal(); i++)
   {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      if(OrderMagicNumber()==Magic && OrderSymbol()==Symbol()
        && OrderOpenTime()>=time_begin && OrderOpenPrice()<=time_end) {
         return(true);
      }
   }
   for(i=0; i<OrdersHistoryTotal(); i++)
   {
      OrderSelect(i, SELECT_BY_POS, MODE_HISTORY);
      if(OrderMagicNumber()==Magic && OrderSymbol()==Symbol()
        && OrderOpenTime()>=time_begin && OrderOpenPrice()<=time_end) {
         return(true);
      }
   }
   
   return(false);
  }
  
//+------------------------------------------------------------------+
//| Convert the orders profit into pips @http://kolier.li            |
//+------------------------------------------------------------------+
double profit2Pips(string symbol, double profit, double lots)
  {
   double tick_value = MarketInfo(symbol, MODE_TICKVALUE);
   double points = profit/(lots*tick_value);
   double pips = points2Pips(points);

   return(pips);
  }
  
//+------------------------------------------------------------------+
//| points2Pips() @http://kolier.li                                  |
//+------------------------------------------------------------------+
double points2Pips(double points)
  {
   double pips;
   if(Close[0]<10) {
      pips = points/MathPow(10,Digits-4);
   }
   else if(Close[0]>10) {
      pips = points/MathPow(10,Digits-2);
   }
   
   return(pips);
  }