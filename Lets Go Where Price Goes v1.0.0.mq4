//+------------------------------------------------------------------+
//|                              Lets Go Where Price Goes EA, v1.0.0 |
//|                              Copyright � 2011, trapule@gmail.com |
//|                                                trapule@gmail.com |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, trapule@gmail.com"
#property link      "trapule@gmail.com"

extern string info1        = "Magic number";
extern int magicnumber     = 20110110;
extern string info2        = "Pip distance between levels";
extern int levelpipdist    = 20;
extern string info3        = "Fixed lot for each order";
extern double fixedlot     = 0.01;
extern string info4        = "Account growth (in account currency) to exit all open positions";
extern double gainforclose = 10.0;

double pip;
double levelrefup;
double levelrefdown;

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
{
  if (Digits==2 || Digits==3) pip=0.01;
  else if (Digits==4 || Digits==5) pip=0.0001;
  levelrefup = ndd(Bid+levelpipdist/2*pip);
  levelrefdown = ndd(levelrefup-levelpipdist*pip);
  return(0);
}

//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
{
   return(0);
}

//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
{
  int bsn=0, ssn=0;
  int i;
  
  if ((AccountEquity()-AccountBalance())>gainforclose)
  {
    for (i=OrdersTotal()-1; i>=0; i--)
    {
      OrderSelect(i, SELECT_BY_POS);
    
      if (OrderMagicNumber()==magicnumber && OrderSymbol()==Symbol())
      {
        if (OrderType()==OP_BUYSTOP || OrderType()==OP_SELLSTOP)
          OrderDelete(OrderTicket());
          
        if (OrderType()==OP_BUY)
          OrderClose(OrderTicket(),OrderLots(),ndd(Bid),100);
  
        if (OrderType()==OP_SELL)
          OrderClose(OrderTicket(),OrderLots(),ndd(Ask),100);
      }
    }
  }
  
  for (i=OrdersTotal()-1; i>=0; i--)
  {
    OrderSelect(i, SELECT_BY_POS);
    
    if (OrderMagicNumber()==magicnumber && OrderSymbol()==Symbol())
    {
      if (OrderType()==OP_BUYSTOP) bsn++;
      if (OrderType()==OP_SELLSTOP) ssn++;
      
      if (OrderType()==OP_BUY)
      {
        if (ndd(OrderStopLoss())!=ndd(OrderOpenPrice()) && ndi((Bid-OrderOpenPrice())/pip)>=levelpipdist)
          OrderModify (OrderTicket(), OrderOpenPrice(), OrderOpenPrice(), 0.0, 0);
      }
      
      if (OrderType()==OP_SELL)
      {
        if (ndd(OrderStopLoss())!=ndd(OrderOpenPrice()) && ndi((OrderOpenPrice()-Ask)/pip)>=levelpipdist)
          OrderModify (OrderTicket(), OrderOpenPrice(), OrderOpenPrice(), 0.0, 0);
      }
    }
  }
    
  if (bsn==0 && ssn==0)
  {
    levelrefup = ndd(Bid+levelpipdist/2*pip);
    levelrefdown = ndd(levelrefup-levelpipdist*pip);
    OrderSend (Symbol(), OP_BUYSTOP, fixedlot, levelrefup, 100, levelrefdown, 0.0, "Lets Go Where Price Goes", magicnumber);
    OrderSend (Symbol(), OP_SELLSTOP, fixedlot, levelrefdown, 100, levelrefup, 0.0, "Lets Go Where Price Goes", magicnumber);
  }
    
  if (Ask>=levelrefup)
  {
    levelrefup = ndd(levelrefup+levelpipdist*pip);
    levelrefdown = ndd(levelrefup-2*levelpipdist*pip);
    OrderSend (Symbol(), OP_BUYSTOP, fixedlot, levelrefup, 100, levelrefdown, 0.0, "Lets Go Where Price Goes", magicnumber);
    OrderSend (Symbol(), OP_SELLSTOP, fixedlot, levelrefdown, 100, levelrefup, 0.0, "Lets Go Where Price Goes", magicnumber);
  }

  if (Bid<=levelrefdown)
  {
    levelrefdown = ndd(levelrefdown-levelpipdist*pip);
    levelrefup = ndd(levelrefdown+2*levelpipdist*pip);
    OrderSend (Symbol(), OP_BUYSTOP, fixedlot, levelrefup, 100, levelrefdown, 0.0, "Lets Go Where Price Goes", magicnumber);
    OrderSend (Symbol(), OP_SELLSTOP, fixedlot, levelrefdown, 100, levelrefup, 0.0, "Lets Go Where Price Goes", magicnumber);
  }
  
  return(0);
}
//+------------------------------------------------------------------+

double ndd (double Price)
{
  return (NormalizeDouble(Price,Digits));
}
//+------------------------------------------------------------------+

double ndi (double Value)
{
  return (NormalizeDouble(Value,0));
}
//+------------------------------------------------------------------+

