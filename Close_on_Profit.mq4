//+------------------------------------------------------------------+
//|                                              Close_on_Profit.mq4 |
//|                                Copyright � 2010, Dennis Hamilton |
//|                                              ramble_32@yahoo.com |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2010, Dennis Hamilton"
#property link      "ramble_32@yahoo.com"

extern double  p_target=10.0;
extern double  targ_base=0;
extern bool    recurring=true;
extern bool    use_magic=true;
extern int     magic=123;
extern bool    closeall=false;

double AE;
int magic2, t_cnt, oneshot;
datetime get_time;

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   t_cnt=0;
   oneshot=0;
   AE=AccountEquity();
   GlobalVariableSet("inhibit",0);
   if(!GlobalVariableGet("targ_base") && targ_base==0) GlobalVariableSet("targ_base",AE);
   if(targ_base>0) GlobalVariableSet("targ_base",targ_base);
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
   AE=AccountEquity();
   targ_base=GlobalVariableGet("targ_base");
   if(AE>=targ_base+p_target || closeall){
   get_time=TimeCurrent(); GlobalVariableSet("targ_base",AE); t_cnt++; }
   if(!recurring){ oneshot=t_cnt; GlobalVariableSet("inhibit",t_cnt); }
   if(!use_magic){ magic=0; magic2=0; }
   closeall=false;
//----
   if(recurring || oneshot==1)
   {
      for(int i=OrdersTotal()-1; i>=0; i--)
      {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true)
         {
            if(use_magic) magic2=OrderMagicNumber();
            if(OrderOpenTime()<get_time && magic2==magic)
            {
               if(OrderType()>1) OrderDelete(OrderTicket());
               if(OrderType()<2) OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
            }
         }
      }
   }
//----
   int gt_cnt=0;
   for(i=0; i<OrdersTotal(); i++)
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true)
      {
         if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
         if(OrderOpenTime()<get_time && magic2==magic) gt_cnt++;
      }
   }
//----
   Comment("Equity = "+DoubleToStr(AE,2)+"  |  Target = "+DoubleToStr(targ_base+p_target,2)+"  |  Magic = "+magic+"  |  Close Time = "+get_time+"  |  Orders = "+gt_cnt);
//----
   return(0);
  }
//+------------------------------------------------------------------+