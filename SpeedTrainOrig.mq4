//+------------------------------------------------------------------+
//|                                                      EMA Cut.mq4 |
//|                                                            Jasus |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "roundrock"
#property link      ""

#include <stderror.mqh>
#include <stdlib.mqh>



extern int    Slippage         =       3;




extern double     LotSize= 0.1;

extern string PipStep_Remarks="------ Pip Distance. 30 pips is 30 pips for all brokers -------";
extern double     PipStep= 50;

extern string MoveSL2BE_Remarks="------ Move Prev order SL to BE ------";
extern bool  MoveSL2BE = true;

extern string InitialBuyOrder_Remarks="------ First Order on start, if true its LONG ------";
extern bool  InitialBuyOrder = true;

extern string UseTradingHours_Remarks="------ Place orders only during these hours. however closing can happen any time ------";
extern bool   UseTradingHours     =    false;
extern int    StartHour           =       7;
extern int    FinishHour          =      19;

extern string ProfitPct_Remarks="------ Profit Percentage ------";
extern double     ProfitPct= 1.0;


extern int    MagicNumber         =  1771611;

int longOrderCount=0;
int shortOrderCount=0;
double DollarProfitTarget;
double valLastFlatEq;
double valTargetFlatEq;
bool CloseAll;
bool longBias=false;
int totalOrders=0;
//double currentPL=0;

double startPrice=0.0;

double avgEntryPrice=0.0;

int    digits=4;
double pip = 0;

bool initOrdersDone=false;

int init()
  {
   digits=MarketInfo(Symbol(),MODE_DIGITS); 
 
  if (digits < 4)
    pip = 0.01;
  else
    pip = 0.0001;
    
   resetAccountCalc();
   
   return(0);
  }

int deinit()
  {
//---- TODO: add your code here
   deleteAll();
//----
   return(0);
  }
  

void deleteAll(){ 
 
  for(int j = 0; j <= 10000; j++){
      ObjectDelete("Comment" + j);     
      
   } 
     
  }


int start()
  {
        //DollarProfitTarget = StrToDouble(DoubleToStr(AccountEquity()*ProfitPct/100.0,2));
        if(startPrice ==0.0) {
            startPrice=iOpen( NULL, PERIOD_M15, 0);
            Print("----- startPrice ...... "+startPrice);
        }
        
        if(!initOrdersDone && inTradingHours()){
         
          
          placeBuyStopOrder();
          placeSellStopOrder();
          if(InitialBuyOrder) {
            longBias=true;
            placeInitBuyOrder();
          }else{
            placeInitSellOrder();
            longBias=false;
          }
            
          initOrdersDone=true;
        }
     
      double currentPL = getEAPL();
     
     
      //  if (!CloseAll && currentPL > DollarProfitTarget)
        
        if (!CloseAll && currentPL+AccountBalance() > valTargetFlatEq)
         {           
           Print("================ closing ================");
           Print("currentPL "+currentPL+" AccountBalance "+AccountBalance()+" valTargetFlatEq "+valTargetFlatEq);
            CloseAllTrades();
            return(0);
         }
     
        
        if(inTradingHours()) { 
     
            if(iClose( NULL, PERIOD_M15, 0) > (startPrice + PipStep*pip)){
              longBias=true;
              startPrice=iClose( NULL, PERIOD_M15, 0);
               Print("----- moved to startPrice ...... "+startPrice);
              // need to set the sl to BE of last order
              if(MoveSL2BE)   moveSellOrderToBE();
              CloseAllPendingOrders();
              // place buy limit order
              placeBuyStopOrder();  
              placeSellStopOrder();      
          
           
            }else if(iClose( NULL, PERIOD_M15, 0) < (startPrice - PipStep*pip)){
             longBias=false;
              startPrice=iClose( NULL, PERIOD_M15, 0);
              Print("------ moved to startPrice ...... "+startPrice);
              // need to set the sl to BE of last order
             if(MoveSL2BE) moveBuyOrderToBE();  
             CloseAllPendingOrders();
              // place buy limit order
              placeSellStopOrder();
              placeBuyStopOrder();
                 
          
            }
        }
        
        printComment(currentPL);
               
        
      
    }
    
    bool inTradingHours(){
    
      bool returnVal=true;
       
       if (UseTradingHours)
      {
          if (TimeHour(iTime( NULL, 0, 0) ) < StartHour &&
            ((TimeHour(iTime( NULL, 0, 0)) > FinishHour && FinishHour != 0) ||
            (TimeHour(iTime( NULL, 0, 0)) < 24 && FinishHour == 0)))
           {
               returnVal=false;
           }
           
       }
    
     return(returnVal);
    }
    
    
    void printComment(double currentPL){
    
        
        double curPL = -valLastFlatEq + AccountBalance()+currentPL;
        double drawdown = (curPL/valLastFlatEq)*100;
     
        string commentSt = "\nEquity "+DoubleToStr(valLastFlatEq,0) + " Dollar Profit "+DoubleToStr(DollarProfitTarget,2)+" PL " +DoubleToStr(curPL,2)
        +" DD "+DoubleToStr(drawdown,2);
        
        ObjectCreate("CommentStr",OBJ_LABEL,0,0,0);
         ObjectSet("CommentStr",OBJPROP_XDISTANCE,5);
         ObjectSet("CommentStr",OBJPROP_YDISTANCE,40);
         ObjectSet("CommentStr",OBJPROP_WIDTH,5);
         ObjectSetText("CommentStr",commentSt,12,"Arial",DodgerBlue);
         
         string bias="Short";
         double targetPrice=0;
         if(longBias) {
           bias="Long";
           if(totalOrders>0) {
           
            //  Print("profit "+DollarProfitTarget+" totalOrders "+totalOrders+" LotSize "+LotSize+" qty "+MarketInfo(Symbol(), MODE_LOTSIZE) +" avgEntryPrice "+avgEntryPrice+
             // " bottom "+totalOrders*LotSize*MarketInfo(Symbol(), MODE_LOTSIZE) +" top "+(DollarProfitTarget*pip));
              
               targetPrice = (DollarProfitTarget/(totalOrders*LotSize*MarketInfo(Symbol(), MODE_LOTSIZE)))+avgEntryPrice;
            }
          }else {
              if(totalOrders>0) {
            //  Print("totalOrders "+totalOrders+" LotSize "+LotSize+" qty "+MarketInfo(Symbol(), MODE_LOTSIZE) +" avgEntryPrice "+avgEntryPrice);
              
               targetPrice =  -(DollarProfitTarget/(totalOrders*LotSize*MarketInfo(Symbol(), MODE_LOTSIZE)))+avgEntryPrice;
            }
          }
         
         string commentSt1 = "\nBias "+bias + ", Orders "+totalOrders+", Target " +DoubleToStr(targetPrice,4) ;
        
        ObjectCreate("CommentStr1",OBJ_LABEL,0,0,0);
         ObjectSet("CommentStr1",OBJPROP_XDISTANCE,5);
         ObjectSet("CommentStr1",OBJPROP_YDISTANCE,60);
         ObjectSet("CommentStr1",OBJPROP_WIDTH,5);
         ObjectSetText("CommentStr1",commentSt1,12,"Arial",MediumSpringGreen);
    
    }
    
    
    void resetAccountCalc(){
      DollarProfitTarget = StrToDouble(DoubleToStr(AccountBalance()*ProfitPct/100.0,2)); 
      valLastFlatEq = AccountBalance(); 
      valTargetFlatEq =   valLastFlatEq +  DollarProfitTarget;
      Print("\nDollarProfitTarget ...... "+DollarProfitTarget+" valLastFlatEq "+valLastFlatEq);
    }
    
    
//+------------------------------------------------------------------+
//|                 Close all of the open trades.                    |
//+------------------------------------------------------------------+

void CloseAllTrades()
{
 
    for(int i = 0; i < OrdersTotal(); i++)
    {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);

      if (OrderMagicNumber() == MagicNumber)
      {
        if (OrderType() == OP_BUY)
        {
          Comment("In full closure mode.  Closing a ", OrderSymbol(), " buy trade...");
          Print("======================================================== In full closure mode.  Closing a ", OrderSymbol(), " buy trade...");
          bool closeStatus = OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(), MODE_BID),Slippage,Blue);
           if (!closeStatus) {
             Print("Close Buy failed with error #",GetLastError());  
              int check=GetLastError();
               if(check!=0) Print("Close Buy failed with error: ",ErrorDescription(check));             
          }
          Sleep(100);
        }    
        else
        if (OrderType() == OP_SELL)
        {
          Comment("In full closure mode.  Closing a ", OrderSymbol(), " sell trade...");
          Print("============================================================In full closure mode.  Closing a ", OrderSymbol(), " sell trade...");
          closeStatus = OrderClose(OrderTicket(),OrderLots(),MarketInfo(OrderSymbol(), MODE_ASK),Slippage,Red);
           if (!closeStatus) {
              Print("Close Sell failed with error #",GetLastError());    
               check=GetLastError();
               if(check!=0) Print("Close Sell failed with error: ",ErrorDescription(check));           
          }
          Sleep(100);
        }else if (OrderType() == OP_BUYSTOP)
        {
          Comment("In full closure mode.  Closing pending order ", OrderSymbol(), " buy trade...");
          Print("======================================================== In full closure mode.  Closing a ", OrderSymbol(), " buy trade...");
          closeStatus = OrderDelete(OrderTicket(),Blue);
          if (!closeStatus) {
              Print("Close Buy Stop failed with error #",GetLastError());       
               check=GetLastError();
               if(check!=0) Print("Close Buy Stop failed with error: ",ErrorDescription(check));         
          }
          Sleep(100);
        }    
        else
        if (OrderType() == OP_SELLSTOP)
        {
          Comment("In full closure mode.  Closing pending order ", OrderSymbol(), " sell trade...");
          Print("============================================================In full closure mode.  Closing a ", OrderSymbol(), " sell trade...");
          closeStatus=OrderDelete(OrderTicket(),Red);
          if (!closeStatus) {
              Print("Close Sell Stop with error #",GetLastError());   
               check=GetLastError();
               if(check!=0) Print("Close Sell Stop with error: ",ErrorDescription(check));             
          }
          Sleep(100);
        }
      }
    }
    Sleep(10000);
    resetAccountCalc();
    startPrice=iOpen( NULL, PERIOD_M15, 0);
    Print("resetting startPrice ...... "+startPrice);
   initOrdersDone=false;
  
}


void CloseAllPendingOrders()
{
 
    for(int i = 0; i < OrdersTotal(); i++)
    {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);

      if (OrderMagicNumber() == MagicNumber)
      {
       // Print(" order type "+OrderType());
         if (OrderType() == OP_BUYSTOP)
        {
          Comment("..... Closing pending order ", OrderSymbol(), " buy trade...");
          Print("========================================================  Closing a ", OrderSymbol(), " buy stop order...");
          bool closeStatus = OrderDelete(OrderTicket(),Blue);
          if (!closeStatus) {
              Print("Close Buy Stop failed with error #",GetLastError());       
               int check=GetLastError();
               if(check!=0) Print("Close Buy Stop failed with error: ",ErrorDescription(check));         
          }
          Sleep(100);
        }    
        else
        if (OrderType() == OP_SELLSTOP)
        {
          Comment("..... Closing pending order ", OrderSymbol(), " sell trade...");
          Print("=========================================================== Closing a ", OrderSymbol(), " sell stop order...");
          closeStatus=OrderDelete(OrderTicket(),Red);
          if (!closeStatus) {
              Print("Close Sell Stop with error #",GetLastError());   
               check=GetLastError();
               if(check!=0) Print("Close Sell Stop with error: ",ErrorDescription(check));             
          }
          Sleep(100);
        }
      }
    }
     
}

double getEAPL()
{
   double overallPL;
   totalOrders=0;
   
   avgEntryPrice=0;
   double totalEntryPrice=0.0;
   
   
    for(int i = 0; i < OrdersTotal(); i++)
    {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
     
      if (OrderMagicNumber() == MagicNumber && ( (OrderType() == OP_BUY)||(OrderType() == OP_SELL)))
      {      
      
       double curPL = OrderProfit() + OrderSwap() + OrderCommission();
       double openPrice = OrderOpenPrice();
     //  Print(" openPrice "+openPrice);
       totalEntryPrice = totalEntryPrice + openPrice;
      //  Print(" totalEntryPrice "+totalEntryPrice);
       // Print("curPL "+curPL);
       overallPL = overallPL + curPL;
       totalOrders++;
       
       }
    }
    
    if(totalOrders>0) {
      avgEntryPrice = totalEntryPrice/totalOrders;
      //Print(" avgEntryPrice "+avgEntryPrice+" totalOrders "+totalOrders+" totalEntryPrice "+totalEntryPrice);
    }
   
  
  return(overallPL);
}

        
     void placeSellStopOrder(){
     
          double bid = MarketInfo(Symbol(), MODE_BID) ; 
          
          double sellPrice = bid - (PipStep*pip);          
          double sellSL = bid;
          
          
          Print("Placing sell stop order at "+sellPrice+" with SL at "+sellSL);
         
         int  ticketshort = OrderSend(Symbol(),OP_SELLSTOP,LotSize,NormalizeDouble(sellPrice, digits),
                                      Slippage,NormalizeDouble(sellSL, digits),0,"Short Speed Train",MagicNumber,0,Red);
                                      
          if (ticketshort < 0) {
            //  Print("Short OrderSend OP_SELLSTOP failed with error #",GetLastError()); 
              int check=GetLastError();
               if(check!=0) Print("Short OrderSend OP_SELLSTOP failed with error: ",ErrorDescription(check));    
              return;
          }
          
          Print("Sell stop order placed with ticket "+ticketshort+"  at "+sellPrice);
          
          shortOrderCount++;
            //Sleep(1000);          
            
         
     
     }
       
     void placeBuyStopOrder(){
     
          double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
          double buyPrice = ask + (PipStep*pip);          
          double buySL = ask;
          
          
          Print("Placing buy stop order at "+buyPrice+" with SL at "+buySL);
         
         int  ticketlong = OrderSend(Symbol(),OP_BUYSTOP,LotSize,NormalizeDouble(buyPrice, digits),
                                      Slippage,NormalizeDouble(buySL, digits),0,"Long Speed Train",MagicNumber,0,Blue);
                                      
          if (ticketlong < 0) {
              //Print("Long OrderSend OP_BUYSTOP failed with error #",GetLastError()); 
              
              int check=GetLastError();
               if(check!=0) Print("Long OrderSend OP_BUYSTOP failed with error: ",ErrorDescription(check));

              
              return;
          }
          
          Print("Buy stop order placed with ticket "+ticketlong+"  at "+buyPrice);
          
          longOrderCount++;
           // Sleep(1000);          
            
         
     
     }
      
      
      void placeInitBuyOrder(){
     
          double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
          double buySL = ask - (PipStep*pip);          
         
          
          
          Print("Placing buy  order at "+ask+" with SL at "+buySL);
         
         int  ticketlong = OrderSend(Symbol(),OP_BUY,LotSize,Ask,
                                      Slippage,0,0,"Long Speed Train",MagicNumber,0,Blue);
                                      
          if (ticketlong < 0) {
             // Print("Long OrderSend OP_BUY failed with error #",GetLastError()); 
               int check=GetLastError();
               if(check!=0) Print("Long OrderSend OP_BUY failed with error: ",ErrorDescription(check));  
              return;
          }
          
          Print("Buy stop order placed with ticket "+ticketlong+"  at "+ask);
          
           int ticketlong1 = OrderModify(ticketlong,OrderOpenPrice(),NormalizeDouble(buySL, digits),0,0,Blue);
            Sleep(1000);
            
           // Print("ticketlong1 "+ticketlong1);
          
             if (ticketlong1 < 1) {
             // Print("Long OrderModify failed with error #",GetLastError()); 
               check=GetLastError();
              // Print("check "+check);
               if(check!=0) Print("Long OrderModify failed with error: ",ErrorDescription(check));
            }
            
          
          longOrderCount++;        
     
     }
      
      
      void placeInitSellOrder(){
     
          double bid = MarketInfo(Symbol(), MODE_BID) ; 
          
          double sellSL = bid + (PipStep*pip);            
          
          Print("Placing sell  order at "+bid+" with SL at "+sellSL);
         
         int  ticketshort = OrderSend(Symbol(),OP_SELL,LotSize,NormalizeDouble(bid, digits),
                                      Slippage,0,0,"Short Speed Train",MagicNumber,0,Red);
                                      
          if (ticketshort < 0) {
              //Print("Short OrderSend OP_SELL failed with error #",GetLastError()); 
               int check=GetLastError();
               if(check!=0) Print("Short OrderSend OP_SELL failed with error: ",ErrorDescription(check));
              return;
          }
          
          Print("Sell stop order placed with ticket "+ticketshort+"  at "+bid);
          
          int ticketshort1 = OrderModify(ticketshort,OrderOpenPrice(),NormalizeDouble(sellSL, digits),0,0,Red);
            Sleep(1000);
          
             if (ticketshort1 < 1) {
            //  Print("Short OrderModify failed with error #",GetLastError()); 
               check=GetLastError();
               if(check!=0) Print("Short OrderModify failed with error: ",ErrorDescription(check));
            }
          
          shortOrderCount++;        
     
     }
      
      
      void moveBuyOrderToBE(){
         int lastTicket=0;
      
         for(int j = 0; j < OrdersTotal(); j++)
        {
            OrderSelect(j,SELECT_BY_POS,MODE_TRADES);
           // Print("order type "+OrderType());
            
            if (OrderMagicNumber() == MagicNumber && OrderType() == OP_BUYSTOP)
               {
                  int ticketId = OrderTicket() ;
                  //Print("ticketId "+ticketId);
                 if( ticketId>   lastTicket) lastTicket=ticketId;  
                  
                 //Print("lastTicket "+lastTicket);            
               }      
       }
       
       if(lastTicket >0) {             
            Print(" ----------------------- Moving long order number "+lastTicket+ " to BE at  "+OrderOpenPrice());    
       
            int ticketlong = OrderModify(lastTicket,OrderOpenPrice(),OrderOpenPrice(),0,0,Blue);
          
                                      
            if (ticketlong < 0) {
                 //  Print("Long SL2BE order failed with error #",GetLastError()); 
                   int check=GetLastError();
               if(check!=0) Print("Long SL2BE order failed with error: ",ErrorDescription(check));
                   return;
            }
              
           
        }           
         
     
     }
     
     
     
         
      
      
      
      void moveSellOrderToBE(){
         int lastTicket=0;
      
         for(int j = 0; j < OrdersTotal(); j++)
        {
            OrderSelect(j,SELECT_BY_POS,MODE_TRADES);

           // Print("order type "+OrderType());
            if (OrderMagicNumber() == MagicNumber && OrderType() == OP_SELLSTOP)
               {
                  int ticketId = OrderTicket() ;
                 // Print(" ticketId "+ticketId);    
                  if( ticketId>   lastTicket) lastTicket=ticketId; 
                 // Print(" lastTicket "+lastTicket);             
               }      
       }
       
          
       
       
       
       if(lastTicket>0) {
       
            Print("------------ Moving short order number "+lastTicket+ " to BE at  "+OrderOpenPrice());
            int ticketshort = OrderModify(lastTicket,OrderOpenPrice(),OrderOpenPrice(),0,0,Red);          
                                      
            if (ticketshort < 0) {
                   //Print("Short SL2BE order failed with error #",GetLastError()); 
                     int check=GetLastError();
               if(check!=0) Print("Short SL2BE order failed with error: ",ErrorDescription(check));
                   return;
            }
             
        }
          
                        
         
     
     }
      
     
  
   


