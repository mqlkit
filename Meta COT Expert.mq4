//+------------------------------------------------------------------+
//|                                       Meta COT Net Positions.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+
#property copyright "Copyright  2009, C-4, All Rights Reserved."
#property link      "vs-box@mail.ru"
#property show_inputs
extern int  period=26;
extern int  indicator_tape=21; //     cotlib.mq4   defines
int movement_index=6;
#include <cotlib.mq4>

int handle;
int buy_level_down;
int buy_level_up;
int sell_level_down;
int sell_level_up;
double ticksize;
int init(){
   //handle=FileOpen("COT - U.S. DOLLAR CONCATENATE.csv",FILE_READ|FILE_CSV);
   //if(handle==-1)Print("   ");
   if(init_data()==false)error=true;
   if(error==false)load_data();
   if(error==false)count_data();
   //if(error==true)Print("---------->>>>>>> ERORR!!! <<<<<<<<<--------------");
   setparam();
}

int start()
{
   if(Volume[0]==1){
      if(OrdersTotal()>0)return;
      if(buy_level_up==0)return;
      if(type_op()==OP_BUY&&delta_fma()>0&&delta_sma()>0){
         OrderSend(Symbol(),OP_BUY,0.1,Ask,3,Ask-ticksize*100*(period/26),Ask+ticksize*300*(period/26),"",12345,0,Blue);
      }
      if(type_op()==OP_SELL&&delta_fma()<0&&delta_sma()<0){
         OrderSend(Symbol(),OP_SELL,0.1,Bid,3,Ask+ticksize*100*(period/26),Ask-ticksize*300*(period/26),"",12345,0,Red);
      }
      //Print(get_data(WILLCO_OPERATORS, 0));
   }
}

double delta_fma()
{
   //return(iMA(Symbol(),0,5+(1-(period/26)),0,MODE_SMA,PRICE_CLOSE,0)-
   //       iMA(Symbol(),0,5+(1-(period/26)),0,MODE_SMA,PRICE_CLOSE,2));
   return(iMA(Symbol(),0,5,0,MODE_SMA,PRICE_CLOSE,0)-
          iMA(Symbol(),0,5,0,MODE_SMA,PRICE_CLOSE,2));
}

double delta_sma()
{
   return(iMA(Symbol(),PERIOD_W1,period,0,MODE_SMA,PRICE_CLOSE,0)-
          iMA(Symbol(),PERIOD_W1,period,0,MODE_SMA,PRICE_CLOSE,2));
}

void setparam()
{
   switch(indicator_tape){
      case WILLCO_OPERATORS:        // 17
      case INDEX_OI:                // 19
      case INDEX_OPERATORS:         // 21
           buy_level_down=80;
           buy_level_up=100;
           sell_level_down=0;
           sell_level_up=20;
      case WILLCO_NONCOMM:          // 16
      case WILLCO_NONREP:           // 18
      case INDEX_NONCOMM:           // 20
      case INDEX_NONREP:            // 22
           buy_level_down=0;
           buy_level_up=20;
           sell_level_down=80;
           sell_level_up=100;
      case MOVEMENT_NONCOMM:        // 23
      case MOVEMENT_NONREP:         // 25
      case MOVEMENT_OI:             // 26
           /*buy_level_down=-100;
           buy_level_up=-40;
           sell_level_down=40;
           sell_level_up=100;*/
           buy_level_down=40;
           buy_level_up=100;
           sell_level_down=-100;
           sell_level_up=-40;
      case MOVEMENT_OPERATORS:      // 24
           buy_level_down=40;
           buy_level_up=100;
           sell_level_down=-100;
           sell_level_up=-40;
   }
   ticksize=MarketInfo(Symbol(), MODE_TICKSIZE);
}

int type_op()
{
   if(get_data(indicator_tape,0)>=buy_level_down&&get_data(indicator_tape,0)<=buy_level_up)
      return(OP_BUY);
   if(get_data(indicator_tape,0)>=sell_level_down&&get_data(indicator_tape,0)<=sell_level_up)
      return(OP_SELL);
   else return(-1);
}
