For correct installation of the Meta COT project follow the instruction:
1. Unzip archive MetaCOT in a temp folder (in it should there will be a folder experts).
2. Copy directory contents '\experts\files' in '..\MetaTrader\experts\files' and '..\MetaTrader\tester\files'
3. Copy a file '\experts\include\cotlib.mq4' in '..\MetaTrader\experts\include\cotlib.mq4'.
4. Copy directory contents '\experts\indicators' in '..\MetaTrader\experts\indicators'.
5. Copy directory contents '\experts\scripts' in '..\MetaTrader\experts\scripts'.
6. Copy a file '\experts\Meta COT Expert.mq4' in '..\MetaTrader\experts\Meta COT Expert.mq4'
Directory '..\MetaTrader\'- the directory in which is installed your terminal MetaTrader 4, for example: 'C:\Programm Files\Meta Trader\'.
7. Restart the terminal (Under Windows Vista and Windows 7 your should start the terminal with the Administration privilege!!!).
8. Download files of reports under futures and options in format CSV.
9. Unzip them in the directory '.\Meta Trader\experts\files'
10. Rename the report for 2009 in '2009_Futures-and-Options.txt', the report for 2008 in '2009_Futures-and-Options.txt' etc.
11. Start script 'Meta COT Script Build'.
12. Start script 'Meta COT Script Concatenate'.
13. Start any indicator.
14. Before upgrade of the data start a file erase_cot.bat (it is in folders ' experts\files \'). This program will  erase all files with reports. After removal of all files pass to item 8.

Vasiliy Sokolov, St.-Petersburg, Russia, 2009