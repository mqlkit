//+------------------------------------------------------------------+
//|                                              Meta COT cotlib.mq4 |
//|   Copyright  2009, C-4 (Vasiliy Sokolov, Russia, St.-Petersburg,|
//|                                      2009), All Rights Reserved. |
//|                                                   vs-box@mail.ru |
//+------------------------------------------------------------------+
#property copyright "Copyright  2009, C-4 (Vasiliy Sokolov, SPb), All Rights Reserved."
#property link      "vs-box@mail.ru"

//    "type"  get_data()
#define OI                    0
#define NONCOMM_LONG          1
#define NONCOMM_SHORT         2
#define OPERATORS_LONG        3
#define OPERATORS_SHORT       4
#define NONREP_LONG           5
#define NONREP_SHORT          6
#define NET_NONCOMM           7
#define NET_OPERATORS         8
#define NET_NONREP            9
#define OI_NONCOMM_LONG       10
#define OI_NONCOMM_SHORT      11
#define OI_OPERATORS_LONG     12
#define OI_OPERATORS_SHORT    13
#define OI_NONREP_LONG        14
#define OI_NONREP_SHORT       15
#define WILLCO_NONCOMM        16
#define WILLCO_OPERATORS      17
#define WILLCO_NONREP         18
#define INDEX_OI              19
#define INDEX_NONCOMM         20
#define INDEX_OPERATORS       21
#define INDEX_NONREP          22
#define MOVEMENT_NONCOMM      23
#define MOVEMENT_OPERATORS    24
#define MOVEMENT_NONREP       25
#define MOVEMENT_OI           26
#define OI_NET_NONCOMM        27
#define OI_NET_OPERATORS      28
#define OI_NET_NONREP         29


extern bool   load_cot_file=false;
extern string cot_file="COT - U.S. DOLLAR CONCATENATE.csv";
extern string settings="settings.ini";
string name;
bool error=false;
int column;
bool DrawData=true;
bool LoadData=true;


//***************************************************************************************************************
//                                       COT
int n_str;                    // - 

//   
datetime realize_data[];
//     
double open_interest[];       //  
double noncomm_long[];        //    
double noncomm_short[];       //    
double noncomm_spread[];      //   
double operators_long[];      //     ()
double operators_short[];     //     ()
double nonrep_long[];         //     ()
double nonrep_short[];        //     ()

//                 
double oi_noncomm_long[];     //   /    
double oi_noncomm_short[];    //   /    
double oi_operators_long[];   //   /     ()
double oi_operators_short[];  //   /     ()
double oi_nonrep_long[];      //   /     ()
double oi_nonrep_short[];     //   /     ()

//             
//    WILLCO
double oi_net_noncomm[];
double oi_net_operators[];
double oi_net_nonrep[];

//      ,     
double net_noncomm[];         //    
double net_operators[];       //    
double net_nonrep[];          //    

//         
double index_oi[];            //   
double index_ncomm[];         //   
double index_operators[];     //    ()
double index_nonrep[];        //    ()

//                
// Stohastic(OI/NET_POSITION)
double willco_ncomm[];        // 
double willco_operators[];    //
double willco_nonrep[];       //

//MOVEMENT INDEX
double movement_oi[];
double movement_ncomm[];
double movement_operators[];
double movement_nonrep[];
//***********************************************************************************************************************************

//
bool init_data()
{
   string data;
   int handle_cotfile;
   int str;
   //settings_load();
   settings_load();
   handle_cotfile=FileOpen(cot_file,FILE_READ|FILE_CSV);
   //handle_cotfile=FileOpen("COT - U.S. DOLLAR CONCATENATE.csv",FILE_READ|FILE_CSV);
   if(handle_cotfile==-1){
      Print("   .    ", cot_file);
      Print(GetLastError());
      return(false);
   }
   while(FileIsEnding(handle_cotfile)==false){
      data=FileReadString(handle_cotfile);
      if(FileIsLineEnding(handle_cotfile)&&data!="")str++;
   }
   ArrayResize(realize_data,str);
   
   ArrayResize(open_interest,str);
   ArrayResize(noncomm_long,str);
   ArrayResize(noncomm_short,str);
   ArrayResize(noncomm_spread,str);
   ArrayResize(operators_long,str);
   ArrayResize(operators_short,str);
   ArrayResize(nonrep_long,str);
   ArrayResize(nonrep_short,str);
   
   ArrayResize(oi_noncomm_long,str);
   ArrayResize(oi_noncomm_short,str);
   ArrayResize(oi_operators_long,str);
   ArrayResize(oi_operators_short,str);
   ArrayResize(oi_nonrep_long,str);
   ArrayResize(oi_nonrep_short,str);
   
   ArrayResize(oi_net_noncomm,str);
   ArrayResize(oi_net_operators,str);
   ArrayResize(oi_net_nonrep,str);
   
   ArrayResize(net_noncomm,str);
   ArrayResize(net_operators,str);
   ArrayResize(net_nonrep,str);
   
   ArrayResize(index_oi,str);
   ArrayResize(index_ncomm,str);
   ArrayResize(index_operators,str);
   ArrayResize(index_nonrep,str);
   
   ArrayResize(willco_ncomm,str);
   ArrayResize(willco_operators,str);
   ArrayResize(willco_nonrep,str);
   
   ArrayResize(movement_oi,str);
   ArrayResize(movement_ncomm,str);
   ArrayResize(movement_operators,str);
   ArrayResize(movement_nonrep,str);
   
   FileClose(handle_cotfile);
   return(true);
}


/*void settings_load()
{
   int h_set, _column;
   string set, sname;
   if(load_cot_file==true)return;
   h_set=FileOpen(settings,FILE_READ|FILE_CSV,";");
   //FileOpen("\Temp\Cot_file.txt",FILE_WRITE,FILE_CSV);
   if(h_set==-1)return;
   while(FileIsEnding(h_set)==false){
      set=FileReadString(h_set);
      _column++;
      if(set==Symbol()&&_column!=1){cot_file=sname;return;}
      if(_column==2&&set!="")name=set;             //   -   
      if(FileIsLineEnding(h_set)==true)_column=0;
      if(_column==1)sname=set;
      
   }
   FileClose(h_set);
}*/

//   ,    ,     
//      Broco Company.      .
//  ,        ,   ,   
// !!!
void settings_load()
{
   int h_set, _column;
   string set, sname;
   if(load_cot_file==true)return;
   h_set=FileOpen(settings,FILE_READ|FILE_CSV,";");
   //FileOpen("\Temp\Cot_file.txt",FILE_WRITE,FILE_CSV);
   if(h_set==-1)return;
   while(FileIsEnding(h_set)==false){
      set=FileReadString(h_set);
      _column++;
      if(_column==1&&set!="")sname=set;
      if(_column==2&&set!="")name=set;             //   -   
      if(_column>2&&set!=""&&StringFind(Symbol(),set)==0){cot_file=sname;return;}
      if(FileIsLineEnding(h_set)==true)_column=0;
   }
}

void load_data()
{
   datetime data_realize;
   int handle_cotfile;
   string data;
   handle_cotfile=FileOpen(cot_file,FILE_READ|FILE_CSV, ";");
   Print(" ");
   while(FileIsEnding(handle_cotfile)==false){
      data=FileReadString(handle_cotfile);
      column++;
      if(FileIsLineEnding(handle_cotfile)==true||FileIsEnding(handle_cotfile)==true){//column=12 -   
        if(column==12)
           nonrep_short[n_str]=StrToDouble(data);
        column=0;
        if(data!="")n_str++;
      }
      else{
         switch(column){
            case 2:
               realize_data[n_str]=StrToTime(data);
               break;
            case 3:
               open_interest[n_str]=StrToDouble(data);
               //Print(open_interest[n_str]);
               break;
            case 4:
               noncomm_long[n_str]=StrToDouble(data);
               break;
            case 5:
               noncomm_short[n_str]=StrToDouble(data);
               break;          
            case 6:
               noncomm_spread[n_str]=StrToDouble(data);
               break;
            case 7:
               operators_long[n_str]=StrToDouble(data);
               break;
            case 8:
               operators_short[n_str]=StrToDouble(data);
               break;
            case 11:
               nonrep_long[n_str]=StrToDouble(data);
               break;
         }
      } 
   }
   FileClose(handle_cotfile);
   Print(" ");
}

void count_data()
{
   int    max,
          min;
   double delta;
   
   for(int i=0;i<n_str;i++){
      index_oi[i]=EMPTY_VALUE;
      index_ncomm[i]=EMPTY_VALUE;
      index_operators[i]=EMPTY_VALUE;
      index_nonrep[i]=EMPTY_VALUE;
      willco_ncomm[i]=EMPTY_VALUE;
      willco_operators[i]=EMPTY_VALUE;
      willco_nonrep[i]=EMPTY_VALUE;
      movement_ncomm[i]=EMPTY_VALUE;
      movement_operators[i]=EMPTY_VALUE;
      movement_nonrep[i]=EMPTY_VALUE;
   }
   for(i=0;i<n_str;i++){
      if(open_interest[i]==0){
         oi_noncomm_long[i]=0;
         oi_noncomm_short[i]=0;
         oi_operators_long[i]=0;
         oi_operators_short[i]=0;
         oi_nonrep_long[i]=0;
         oi_nonrep_short[i]=0;
      }
      else{
         oi_noncomm_long[i]=noncomm_long[i]/open_interest[i];
         oi_noncomm_short[i]=noncomm_short[i]/open_interest[i];
         oi_operators_long[i]=operators_long[i]/open_interest[i];
         oi_operators_short[i]=operators_short[i]/open_interest[i];
         oi_nonrep_long[i]=nonrep_long[i]/open_interest[i];
         oi_nonrep_short[i]=nonrep_short[i]/open_interest[i];
      }
      
      net_noncomm[i]=noncomm_long[i]-noncomm_short[i];
      net_operators[i]=operators_long[i]-operators_short[i];
      net_nonrep[i]=nonrep_long[i]-nonrep_short[i];
      
      if(open_interest[i]==0){
         oi_net_noncomm[i]=0;
         oi_net_operators[i]=0;
         oi_net_nonrep[i]=0;
      }
      else{
         oi_net_noncomm[i]=net_noncomm[i]/open_interest[i];
         oi_net_operators[i]=net_operators[i]/open_interest[i];
         oi_net_nonrep[i]=net_nonrep[i]/open_interest[i];
      }  
   }
   
   for(i=0;i<n_str-period;i++){  
      
      max=ArrayMaximum(open_interest,period,i);
      min=ArrayMinimum(open_interest,period,i);
      delta=open_interest[max]-open_interest[min];
      if(delta==0)delta=1;
      index_oi[i]=(open_interest[i]-open_interest[min])/delta*100;
      
      max=ArrayMaximum(net_noncomm,period,i);
      min=ArrayMinimum(net_noncomm,period,i);
      delta=net_noncomm[max]-net_noncomm[min];
      if(delta==0)delta=1;
      index_ncomm[i]=(net_noncomm[i]-net_noncomm[min])/delta*100;
      
      max=ArrayMaximum(net_operators,period,i);
      min=ArrayMinimum(net_operators,period,i);
      delta=net_operators[max]-net_operators[min];
      if(delta==0)delta=1;
      index_operators[i]=(net_operators[i]-net_operators[min])/delta*100;
      
      max=ArrayMaximum(net_nonrep,period,i);
      min=ArrayMinimum(net_nonrep,period,i);
      delta=net_nonrep[max]-net_nonrep[min];
      if(delta==0)delta=1;
      index_nonrep[i]=(net_nonrep[i]-net_nonrep[min])/delta*100;
      
      max=ArrayMaximum(oi_net_noncomm,period,i);
      min=ArrayMinimum(oi_net_noncomm,period,i);
      delta=oi_net_noncomm[max]-oi_net_noncomm[min];
      if(delta==0)delta=1;
      willco_ncomm[i]=(oi_net_noncomm[i]-oi_net_noncomm[min])/delta*100;
      
      max=ArrayMaximum(oi_net_operators,period,i);
      min=ArrayMinimum(oi_net_operators,period,i);
      delta=oi_net_operators[max]-oi_net_operators[min];
      if(delta==0)delta=1;
      willco_operators[i]=(oi_net_operators[i]-oi_net_operators[min])/delta*100;
      
      max=ArrayMaximum(oi_net_nonrep,period,i);
      min=ArrayMinimum(oi_net_nonrep,period,i);
      delta=oi_net_nonrep[max]-oi_net_nonrep[min];
      if(delta==0)delta=1;
      willco_nonrep[i]=(oi_net_nonrep[i]-oi_net_nonrep[min])/delta*100;
   }
   for(i=0;i<n_str-period-movement_index;i++){
      movement_oi[i]=index_oi[i]-index_oi[i-movement_index];
      movement_ncomm[i]=index_ncomm[i]-index_ncomm[i-movement_index];
      movement_operators[i]=index_operators[i]-index_operators[i-movement_index];
      movement_nonrep[i]=index_nonrep[i]-index_nonrep[i-movement_index];
   }
}

//     COT    "type"   "bar"
double get_data(int type, int bar)
{
   double data;
   int i_data=get_cot(bar);
   if(i_data==EMPTY_VALUE)return(EMPTY_VALUE);
   switch (type){
      case OI:
         return(open_interest[i_data]);
      case NONCOMM_LONG:
         return(noncomm_long[i_data]+noncomm_spread[i_data]);
         //return(noncomm_long[i_data]);
      case NONCOMM_SHORT:
         return(noncomm_short[i_data]+noncomm_spread[i_data]);
         //return(noncomm_short[i_data]);
      case OPERATORS_LONG:
         return(operators_long[i_data]);
      case OPERATORS_SHORT:
         return(operators_short[i_data]);
      case NONREP_LONG:
         return(nonrep_long[i_data]);
      case NONREP_SHORT:
         return(nonrep_short[i_data]);
      case NET_NONCOMM:
         return(net_noncomm[i_data]);
      case NET_OPERATORS:
         return(net_operators[i_data]);
      case NET_NONREP:
         return(net_nonrep[i_data]);
      case INDEX_OI:
         return(index_oi[i_data]);
      case INDEX_NONCOMM:
         return(index_ncomm[i_data]);
      case INDEX_OPERATORS:
         return(index_operators[i_data]);
      case INDEX_NONREP:
         return(index_nonrep[i_data]);
      case OI_NONCOMM_LONG:
         return(oi_noncomm_long[i_data]);
      case OI_NONCOMM_SHORT:
         return(oi_noncomm_short[i_data]);
      case OI_OPERATORS_LONG:
         return(oi_operators_long[i_data]);
      case OI_OPERATORS_SHORT:
         return(oi_operators_short[i_data]);
      case OI_NONREP_LONG:
         return(oi_nonrep_long[i_data]);
      case OI_NONREP_SHORT:
         return(oi_nonrep_short[i_data]);
      case WILLCO_NONCOMM:
         return(willco_ncomm[i_data]);
      case WILLCO_OPERATORS:
         return(willco_operators[i_data]);
      case WILLCO_NONREP:
         return(willco_nonrep[i_data]);
      case OI_NET_NONCOMM:
         return(oi_net_nonrep[i_data]);
      case OI_NET_OPERATORS:
         return(oi_net_operators[i_data]);
      case OI_NET_NONREP:
         return(oi_net_nonrep[i_data]);
      case MOVEMENT_NONCOMM:
         return(movement_ncomm[i_data]);
      case MOVEMENT_OPERATORS:
         return(movement_operators[i_data]);
      case MOVEMENT_NONREP:
         return(movement_nonrep[i_data]);
      case MOVEMENT_OI:
         return(movement_oi[i_data]);
   }
   return(EMPTY_VALUE);
}

//    ,     ,   EMPTY_VALUE,     
int get_cot(int i)
{
   datetime tbar=iTime(Symbol(),0,i);
   for(int k=0;k<n_str;k++)
      if(realize_data[k]<tbar)return(k);
   return(EMPTY_VALUE);
}

//        COT 
int get_lastdata()
{
   return(iBarShift(Symbol(),0,realize_data[n_str-1],false));
}

//       6  ( "COT - FileName.csv"  "FileName")
string str_trim(string fname)
{
   int strlen=StringLen(fname)-1;
   int char;
   string str;
   for(int i=strlen;i>0;i--){
      char=StringGetChar(fname,i);
      if(char=='.'){
         for(int b=6;b<i;b++)//  b=0      6 
            str=str+CharToStr(StringGetChar(fname,b));
         return(str);
      }
   }
   return(fname);
}
