//+------------------------------------------------------------------+
//|                                                   MatrixMath.mq4 |
//|                                    Copyleft� 2007, GammaRatForex |
//|                                       http://gwmmarat.com/Forex/ |
//+------------------------------------------------------------------+
#property copyright "Copyleft� 2007, GammaRatForex"
#property link      "http://gwmmarat.com/Forex/"

//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2005

//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);

// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import

//+------------------------------------------------------------------+
//| EX4 imports                                                      |
//+------------------------------------------------------------------+
//#import "stdlib.ex4"
#import "GRFMatrixMath.ex4"
   double determinant(double a[][]);
   int MatrixAdd(double a[][], double b[][],double &c[][],double w=1);
   int MatrixMul(double a[][], double b[][],double & c[][]);
   int MatrixScalarMul(double b,double & a[][]);
   int MatrixInvert(double a[][],double & b[][]);
   int MatrixTranspose(double a[][], double & a_trans[][]);
   int MatrixZero(double & a[][]);
   int MatrixEye(double & a[][]);
   int MatrixPrint(double a[][]);
#import
//+------------------------------------------------------------------+