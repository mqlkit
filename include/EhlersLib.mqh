//+------------------------------------------------------------------+
//|                                                    EhlersLib.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      ""

//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2005

//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);

// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import

//+------------------------------------------------------------------+
//| EX4 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex4"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+

double gdAlpha = 0;

void
iEhlersMAOnArray(
     double &in_signal_buffer[], 
     double &in_ema_buffer[],
     int in_buffer_length, 
     double &out_ec_buffer[], 
     int out_buffer_length, 
     int ma_period,
     int ma_gain_limit,
     int value_index)
{
    int    i, j, iCounted, iLimit;
    double dGain, dBestGain, dError, dLeastError, dEC;
    
    iCounted = IndicatorCounted();
    if(iCounted > 0) iCounted--;
    iLimit = Bars - iCounted;
    if (iLimit > out_buffer_length) iLimit = out_buffer_length;

    if (in_buffer_length < 1 || value_index >= in_buffer_length) return (0.0);
    
    gdAlpha = 2.0 / (ma_period + 1.0);
    
    for(i = iLimit-1; i >= 0; i--)
    {
        double currentPrice = in_signal_buffer[i];
        in_ema_buffer[i] = gdAlpha * currentPrice + (1.0 - gdAlpha) * in_ema_buffer[i+1];
        dLeastError = 1000000.0;
        for(j = -ma_gain_limit; j <= ma_gain_limit; j++)
        {
            dGain = j / 10.0;
            dEC = gdAlpha * (in_ema_buffer[i] + dGain * (currentPrice - out_ec_buffer[i+1])) + (1.0 - gdAlpha) * out_ec_buffer[i+1];
            dError = currentPrice - dEC;
            if(MathAbs(dError) < dLeastError)
            {
                dLeastError = MathAbs(dError);
                dBestGain = dGain;
            }
        }
        out_ec_buffer[i] = gdAlpha * (in_ema_buffer[i] + dBestGain * (currentPrice - out_ec_buffer[i+1])) + (1.0 - gdAlpha) * out_ec_buffer[i+1];
        
        if (i == value_index) break;
    }
}