//+------------------------------------------------------------------+
//|                              Lets Go Where Price Goes EA, v1.0.0 |
//|                              Copyright � 2011 |
//|                                                |
//+------------------------------------------------------------------+
// modified by WL Tam
#property copyright "Copyright � 2011, trapule@gmail.com"
#property link      "trapule@gmail.com"

extern string info1        = "Magic number";
extern int magicnumber     = 20110110;
extern string info2        = "Pip distance between levels";
extern int pipstep         = 20;
extern string info3        = "Fixed lot for each order";
extern double fixedlot     = 0.1;
extern int maxpip          = 80;




double pip,refup, refdown;
int i;

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
{
  if (Digits==2 || Digits==3) pip=0.01;
  else if (Digits==4 || Digits==5) pip=0.0001;
  return(0);
}

//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
{
   return(0);
}



//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
{
// no pending order or no active order
   if ( count(OP_BUY) + count(OP_SELL) + count(OP_BUYSTOP) + count(OP_SELLSTOP) == 0)
   {

      if (iMA(Symbol(),PERIOD_M5,21,0,MODE_EMA,PRICE_CLOSE,0) < (Ask/2 + Bid /2))
      {
         refup = Ask + 0.5*pipstep*pip;
         refdown = refup - pipstep*pip;
         Print(iMA(Symbol(),PERIOD_M5,21,0,MODE_EMA,PRICE_CLOSE,0),"  BUY ", (Ask/2 + Bid /2));
         Print("refup =",refup,"    || refdown =",refdown);
         OrderSend (Symbol(), OP_BUYSTOP, fixedlot,refup , 100, refdown, 0.0, "Lets Go Where Price Goes", magicnumber,0,Blue);
      }
      if (iMA(Symbol(),PERIOD_M5,21,0,MODE_EMA,PRICE_CLOSE,0) > (Ask/2 + Bid /2))
      {
         refdown = Bid - 0.5*pipstep*pip;
         refup = refdown + pipstep*pip;      
         Print(iMA(Symbol(),PERIOD_M5,21,0,MODE_EMA,PRICE_CLOSE,0),"  SELL ", (Ask/2 + Bid /2));
         Print("refup =",refup,"    || refdown =",refdown);
         OrderSend (Symbol(), OP_SELLSTOP, fixedlot, refdown, 100, refup, 0.0, "Lets Go Where Price Goes", magicnumber,0,Red);
      }
   }
// no active order but trend changed
   if ( count(OP_BUY) + count(OP_SELL) == 0)
   {
      if( count(OP_BUYSTOP)== 1 && iMA(Symbol(),PERIOD_M5,21,0,MODE_EMA,PRICE_CLOSE,0) > (Ask/2 + Bid /2))
      {
         Print("trend from buy to sell");
         OrderSelect(0, SELECT_BY_POS);
         OrderDelete(OrderTicket());        
      }
      if( count(OP_SELLSTOP)== 1 && iMA(Symbol(),PERIOD_M5,21,0,MODE_EMA,PRICE_CLOSE,0) < (Ask/2 + Bid /2))
      {
         Print("trend from sell to buy");
         OrderSelect(0, SELECT_BY_POS);
         OrderDelete(OrderTicket());        
      }      
   }

// no pending order  
   if( (count(OP_BUYSTOP) + count(OP_SELLSTOP)) == 0 && (count(OP_BUY)+count(OP_SELL))>0)
   
   {
      if(count(OP_BUY)>0)
      {
         refup    = refup + pipstep*pip;
         refdown  = refup - pipstep*pip;
         OrderSend (Symbol(), OP_BUYSTOP, fixedlot,refup , 100, refdown, 0.0, "Lets Go Where Price Goes", magicnumber,0,Blue);
      }   
      if(count(OP_SELL)>0)
      {
         refup = refup - pipstep*pip;
         refdown = refup - pipstep*pip; 
         OrderSend (Symbol(), OP_SELLSTOP, fixedlot, refdown, 100, refup, 0.0, "Lets Go Where Price Goes", magicnumber,0,Red);
           
      }
   
      for (i=OrdersTotal()-1; i>=0; i--)
      {
         OrderSelect(i, SELECT_BY_POS);
         if(OrderType() == OP_BUY && OrderMagicNumber()==magicnumber && OrderSymbol()==Symbol())
         {
            OrderModify (OrderTicket(), OrderOpenPrice(),refdown -   pipstep*pip, 0.0, 0,LightBlue);
         }
         if(OrderType() == OP_SELL && OrderMagicNumber()==magicnumber && OrderSymbol()==Symbol())
         {
            OrderModify (OrderTicket(), OrderOpenPrice(), refup +   pipstep*pip, 0.0, 0,Tomato);
         }   
      }      

   } 
   
   
      for (i=OrdersTotal()-1; i>=0; i--)
      {
         OrderSelect(i, SELECT_BY_POS);
         if((count(OP_BUYSTOP) + count(OP_SELLSTOP)) == 1 && OrderOpenPrice() - Bid > pipstep*pip)
            closeall();            
      }     
  return(0);
}
//+------------------------------------------------------------------+

//double ndd (double Price)
//{
//  return (NormalizeDouble(Price,Digits));
//}
//double ndi (double Value)
//{
//  return (NormalizeDouble(Value,0));
//}
//+------------------------------------------------------------------+

int count(int a)
{
   int i=0;
   int buy=0;
   int sell=0;
   int buystop=0;
   int sellstop=0;

   for(i=0;i<OrdersTotal();i++)
      {
       OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
       if(OrderMagicNumber()==magicnumber && OrderSymbol()==Symbol())
       {
         if(OrderType()==OP_BUY)
          {
            buy++;
          }
         if(OrderType()==OP_SELL)
          {
            sell++;
          }
         if(OrderType()==OP_BUYSTOP)
          {
            buystop++;
          }
         if(OrderType()==OP_SELLSTOP)
          {
            sellstop++;
          }
      }
      }
   if(a==OP_BUY){return(buy);}
   if(a==OP_SELL){return(sell);}
   if(a==OP_BUYSTOP){return(buystop);}
   if(a==OP_SELLSTOP){return(sellstop);}

}

bool closeall()
{
    for (i=OrdersTotal()-1; i>=0; i--)
    {
      OrderSelect(i, SELECT_BY_POS);
    
      if (OrderMagicNumber()==magicnumber && OrderSymbol()==Symbol())
      {
        if (OrderType()==OP_BUYSTOP || OrderType()==OP_SELLSTOP)
          OrderDelete(OrderTicket());
          
        if (OrderType()==OP_BUY)
          OrderClose(OrderTicket(),OrderLots(),Bid,100);
  
        if (OrderType()==OP_SELL)
          OrderClose(OrderTicket(),OrderLots(),Ask,100);
      }
    }
}
  