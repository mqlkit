#property copyright "Zarko Asenov"
#property link      "jaltoh.6x.to"


#define RETRIES 5
#define SLIPPAGE 3


/* General functionality settings */
extern string       _ = " General Settings ";
extern double       Lot_Size = 0.12;
extern double       Pip_Step = 0; // On variable spread brokers keep it over average spread, 0 automatic step
extern bool         Add_Spread_to_Pip_Step = false; // For fixed spread brokers - be sure about this!
extern bool         Add_Min_Stop_to_Pip_Step = false;
extern double       Order_Lifetime_Hours = 1; // Pending order life time
extern double       Max_spread = 22; // dont send orders with prices above this spread
extern double       Hours_Bring_Even = 0; // wait for N hours before an active order stop loss is pushed up
extern bool         Debug_Print = true;
extern double       Panic_Profit = -200.0;
extern int          Midprice_Time_Frame = PERIOD_H1;
extern double       Calmdown_Hours = 2;
extern double       Go_After = 0.4; // periods
extern int          Magic_Number =  775225426;

/* Grid Bandwidth */
extern string       __ = " Market noise bandwidth ";
extern int          BW_Back_Period = 39;
extern int          BW_Time_Frame = 0;

/* Sentiment */
extern string       ___ = " Sentiment";
extern double       Sentiment_Threshold = 0.15;
extern int          Sentiment_MA_Bars = 12;
extern double       Sentiment_Time_Frame = 0;
extern int          Sentiment_Bars_Check = 1;
extern double       Sentiment_Time_Factor = 0.01;

/* Average True Range */
extern string       ____ = " ATR";
extern double       ATR_Threshold = 1.5;
extern int          ATR_Time_Frame = PERIOD_H1;
extern int          ATR_Period = 6;
extern int          ATR_Period_Long = 22;
extern int          ATR_MA_Bars = 6;
extern int          ATR_Bars_Check = 2;


/* Global variables */
double current_atr = 0.0;

double start_price = 0.0;
bool init_done = false;
double spread;

double pipstep_adjusted;
double pipstep_pt;

int order_lifetime;
double time_bring_even;

double tick_value;
double tick_value_pt;

double min_stop;
double min_stop_pt;

double total_spread_loss;
int start_bin;
double inv_sentiment_threshold;
double calmdown_period;

int num_orders;
double auto_step;
datetime prev_bar_time;






int init()
{
     if (RefreshRates() == false) 
          Print("Refresh rates failed!");
     

     order_lifetime = MathRound(Order_Lifetime_Hours * 3600);
     time_bring_even = 3600 * Hours_Bring_Even;
     
     min_stop = MarketInfo(Symbol(), MODE_STOPLEVEL);
     min_stop_pt = Point * MarketInfo(Symbol(), MODE_STOPLEVEL);
     
     tick_value = MarketInfo(Symbol(), MODE_TICKVALUE); 
     tick_value_pt = Point * MarketInfo(Symbol(), MODE_TICKVALUE); 

     pipstep_pt = Pip_Step * Point;

     inv_sentiment_threshold = -1.0 * Sentiment_Threshold;
     calmdown_period = Calmdown_Hours * 3600;

     num_orders = count_orders();
     auto_step = 0;
     prev_bar_time = 0;
    
     Print(
          "Min stop: " + min_stop + 
          ", Tick Value: " + tick_value + 
          ", Number of our orders: " + num_orders + 
          ", Grid height: " + pipstep_pt);
   
     return(0);
}


bool end_is_neigh(int cur_period)
{
  return (true);

  if ( (TimeCurrent() - Time[0]) < (cur_period * 60. * Go_After) ) return (false);
  else return (true);
}  

double
calc_profit()
{
    double presult = 0.0;
    total_spread_loss = 0.0;
    tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
    
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if ((OrderSymbol() != Symbol() || OrderMagicNumber() != Magic_Number) && 
          OrderType() != OP_BUY && OrderType() != OP_SELL) continue;

        double spread_loss = tick_value * OrderLots() * spread;
        presult += (OrderProfit() - spread_loss);
        total_spread_loss += spread_loss;
    }

    return (presult);   
}

datetime last_go_time = 0;

void
close_all_orders()
{
     close_all_orders_type(OP_BUY);
     close_all_orders_type(OP_SELL);
}

void
close_all_orders_type(int order_type)
{
     for (int i = 0; i < OrdersTotal(); i++) {
        
          OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
          if (OrderSymbol() != Symbol() || OrderMagicNumber() != Magic_Number) continue;
         
          if (OrderType() != order_type) continue;
         
          switch ( OrderType() ) {
            
          case OP_BUY:
               OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), SLIPPAGE, Blue);
               break;
                    
          case OP_SELL:
               OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), SLIPPAGE, Red);
               break;
                    
          case OP_SELLSTOP: 
          case OP_BUYSTOP:
               OrderDelete( OrderTicket() );
               break;
                    
          default:
               if (Debug_Print) Print("Uknown order type " + OrderType() + " of order #" + OrderTicket() );
          }
     }
}

int
count_orders()
{
     int count = 0;
     for (int i = 0; i < OrdersTotal(); i++) {
        
          OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
          if ( (OrderSymbol() != Symbol()) || (OrderMagicNumber() != Magic_Number) ) continue;

          count++;         
     }
     
     return (count);
}


bool
atr_is_go(int num_pos)
{
    for (int i = 0; i < num_pos; i++) {
        current_atr = iCustom(Symbol(), ATR_Time_Frame, "ATR ratio", ATR_Period, ATR_Period_Long, 0.0, 1.0, ATR_MA_Bars, false, 1, i);
        double ma_atr = iCustom(Symbol(), ATR_Time_Frame, "ATR ratio", ATR_Period, ATR_Period_Long, 0.0, 1.0, ATR_MA_Bars, false, 0, i);
        
        if (current_atr < ma_atr || current_atr < ATR_Threshold) return (false);
    }
        
   return (true);
}

bool
in_trend()
{
     return ( atr_is_go(ATR_Bars_Check) );
}


#define NEUTRAL 0
#define BULLISH 1
#define BEARISH 2

int sentiment = NEUTRAL;

double
get_grid()
{
     return ( 
               1.2 * iCustom(
                         Symbol(), BW_Time_Frame, "Bandwidth", 
                         0, BW_Back_Period, 1, "", false, PRICE_WEIGHTED, 
                         2, 0) 
             );
}

double
get_sentiment(int index, int bar)
{
     return ( 
               iCustom(
                    Symbol(), Sentiment_Time_Frame, "jmarket_sentiment", 
                    Sentiment_MA_Bars, Sentiment_MA_Bars, 0, Sentiment_MA_Bars, Sentiment_Time_Factor, 0.9, 0, "", "", "", NEUTRAL, "", "", "", 
                    index, bar) 
            );
}

bool
go_buy()
{
     double threshold_adjusted = Sentiment_Threshold;

     for (int ix = 0; ix < Sentiment_Bars_Check; ix++) {
          if (get_sentiment(0, ix) < threshold_adjusted) return (false);
     }    
     
     sentiment = BULLISH;
     return (true);
}

bool
go_sell()
{
     double inv_threshold_adjusted = inv_sentiment_threshold;

     for (int ix = 0; ix < Sentiment_Bars_Check; ix++) {
          if (get_sentiment(0, ix) > inv_threshold_adjusted) return (false);
     }
     
     sentiment = BEARISH;
     return (true);
}

double
get_auto_step()
{
     // init auto step
     if (auto_step == 0) auto_step = get_grid();
     
     // return previous unchaged step if already trading
     if (num_orders > 0) return (auto_step);

     // same bar same step
     if (Time[0] == prev_bar_time) return (auto_step);
     

     // Lets see ..
     
     // get recommended grid height
     double _step = get_grid();
     
     // if errored out stick with previous one
     if (_step == 0) {
          Print("Fatal, got zero auto step. Returning old step " + auto_step);
          return (auto_step);
     } else {
          auto_step = _step;
     }
     
     if (Debug_Print == true) Print("New step set to: " + auto_step);
     
     prev_bar_time = Time[0];
     return (auto_step);
}

int 
start()
{
    num_orders = count_orders();
    if (num_orders < 1) {
          init_done = false; // reinit!
          last_go_time = 0;
    }


    RefreshRates();       
    spread = Point * MarketInfo(Symbol(), MODE_SPREAD);
        
    if (
     (last_go_time != 0) && 
     ((TimeCurrent() - last_go_time) > calmdown_period) 
     ) 
    {
          close_all_orders();
          last_go_time = 0;
    }

    if (pipstep_pt != 0) pipstep_adjusted = pipstep_pt;
    else pipstep_adjusted = get_auto_step();
    
    /* nothing to do with step 0 */
    if (pipstep_adjusted == 0) {
          Print("Fatal, pipstep is zero! Skipping tick.");
          return (0);
    }
    
    if (Add_Spread_to_Pip_Step) pipstep_adjusted += spread;
    if (Add_Min_Stop_to_Pip_Step) pipstep_adjusted += min_stop_pt;
    
    
    if ( init_done == false ) {
    /* fire up new grid */
           
        if ( go_buy() ) {
            start_price = Ask;
            start_bin = MathFloor(start_price / pipstep_adjusted);
            if (Debug_Print) Print("Init go long, start price: " + start_price + " bin: " + start_bin);
            
            place_pending_buy(start_price, order_lifetime + TimeCurrent() );
            //place_pending_sell(start_price, order_lifetime + TimeCurrent() );
            place_buy(start_price);
            
            last_go_time = TimeCurrent();
            init_done = true;
        } else if ( go_sell() ) {
            start_price = Bid;
            start_bin = MathFloor(start_price / pipstep_adjusted);
            if (Debug_Print) Print("Init go short, start price: " + start_price + " bin: " + start_bin);
            
    //place_pending_buy(start_price, order_lifetime + TimeCurrent() );
            place_sell(start_price);
            place_pending_sell(start_price, order_lifetime + TimeCurrent() );
            
            last_go_time = TimeCurrent();
            init_done = true;
        }
        
    } else if (start_price != 0) {
    /* in trend */

         if ( Ask > (start_price + pipstep_adjusted) ) {
    
               start_price = Ask;
               int bin = MathFloor(start_price / pipstep_adjusted);
        
               if (Debug_Print) Print("Go buy new start_price:" + start_price + " bin: " + bin);
    
               /* things to do once per bin */
               if (start_bin != bin) {
                    move_up_buys_sl();
                    start_bin = bin;
               }
                       
               // place buy stops
               if ( go_buy() ) {
                    close_all_orders_type(OP_SELL);
                    place_pending_buy(start_price, order_lifetime + TimeCurrent());
                    place_pending_sell(start_price, order_lifetime + TimeCurrent());
                    last_go_time = TimeCurrent();
               } else if ( go_sell() ) {
                    close_all_orders_type(OP_BUY);
                    place_pending_sell(start_price, order_lifetime + TimeCurrent());
                    place_pending_buy(start_price, order_lifetime + TimeCurrent());
                    last_go_time = TimeCurrent();
               }
          } else if ( Bid < (start_price - pipstep_adjusted) ) {
    
               start_price = Bid;
               bin = MathFloor(start_price / pipstep_adjusted);
        
               if (Debug_Print) Print("Go sell new start_price: " + start_price + " bin: " + bin);

               /* things to do once per bin */
               if (start_bin != bin) {
                    move_down_sells_sl();
                    start_bin = bin;
               }          
        
               // place sell stops
               if ( go_sell() ) {
                    close_all_orders_type(OP_BUY);
                    place_pending_sell(start_price, order_lifetime + TimeCurrent());
                    place_pending_buy(start_price, order_lifetime + TimeCurrent());
                    last_go_time = TimeCurrent();
               } else if ( go_buy() ) {
                    close_all_orders_type(OP_SELL);
                    place_pending_buy(start_price, order_lifetime + TimeCurrent());
                    place_pending_sell(start_price, order_lifetime + TimeCurrent());
                    last_go_time = TimeCurrent();
               }
         }
         
          
    }
    
    if (Debug_Print) {
        Comment(
            "SpeedTrain Forte\nby DrZed 2011\n" +
            "Symbol: " + Symbol() + "\n" +
            "Spread: " + spread + "\n" +
            "ATR value: " + current_atr + "\n" +
            "Pipstep adjusted: " + pipstep_adjusted + "\n" +
            "Last orders price: " + start_price + "\n" +
            "Current close M15 price: " + iClose(NULL, PERIOD_M15, 0) + "\n" +
            "Total spread loss: " + total_spread_loss + " USD\n"
            );
    }
}

void 
place_pending_buy(double base_price, datetime lifetime)
{
     double buy_price = base_price + pipstep_adjusted;
     double stop_loss = base_price;
     double take_profit = 0;
          
     if (Debug_Print) Print("Placing buy stop order at " + buy_price + " with SL at " + stop_loss);
         
     int ticket = -1;
     int retrct = 0;
     while (ticket < 0 && retrct < RETRIES) {
         
          ticket = OrderSend(
                               Symbol(), 
                               OP_BUYSTOP, 
                               Lot_Size, 
                               NormalizeDouble(buy_price, Digits),
                               SLIPPAGE, 
                               NormalizeDouble(stop_loss, Digits), 
                               NormalizeDouble(take_profit, Digits), 
                               "",
                               Magic_Number, 
                               lifetime, 
                               Blue);
          retrct++;
    }
                                      
    if (ticket < 0) {
        Print("Long OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Debug_Print) Print("Buy stop order placed with ticket " + ticket + " at " + buy_price);
}
    
void 
place_pending_sell(double base_price, datetime lifetime)
{
     double sell_price = base_price - pipstep_adjusted;
     double stop_loss = base_price;
     double take_profit = 0.0;
          
     if (Debug_Print) Print("Placing sell stop order at " + sell_price + " with SL at " + stop_loss);
         
     int ticket = -1;
     int retrct = 0;
     while (ticket < 0 && retrct < RETRIES) {
         
          ticket = OrderSend(
                         Symbol(), 
                         OP_SELLSTOP, 
                         Lot_Size, 
                         NormalizeDouble(sell_price, Digits),
                         SLIPPAGE, 
                         NormalizeDouble(stop_loss, Digits), 
                         NormalizeDouble(take_profit, Digits), 
                         "", 
                         Magic_Number, 
                         lifetime, 
                         Red);
          retrct++;
    }
                                      
    if (ticket < 0) {
        Print("Short OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Debug_Print) Print(
                         "Sell stop order placed with ticket " + ticket + " at " + sell_price);
}
      
void 
place_buy(double base_price)
{
     double stop_loss = base_price - pipstep_adjusted;

     if (Debug_Print) Print("Placing long order at " + base_price + " with SL at " + stop_loss);
         
     int ticket = -1;
     int retrct = 0;
     while (ticket < 0 && retrct < RETRIES) {
          ticket = OrderSend(
                    Symbol(),
                    OP_BUY, 
                    Lot_Size, 
                    NormalizeDouble(base_price, Digits),
                    SLIPPAGE, 
                    NormalizeDouble(stop_loss, Digits), 
                    0, 
                    "", 
                    Magic_Number, 
                    0, 
                    Blue);
          retrct++;
    }
            
    if (Debug_Print == false) return;

    if (ticket < 0) Print("Long OrderSend failed with error #", GetLastError()); 
    else 
        Print("Buy stop order placed with ticket " + ticket + "  at " + Ask);
}
      
      
void 
place_sell(double base_price)
{
     double stop_loss = base_price + pipstep_adjusted;
          
     if (Debug_Print) Print("Placing short order at " + base_price + " with SL at " + stop_loss);

     int ticket = -1;
     int retrct = 0;
     while (ticket < 0 && retrct < RETRIES) {
         
          ticket = OrderSend(
                    Symbol(), 
                    OP_SELL, 
                    Lot_Size, 
                    NormalizeDouble(base_price, Digits), 
                    SLIPPAGE, 
                    NormalizeDouble(stop_loss, Digits), 
                    0, 
                    "", 
                    Magic_Number, 
                    0, 
                    Red);
          retrct++;
          
    }
                                      
    if (ticket < 0) {
          if (Debug_Print) Print("Short OrderSend failed with error #", GetLastError());
          return;
    }
          
    if (Debug_Print) Print("Sell stop order placed with ticket " + ticket + "  at " + base_price);
}

void
move_up_buys_sl()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if (
          (OrderMagicNumber() == Magic_Number) && 
          (OrderType() == OP_BUY) &&
          (TimeCurrent() - OrderOpenTime()) > time_bring_even) {
          
                double stop_loss = OrderStopLoss();

                for (
                    int new_stop_loss_bin = MathRound( (Ask - min_stop_pt - stop_loss) / pipstep_adjusted );
                    new_stop_loss_bin > 0;
                    new_stop_loss_bin--) {
                
                    double new_stop_loss = NormalizeDouble(stop_loss + (new_stop_loss * pipstep_adjusted), Digits);
                
                    if (Debug_Print) Print(
                                   "Moving long ticket " + OrderTicket() + " stop loss " + stop_loss + 
                                   " to " + new_stop_loss + ", current ask price " + Ask);
            
                    bool res = false;
                    int retrct = 0;
                    while (res == false && retrct < RETRIES) {
                         res = OrderModify(
                              OrderTicket(),                                
                              OrderOpenPrice(), 
                              new_stop_loss, 
                              OrderTakeProfit(), 
                              OrderExpiration(), 
                              Blue);
                         retrct++;
                    }
               
                    // Order modify failed.
                    if (res == false) Print("Failed moving order " + OrderTicket() + " stop loss to " + new_stop_loss);
                    else break;
               }
               
               if (OrderProfit() < Panic_Profit) OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), SLIPPAGE, Blue);
         }

    }
}
      
void 
move_down_sells_sl()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if (
          (OrderMagicNumber() == Magic_Number) && 
          (OrderType() == OP_SELL) &&
          ((TimeCurrent() - OrderOpenTime()) > time_bring_even)) {
            
                double stop_loss = OrderStopLoss();
                
                for (
                    int new_stop_loss_bin = MathRound( (stop_loss - min_stop_pt - Bid) / pipstep_adjusted );
                    new_stop_loss_bin > 0;
                    new_stop_loss_bin--) {
                    
                    double new_stop_loss = NormalizeDouble(stop_loss - (new_stop_loss_bin * pipstep_adjusted), Digits);
                    if (Debug_Print) Print(
                                   "Moving short SL @ " + OrderOpenPrice() + " SL " + stop_loss + 
                                   " to " + new_stop_loss + " current BID " + Bid);

                    bool res = false;
                    int retrct = 0;
                    while (res == false && retrct < RETRIES) {
 
                         res = OrderModify(
                              OrderTicket(),
                              OrderOpenPrice(),
                              new_stop_loss,
                              OrderTakeProfit(),
                              OrderExpiration(), 
                              Red);
                         retrct++;
                    }
                    
                    // Order modify failed.
                    if (res == false) Print("Failed moving order " + OrderTicket() + " stop loss to " + new_stop_loss);
                    else break;
               }

 
               if (OrderProfit() < Panic_Profit) OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), SLIPPAGE, Red);
        }

    }
}

