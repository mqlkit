//+------------------------------------------------------------------+
//|                                                     getvalue.mq4 |
//|                      Copyright � 2010, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2010, MetaQuotes Software Corp."
#property link      "http://www.metaquotes.net"

double alertTag;
double prevtime=0;
int cntalert=0;
double prev=0;
int prevminutet=0;
int active;
extern double lot=0.01;
double balance;
int cntbal=0;
extern int dis=100;
extern double profit_close = 10.0;

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
int  ticket, total,cntsell,cntbuy,first=0,second=0;
double profit2;



int spread = MarketInfo(Symbol(), MODE_SPREAD)+3;

//first Order   
int ordt=OrdersTotal();
if(CntAll(OP_SELLSTOP)==0 && CntAll(OP_BUYSTOP)==0){first=1;}else{first=0;}
if(CntAll(OP_SELLSTOP)+ CntAll(OP_BUYSTOP)==1){second=1;}else{second=0;}



   if (alertTag!=Time[0] && first==1 && second==0)
   {    
        alertTag = Time[0];
        if(CntAll(OP_SELLSTOP)==0 && CntAll(OP_BUYSTOP)==0)
        {
         OrderSend(Symbol(),OP_BUYSTOP,lot,Ask+dis*Point,3,Ask,0,"My order BuyStop",16384,0,Green);  
         Sleep(1000);
         RefreshRates();
         OrderSend(Symbol(),OP_SELLSTOP,lot,Bid-dis*Point,3,Bid,0,"My order SellStop",16384,0,Red);  
         Sleep(5000);  
         RefreshRates();
         first=0;
        }
   }
   
   if (alertTag!=Time[0] && second==1 && first==0)
   {    
        alertTag = Time[0];
        if(CntAll(OP_SELLSTOP)==0 || CntAll(OP_BUYSTOP)==0)
        {
         if(OrdersTotal()>0)
         {CloseOr(3);
         CloseOr(4);    }
        
         for(int cnt=0;cnt<OrdersTotal();cnt++) {
               Sleep(1000);
               RefreshRates();
               OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
               if(OrderType() == OP_SELL && Bid < OrderOpenPrice() - dis * Point) {
                  OrderModify(OrderTicket(), OrderOpenPrice(), OrderStopLoss() - dis * Point, OrderTakeProfit(), 3, Red);
               } else if(OrderType()==OP_BUY && Ask > OrderOpenPrice() + dis * Point) {
                  OrderModify(OrderTicket(), OrderOpenPrice(), OrderStopLoss() + dis * Point, OrderTakeProfit(), 3, Green);
               }
         }
         
         Sleep(1000);  
         RefreshRates();
         OrderSend(Symbol(),OP_BUYSTOP,lot,Ask+dis*Point,3,Ask,0,"My order BuyStop",16384,0,Green);  
         Sleep(1000);
         OrderSend(Symbol(),OP_SELLSTOP,lot,Bid-dis*Point,3,Bid,0,"My order SellStop",16384,0,Red);  
         second=0;
        }
   } 
  
   
   if (AccountProfit() > profit_close) CloseAll();

                  
   return(0);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
int CloseOr(int a)
{
int i=0;
for(i=0;i<OrdersTotal();i++)
   {
    OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
    Sleep(1000);
    if(OrderType()==OP_BUY && a==1)
     {
      OrderClose(OrderTicket(),OrderLots(),Bid,3,Red);
      continue;
     }
    if(OrderType()==OP_SELL && a==2)
     {
      OrderClose(OrderTicket(),OrderLots(),Ask,3,Red);
      continue;
     }
    if(OrderType()==OP_BUYSTOP||OP_BUYLIMIT  && a==3)
     {
      OrderDelete(OrderTicket());
      continue;
     }
    if(OrderType()==OP_SELLSTOP||OP_SELLLIMIT && a==4 ) 
      {
       OrderDelete(OrderTicket());
       continue;
      }
   }
return(0);
}

 //-------------------------------------------------------+
 //functions lots of functions
 //-------------------------------------------------------+
//////////////////////////////////////////////////////////
int CntAll(int a)
{
int i=0;
int buy=0;
int sell=0;
int buystop=0;
int sellstop=0;

for(i=0;i<OrdersTotal();i++)
   {
    OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
    if(OrderType()==OP_BUY && OrderSymbol()==Symbol())
     {
       buy++;
     }
    if(OrderType()==OP_SELL && OrderSymbol()==Symbol())
     {
       sell++;
     }
    if(OrderType()==OP_BUYSTOP && OrderSymbol()==Symbol())
     {
       buystop++;
     }
    if(OrderType()==OP_SELLSTOP && OrderSymbol()==Symbol())
     {
       sellstop++;
     }
   }
if(a==OP_BUY){return(buy);}
if(a==OP_SELL){return(sell);}
if(a==OP_BUYSTOP){return(buystop);}
if(a==OP_SELLSTOP){return(sellstop);}

}
///////////////////////////////////////////////////
int gor(int a)
{  
   int value=0;  
   if(Close[a]>Open[a]){value=1;}
   else if(Close[a]<Open[a]){value=-1;}
   else if(Close[a]==Open[a]){value=0;}
   return(value);
}
/////////////////////////////////////
int CloseAll()
{
   int i=0;
   for(i=0; i < OrdersTotal(); i++)
   {
    OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
    Sleep(1000);
    RefreshRates();
    if(OrderType()==OP_BUY)
     {
      OrderClose(OrderTicket(),OrderLots(),Bid,3,Red);
      continue;
     }
    if(OrderType()==OP_SELL)
     {
      OrderClose(OrderTicket(),OrderLots(),Ask,3,Red);
      continue;
     }
    if(OrderType()==OP_BUYSTOP||OP_BUYLIMIT)
     {
      OrderDelete(OrderTicket());
      continue;
     }
    if(OrderType()==OP_SELLSTOP||OP_SELLLIMIT) 
      {
       OrderDelete(OrderTicket());
       continue;
      }
   }
   return(0);
}