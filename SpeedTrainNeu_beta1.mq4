#property copyright "Zarko Asenov"
#property link      "jaltoh.6x.to"

/* General functionality settings */
extern int          Slippage = 2;
extern double       LotSize = 0.3;
extern double       PipStep = 16.0; // For variable spread brokers keep it at the average guaranteed spread
extern bool         InitialBuyOrder = true;
extern int          MagicNumber =  1771611;
extern double       Close_All_Profit = 150.0;
extern bool         Clear_Stops = true;
extern double       Order_Lifetime_Days = 0.2;
extern bool         Add_Spread_to_PipStep = false; // For fixed spread brokers - be sure about this!
extern bool         Add_MinStop_to_PipStep = true;
extern double       Max_spread = 22.0;
extern double       Hours_Bring_Even = 0.0;
extern double       Multi_Coef = 1.0; // multiplies LotSize PipStep Close_All_Profit Min_Bands
extern bool         Verbose_Mode = false;
extern double       LTS_LotSize = 0.0; /* Leave 0 for no Long Term Strategy trading */
extern double       LTS_Close_All_Profit = 10.0;
extern int          Max_LTS_Positions = 1;
extern int          Grid_Base_Period = PERIOD_M5;

/* Bollinger bands */
extern int          Bands_Days = 10;
extern int          Bands_Deviations = 4;
extern double       Min_Bands = 0.017;
extern int          Num_Bands = 2;

/* Stochastic oscillator */
extern bool         Check_Stoch = true;
extern int          Stoch_K = 6;
extern int          Stoch_D = 4;
extern int          Stoch_Slow = 3;
extern int          Stoch_num_pos = 2;
extern double       Stoch_border = 40.0;
extern int          Stochastic_Period = PERIOD_H4;

/* Average Directional Movement Index */
extern bool         Check_ADX = true;
extern int          ADX_positions_to_check = 2;
extern int          ADX_period_in_days = 4;
extern double       ADX_min = 37.0;

/* Average True Range */
extern double       ATR_Floor = 0.0055;
extern int          ATR_period_in_days = 35;
extern int          ATR_positions_to_check = 3;
extern int          ATR_Period = PERIOD_H4;

/* Globals variables */
int longOrderCount = 0;
int shortOrderCount = 0;
double startPrice = 0.0;
int digits = 5;
bool initOrdersDone = false;
double spread = 0.0;
double pipstep_adjusted = 0.0;
double current_adx = 0.0;
double neg_adx_min = 0.0;
double neg_stoch_border = 0.0;
double current_atr = 0.0;
int order_lifetime = 0;
double time_bring_even = 0.0;
double bandwidth = 0.0;
double adjustedLotSize = 0.0;
double adjustedPipStep = 0.0;
double adjustedClose_All_Profit = 0.0;
double adjustedMin_Bands = 0.0;
double tick_value = 0.0;
double total_spread_loss = 0.0;
int prev_signal = 0;
int lts_magic_number = -1;
int lts_long_positions = 0;
int lts_short_positions = 0;
double current_stoch_k = 0.0;
double current_stoch_d = 0.0;

double startPrice_ask = 0.0;
double startPrice_bid = 0.0;
double close_ask_diff = 0.0;
double close_bid_diff = 0.0;


#define SIG_NO_TREND 0
#define SIG_IN_TREND 1
#define SIG_GO_BUY 2
#define SIG_GO_SELL 3


int init()
{
    if (Stoch_border > 100.0) Stoch_border = 100.0;
    
    neg_stoch_border = 100.0 - Stoch_border;
    neg_adx_min = -1.0 * ADX_min;
    
    order_lifetime = MathRound(Order_Lifetime_Days * 86400);
    time_bring_even = 3600 * Hours_Bring_Even;
    lts_magic_number = MagicNumber + 1;

    adjustedLotSize = LotSize * Multi_Coef;
    adjustedPipStep = PipStep;
    adjustedClose_All_Profit = Close_All_Profit * Multi_Coef;
    adjustedMin_Bands = Min_Bands;
    
    tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
    digits = MarketInfo(Symbol(), MODE_DIGITS);
    
    return(0);
}

double get_minstop()
{
    return( MarketInfo(Symbol(), MODE_STOPLEVEL) );
}


double
get_adx(int index)
{
    double ADX1, ADX2;

    current_adx = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_MAIN, index);
    ADX1 = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_PLUSDI, index);
    ADX2 = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_MINUSDI, index);

    if(ADX1 >= ADX2)
        return(current_adx);
    else
        return(-1.0 * current_adx);
}

bool
adx_is_sell(int num_pos)
{
    if (Check_ADX == false) return (true);

    
    for (int i = 0; i < num_pos; i++) {
        if (get_adx(i) > neg_adx_min) return (false);
    }

    return (true);
}

bool
adx_is_buy(int num_pos)
{
    if (Check_ADX == false) return (true);

    for (int i = 0; i < num_pos; i++) {
        current_adx = get_adx(i);
        if (current_adx < ADX_min) return (false);
    }

    return (true);
}

bool
stoch_is_buy(int num_pos)
{
    if (Check_Stoch == false) return (true);
    
    for (int i = 0; i < num_pos; i++) {
          current_stoch_k = iStochastic(NULL, Stochastic_Period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0 /*LOWHIGH*/, MODE_MAIN, i);
          current_stoch_d = iStochastic(NULL, Stochastic_Period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0 /*LOWHIGH*/, MODE_SIGNAL, i);
          
          if (current_stoch_k > Stoch_border) return(false);
//          if (current_stoch_k < current_stoch_d) return (false);
    }
    
    return(true);
}
     
bool
stoch_is_sell(int num_pos)
{
    if (Check_Stoch == false) return (true);
    
    for (int i = 0; i < num_pos; i++) {
          current_stoch_k = iStochastic(NULL, Stochastic_Period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0 /*LOWHIGH*/, MODE_MAIN, i);
          current_stoch_d = iStochastic(NULL, Stochastic_Period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0 /*LOWHIGH*/, MODE_SIGNAL, i);
          
          if (current_stoch_k < neg_stoch_border) return(false);
//          if (current_stoch_k > current_stoch_d) return (false);
    }
    
    return(true);
}

bool
atr_is_go(int num_pos)
{
    for (int i = 0; i < num_pos; i++) {
        current_atr = iATR(NULL, ATR_Period, ATR_period_in_days, i);
        if (current_atr < ATR_Floor) return (false);
    }
        
   return (true);
}

double
calc_profit()
{
    double presult = 0.0;
    total_spread_loss = 0.0;
    tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
    
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if ((OrderSymbol() != Symbol() || OrderMagicNumber() != MagicNumber) && 
          OrderType() != OP_BUY && OrderType() != OP_SELL) continue;

        double spread_loss = tick_value * OrderLots() * spread;
        presult += (OrderProfit() - spread_loss);
        total_spread_loss += spread_loss;
    }

    return (presult);   
}

double
calc_profit_lts()
{
    if (LTS_LotSize == 0 || lts_magic_number == -1 || 
          lts_long_positions + lts_short_positions < 1) return;
     
    double presult = 0.0;
    total_spread_loss = 0.0;
    tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
    
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if ((OrderSymbol() != Symbol() || OrderMagicNumber() != lts_magic_number) && 
          OrderType() != OP_BUY && OrderType() != OP_SELL) continue;

        double spread_loss = tick_value * OrderLots() * spread;
        presult += (OrderProfit() - spread_loss);
        total_spread_loss += spread_loss;
    }

    return (presult);   
}

double
getBandwidth(int pos)
{
    bandwidth = iBands(NULL, Grid_Base_Period, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_UPPER, pos) - 
          iBands(NULL, Grid_Base_Period, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_LOWER, pos);
   
    return (bandwidth);
}

bool
inBands()
{
    for (int i = 0; i < Num_Bands; i++)
        if (getBandwidth(i) < adjustedMin_Bands) 
           return (false);
  
    return (true);
}

bool
in_trend()
{
     return ( atr_is_go(ATR_positions_to_check) && inBands() );
}

bool
go_buy()
{
     return ( adx_is_buy(ADX_positions_to_check) && stoch_is_buy(Stoch_num_pos) );
}

bool
go_sell()
{
     return ( adx_is_sell(ADX_positions_to_check) && stoch_is_sell(Stoch_num_pos) );
}

string current_signal_str = "";

int 
get_current_signal() {

     bool in_trend = in_trend();
          
     if ( in_trend && go_buy() ) {
          if (Verbose_Mode) Print("Current signal is buy!");
          current_signal_str = "BUY";
          return(SIG_GO_BUY);
     } else if ( in_trend && go_sell() ) {
          if (Verbose_Mode) Print("Current signal is sell!");
          current_signal_str = "SELL";
          return(SIG_GO_SELL);
     } else if ( in_trend == true) {
          if (Verbose_Mode) Print("Current signal is we are in a trend!");
          current_signal_str = "TREND";
          return(SIG_IN_TREND);
     } else {
         if (Verbose_Mode) Print("Current signal is range!");
          current_signal_str = "RANGE";
         return(SIG_NO_TREND);
     }
}

void
updatePriceDiffs()
{
   close_ask_diff = iClose(NULL, Grid_Base_Period, 0) - startPrice_ask;
   close_bid_diff = iClose(NULL, Grid_Base_Period, 0) - startPrice_bid;
}

void
updateStartPrice_ask()
{
   startPrice_ask = MarketInfo(Symbol(), MODE_ASK);
   updatePriceDiffs();
   if (true) Print("New startPrice ask: " + startPrice_ask + " close_ask_diff: " + close_ask_diff+ " " + current_signal_str);
}

void
updateStartPrice_bid()
{
   startPrice_bid = MarketInfo(Symbol(), MODE_BID);
   updatePriceDiffs();
   if (true) Print("New startPrice bid: " + startPrice_bid + " close_bid_diff: " + close_bid_diff + " " + current_signal_str);
}

int 
start()
{
    if(startPrice_ask == 0.0) updateStartPrice_ask();
    if(startPrice_bid == 0.0) updateStartPrice_bid();

    pipstep_adjusted = adjustedPipStep;
      
    if (Add_Spread_to_PipStep)
        pipstep_adjusted += spread / MarketInfo(Symbol(), MODE_POINT);

    if (Add_MinStop_to_PipStep)
        pipstep_adjusted += get_minstop();

    
    if ( !initOrdersDone && in_trend() ) {
           
        if (go_buy()) {
        
            placeBuyOrder();
            placeInitBuyOrder();
            
        } else if (go_sell()) {
        
            placeSellOrder();
            placeInitSellOrder();
            
        }
        
        initOrdersDone = true;
    }
    
    RefreshRates();
    
    int current_signal = get_current_signal();
    
    // Do buys
    if(MarketInfo(Symbol(), MODE_ASK) > startPrice_ask + pipstep_adjusted * Point) {
    
        updateStartPrice_ask();
          
        // need to set the sl to BE of last order
        moveSellOrderToBE();
        
        moveHitBuyOrdersToBE();
        
        // place buy stops
        if ( current_signal == SIG_GO_BUY ) {
          if (true) 
               Print("Go buy, placing sell order.");
          placeBuyOrder();
        }          
    
    // Do sells    
    } else if (MarketInfo(Symbol(), MODE_BID) < startPrice_bid - pipstep_adjusted * Point) {
    
        updateStartPrice_bid();
        
        // need to set the sl to BE of last order
        moveBuyOrderToBE();  

        moveHitSellOrdersToBE();
        
        // place sell stops
        if ( current_signal == SIG_GO_SELL ) {
          if (true) 
               Print("Go sell, placing sell order.");
          placeSellOrder();
        }          
          
    }

    /* Long Term Strategy */
    if (LTS_LotSize != 0.0) {
        if (calc_profit_lts() > LTS_Close_All_Profit) close_all_lts();
        else if (current_signal == SIG_GO_SELL && prev_signal != SIG_GO_SELL) place_lts_sell();
        else if (current_signal == SIG_GO_BUY && prev_signal != SIG_GO_BUY) place_lts_buy();
        else if (current_signal == SIG_GO_BUY && lts_short_positions > 0) close_all_lts();
        else if (current_signal == SIG_GO_SELL && lts_long_positions > 0) close_all_lts();
        prev_signal = current_signal;
    }

    spread = Point * MathAbs(MarketInfo(Symbol(), MODE_SPREAD));
    double current_profit = calc_profit();

    if (Verbose_Mode) {
        Comment(
            "SpeedTrain Forte\nby DrZed 2011\n" +
            "Symbol: " + Symbol() + "\n" +
            "Spread: " + spread + "\n" +
            "Bandwidth: " + bandwidth + "\n" +
            "ADX difference: " + current_adx + "\n" +
            "Stoch base: " + current_stoch_k + "\n" +
            "Stoch signal: " + current_stoch_d + "\n" +
            "ATR value: " + current_atr + "\n" +
            "Pipstep adjusted: " + pipstep_adjusted + "\n" +
            "Last orders price: ask: " + startPrice_ask + ", bid: " + startPrice_bid + "\n" +
            "Current close M15 price: ask: " + MarketInfo(Symbol(), MODE_ASK) + ", bid: " + MarketInfo(Symbol(), MODE_BID) + "\n" +
            "Current signal " + current_signal_str + "\n" +
            "Profit: " + current_profit + " USD\n" +
            "Total spread loss: " + total_spread_loss + " USD\n"
            );      
    }
   
    if (current_profit > adjustedClose_All_Profit && spread < Max_spread) {
    
        if (Verbose_Mode) 
            Print("Closing orders on profit " + current_profit + 
                " total spread loss: " + total_spread_loss + 
                " spread: " + spread + " pips.");
            
        for (int i = 0; i < OrdersTotal(); i++) {
        
            OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
            if (OrderSymbol() != Symbol() || OrderMagicNumber() != MagicNumber) continue;
         
            switch ( OrderType() ) {
            
               case OP_BUY:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Blue);
                    break;
                    
               case OP_SELL:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Red);
                    break;
                    
               case OP_SELLSTOP: 
               case OP_BUYSTOP:
                    OrderDelete( OrderTicket() );
                    break;
                    
               default:
                    if (Verbose_Mode) 
                         Print("Uknown order type " + OrderType() + " of order #" + OrderTicket() );
            }
        }
        
        shortOrderCount = 0;
        longOrderCount = 0;
    }
      
}

void
place_lts_buy()
{
    if (LTS_LotSize == 0.0 || lts_magic_number == -1 || 
          lts_long_positions + lts_short_positions > Max_LTS_Positions) {
        if (Verbose_Mode) Print("Long Term trading Stradegy disabled!");
        return;
    }
    
    double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
    if (Verbose_Mode) Print("LTS Placing buy order at " + ask);
         
    int  ticketlong = OrderSend(
                                Symbol(),
                                OP_BUY, 
                                LTS_LotSize,
                                NormalizeDouble(ask, digits),
                                Slippage, 
                                0,
                                0, 
                                "Long Speed Train LTS", 
                                lts_magic_number,
                                0, 
                                Yellow);

    if (ticketlong < 0) {
        if (Verbose_Mode) 
           Print("LTS Long OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Verbose_Mode) Print("LTS Buy order placed with ticket " + ticketlong + "  at " + ask);
          
    lts_long_positions++;
}

void
place_lts_sell()
{
    if (LTS_LotSize == 0.0 || lts_magic_number == -1 || 
          lts_long_positions + lts_short_positions > Max_LTS_Positions) {
        if (Verbose_Mode) Print("Long Term trading Stradegy disabled!");
        return;
    }

    double bid = MarketInfo(Symbol(), MODE_BID); 
    double sellSL = bid + pipstep_adjusted * Point;
          
    if (Verbose_Mode) Print("Placing LTS sell order at " + bid);
         
    int  ticketshort = OrderSend(
                                 Symbol(), 
                                 OP_SELL, 
                                 LTS_LotSize,
                                 NormalizeDouble(bid, digits), 
                                 Slippage, 
                                 0, 
                                 0, 
                                 "Short Speed Train LTS", 
                                 lts_magic_number, 
                                 0, 
                                 Red);
                                      
    if (ticketshort < 0) {
        if (Verbose_Mode) Print("Short OrderSend failed with error #", GetLastError());
          
        return;
    }
          
    if (Verbose_Mode) Print("Sell order placed with ticket " + ticketshort + "  at " + bid);
          
    lts_short_positions++;
}

void
close_all_lts()
{
        for (int i = 0; i < OrdersTotal(); i++) {
        
            OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
            if (OrderSymbol() != Symbol() || OrderMagicNumber() != lts_magic_number) continue;
         
            switch ( OrderType() ) {
            
               case OP_BUY:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Blue);
                    break;
                    
               case OP_SELL:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Red);
                    break;
                    
               case OP_SELLSTOP: 
               case OP_BUYSTOP:
                    OrderDelete( OrderTicket() );
                    break;
                    
               default:
                    if (Verbose_Mode) Print("Uknown LTS order type " + OrderType() + " of order #" + OrderTicket() );
            }
        }
    
    lts_short_positions = 0;
    lts_long_positions = 0;
}

void 
placeBuyOrder()
{
    double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
    double buyPrice = ask + pipstep_adjusted * Point;
    double buySL = ask;
          
    if (Verbose_Mode) 
        Print("Placing buy stop order at " + buyPrice+" with SL at " + buySL);
         
    int ticketlong = OrderSend(
                               Symbol(), 
                               OP_BUYSTOP, 
                               adjustedLotSize, 
                               NormalizeDouble(buyPrice, digits),
                               Slippage, 
                               NormalizeDouble(buySL, digits), 
                               0.0, 
                               "Long Speed Train",
                               MagicNumber, 
                               order_lifetime + TimeCurrent(), 
                               Blue);
                                      
    if (ticketlong < 0) {
        Print("Long OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Verbose_Mode) 
       Print("Buy stop order placed with ticket " + ticketlong + " at " + buyPrice);
          
    longOrderCount++;
}
    
void 
placeSellOrder()
{
    double bid = MarketInfo(Symbol(), MODE_BID) ; 
          
    double sellPrice = bid - pipstep_adjusted * Point;
    double sellSL = bid;
          
          
    if (Verbose_Mode) 
         Print("Placing sell stop order at " + sellPrice + " with SL at " + sellSL);
         
    int  ticketshort = OrderSend(
                                 Symbol(), 
                                 OP_SELLSTOP, 
                                 adjustedLotSize, 
                                 NormalizeDouble(sellPrice, digits),
                                 Slippage, 
                                 NormalizeDouble(sellSL, digits), 
                                 0.0, 
                                 "Short Speed Train", 
                                 MagicNumber, 
                                 order_lifetime + TimeCurrent(), 
                                 Red);
                                      
    if (ticketshort < 0) {
        Print("Short OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Verbose_Mode) 
         Print("Sell stop order placed with ticket " + ticketshort + " at " + sellPrice);
          
    shortOrderCount++;
}
      
void 
placeInitBuyOrder()
{
    double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
    double buySL = ask - pipstep_adjusted * Point;

    if (Verbose_Mode) 
         Print("Placing buy order at " + ask + " with SL at " + buySL);
         
    int  ticketlong = OrderSend(
                                Symbol(),
                                OP_BUY, 
                                adjustedLotSize, 
                                NormalizeDouble(ask, digits),
                                Slippage, 
                                NormalizeDouble(buySL, digits), 
                                0, 
                                "Long Speed Train", 
                                MagicNumber, 
                                0, 
                                Blue);
                                      
    if (ticketlong < 0) {
        if (Verbose_Mode) 
           Print("Long OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Verbose_Mode) 
          Print("Buy order placed with ticket " + ticketlong + "  at " + ask);
          
    longOrderCount++;        
}
      
      
void 
placeInitSellOrder()
{
     
    double bid = MarketInfo(Symbol(), MODE_BID); 
    double sellSL = bid + pipstep_adjusted * Point;
          
    if (Verbose_Mode) 
        Print("Placing sell order at "+bid+" with SL at "+sellSL);
         
    int  ticketshort = OrderSend(
                                 Symbol(), 
                                 OP_SELL, 
                                 adjustedLotSize, 
                                 NormalizeDouble(bid, digits), 
                                 Slippage, 
                                 NormalizeDouble(sellSL, digits), 
                                 0, 
                                 "Short Speed Train", 
                                 MagicNumber, 
                                 0, 
                                 Red);
                                      
    if (ticketshort < 0) {
        if (Verbose_Mode) 
          Print("Short OrderSend failed with error #", GetLastError());
          
        return;
    }
          
    if (Verbose_Mode) 
       Print("Sell order placed with ticket " + ticketshort + "  at " + bid);
          
    shortOrderCount++;        
}

void 
moveHitBuyOrdersToBE()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if (OrderMagicNumber() == MagicNumber && OrderType() == OP_BUY &&
            TimeCurrent() - OrderOpenTime() > time_bring_even)
            
                OrderModify(
                         OrderTicket(),                                
                         OrderOpenPrice(), 
                         OrderOpenPrice(), 
                         OrderTakeProfit(), 
                         OrderExpiration(), 
                         Blue);

    }
}
      
void 
moveHitSellOrdersToBE()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if (OrderMagicNumber() == MagicNumber && OrderType() == OP_SELL &&
            TimeCurrent() - OrderOpenTime() > time_bring_even)

                OrderModify(
                         OrderTicket(),                                
                         OrderOpenPrice(), 
                         OrderOpenPrice(), 
                         OrderTakeProfit(), 
                         OrderExpiration(), 
                         Red);   

    }
}

void 
moveBuyOrderToBE()
{
        int lastTicket = 0;
          
        for(int j = 0; j < OrdersTotal(); j++)
        {
            OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
            if (OrderMagicNumber() == MagicNumber && OrderType() == OP_BUYSTOP)
            {
               int ticketId = OrderTicket() ;
               if (ticketId > lastTicket) lastTicket = ticketId;
           }
       }
       
       if(lastTicket > 0) {
       
            OrderSelect(lastTicket, SELECT_BY_TICKET, MODE_TRADES);
            
            if (Verbose_Mode) 
               Print("Moving long order number " + lastTicket + " to BE at " + OrderOpenPrice() );
       
            int ticketlong = OrderModify(
                                        lastTicket, 
                                        OrderOpenPrice(), 
                                        OrderOpenPrice(), 
                                        OrderTakeProfit(), 
                                        OrderExpiration(), 
                                        Blue);
          
                                      
            if (ticketlong < 0 && Verbose_Mode) 
                Print("Long SL2BE order failed with error #", GetLastError());
                 
       }           
}
      
void moveSellOrderToBE()
{
     int lastTicket=0;
      
     for(int j = 0; j < OrdersTotal(); j++) {
          OrderSelect(j, SELECT_BY_POS, MODE_TRADES);

          if (OrderMagicNumber() == MagicNumber && OrderType() == OP_SELLSTOP)
          {
               int ticketId = OrderTicket();
               if(ticketId > lastTicket) lastTicket = ticketId;
          }

     }
       
     if(lastTicket > 0) {
          OrderSelect(lastTicket, SELECT_BY_TICKET, MODE_TRADES);
       
          if (Verbose_Mode) 
              Print("Moving short order number " + lastTicket+ " to BE at " + OrderOpenPrice());
               
          int ticketshort = OrderModify(
                                        lastTicket, 
                                        OrderOpenPrice(), 
                                        OrderOpenPrice(), 
                                        OrderTakeProfit(), 
                                        OrderExpiration(), 
                                        Red);
                                      
          if (ticketshort < 0)
                 if (Verbose_Mode) Print("Short SL2BE order failed with error #",GetLastError()); 
             
     }
     
}

