//+------------------------------------------------------------------+
//|                                         SWB_SuperTrend_V1.01.mq4 |
//|                                Copyright � 2010, Dennis Hamilton |
//|                                              ramble_32@yahoo.com |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2010, Dennis Hamilton"
#property link      "ramble_32@yahoo.com"
//----
extern string  Separator_01="----  Input  Settings  ----";
extern double   lot=0.05;
extern double   tp_1=25;
extern double   tp_2=25;
extern int      magic=123;
extern int      bars_closed=0;
extern string  Separator_02="----  Range  &  Offsets  ----";
extern double   range=40;
extern int      max_level=10;
extern int      curb_level=3;
extern double   p1_offset=10;
extern double   p2_offset=15;
extern int      pend_delete=30;
extern double   profit_target=3.0;
extern double   curb_factor=0.5;
extern double  safety_coef = 0.5;
extern bool     stealth=false;
extern bool     tier2_sig=true;
extern string  Separator_03="----  Lot  Settings  ----";
extern bool     lot_multiplier=false;
extern double   multiplier=2.0;
extern double   increment=0.02;
extern bool     power_lots=true;
extern string  Separator_04="----  Additional  ----";
extern double   deviation_reset=0;
extern int      reset_level=3;
extern bool     closeall=false;
extern bool     stop_ea=false;
//----
double  pt;
double  p_off;
double  p1_off;
double  p2_off;
double  std=0.1;
double  AE1;
double  p_lot;
double  lot2;
double  bal_2;
double  incr_1;
double  b_price;
double  s_price;
double  d_reset;
int     OT;
bool    deleteall;
bool    hedge=false;
bool    b_send;
bool    s_send;
string  f_trade="NULL";
//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   pt=Point; if(Digits==3 || Digits==5) pt=10*Point;
   tp_1*=pt;
   tp_2*=pt;
   range*=pt;
   if(p1_offset<5.0) p1_offset=0.0;
   if(p2_offset<5.0) p2_offset=0.0;
   p1_off=p1_offset*pt;
   p2_off=p2_offset*pt;
   d_reset=deviation_reset;
   if(curb_level<3) curb_level=0;
//----
   AE1=AccountEquity(); p_lot=lot/AE1;
   if(StringLen(Symbol())>6) std=1.0;
   profit_target*=std;
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
   int total=0, t_cnt=0, p_cnt=0, b_cnt=0, s_cnt=0, b2_cnt=0, s2_cnt=0;
   int b3_cnt=0, s3_cnt=0;  double FOOP, B_LOOP, S_LOOP;
   for(int i=0; i<OrdersTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      total++;
//----
      if(cmd()< 2) t_cnt++;
      if(cmd()> 1) p_cnt++;
      if(cmd()==0){ b_cnt++; b3_cnt++; } if(cmd()==4){ b2_cnt++; b3_cnt++; }
      if(cmd()==1){ s_cnt++; s3_cnt++; } if(cmd()==5){ s2_cnt++; s3_cnt++; }
      if(b3_cnt==1 || (b3_cnt>1 && OOP()<B_LOOP)) B_LOOP=OOP();
      if(s3_cnt==1 || (s3_cnt>1 && OOP()>S_LOOP)) S_LOOP=OOP();
      if(b_cnt ==1 || (b_cnt >1 && OOP()>FOOP)) FOOP=OOP();
      if(s_cnt ==1 || (s_cnt >1 && OOP()<FOOP)) FOOP=OOP();
   }
//----
   string H=""; if(stealth) H="_S"; if(hedge) H="_H"; if(stealth && hedge) H="_SH";
   if(total==0) f_trade="Null"+H;
   if(t_cnt==0 && p_cnt==1)  f_trade="Pend"+H;
   if(t_cnt==1 && p_cnt==0){ f_trade="Buy"+H; if(s_cnt==1) f_trade="Sell"+H; }
//+------------------------------------------------------------------+
   for(i=0; i<OrdersTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      double OTP=OrderTakeProfit(), tp_3=tp_1;
      if(cmd()<2 && OTP==0)
      {
         if(t_cnt>1) tp_3=tp_2; if(cmd()==1) tp_3*=-1;
         if(!stealth || (stealth && OrderComment()=="L1")) OTP=OOP()+tp_3;
         if(OTP!=0){ OrderModify(OrderTicket(),0,0,OTP,0,CLR_NONE); deleteall=true; }
      }
//----
      if(t_cnt==0 && pend_delete>0)
      {
         datetime OOT=OrderOpenTime(), get_time=TimeCurrent();
         if(cmd()>1 && get_time-OOT>pend_delete*60) OrderDelete(OrderTicket());
      }
   }
//+------------------------------------------------------------------+
   double o_charts=GlobalVariableGet("open_charts"); o_charts=1; // Global currently by-passed
   double AB=AccountBalance(), lot2=lot/o_charts, pl_factor=1/o_charts;
//----
   if(power_lots)
   {
      if(AB>AE1) lot2=NormalizeDouble((p_lot*AB)/o_charts,Digits);
      pl_factor=(lot2/lot)/o_charts; double lot3=lot2;
   }
//----
   double p_targ=profit_target*pl_factor, incr_1=increment*pl_factor, b_lot=lot2, s_lot=lot2;
//+------------------------------------------------------------------+
   bool t_buy=false, t_sell=false, t2_buy=false, t2_sell=false;
   bool isfriday = false; /* aseza put something */
   if(DayOfWeek()==5) isfriday = true;
   if((p1_off==0 && t_cnt==0) || (p1_off>0 && b3_cnt==0 && (s_cnt==0 || f_trade=="Buy"+H)))  t_buy=true;
   if((p1_off==0 && t_cnt==0) || (p1_off>0 && s3_cnt==0 && (b_cnt==0 || f_trade=="Sell"+H))) t_sell=true;
   if(f_trade=="Buy"+H  && b3_cnt>0 && b_cnt<max_level) t2_buy=true;
   if(f_trade=="Sell"+H && s3_cnt>0 && s_cnt<max_level) t2_sell=true;
//----
   if(total==0){ closeall=false; b_send=false; s_send=false; }
   if(b2_cnt==0 && s2_cnt==0) deleteall=false;
   if(!stop_ea && !closeall && !deleteall && !isfriday)
   {
      if(t_buy || (t2_buy && Ask<=B_LOOP-range-p2_off)) // BUY
      {
         if(signal()==0 || (b_cnt>0 && !tier2_sig))
         {
            if(lot_multiplier)  b_lot=lot2*MathPow(multiplier,b_cnt);
            if(!lot_multiplier) b_lot=lot2+(incr_1*b_cnt);
            OT=0; p_off=p1_off;
            if(b_cnt>0) p_off=p2_off; if(p_off>0) OT=4;
            //----
            OrderSend(Symbol(),OT,b_lot,Ask+p_off,3,0,0,"L"+(b_cnt+1),magic,0,Blue);
         }
      }
//----
      if(t_sell || (t2_sell && Bid>=S_LOOP+range+p2_off)) // SELL
      {
         if(signal()==1 || (s_cnt>0 && !tier2_sig))
         {
            if(lot_multiplier)  s_lot=lot2*MathPow(multiplier,s_cnt);
            if(!lot_multiplier) s_lot=lot2+(incr_1*s_cnt);
            OT=1; p_off=p1_off;
            if(s_cnt>0) p_off=p2_off; if(p_off>0) OT=5;
            //----
            OrderSend(Symbol(),OT,s_lot,Bid-p_off,3,0,0,"L"+(s_cnt+1),magic,0,Red);
         }
      }
   }
//+------------------------------------------------------------------+
   double profit=0, profit_1=0, profit_2=0, profit_3=0, t_lot=0, b2_lot=0, s2_lot=0;
   for(i=0; i<OrdersTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      profit+=OrderProfit();
      if(t_cnt>2 && OT2()=="L")
      {
         if(OrderComment()=="L"+t_cnt)     profit_1=OrderProfit();
         if(OrderComment()=="L"+(t_cnt-1)) profit_2=OrderProfit();
         profit_3=profit_1+profit_2;
      }
      if(cmd()< 2) t_lot+=OrderLots();
      if(cmd()==0){ b2_lot+=OrderLots(); if(OOP()==FOOP) b_price=OOP(); }
      if(cmd()==1){ s2_lot+=OrderLots(); if(OOP()==FOOP) s_price=OOP(); }
   }
//----
   double dev_b, dev_s, deviate=0;
   if(b_cnt>0){ dev_b=b_price-Ask; if(dev_b>0) deviate=dev_b/pt; }
   if(s_cnt>0){ dev_s=Bid-s_price; if(dev_s>0) deviate=dev_s/pt; }
//+------------------------------------------------------------------+
   for(i=OrdersTotal()-1; i>=0; i--)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      if(cmd()<2)
      {
         if(deviate>=d_reset && d_reset>0 && t_cnt>=reset_level)
         {
            if(OOP()==FOOP)
            OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
         }
         if(t_cnt>=curb_level && profit_3>=p_targ*curb_factor)
         {
            if(OCM()=="L"+t_cnt || OCM()=="L"+(t_cnt-1))
            OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
         }
      }
   }
//+------------------------------------------------------------------+
   double balance=0;
   for(i=0; i<OrdersHistoryTotal(); i++)
   {
      OrderSelect(i,SELECT_BY_POS,MODE_HISTORY);
      if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
      balance+=OrderProfit();
   }
//----
   if(t_cnt==0) GlobalVariableSet("bal_2"+Symbol()+magic,balance);
   bal_2=GlobalVariableGet("bal_2"+Symbol()+magic);
//+------------------------------------------------------------------+
   if(balance+profit>=bal_2+p_targ || AB+profit<AB*safety_coef) closeall=true;
   if(closeall || deleteall)
   {
      for(i=OrdersTotal()-1; i>=0; i--)
      {
         OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
         if(OrderSymbol()!=Symbol() || OrderMagicNumber()!=magic) continue;
         if(closeall) deleteall=true;
         if(cmd()>1 && deleteall) OrderDelete(OrderTicket());
         if(cmd()<2 && closeall)  OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,CLR_NONE);
      }
   }
//+------------------------------------------------------------------+
   string multi="Multiplier = "+DoubleToStr(multiplier,2), tier2="Yes", dev_r="";
   if(!lot_multiplier) multi="Increment = "+DoubleToStr(incr_1,2);
   if(!tier2_sig) tier2="No"; if(d_reset>0) dev_r=" / "+DoubleToStr(d_reset,0)+" / "+t_cnt+" / "+reset_level;
//----
   string line_1="Balance = "+DoubleToStr(bal_2,2)+"  |  Profit = "+DoubleToStr(bal_2+profit,2)+"  |  Equity = "+DoubleToStr(AccountEquity(),2)+"  |  Target = "+DoubleToStr(profit_target,2)+" / "+DoubleToStr(p_targ,2)+" / "+DoubleToStr(profit,2)+"\n";
   string line_2="Start Lot = "+DoubleToStr(lot,2)+"  |  Power lots = "+DoubleToStr(lot3,2)+"  |  Open = "+b_cnt+" / "+s_cnt+"  |  Pending = "+b2_cnt+" / "+s2_cnt+"  |  Deviate = "+DoubleToStr(deviate,0)+dev_r+"\n";
   string line_3="OPT = "+f_trade+"  |  "+multi+"  |  Spread = "+DoubleToStr((Ask-Bid)/pt,2)+"  |  Total Lots = "+DoubleToStr(t_lot,2)+"  |  Tier 2 = "+tier2+"\n";
//----
   Comment(line_1, line_2, line_3);
//----
   return(0);
  }
//+------------------------------------------------------------------+
int cmd()
  { int cmd=OrderType();
    return(cmd); }
//----
double OOP()
  { double OOP=OrderOpenPrice();
    return(OOP); }
//----
string OCM()
  { string OCM=OrderComment();
    return(OCM); }
//----
string OT2()
  { string OT2=StringSubstr(OrderComment(),0,1);
    return(OT2); }
//+------------------------------------------------------------------+
int signal()
  {
   int signal=2;
   if(iCustom(NULL,0,"SuperTrend",10,3.0,0,bars_closed)!=EMPTY_VALUE) signal=0;
   if(iCustom(NULL,0,"SuperTrend",10,3.0,1,bars_closed)!=EMPTY_VALUE) signal=1;
   return(signal);
  }
//+------------------------------------------------------------------+