//+------------------------------------------------------------------+
//|                        VolatilyScalp                             |
//|                                                                  |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Ch1qho"
#property link      "ch1qho@yahoo.com"
#define Magic        514


//--------TradingTime--------
extern  string  Time_Info   = "Base on Server Time";
extern  int     OpenHour    = 22; 
extern  int     CloseHour   = 5;

extern  bool    MM        = true;
extern  int     Risk      = 50;  
extern  double  Lots      = 0.1; 
extern  int     LotsDigit   = 2;
extern  int     Slippage    = 3;
extern  int     StopLoss        = 60;
//------------------------------------
extern string   Atr_Info   ="Read my manual before apply spesific filter";
extern bool     TradeLowRangeDay=true;
extern bool     TradeOutSideBand=true;
extern  double  MaxDayAtr   =250;
//-------------------------------------
extern string   EquityInfo ="It is better to set amount target when running on multiple pair";
extern double   EquityTarget=0;
extern bool     SecureMyEquityTarget=false;
//----------------------------------------------------------------------------
extern  int     MaxTradePerBar    = 1;  
extern  int     MaxTradePerPosition = 4;

extern  string  EAName  ="Volatily Scalp";
extern  string  Typical ="Sideway, counter trend system";
extern  string  Creator ="Ch1qho";
extern  string  Distribution="Donation";
extern  string  Version ="VS_EURCHF D_01";
extern string   email_YM          = "ch1qho@yahoo.com";

//----------------------------------------------------------------------------
int            TradePerBar = 0;

//---------------------------------------------------------------------------- 




bool allowtrading=true;

double diff=0;
bool allowbuy=true;
bool allowsell=true;
double SignalFilter=0;
double spread=0;
double mypoint=0.01;
int BarCount=0;
//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
{
   if(Digits==3){
      mypoint=0.01;
      spread=MarketInfo(Symbol(),MODE_SPREAD)/10;
   }else if(Digits==5){
      mypoint=0.0001;
      spread=MarketInfo(Symbol(),MODE_SPREAD)/10;
   }else{
      mypoint=Point;
      spread=MarketInfo(Symbol(),MODE_SPREAD);
   }
}
 
int start()
{
if(GetLastError()>1)Sleep(1000);// May this delay help to run on multiple pair 

if(SecureMyEquityTarget&&EquityTarget>0&&AccountEquity()>=EquityTarget){
   ForceCloseAll(Magic,OP_BUY);
   ForceCloseAll(Magic,OP_SELL);
   allowbuy=false;
   allowsell=false;
}

double speed1=iCustom(Symbol(),0,"CK_Speed",20,50,2,0);
double speed2=iCustom(Symbol(),0,"CK_Speed",20,25,2,1);
double speed3=iCustom(Symbol(),0,"CK_Speed",5,50,0,1);
double speed4=iCustom(Symbol(),0,"CK_Speed",5,50,1,1);
double kcup=iCustom(Symbol(),0,"Keltner Channels",20,2,0,0);//1st upper
double kcdo=iCustom(Symbol(),0,"Keltner Channels",20,2,2,0);//1st lower
double kcup2=iCustom(Symbol(),0,"Keltner Channels",20,2,3,0);//2nd upper
double kcdo2=iCustom(Symbol(),0,"Keltner Channels",20,2,4,0);//2nd lower
double up1=iBands(Symbol(),0,20,2,0,PRICE_MEDIAN,MODE_UPPER,1);
double do1=iBands(Symbol(),0,20,2,0,PRICE_MEDIAN,MODE_LOWER,1);
double wpr1=iWPR(Symbol(),1,20,0);
double ma=iMA(Symbol(),0,20,0,0,PRICE_MEDIAN,0);

double sp1=spread*2;
double atrfilter=iATR(Symbol(),0,20,1)/mypoint;

SignalFilter=MathMax(atrfilter,spread*2);//this is for averaging level
if(BarCount != Bars){
  TradePerBar = 0;
  BarCount = Bars;
}
if(TotalOrder(Magic,OP_BUY)==MaxTradePerPosition)allowbuy=false;
if(TotalOrder(Magic,OP_SELL)==MaxTradePerPosition)allowsell=false;
if(speed2==1&&speed1==1){
//allowbuy=false;
//allowsell=false;
//ForceCloseAll(Magic,OP_BUY);
//ForceCloseAll(Magic,OP_SELL);
}
/*
   No matter price accross ma or not, but wpr of 1 Minute show overbought/oversold
*/
if(wpr1>-20){
   if(TotalProfit(Magic,OP_BUY)>=SignalFilter){
      ForceCloseAll(Magic,OP_BUY);//profit Accumulation first, if rich we close all same position
      if(Close[0]<ma)allowbuy=false;
  }
  // CloseProfitOrder(Magic,OP_BUY,atrfilter,1,30000000);//otherwise, an open position has profit as much current atrfilter, close it.
}
if(wpr1<-80){
 if(TotalProfit(Magic,OP_SELL)>=SignalFilter){
   ForceCloseAll(Magic,OP_SELL);//profit Accumulation first, if rich we close all same position
   if(Close[0]>ma)allowsell=false;
 }
  // CloseProfitOrder(Magic,OP_SELL,atrfilter,1,30000000);//otherwise, an open position has profit as much current atrfilter, close it.
}

/*
   only if price accross ma
*/

if(speed1==1){
if(Close[0]>ma){
ForceCloseAll(Magic,OP_BUY);
//   if(TotalProfit(Magic,OP_BUY)>=1)ForceCloseAll(Magic,OP_BUY);//profit Accumulation first, if rich we close all same position
   //CloseProfitOrder(Magic,OP_BUY,1,1,30000000);//otherwise, an open position has profit as much 1 pips, close it.
}

if(Close[0]<ma){
ForceCloseAll(Magic,OP_SELL);
  // if(TotalProfit(Magic,OP_SELL)>=1)ForceCloseAll(Magic,OP_SELL);
  // CloseProfitOrder(Magic,OP_SELL,1,1,30000000);
}
}
if(Close[0]>kcup&&speed4==2){
   ForceCloseAll(Magic,OP_BUY);
   ForceCloseAll(Magic-1,OP_BUY);
}
//CloseProfitOrder(Magic,OP_BUY,-5,1,30000000);//we shoud make profit just after price accross ma
if(Close[0]<kcdo&&speed4==2){
   ForceCloseAll(Magic,OP_SELL);
   ForceCloseAll(Magic-1,OP_SELL);
}
if(Close[0]>kcup2){
   ForceCloseAll(Magic,OP_BUY);
   ForceCloseAll(Magic-1,OP_BUY);
}
//CloseProfitOrder(Magic,OP_BUY,-5,1,30000000);//we shoud make profit just after price accross ma
if(Close[0]<kcdo2){
   ForceCloseAll(Magic,OP_SELL);
   ForceCloseAll(Magic-1,OP_SELL);
}
//CloseProfitOrder(Magic,OP_SELL,-5,1,30000000);//we shoud make profit just after price accross ma

// choose your exit strategi after trading time has been passed
if(Tradetime()==0){
allowbuy=true;
allowsell=true;
   if(TotalProfit(Magic,OP_BUY)>=1)ForceCloseAll(Magic,OP_BUY);
   if(TotalProfit(Magic,OP_SELL)>=1)ForceCloseAll(Magic,OP_SELL);
}

   
   int            BUY_OpenPosition     = 0;
   int            SELL_OpenPosition    = 0;
   int            TOTAL_OpenPosition   = 0;
   int            Ticket               = -1;
   int            cnt                  = 0;
   double         Last_BUY_OpenPrice   =0;
   double         Last_SELL_OpenPrice   =0;
   for (cnt = 0; cnt < OrdersTotal(); cnt++) 
   {
      OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
      if (OrderSymbol() == Symbol() && OrderMagicNumber() == Magic && OrderCloseTime()==0) 
      {
         TOTAL_OpenPosition++;
         if (OrderType() == OP_BUY) 
         {
            BUY_OpenPosition++;
            Last_BUY_OpenPrice = OrderOpenPrice();
         }
         if (OrderType() == OP_SELL) 
         {
            SELL_OpenPosition++;
            Last_SELL_OpenPrice = OrderOpenPrice();
         }
      }
   }
   
 
 
  //////////////////////////////// //////////////////////////////////////////////////////////////////////////////////
   if (Tradetime()==1){
     if(AccountFreeMarginCheck(Symbol(),OP_BUY,GetLots())<=0 || GetLastError()==134)  {
                 Print("Not Enough Money ");
                 return(0);
      }  
      RefreshRates();
  //////////////////////////////// //////////////////////////////////////////////////////////////////////////////////
         if ((allowsell&&SELL_OpenPosition < MaxTradePerPosition)  && (TradePerBar < MaxTradePerBar) && ((Ask - Last_SELL_OpenPrice >= SignalFilter * mypoint) || SELL_OpenPosition < 1 ) && GetSignal(OP_SELL)==1){
            Ticket = OrderSend(Symbol(),OP_SELL,GetLots(),Bid,Slippage,Bid + StopLoss * mypoint,0,"Volatily Scalp"+Symbol(),Magic,0,Red); 
              if (Ticket > 0) {
                     TradePerBar++;
               }
               
            
          }
     //////////////////////////////// //////////////////////////////////////////////////////////////////////////////////
          if ((allowbuy&&BUY_OpenPosition < MaxTradePerPosition)  && (TradePerBar < MaxTradePerBar) && ((Last_BUY_OpenPrice - Bid >= SignalFilter * mypoint ) || BUY_OpenPosition < 1 ) && GetSignal(OP_BUY)==1){
		            Ticket = OrderSend(Symbol(),OP_BUY,GetLots(),Ask,Slippage,Ask - StopLoss * mypoint, 0,"Volatily Scalp"+Symbol(),Magic,0,Blue); 
                  if (Ticket > 0) {
                   
                     TradePerBar++;
                   
                   }
           }
    }
 /////////////////////locking funtion

  return(0);
} 

//----------------------------------------------------------------------------- 
int GetSignal(int OP)
{
int sideway =GetSignal3();
if(sideway==0)return(0);

double up=iBands(Symbol(),0,20,2,0,PRICE_MEDIAN,MODE_UPPER,0);
double do=iBands(Symbol(),0,20,2,0,PRICE_MEDIAN,MODE_LOWER,0);
/*
//Historical template, firs time i use fix envelopes 
*/
double evup=iEnvelopes(Symbol(),0,20,0,0,PRICE_MEDIAN,0.2,MODE_UPPER,0);
double evdo=iEnvelopes(Symbol(),0,20,0,0,PRICE_MEDIAN,0.2,MODE_LOWER,0);

double wpr=iWPR(Symbol(),0,20,0);
double wpr1=iWPR(Symbol(),1,20,0);
double ma=iMA(Symbol(),0,20,0,0,PRICE_MEDIAN,0);

double kcup=iCustom(Symbol(),0,"Keltner Channels",20,2,3,0);
double kcdo=iCustom(Symbol(),0,"Keltner Channels",20,2,4,0);

if (OP==OP_BUY) {
   //if(wpr1<-80&&Bid<do&&do>evdo)return(1);
   if (wpr1<-80&&Bid<=ma-(SignalFilter)*mypoint&&(Bid>do||Bid>kcdo)) return(1);//as long price not penetrate both bband and channel, go ahead
}
else if (OP==OP_SELL){
   //if(wpr1>-20&&Bid>up&&up<evup)return(1);
   if (wpr1>-20&&Bid>=ma+(SignalFilter)*mypoint&&(Bid<up||Bid<kcup)) return(1);//as long price not penetrate both bband and channel, go ahead
}
return(0);
}
//---------------------------------------------------------------------------------

//----------------------------------------------------------------------------- 
int GetSignal3()
{


double bbup=iBands(Symbol(),1440,20,2,0,PRICE_MEDIAN,MODE_UPPER,1);
double bbdo=iBands(Symbol(),1440,20,2,0,PRICE_MEDIAN,MODE_LOWER,1);
double dailyatr=iATR(Symbol(),1440,5,0)/mypoint;

double speed1=iCustom(Symbol(),0,"CK_Speed",20,50,0,0);
double speed2=iCustom(Symbol(),0,"CK_Speed",20,50,0,1);
double speed5=iCustom(Symbol(),0,"CK_Speed",5,50,0,0);
double speed6=iCustom(Symbol(),0,"CK_Speed",20,50,2,1);
double speed3=iCustom(Symbol(),0,"CK_Speed",5,50,0,0);
double speed4=iCustom(Symbol(),0,"CK_Speed",5,50,0,1);

double range=(iHigh(Symbol(),1440,1)-iLow(Symbol(),1440,1))/mypoint;

if(SignalFilter>=StopLoss) return(0);

if(dailyatr>=MaxDayAtr)return(0);

if(!TradeOutSideBand) if(iClose(Symbol(),1440,1)>bbup||iClose(Symbol(),1440,1)<bbdo)return(0);

if(!TradeLowRangeDay)if(range<(dailyatr/2))return(0);

   if(speed1==0&&speed2==0&&speed3==0&&speed4==0)return(1);
return(0);
}
//---------------------------------------------------------------------------------

//----------------------------------------------------------------------------- 

//----------------------------------------------------------------------------- 
void ForceCloseAll(int magic, int cmd) 
{ 
   for (int cnt = OrdersTotal()-1 ; cnt >= 0; cnt--) 
   { 
      OrderSelect(cnt,SELECT_BY_POS,MODE_TRADES); 
      if (OrderSymbol() == Symbol() && OrderMagicNumber() == magic && OrderCloseTime()==0) 
      { 
            if(OrderType()==cmd)  {
               OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,Blue); 
             }
           
      } 
   } 
}
//----------------------------------------------------------------------------- 
int Tradetime() 
{
   int TradingTime=0;
  if(OpenHour>CloseHour){
     if (Hour() <= CloseHour || Hour() >= OpenHour)TradingTime=1;
   }
   if(OpenHour<CloseHour){
      if(Hour()>=OpenHour&&Hour()<=CloseHour)TradingTime=1;
   }
   if(OpenHour==CloseHour){
      if(Hour()==OpenHour)TradingTime=1;
   }
   if (DayOfWeek() == 5 && Hour() >8)TradingTime=0;//No friday trade
   if  (DayOfWeek()==1&&Hour()<2)TradingTime=0;//No trade for first 2 hours on Monday
   if(DayOfYear()<7)TradingTime=0;//No Trade First week of Year 
   if(Month()==12&&Day()>20)TradingTime=0;//No trade after 20 December
   return(TradingTime); 
}
//----------------------------------------------------------------------------- 

double GetLots() 
{
   double lots,MD,RM,FMM,MinLots,LotSize; int lotsdigit;
   LotSize = MarketInfo(Symbol(), MODE_LOTSIZE);
   MD = NormalizeDouble(MarketInfo(Symbol(), MODE_LOTSTEP), 2); 
   RM = NormalizeDouble(MarketInfo(Symbol(), MODE_MARGINREQUIRED), 4);
   FMM = (RM+5)*100;
   if(LotsDigit==0)
   {
      if (MD==0.01) lotsdigit=2;
      else lotsdigit=1;
      LotsDigit=lotsdigit;
   }
   if (MM==true) lots = NormalizeDouble((AccountFreeMargin()*Risk/LotSize)/MaxTradePerPosition,LotsDigit);
   else lots=Lots;
   MinLots=NormalizeDouble(MarketInfo(Symbol(),MODE_MINLOT),2);
   double MaxLots =NormalizeDouble(MarketInfo(Symbol(),MODE_MAXLOT),2);   
   //if (LotsDigit == 2) MinLots = 0.01; 
   if (lots < MinLots) lots = MinLots;  
   if (lots > MaxLots) lots = MaxLots;     
   return (lots);      
}
//----------------------------------------------------------------------------- 

//----------------------------------------------------------------------------- 

//----------------------------------------------------------------------------- 
/*
   int magic= magicn umber
   int cmd= OP_BUY or OP_SELL
   int target= Target for profit in pip
   double diff=Minimum Minute in Market
   double diff=Maximum Minute in Market
   
*/
bool CloseProfitOrder(int magic, int cmd, int target, double diff, double diff2){
   bool result=false;
   int todelete=0;
   for(int i=OrdersTotal()-1;i>=0;i--){
   int profit=0;
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)){
         if(OrderType()==cmd&&OrderMagicNumber()==magic&&OrderSymbol()==Symbol()){
            if(OrderType()==OP_BUY){
               profit=(OrderClosePrice()-OrderOpenPrice())/mypoint;
             }
            if(OrderType()==OP_SELL){
               profit=(OrderOpenPrice()-OrderClosePrice())/mypoint;
             }
            if(profit>=target&& (TimeCurrent()-OrderOpenTime())>=diff*60&&(TimeCurrent()-OrderOpenTime())<=diff2*60){
               result=OrderClose(OrderTicket(),OrderLots(),NormalizeDouble(OrderClosePrice(),Digits),3,CLR_NONE);
                 
            }
            
         }
      }
   }
   return(result);
}
////////////////////////////////////////////////////////////////////////////////////////////////
/*
   return value= TotalProfit int pips
*/
int TotalProfit(int magic,int cmd){
   int result=0;
   double harga1;
   for(int cnt=OrdersTotal()-1;cnt>=0;cnt--)
        {
         OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
         if (OrderSymbol()==Symbol() && OrderMagicNumber()==magic)
           {
            if(cmd==OP_BUY&&cmd==OrderType()){
               result=result+((OrderClosePrice()-OrderOpenPrice())/mypoint);
            }
            if(cmd==OP_SELL&&cmd==OrderType()){
               result=result+((OrderOpenPrice()-OrderClosePrice())/mypoint);
            }  
           }
         }
  return(result);   
}

int TotalOrder(int magic,int cmd){
   int result=0;
   
   for(int cnt=OrdersTotal()-1;cnt>=0;cnt--)
        {
         OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
         if (OrderSymbol()==Symbol() && OrderMagicNumber()==magic)
           {
            if(cmd==OrderType()){
               result++;
            }
            
           }
         }
  return(result);   
}