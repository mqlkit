<<<<<<< HEAD
=======


>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
//+------------------------------------------------------------------+
//|                                                      scalper.mq4 |
//|                                   Copyright � 2011, Zarko Asenov |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, Zarko Asenov"
#property link      ""

/* General settings */
extern int          Slippage = 3;
extern double       Lot_Size = 0.12;
extern int          Magic_Number =  9876543210;
extern bool         Debug_Print = true;
<<<<<<< HEAD

/* Stochastic oscillator */
extern int          Stoch_K = 6;
extern int          Stoch_D = 4;
extern int          Stoch_Slow = 3;

/* MA settings */
extern int MAPeriod = 10;
extern int MAGainLimit = 10;
extern double DeltaCoef = 1.5;
extern double MA_Floor = 0.0015;

/* Some grid safety */
extern double Grid_Size_Pips = 0.00030;
extern int Num_stops = 2;


int buy_pos_id = 0;
int sell_pos_id = 0;
int digits = 0;
double tick_value = 0;
double buy_ceiling_price = 0.0;

=======
extern string       Orders_Label = "Scalper Zed";

/* Stochastic oscillator */
extern int          Stoch_K = 5;
extern int          Stoch_D = 5;
extern int          Stoch_Slow = 3;
extern int          Stoch_Period = PERIOD_H4;

/* ATR */
extern int          ATR_Fast = 7;
extern int          ATR_Slow = 39;
extern double       ATR_Ratio_Floor = 0.75;
extern int          ATR_Period = PERIOD_H1;

/* Some grid safety */
extern double Grid_Size_Pips = 0.00032;
extern int Num_stops = 2;
extern bool Trade_Fridays = false;


int deletes_count = 0;
int pending_deletes[32];
datetime last_go_buy = 0;
datetime last_go_sell = 0;
double buy_ceiling_price = 0.0;
double sell_ceiling_price = 0.0;
double spread = 0.0;
double stop_level = 0.0;
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

double
calc_profit(int magic_number, int op)
{
    double presult = 0.0;
     
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if (OrderSymbol() != Symbol() || OrderMagicNumber() != magic_number || OrderType() != op) continue;

        presult += OrderProfit();
        
    }

    return (presult);   
}

double
profit_buys()
{
     return (calc_profit(Magic_Number, OP_BUY));
}

double
profit_sells()
{
     return (calc_profit(Magic_Number, OP_SELL));
}

<<<<<<< HEAD
bool move_orders_stoploss(double price, int op)
{
    bool result = true;
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if (OrderSymbol() != Symbol() || OrderMagicNumber() != Magic_Number || OrderType() != op) continue;

        double take_profit = OrderTakeProfit();
        
        if (take_profit < price) take_profit = price + Grid_Size_Pips * 2;

        bool rc = OrderModify(OrderTicket(), OrderOpenPrice(), price, take_profit, OrderExpiration(), Blue);
        result = rc && result;
        if (rc == false && Debug_Print == true) {
          Print("Failed modyfying order "+OrderTicket()+" stop loss to "+price);
        }
        
    }
    return (result);
}

bool move_buys_stoploss(double price)
{
    return ( move_orders_stoploss(price, OP_BUY) );
=======
int
get_periods_time(datetime order_time)
{
     double time_diff = TimeCurrent() - order_time;
     return ( MathFloor( order_time / ( 60 * Period() ) ) );
}

double
get_high_price(datetime order_time)
{
     int time_pos = get_periods_time(order_time);
     return (High[time_pos]);
}

double
get_low_price(datetime order_time)
{
     int time_pos = get_periods_time(order_time);
     return (Low[time_pos]);
}

color
get_order_color(int op)
{
    switch(op) {
        case OP_BUY:
          return (Blue);
          
        case OP_BUYSTOP:
        case OP_BUYLIMIT:
          return (Aqua);
          
        case OP_SELL:
          return (Red);
          
        case OP_SELLSTOP:
        case OP_SELLLIMIT:
          return (Orange);
          
        default:
          if (Debug_Print) Print("Unknown Order Type "+op);
          return (0);
   }
}

bool 
move_orders_stoploss(
               int op, 
               datetime from, 
               datetime to)
{
    bool result = true;
    color order_color;
    
    order_color = get_order_color(op);
    
    for (int i = 0; i < OrdersTotal(); i++) {
    
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if (OrderSymbol() != Symbol() || 
          OrderMagicNumber() != Magic_Number || 
          OrderType() != op ||
          OrderOpenTime() < from ||
          OrderOpenTime() > to) 
               continue;

        double price;
        
        switch (op) {
          case OP_BUY:
          case OP_BUYSTOP:
          case OP_BUYLIMIT:
           price = get_low_price( OrderOpenTime() );
           break;
           
          case OP_SELL:
          case OP_SELLSTOP:
          case OP_SELLLIMIT:
           price = get_high_price( OrderOpenTime() );
           break;
           
          default:
           if (Debug_Print) Print("Uknown order type " + op);
           continue;
        }
       
        
        bool rc = OrderModify(OrderTicket(), OrderOpenPrice(), price, OrderTakeProfit(), OrderExpiration(), order_color);
        result = rc && result;
        
        
        if (rc == true) continue;
        
        if (Debug_Print == true) 
          Print("Failed modyfying order "+OrderTicket()+" stop loss to "+price);
          
        switch (op) {
          case OP_BUY:
          case OP_SELL:
               // order should be killed by end of next period no matter what
               if (OrderOpenTime() + Period() * 60 < Time[0] )
                    rc = OrderClose( OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, order_color);
               result = result && rc;
               if (Debug_Print == true && rc == false)
                    Print("Closing order " + OrderTicket() + " failed. Check your shit my friend.");
               break;
               
          case OP_BUYLIMIT:
          case OP_SELLLIMIT:
          case OP_BUYSTOP:
          case OP_SELLSTOP:
               OrderDelete( OrderTicket() );
               break;
               
          default:
               if (Debug_Print) Print("Uknown order type " + op);
               continue;
        }
        
    }
    
    return (result);
}

bool move_prev_buys_stoploss()
{
    bool rc = true;
    rc = rc && move_orders_stoploss(OP_BUY, 0, Time[0]);
    rc = rc && move_orders_stoploss(OP_BUYSTOP, 0, Time[0] );
    rc = rc && move_orders_stoploss(OP_BUYLIMIT, 0, Time[0] );
    
    return (rc);
}

bool move_prev_sells_stoploss()
{
    bool rc = true;
    rc = rc && move_orders_stoploss(OP_SELL, 0, Time[0]);
    rc = rc && move_orders_stoploss(OP_SELLSTOP, 0, Time[0]);
    rc = rc && move_orders_stoploss(OP_SELLLIMIT, 0, Time[0]);
    
    return (rc);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
}

bool
delete_all(int op)
{
    bool result = true;
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if (OrderSymbol() != Symbol() || OrderMagicNumber() != Magic_Number || OrderType() != op) continue;

        bool rc = OrderDelete(OrderTicket());
        result = rc && result;
        if (rc == false && Debug_Print == true) {
          Print("Failed deleting order " + OrderTicket() + " type " + op);
        }
        
    }
    return (result);
}

bool
<<<<<<< HEAD
delete_all_buy_stops()
=======
delete_all_pending_buys()
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
{
   return ( delete_all(OP_BUYSTOP) );
}

<<<<<<< HEAD
int
place_buy(double lot_size)
=======

bool
delete_all_pending_sells()
{
   return ( delete_all(OP_SELLSTOP) );
}

bool
delete_pending()
{
     bool ret = true;

     for (int i = 0; i < deletes_count; i++) {
          bool rc = OrderSelect(i, SELECT_BY_TICKET, MODE_TRADES);
          if (rc == false) {
               Print("Select of order " + pending_deletes[i] + " failed.");
               continue;
          }
          
          rc = OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Khaki);
          if (rc == false) Print("Uh-oh delete order " + OrderTicket() + " failed. Please check this out manually!");
               
     }
     
     deletes_count = 0;
}

int
place_buy(double lot_size, double take_profit, double stop_loss)
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
{
    if (lot_size == 0.0 ) {
        if (Debug_Print) Print("Scalping disabled!");
        return (0);
    }

<<<<<<< HEAD
    double ask = MarketInfo(Symbol(), MODE_ASK); 
          
    if (Debug_Print) Print("Placing scalp buy order at " + ask);
         
    int  ticketlong = OrderSend(
                                 Symbol(), 
                                 OP_BUY, 
                                 lot_size,
                                 NormalizeDouble(ask, digits), 
                                 Slippage, 
                                 0, 
                                 0, 
=======
    if (Debug_Print) Print("Placing scalp buy order at " + Ask);
         
    int ticket_num = OrderSend(
                                 Symbol(), 
                                 OP_BUY, 
                                 lot_size,
                                 Ask, 
                                 Slippage, 
                                 stop_loss, 
                                 take_profit, 
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
                                 "Scalper Zed", 
                                 Magic_Number,
                                 0, 
                                 Blue);
        
    if (Debug_Print) {                                      
<<<<<<< HEAD
         if (ticketlong < 0) {
             Print("Whaa long OrderSend failed with error #", GetLastError() );
             ticketlong = 0;
         } else {
             Print("Buy order placed with ticket " + ticketlong + "  at " + ask);
         }
    }
    
    return (ticketlong);
}

int
place_buy_stop(double lot_size, double price)
=======
         if (ticket_num < 0) {
             Print("Whaa long order failed with error " + GetLastError() );
             ticket_num = 0;
         } else {
             Print("Buy order placed with ticket " + ticket_num + "  at " + Ask);
         }
    }
    
    return (ticket_num);
}

int
place_sell(double lot_size, double take_profit, double stop_loss)
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
{
    if (lot_size == 0.0 ) {
        if (Debug_Print) Print("Scalping disabled!");
        return (0);
    }

<<<<<<< HEAD
    if (Debug_Print) Print("Placing scalp buy stop order at " + price);
         
    int  ticketlong = OrderSend(
                                 Symbol(), 
                                 OP_BUYLIMIT, 
                                 lot_size,
                                 NormalizeDouble(price, Digits), 
                                 Slippage, 
                                 0, 
                                 NormalizeDouble(buy_ceiling_price, Digits), 
                                 "Scalper Zed", 
                                 Magic_Number,
                                 0, 
                                 Blue);
        
    if (Debug_Print) {                                      
         if (ticketlong < 0) {
             Print("Whaa OrderSend OP_BUYSTOP, failed with error #" + GetLastError() + 
                   " current ASK price for " + Symbol() + " is " + Ask);
             ticketlong = 0;
         } else {
             Print("Buy stop order placed with ticket " + ticketlong + "  at " + price);
         }
    }
    
    return (ticketlong);
}

int
place_sell(double lot_size)
=======
    if (Debug_Print) Print("Placing scalp sell order at " + Bid);
         
    int ticket_num = OrderSend(
                                 Symbol(), 
                                 OP_SELL, 
                                 lot_size,
                                 Bid, 
                                 Slippage, 
                                 stop_loss, 
                                 take_profit, 
                                 "Scalper Zed", 
                                 Magic_Number,
                                 0, 
                                 Red);
        
    if (Debug_Print) {                                      
         if (ticket_num < 0) {
             Print("Whaa short order failed with error " + GetLastError() );
             ticket_num = 0;
         } else {
             Print("Short order placed with ticket " + ticket_num + "  at " + Bid);
         }
    }
    
    return (ticket_num);
}

int
place_pending_buy(double lot_size, double price, double take_profit)
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
{
    if (lot_size == 0.0 ) {
        if (Debug_Print) Print("Scalping disabled!");
        return (0);
    }

<<<<<<< HEAD
    double bid = MarketInfo(Symbol(), MODE_BID); 
          
    if (Debug_Print) Print("Placing scalp sell order at " + bid);
         
    int  ticketshort = OrderSend(
                                 Symbol(), 
                                 OP_SELL, 
                                 lot_size,
                                 NormalizeDouble(bid, digits), 
                                 Slippage, 
                                 0, 
                                 0, 
                                 "Scalper Zed", 
                                 Magic_Number, 
                                 0, 
                                 Red);
    if (Debug_Print) {                                      
         if (ticketshort < 0) {
             Print("Whaa short OrderSend failed with error #", GetLastError() );
             ticketshort = 0;
         } else {
             Print("Sell order placed with ticket " + ticketshort + "  at " + bid);
         }
    }
    
    return (ticketshort);
=======
    price = NormalizeDouble(price, Digits);

    if (Debug_Print) Print("Placing pending buy order at " + price);
    
    int op;
    int ticket_num = 0;
    double stop_loss = 0.0;
    
    if (price < Ask) op = OP_BUYLIMIT;
    else if (price > Ask) op = OP_BUYSTOP;
    else ticket_num = place_buy(lot_size, take_profit, 0.0);
    
    color order_color = get_order_color(OP_BUY); 
    datetime expire_time = Time[0] + Period() * 60;
    
    if (ticket_num == 0)      
        ticket_num = OrderSend(
                          Symbol(), 
                          op, 
                          lot_size,
                          price, 
                          Slippage,
                          stop_loss,
                          take_profit, 
                          Orders_Label, 
                          Magic_Number,
                          expire_time, 
                          order_color);
        
    if (Debug_Print) {                                      
         if (ticket_num < 0) {
             Print("Whaa pending order failed with error #" + GetLastError() + 
                   " current ask for " + Symbol() + " is " + Ask);
             ticket_num = 0;
         } else {
             Print("Pending buy order placed with ticket " + ticket_num + " at " + price);
         }
    }
    
    return (ticket_num);
}

int
place_pending_sell(double lot_size, double price, double take_profit)
{
    if (lot_size == 0.0 ) {
        if (Debug_Print) Print("Scalping disabled!");
        return (0);
    }

    price = NormalizeDouble(price, Digits);

    if (Debug_Print) Print("Placing pending sell order at " + price);
    
    int op;
    int ticket_num = 0;
    double stop_loss = 0.0;
    
    if (price > Bid) op = OP_SELLLIMIT;
    else if (price < Bid) op = OP_SELLSTOP;
    else ticket_num = place_sell(lot_size, take_profit, 0.0);
    
    color order_color = get_order_color(OP_SELL); 
    datetime expire_time = Time[0] + Period() * 60;
    
    if (ticket_num == 0)      
        ticket_num = OrderSend(
                          Symbol(), 
                          op, 
                          lot_size,
                          price, 
                          Slippage,
                          stop_loss,
                          take_profit, 
                          Orders_Label, 
                          Magic_Number,
                          expire_time, 
                          order_color);
        
    if (Debug_Print) {                                      
         if (ticket_num < 0) {
             Print("Whaa pending order failed with error #" + GetLastError() + 
                   " current bid for " + Symbol() + " is " + Bid);
             ticket_num = 0;
         } else {
             Print("Pending sell order placed with ticket " + ticket_num + " at " + price);
         }
    }
    
    return (ticket_num);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
}

bool
close_all(int magic_number, int op)
{
    bool rc = true;
           
    for (int i = 0; i < OrdersTotal(); i++) {
        
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
        if (OrderSymbol() != Symbol() || 
            OrderMagicNumber() != magic_number || 
            OrderType() != op) continue;

        int ret = OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Gray);
            
        if (ret < 0) {
           Print("Close order failed with error #", GetLastError() );
           rc = false;
        }
        
    }
    
    return (rc);
}

bool force_close_sells = false;
bool force_close_buys = false;

bool
close_all_sells()
{
     bool rc = close_all(Magic_Number, OP_SELL);
     
     if (rc == true) {
          force_close_sells = false;
<<<<<<< HEAD
          sell_pos_id = 0;
     } else {
          force_close_sells = true;
          sell_pos_id = 0;
=======
     } else {
          force_close_sells = true;
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
     }
}

bool
close_all_buys()
{
     bool rc = close_all(Magic_Number, OP_BUY);
     if (rc == true) {
          force_close_buys = false;
     } else {
          force_close_buys = true;
     }
<<<<<<< HEAD
     buy_pos_id = 0;
=======
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
}

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
<<<<<<< HEAD
     tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
     digits = MarketInfo(Symbol(), MODE_DIGITS);
=======
     last_go_buy = 0;
     last_go_sell = 0;
     deletes_count = 0;
     buy_ceiling_price = 0.0;
     sell_ceiling_price = 0.0;
     spread = MarketInfo(Symbol(), MODE_SPREAD) * Point;
     stop_level = MarketInfo(Symbol(), MODE_STOPLEVEL) * Point;
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

     return(0);
  }

//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {

   return(0);
  }
  
<<<<<<< HEAD
bool end_is_neigh()
{
  if (TimeCurrent() - Time[0] < Period() * 60.0 * 0.9) return (false);
  else return (true);
}  

bool
go_buy() 
{
  if (buy_ceiling_price != 0.0) return (false);
    
  if (end_is_neigh() == false) return (false);

  double cur_ma_diff = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 20, 50, 3, 0, 0);
  if (cur_ma_diff <= 0.0) return (false);
  
  double cur_ma_delta_long = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 120, 50, 3, 1, 0);
  double prev_ma_delta_long = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 120, 50, 3, 1, 0);
  if (cur_ma_delta_long < prev_ma_delta_long || cur_ma_delta_long <= 0.0) return (false);

  double cur_ma_delta = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 20, 50, 3, 1, 0);
  double prev_ma_delta = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 20, 50, 3, 1, 1);
  if (cur_ma_delta < prev_ma_delta || cur_ma_delta <= 0.0) return (false);
=======
bool 
end_is_neigh()
{
  if (TimeCurrent() - Time[0] < Period() * 60.0 * 0.8) return (false);
  else return (true);
}  

double
get_mid_price()
{
   return ( (High[0] + Low[0]) / 2.0 );
}

bool
is_friday()
{
     return (DayOfWeek() == 5);
}

bool
atr_go()
{
// trendy enough?
     double atr_ratio = iATR(Symbol(), ATR_Period, ATR_Fast, 0) / iATR(Symbol(), ATR_Period, ATR_Slow, 0);
     
     if (atr_ratio < ATR_Ratio_Floor) return (false);

     double atr_prev_ratio = iATR(Symbol(), ATR_Period, ATR_Fast, 0) / iATR(Symbol(), ATR_Period, ATR_Slow, 0);
     
     if (atr_prev_ratio < ATR_Ratio_Floor) return (false);
     
     if (atr_ratio < atr_prev_ratio) return (false);
     
     return (true);
}

bool
stoch_buy(int period)
{
   double stoch_k = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 0, 0);
   if (stoch_k > 85.0 || stoch_k < 45.0) return (false);
  
   // make sure stoch is going up
   double prev_stoch_k = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 0, 1);
   if (stoch_k <= prev_stoch_k) return (false);

   // were not in a down trend
   double stoch_d = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 1, 0);
   if (stoch_d >= stoch_k) return (false);

   double prev_stoch_d = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 1, 0);
   if (prev_stoch_k - prev_stoch_d > stoch_k - stoch_d) return (false);
   
   return (true);
}

bool
stoch_sell(int period)
{
  double stoch_k = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 0, 0);
  if (stoch_k > 65.0 || stoch_k < 15.0) return (false);
  
  // make sure stoch is going down
  double prev_stoch_k = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 0, 1);
  if (stoch_k >= prev_stoch_k) return (false);

  // were in a down trend
  double stoch_d = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 1, 0);
  if (stoch_d <= stoch_k) return (false);

  double prev_stoch_d = iStochastic(Symbol(), period, Stoch_K, Stoch_D, Stoch_Slow, MODE_EMA, 0, 1, 0);
  if (prev_stoch_k - prev_stoch_d < stoch_k - stoch_d) return (false);
  
  return (true);
}

bool
go_buy() 
{
  if (end_is_neigh() == false) return (false);
  if (Trade_Fridays == false && is_friday() == true) return (false);

  if (buy_ceiling_price != 0.0) return (false);

  double mid_price = get_mid_price();
  
  if (Ask + Grid_Size_Pips + stop_level > mid_price) return (false);

  if (atr_go() == false) return (false);

  if (stoch_buy(Stoch_Period) == false) return (false);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

  return (true);
}

bool
<<<<<<< HEAD
do_close_buys()
{
  if (force_close_buys == true) return (true);

  if (buy_ceiling_price == 0.0) return (false);

  double cur_ma_delta_long = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 120, 50, 3, 1, 0);
  if (cur_ma_delta_long <= 0.0) return (true);
  
  double cur_ma_diff_long = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 120, 50, 3, 0, 0);
  if (end_is_neigh() == false && (cur_ma_delta_long > 0.0 || cur_ma_diff_long > 0.0)) return (false);
  
  double cur_ma_delta = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 20, 50, 3, 1, 0);
  double prev_ma_delta = iCustom(Symbol(), Period(), "Ehlers ZeroLag MA Osc", 20, 50, 3, 1, 1);
  if (cur_ma_delta < prev_ma_delta && cur_ma_delta < 0.0) return (true);

  return (false); 
}

//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
{
   if (go_buy() == true)  {
     buy_ceiling_price = Ask;
     if (Debug_Print) Print("New buy ceiling is "+buy_ceiling_price);
     for (int i = 1; i < Num_stops + 1; i++) {
       place_buy_stop(Lot_Size, buy_ceiling_price - i * Grid_Size_Pips);
     }
   }

   if (buy_ceiling_price != 0.0 && Ask > buy_ceiling_price + Grid_Size_Pips) {
      move_buys_stoploss( Grid_Size_Pips * MathFloor(Ask / Grid_Size_Pips) - Grid_Size_Pips );
   }
      
   if (do_close_buys() == true) {
     if (Debug_Print) Print("Closing everything.");
      buy_ceiling_price = 0.0;
      close_all_buys();
      delete_all_buy_stops();
   }
     
   return(0);
}
//+------------------------------------------------------------------+
=======
go_sell() 
{
  if (end_is_neigh() == false) return (false);
  if (Trade_Fridays == false && is_friday() == true) return (false);

  if (sell_ceiling_price != 0.0) return (false);

  double mid_price = get_mid_price();
  
  if (Bid - Grid_Size_Pips - stop_level < mid_price) return (false);

  if (stoch_sell(Stoch_Period) == false) return (false);

  if (atr_go() == false) return (false);

  return (true);
}

bool
place_buys(double start_price)
{
     bool rc = true;
     
     for (int i = 1; i < Num_stops + 1; i++) {
       rc = rc && place_pending_buy(Lot_Size, start_price - i * Grid_Size_Pips, start_price - spread);
     }
     
     return (rc);
}

bool
place_sells(double start_price)
{
     bool rc = true;
     
     for (int i = 1; i < Num_stops + 1; i++) {
       rc = rc && place_pending_sell(Lot_Size, start_price + i * Grid_Size_Pips, start_price + spread);
     }
     
     return (rc);
}

//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
{
   spread = MarketInfo(Symbol(), MODE_SPREAD) * Point;

   if (last_go_buy == 0 && go_buy() == true)  {
     buy_ceiling_price = get_mid_price();
     if (Debug_Print) Print("New buy ceiling is " + buy_ceiling_price);
     last_go_buy = TimeCurrent();     
   } else if (last_go_sell == 0 && go_sell() == true)  {
     sell_ceiling_price = get_mid_price();
     if (Debug_Print) Print("New sell ceiling is " + sell_ceiling_price);
     last_go_sell = TimeCurrent();     
   } 
   
   if (buy_ceiling_price != 0.0) {
     place_buys(buy_ceiling_price);
     buy_ceiling_price = 0.0;
   } else if (sell_ceiling_price != 0.0) {
     place_sells(sell_ceiling_price);
     sell_ceiling_price = 0.0;
   } 
   
   if (last_go_buy != 0 && TimeCurrent() > last_go_buy + Period() * 60) {
     if (move_prev_buys_stoploss() == true) 
          last_go_buy = 0;
     else
          last_go_buy += 5 * 60;
   } else if (last_go_sell != 0 && TimeCurrent() > last_go_sell + Period() * 60) {
     if (move_prev_sells_stoploss() == true) 
          last_go_sell = 0;
     else
          last_go_sell += 5 * 60;
   } 

   if (Debug_Print) 
     Comment(
          "Buy ceiling: "+buy_ceiling_price+"\n"+
          "Last go buy: "+last_go_buy+"\n"+             
          "Spread: "+spread+"\n"+
          "Debug verbosity is on.");
             
   return(0);
}
//+------------------------------------------------------------------+
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
