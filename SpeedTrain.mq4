#property copyright "Zarko Asenov"
#property link      "jaltoh.6x.to"


extern int    Slippage         =       3;
extern double     LotSize= 0.1;
extern double     PipStep= 20.0;
extern bool  InitialBuyOrder = true;
extern int    MagicNumber         =  1771611;
extern double Close_All_Profit = 10.0;
extern bool Debug_Print = false;
extern int Bands_Days = 20;
extern int Bands_Deviations = 3;
extern double Min_Bands = 0.01;
extern int Num_Bands = 2;
extern double Order_Lifetime_Days = 0.2;
extern bool Add_Spread_to_Profit = false;
extern bool Add_Spread_to_PipStep = true;
extern bool Add_MinStop_to_PipStep = true;
extern bool Check_ADX = false;
extern int ADX_positions_to_check = 2;
extern int ADX_period_in_days = 4;
extern double ADX_min = 37.0;
extern double Hours_Bring_Even = 0;


int longOrderCount = 0;
int shortOrderCount = 0;
double startPrice = 0;
int    digits = 4;
bool initOrdersDone = false;
double spread = 0.0;
double pipstep_adjusted = 0.0;
double adx_diff = 0.0;

double get_minstop()
{
   return( MarketInfo(Symbol(), MODE_STOPLEVEL) + 1.0 );
}

double
get_adx(int index)
{
   double ADX1,ADX2;

   adx_diff = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_MAIN, index);
   
   ADX1 = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_PLUSDI, index);
   ADX2 = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_MINUSDI, index);

   if(ADX1 < ADX2) adx_diff *= -1.0;
      
   return (adx_diff);
}

bool
adx_is_sell(int num_pos)
{
   if (Check_ADX == false) return (true);

   double neg_adx_min = -1.0 * ADX_min;
   for (int i=0; i<num_pos; i++)
      if (get_adx(i) > neg_adx_min) return (false);

   return (true);
}

bool
adx_is_buy(int num_pos)
{
   if (Check_ADX == false) return (true);

   for (int i=0; i<num_pos; i++)
      if (get_adx(i) < ADX_min) return (false);

   return (true);
}

//+------------------------------------------------------------------+}

int init()
{
   digits=MarketInfo(Symbol(),MODE_DIGITS);  
   Print("digits ...... "+digits);
   
   return(0);
}


double
Calc_Profit()
{
   double result = 0.0;
   
   for (int i = 0;i < OrdersTotal(); i++) {
         OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
         if(OrderSymbol() != Symbol() || OrderMagicNumber() != MagicNumber) continue;
         
         if (OrderType() == OP_BUY || OP_SELL)
            result += OrderProfit();
  }

   return (result);   
}

double bandwidth = 0.0;

double
getBandwidth(int pos)
{
    double upper_band = iBands(NULL, PERIOD_M15, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_UPPER, pos);
    double lower_band = iBands(NULL, PERIOD_M15, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_LOWER, pos);
   
    return (upper_band - lower_band);
}

bool
inBands()
{
    for (int i = 0; i < Num_Bands; i++) {
        bandwidth = getBandwidth(i);
        if (bandwidth < Min_Bands) return (false);
    }
  
    return (true);
}

void 
moveHitBuyOrdersToBE()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if ((OrderMagicNumber() == MagicNumber) && (OrderType() == OP_BUY) &&
            ((TimeCurrent() - OrderOpenTime()) > (3600 * Hours_Bring_Even)) )
            
                OrderModify(OrderTicket(), OrderOpenPrice(), OrderOpenPrice(), 0, 0, Blue);

    }
}
      
void 
moveHitSellOrdersToBE()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if ((OrderMagicNumber() == MagicNumber) && (OrderType() == OP_SELL) &&
            ((TimeCurrent() - OrderOpenTime()) > (3600 * Hours_Bring_Even)) )

                OrderModify(OrderTicket(), OrderOpenPrice(), OrderOpenPrice(), 0, 0, Red);

    }
}

int start()
  {
        if(startPrice ==0) {
            startPrice=iOpen( NULL, PERIOD_M15, 0); //PERIOD_M15
            if (Debug_Print) Print("startPrice ...... "+startPrice);
        }

      pipstep_adjusted = PipStep;
      
      if (Add_Spread_to_PipStep)
         pipstep_adjusted += spread / MarketInfo(Symbol(), MODE_POINT);

      if (Add_MinStop_to_PipStep)
         pipstep_adjusted += get_minstop();

    
        if(!initOrdersDone){
          
          placeBuyOrder();
          placeSellOrder();
          if(InitialBuyOrder) {
            placeInitBuyOrder();
          }else{
            placeInitSellOrder();
          }
            
          initOrdersDone=true;
        }
        
        if(iClose( NULL, PERIOD_M15, 0) > (startPrice + pipstep_adjusted*Point)){
          startPrice=iClose( NULL, PERIOD_M15, 0);
           if (Debug_Print) Print("new startPrice ...... "+startPrice);
          // need to set the sl to BE of last order
           moveSellOrderToBE();
           moveHitBuyOrdersToBE();
          // place buy limit order
          if (inBands() && adx_is_buy(ADX_positions_to_check)) placeBuyOrder();        
        } else if (iClose( NULL, PERIOD_M15, 0) < (startPrice - pipstep_adjusted*Point)) {
          startPrice=iClose( NULL, PERIOD_M15, 0);
          if (Debug_Print) Print("new startPrice ...... "+startPrice);
          // need to set the sl to BE of last order
          moveBuyOrderToBE();  
          moveHitSellOrdersToBE();
          // place buy limit order
          if (inBands() && adx_is_sell(ADX_positions_to_check)) placeSellOrder();
        }

     double current_profit = Calc_Profit();
     double bid = MarketInfo(Symbol(), MODE_BID);
     double ask = MarketInfo(Symbol(), MODE_ASK);
      spread = ask - bid;

   if (Debug_Print) {
     Comment(
      "SpeedTrain Forte\nDrZ 2011\n"+
      "Symbol: "+Symbol()+"\n"+
      "Spread: "+spread+"\n"+
      "Bandwidth: "+bandwidth+"\n"+
      "Pipstep: "+pipstep_adjusted+"\n"+
      "Current Price: "+startPrice+"\n"+
      "Close Price: "+iClose( NULL, PERIOD_M15, 0)+"\n"+
      "ADX diff: "+adx_diff+"\n"+
      "Profit: "+current_profit+"\n"
     );      
   }
   
     if (current_profit > Close_All_Profit) {
     
      if (Debug_Print) 
         Print("Closing orders on profit " + AccountProfit() + " Spread: " + (ask - bid) + " pips.");

      int buy_ids[128];
      int sell_ids[128];
      int buys_count = 0;
      int sells_count = 0;
         
      for (int i = 0;i < OrdersTotal(); i++) {
      
         OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
         
         if(OrderSymbol() != Symbol() || OrderMagicNumber() != MagicNumber) continue;
           
         if (OrderType() == OP_BUY) {
            buy_ids[buys_count] = OrderTicket();
            buys_count++;
            OrderClose( OrderTicket(), LotSize, OrderClosePrice(), Red); //bid
         } else if (OrderType() == OP_SELL) {
            sell_ids[sells_count] = OrderTicket();
            sells_count++;
            OrderClose( OrderTicket(), LotSize, OrderClosePrice(), Red); //ask
         } else if (OrderType() == OP_SELLSTOP || OP_BUYSTOP) {
            OrderDelete( OrderTicket() );
         }
      }
      
      if (buys_count != 0 && sells_count != 0) {
         Print("Lets try save.");
         
         int buyix = 0;
         int sellix = 0;
         
         while (buyix < buys_count && sellix < sells_count) {
           bool res = OrderCloseBy(buy_ids[buyix], sell_ids[sellix], Red);
           if (!res) Print("Closing of orders "+buy_ids[buyix]+" and "+sell_ids[sellix]+" failed.");
           buyix++;
           sellix++;
         }

      }
      
      bid = MarketInfo(Symbol(), MODE_BID);
      ask = MarketInfo(Symbol(), MODE_ASK);

      Print("Closing orders the plain ole way.");
      for (i = 0;i < buys_count; i++)
         OrderClose(buy_ids[i], LotSize, bid, Red);
      for (i = 0;i < sells_count; i++)
         OrderClose(sell_ids[i], LotSize, ask, Red);
      
     }
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     void placeSellOrder(){
     
          double bid = MarketInfo(Symbol(), MODE_BID) ; 
          
          double sellPrice = bid - (pipstep_adjusted*Point);          
          double sellSL = bid;
          
          
          Print("Placing sell stop order at " + sellPrice + " with SL at " + sellSL);
         
         int  ticketshort = OrderSend(Symbol(), OP_SELLSTOP, LotSize, NormalizeDouble(sellPrice, digits),
                                      Slippage, NormalizeDouble(sellSL, digits), 0, "Short Speed Train", 
                                      MagicNumber, MathRound(Order_Lifetime_Days * 86400) + TimeCurrent(), Red);
                                      
          if (ticketshort < 0) {
              Print("Short OrderSend failed with error #",GetLastError()); 
              return;
          }
          
          Print("Sell stop order placed with ticket " + ticketshort + " at " + sellPrice);
          
          shortOrderCount++;
            //Sleep(1000);          
            
         
     
     }
       
     void placeBuyOrder(){
     
          double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
          double buyPrice = ask + (pipstep_adjusted*Point);          
          double buySL = ask;
          
          
          if (Debug_Print) Print("Placing buy stop order at "+buyPrice+" with SL at "+buySL);
         
         int  ticketlong = OrderSend(Symbol(), OP_BUYSTOP, LotSize, NormalizeDouble(buyPrice, digits),
                                      Slippage, NormalizeDouble(buySL, digits), 0, "Long Speed Train",
                                      MagicNumber, MathRound(Order_Lifetime_Days * 86400) + TimeCurrent(),Blue);
                                      
          if (ticketlong < 0) {
              Print("Long OrderSend failed with error #",GetLastError()); 
              return;
          }
          
          if (Debug_Print) Print("Buy stop order placed with ticket " + ticketlong + " at " + buyPrice);
          
          longOrderCount++;
     }
      
      
      void placeInitBuyOrder(){
     
          double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
          double buySL = ask - (pipstep_adjusted*Point);          
         
          
          
          if (Debug_Print) Print("Placing buy stop order at "+ask+" with SL at "+buySL);
         
         int  ticketlong = OrderSend(Symbol(), OP_BUY, LotSize, NormalizeDouble(ask, digits),
                                      Slippage, NormalizeDouble(buySL, digits), 0, "Long Speed Train", 
                                      MagicNumber, 0, Blue);
                                      
          if (ticketlong < 0) {
              if (Debug_Print) Print("Long OrderSend failed with error #",GetLastError()); 
              return;
          }
          
          if (Debug_Print) Print("Buy stop order placed with ticket "+ticketlong+"  at "+ask);
          
          longOrderCount++;        
     
     }
      
      
      void placeInitSellOrder(){
     
          double bid = MarketInfo(Symbol(), MODE_BID) ; 
          
          double sellSL = bid + (pipstep_adjusted*Point);            
          
          if (Debug_Print) Print("Placing sell stop order at "+bid+" with SL at "+sellSL);
         
         int  ticketshort = OrderSend(Symbol(),OP_SELL,LotSize,NormalizeDouble(bid, digits),
                                      Slippage,NormalizeDouble(sellSL, digits),0,"Short Speed Train",MagicNumber,0,Red);
                                      
          if (ticketshort < 0) {
              if (Debug_Print) Print("Short OrderSend failed with error #",GetLastError()); 
              return;
          }
          
          if (Debug_Print) Print("Sell stop order placed with ticket "+ticketshort+"  at "+bid);
          
          shortOrderCount++;        
     
     }
      
      
      void moveBuyOrderToBE(){
         int lastTicket=0;
          
         for(int j = 0; j < OrdersTotal(); j++)
        {
            OrderSelect(j,SELECT_BY_POS,MODE_TRADES);
           // Print("order type "+OrderType());
            
            if (OrderMagicNumber() == MagicNumber && OrderType() == OP_BUYSTOP || OP_BUY)
               {
                  int ticketId = OrderTicket() ;
                  //Print("ticketId "+ticketId);
                 if(OrderType() == OP_BUYSTOP && ticketId>   lastTicket) lastTicket=ticketId;  
                  
                 //Print("lastTicket "+lastTicket); 
                 if (TimeCurrent() - OrderOpenTime() > 3600 * Hours_Bring_Even)
                     OrderModify(ticketId,OrderOpenPrice(),OrderOpenPrice(),0,0,Blue);
                     
                     }
       }
       
       if(lastTicket >0) {             
            if (Debug_Print) Print(" ----------------------- Moving long order number "+lastTicket+ " to BE at  "+OrderOpenPrice());    
       
            int ticketlong = OrderModify(lastTicket,OrderOpenPrice(),OrderOpenPrice(),0,0,Blue);
          
                                      
            if (ticketlong < 0) {
                   if (Debug_Print) Print("Long SL2BE order failed with error #",GetLastError()); 
                   return;
            }
              
           
        }           
         
     
     }
     
     
     
         
      
      
      
      void moveSellOrderToBE(){
         int lastTicket=0;
      
         for(int j = 0; j < OrdersTotal(); j++)
        {
            OrderSelect(j,SELECT_BY_POS,MODE_TRADES);

           // Print("order type "+OrderType());
            if (OrderMagicNumber() == MagicNumber && OrderType() == OP_SELLSTOP || OP_SELL)
               {
                  int ticketId = OrderTicket() ;
                 // Print(" ticketId "+ticketId);    
                  if( ticketId>   lastTicket && OrderType() == OP_SELLSTOP) lastTicket=ticketId; 
                 // Print(" lastTicket "+lastTicket);             
                 if (TimeCurrent() - OrderOpenTime() > 3600 * Hours_Bring_Even)
                  OrderModify(ticketId,OrderOpenPrice(),OrderOpenPrice(),0,0,Red);
                  }

       }
       
          
       
       
       
       if(lastTicket>0) {
       
            if (Debug_Print) Print("------------ Moving short order number "+lastTicket+ " to BE at  "+OrderOpenPrice());
            int ticketshort = OrderModify(lastTicket,OrderOpenPrice(),OrderOpenPrice(),0,0,Red);          
                                      
            if (ticketshort < 0) {
                   if (Debug_Print) Print("Short SL2BE order failed with error #",GetLastError()); 
                   return;
            }
             
        }
          
                        
         
     
     }
      
     
  
   


