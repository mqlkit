//������������� � ���������� 17.04.2006 ������� ������� 
/*
���  ������  ����������  �������  �������� ����� 
INDICATOR_COUNTED.mqh 
PriceSeries.mqh 
� ����� (����������): MetaTrader\experts\include\
*/
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//|                                               BrainTrend1Sig.mq4 |
//|                                     BrainTrading Inc. System 7.0 |
//|                                      http://www.braintrading.com |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
#property copyright "BrainTrading Inc. System 7.0"
#property link      "http://www.braintrading.com"

#property indicator_chart_window
#property indicator_buffers 2
#property indicator_color1 Magenta
#property indicator_color2 Aqua
//---- ������� ��������� ���������� ��������������������������������������������������������������������������������������������������+
extern int       EnableAlerts=0;
extern int       SignalID=0;
extern int Input_Price_Customs = 0;  //����� ���, �� ������� ������������ ������ ���������� 
//(0-CLOSE, 1-OPEN, 2-HIGH, 3-LOW, 4-MEDIAN, 5-TYPICAL, 6-WEIGHTED, 7-Heiken Ashi Close, 8-SIMPL, 9-TRENDFOLLOW, 10-0.5*TRENDFOLLOW)
//---- �������������������������������������������������������������������������������������������������������������������������������+
//---- buffers
double Ind_Buffer1[];
double Ind_Buffer2[];
//---- 
int    f,p,x1,x2,value11,a1,tsig,MEMORY[2],T2[2];
double value2,value3,Range,Range1,Range2,val1,val2,d,val3,s;
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//| BrainTrend1Sig initialization function                           |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
int init()
  {
//---- indicator 0
   SetIndexStyle (0,DRAW_ARROW);
   SetIndexArrow (0,108);
   SetIndexBuffer(0,Ind_Buffer1);
   SetIndexEmptyValue(0,0.0);
//---- indicator 1  
   SetIndexStyle (1,DRAW_ARROW);
   SetIndexArrow (1,108);
   SetIndexBuffer(1,Ind_Buffer2);
   SetIndexEmptyValue(1,0.0);
//----
   string short_name;
   short_name="BrainTrend1Sig";
   IndicatorShortName(short_name);
   SetIndexLabel(0,""+short_name+"_SELL");
   SetIndexLabel(1,""+short_name+"_BUY" );
   IndicatorDigits(MarketInfo(Symbol(),MODE_DIGITS));
//---- ��������� ������� �� ������������ �������� ������� ���������� 
   PriceSeriesAlert(Input_Price_Customs);
//---- 
   f=7; d=2.3; x1 = 53; x2 = 47; value11 = 9; s=1.5;
//----
   return(0);
  }
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//| BrainTrend1Sig iteration function                                |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
int start()
  {
//---- �������� ���������� ����� �� ������������� ��� �������
if (Bars < 11)return(0);
//----+ �������� ����� ���������� � ��������� ��� ������������ �����
//---- ������������ ��������� ���� ������������ � ������������ ����� ��� ����������� � ���������
int MaxBar,Tnew,bar,counted_bars=INDICATOR_COUNTED(0); INDICATOR_COUNTED(1);
//---- �������� �� ��������� ������
if (counted_bars<0){INDICATOR_COUNTED(-1);return(-1);}
//---- ��������� ������������ ��� ������ ���� ���������� 
if (counted_bars>0) counted_bars--;
//---- ����������� ������ ������ ������� ����, ������� � �������� ����� �������� �������� ����� �����
MaxBar=Bars-1-10;
bar=Bars-counted_bars-1; 
if (bar>MaxBar)bar=MaxBar;
//+--- �������������� �������� ���������� +================+
Tnew=Time[bar+1];
if (bar<MaxBar)
 if (Tnew==T2[1])p=MEMORY[1]; 
 else 
 if (Tnew==T2[0]){p=MEMORY[0];MEMORY[1]=MEMORY[0];}  
 else
  {
   if (Tnew>T2[1])Print("ERROR01");
   else Print("ERROR02");
   INDICATOR_COUNTED(-1);return(-1);  
  }
//+--- +===================================================+
while(bar>=0)

      {
      Range = iATR(NULL,0,f,bar);
      Range1 = Range / d;
      Range2 = Range * s / 4;
      value2 = iStochastic(NULL,0,value11,value11,1,0,0,0,bar);
      val1 = 0;
      val2 = 0;
      val3 = MathAbs(PriceSeries(Input_Price_Customs,bar) - PriceSeries(Input_Price_Customs,bar+2));
      if (value2 < x2 && val3 > Range1&&(p == 2 || p == 0)) 
            {
            value3 = High[bar] + Range2;
            val1 = value3;
            p = 1;
            }       
      if (value2 > x1 && val3 > Range1&&(p == 1 || p == 0)) 
            {
            value3 = Low[bar] - Range2;
            val2 = value3;
            p = 2;
            }
      Ind_Buffer1[bar]=val1;
      Ind_Buffer2[bar]=val2; 
      //+--- ���������� �������� ���������� +====+
      if ((bar==2)||(bar==1))
        {
         MEMORY[bar-1]=p;
         T2[bar-1]=Time[bar];
        }
      //+---+====================================+      
    
       bar--;
      }
if (EnableAlerts == 1 && val1 > 0 && tsig != 1) 
      {
      tsig = 1;
      a1 = FileOpen("alert1" + SignalID,";");
      FileWrite(a1,"Sell ", Symbol(), " at ", PriceSeries(Input_Price_Customs,0), " S/L ", val1, " BT1 M", Period());
      FileClose(a1);
      }
if (EnableAlerts == 1 && val1 > 0 && tsig != 2) 
      {
      tsig = 2;
      a1 = FileOpen("alert1" + SignalID,";");
      FileWrite(a1,"Buy ",  Symbol(), " at ", PriceSeries(Input_Price_Customs,0)," S/L ", val2, " BT1 M",  Period());
      FileClose(a1);
      }
//----
   return(0);
  }
//+---------------------------------------------------------------------------------------------------------------------------+
//----+ �������� ������� INDICATOR_COUNTED (���� INDICATOR_COUNTED.mqh ������� �������� � ����� (����������): 
#include <INDICATOR_COUNTED.mqh> ////////////////////////////////                                  MetaTrader\experts\include)
//+---------------------------------------------------------------------------------------------------------------------------+
//----+ �������� ������� PriceSeries, ���� PriceSeries.mqh ������� �������� � ����� (����������): MetaTrader\experts\include
//----+ �������� ������� PriceSeriesAlert (�������������� ������� ����� PriceSeries.mqh)
#include <PriceSeries.mqh>
//+---------------------------------------------------------------------------------------------------------------------------+