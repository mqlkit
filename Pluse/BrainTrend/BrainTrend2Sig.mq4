//������������� � ���������� 17.04.2006 ������� ������� 
/*
���  ������  ����������  �������  �������� ����� 
INDICATOR_COUNTED.mqh 
PriceSeries.mqh 
� ����� (����������): MetaTrader\experts\include\
Heiken Ashi#.mq4
� ����� (����������): MetaTrader\experts\indicators\
*/
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//|                                               BrainTrend2Sig.mq4 |
//|                                                www.forex-tsd.com |
//|                                                Nick Bilak        |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
#property copyright "BrainTrading Inc."
#property link      "www.forex-tsd.com"
//---- ��������� ���������� � ������� ����
#property indicator_chart_window 
//---- ���������� ������������ ��������
#property indicator_buffers 2
//---- ����� ����������
#property indicator_color1 Blue
#property indicator_color2 Red
//---- ������� ��������� ���������� ��������������������������������������������������������������������������������������������������+
extern int Input_Price_Customs = 0;  //����� ���, �� ������� ������������ ������ ���������� 
//(0-CLOSE, 1-OPEN, 2-HIGH, 3-LOW, 4-MEDIAN, 5-TYPICAL, 6-WEIGHTED, 7-Heiken Ashi Close, 8-SIMPL, 9-TRENDFOLLOW, 10-0.5*TRENDFOLLOW,
//11-Heiken Ashi High, 12-Heiken Ashi Low, 13-Heiken Ashi Open, 14-Heiken Ashi Close.)
//---- �������������������������������������������������������������������������������������������������������������������������������+
//---- buffers
double Ind_Buffer1[];
double Ind_Buffer2[];

bool   river=True,Expert=true,RIVER[2];
int    artp,glava,p,Curr,T2[2],GLAVA[2];
double spread,Weight,r,r1,Range1,s,val1,val2,EMAXTRA[2],VALUES[1][2];
double dartp,cecf,Emaxtra,widcha,TR,Values[1],ATR,low,high,Series1;
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//| BrainTrend2Sig initialization function                           |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
int init()
  {
//---- indicator 0
   SetIndexStyle (0,DRAW_ARROW);
   SetIndexBuffer(0,Ind_Buffer1);
   SetIndexArrow (0,233);
//---- indicator 1  
   SetIndexStyle (1,DRAW_ARROW);
   SetIndexBuffer(1,Ind_Buffer2);
   SetIndexArrow (1,234);
//----
   string short_name;
   short_name="BrainTrend2Sig";
   IndicatorShortName(short_name);
   SetIndexLabel(0,""+short_name+"_BUY");
   SetIndexLabel(1,""+short_name+"_SELL" );
   IndicatorDigits(MarketInfo(Symbol(),MODE_DIGITS));
   //---- ��������� ������� �� ������������ �������� ������� ���������� 
   PriceSeriesAlert(Input_Price_Customs);
//----
   spread=MarketInfo(Symbol(),MODE_SPREAD)*Point;
   
   artp=7;dartp=7.0;s=2;cecf=0.7;
//----
   ArrayResize( Values, artp);
   ArrayResize( VALUES, artp);
//----
   return(0);
  }
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//| BrainTrend2Sig iteration function                                |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
int start() 
{
//---- �������� ���������� ����� �� ������������� ��� �������
if (Bars < 11)return(0);
//----+ �������� ����� ���������� � ��������� ��� ������������ �����
//---- ������������ ��������� ���� ������������ � ������������ ����� ��� ����������� � ���������
int limit,Tnew,MaxBar,bar,J,counted_bars=INDICATOR_COUNTED(0); INDICATOR_COUNTED(1);
//---- �������� �� ��������� ������
if (counted_bars<0){INDICATOR_COUNTED(-1);return(-1);}
//---- ��������� ������������ ��� ������ ���� ���������� 
if (counted_bars>0) counted_bars--;
//---- ����������� ������ ������ ������� ����, ������� � �������� ����� �������� �������� ����� ����� 
MaxBar=Bars-3;
limit = Bars-counted_bars-1;
if(limit>=MaxBar)
 {
  limit = MaxBar;
  Emaxtra = PriceSeries(Input_Price_Customs,MaxBar + 1);
  glava=0;
  double T_Series2=PriceSeries(Input_Price_Customs,MaxBar+2);
  double T_Series1=PriceSeries(Input_Price_Customs,MaxBar+1);
  if(T_Series2 > T_Series1) river = True; else river = False;
 } 
//+--- �������������� �������� ���������� +================+
Tnew=Time[limit+1];
if (limit<MaxBar)
 if (Tnew==T2[1]) 
   {
    for(int xx=0;xx<=artp-1;xx++)Values[xx]=VALUES[xx][1];
    glava=GLAVA[1];
    Emaxtra=EMAXTRA[1];
    river = RIVER[1];
    Expert=false;
   }  
 else 
 if (Tnew==T2[0]) 
   {
    for(int yy=0;yy<=artp-1;yy++)Values[yy]=VALUES[yy][0];
    glava=GLAVA[0];
    Emaxtra=EMAXTRA[0];
    river = RIVER[0];
    //+--- �������������� �������� ����������
    for(int ww=0;ww<=artp-1;ww++)VALUES[ww][1]=VALUES[ww][0];
    GLAVA[1]=GLAVA[0];
    EMAXTRA[1]=EMAXTRA[0];
    RIVER[1] = RIVER[0];
   }
 else
  {
   if (Tnew>T2[1])Print("ERROR01");
   else Print("ERROR02");
   INDICATOR_COUNTED(-1);return(-1);  
  }
//+--- +===================================================+
bar=limit;
//----   
while(bar>=0)      
   {  
      low=Low [bar];
      high=High[bar];
      Series1=PriceSeries(Input_Price_Customs,bar + 1);
      TR = spread+ high - low;
      if( MathAbs(spread+ high - Series1) > TR ) TR = MathAbs(spread+ high - Series1);
      if( MathAbs(low - Series1) > TR)  TR = MathAbs(low - Series1);
      if (bar == MaxBar) for(J=0;bar<=artp-1;J++) Values[J] = TR;  
 		Values[glava] = TR;
      ATR = 0;
      Weight = artp;
      Curr = glava;
      for (J = 0;J<=artp - 1;J++) 
      {
         ATR += Values[Curr] * Weight;
         Weight -= 1.0;
         Curr--;
         if (Curr == -1) Curr = artp - 1;
      }
      ATR = 2.0 * ATR / (dartp * (dartp + 1.0));
      glava++;
      if (glava == artp) glava = 0;
      widcha = cecf * ATR;
      if (river && low < Emaxtra - widcha) 
      {
         river = False;
         Emaxtra = spread+ high;
      }
      if (!river && spread+ high > Emaxtra + widcha) 
      {
         river = True;
         Emaxtra = low;
      }
      if (river && low > Emaxtra) Emaxtra = low;
      if (!river && spread+ high < Emaxtra ) Emaxtra = spread+ high;
      Range1 = iATR(NULL,0,10,bar);
      val1 = 0;
      val2 = 0;
      if (river) 
        {
         if (p != 1) r1 = low - Range1 * s / 3.0;
         if (p == 1) r1 = -1.0;
         if (r1 > 0) 
         {
            val1 = r1;
            val2 = 0;
         } 
         else 
         {
            val1 = 0;
            val2 = 0;
         }
         Ind_Buffer1[bar]=val1;
         p = 1;
        } 
      else 
        {
         if (p != 2) r1 = spread+ high + Range1 * s / 3.0;
         if (p == 2) r1 = -1.0;
         if (r1 > 0) 
         {
            val1 = 0;
            val2 = r1;
         } 
         else 
         {
            val1 = 0;
            val2 = 0;
         }
         Ind_Buffer2[bar]=val2;
         p = 2;
        }
      //+--- ���������� �������� ���������� +======================+
      if ((bar==2)||((bar==1)&&(Expert==true)))
        {
         for(int zz=0;zz<=artp-1;zz++)VALUES[zz][bar-1]=Values[zz];
         GLAVA[bar-1]=glava;
         EMAXTRA[bar-1]=Emaxtra;
         RIVER[bar-1]=river;
         T2[bar-1]=Time[bar];
        }
      //+---+======================================================+    
      bar--;
   }
   
//----
   return(0);
  }
//+---------------------------------------------------------------------------------------------------------------------------+
//----+ �������� ������� INDICATOR_COUNTED (���� INDICATOR_COUNTED.mqh ������� �������� � ����� (����������): 
#include <INDICATOR_COUNTED.mqh> ////////////////////////////////                                  MetaTrader\experts\include)
//+---------------------------------------------------------------------------------------------------------------------------+
//----+ �������� ������� PriceSeries, ���� PriceSeries.mqh ������� �������� � ����� (����������): MetaTrader\experts\include
//----+ �������� ������� PriceSeriesAlert (�������������� ������� ����� PriceSeries.mqh)
#include <PriceSeries.mqh>
//+---------------------------------------------------------------------------------------------------------------------------+