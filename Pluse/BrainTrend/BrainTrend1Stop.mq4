//������������� � ���������� 17.04.2006 ������� ������� 
/*
���  ������  ����������  �������  �������� ����� 
INDICATOR_COUNTED.mqh 
PriceSeries.mqh 
� ����� (����������): MetaTrader\experts\include\
*/
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//|                                              BrainTrend1Stop.mq4 |
//|                                     BrainTrading Inc. System 7.0 |
//|                                      http://www.braintrading.com |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
#property copyright "Copyright � 2005, MetaQuotes Software Corp."
#property link      "http://www.braintrading.comt"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_color1 Magenta
#property indicator_color2 Aqua
//---- ������� ��������� ���������� ��������������������������������������������������������������������������������������������������+
extern int   EnableAlerts=0;
extern int   SignalID    =0;
extern int Input_Price_Customs = 0;  //����� ���, �� ������� ������������ ������ ���������� 
//(0-CLOSE, 1-OPEN, 2-HIGH, 3-LOW, 4-MEDIAN, 5-TYPICAL, 6-WEIGHTED, 7-Heiken Ashi Close, 8-SIMPL, 9-TRENDFOLLOW, 10-0.5*TRENDFOLLOW)
extern int   Simbol    =119;
//---- �������������������������������������������������������������������������������������������������������������������������������+
//---- buffers
double Ind_Buffer1[];
double Ind_Buffer2[];
//----
int f,x1,x2,value11,limit,p,MEMORY[2],T2[2];
double val1,val2,val3,r,s,d;
double value2,value3,value4,value5,value6,Range,range1,a1,bs,bs1;
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//| Custom indicator initialization function                         |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
int init()
  {
//---- indicator 0
   SetIndexStyle (0,DRAW_ARROW);
   SetIndexArrow (0,Simbol);
   SetIndexBuffer(0,Ind_Buffer1);
   SetIndexEmptyValue(0,0.0);
//---- indicator 1  
   SetIndexStyle (1,DRAW_ARROW);
   SetIndexArrow( 1,Simbol);
   SetIndexBuffer(1,Ind_Buffer2);
   SetIndexEmptyValue(1,0.0);
//----
   string short_name;
   short_name="BrainTrend1Stop";
   IndicatorShortName(short_name);
   SetIndexLabel(0,""+short_name+"_SELL");
   SetIndexLabel(1,""+short_name+"_BUY" );
   IndicatorDigits(MarketInfo(Symbol(),MODE_DIGITS));
   //---- ��������� ������� �� ������������ �������� ������� ���������� 
   PriceSeriesAlert(Input_Price_Customs);
//----
   s=1.5; d=2.3; f=7; value11 = 9; x1 = 53; x2 = 47;
//----
   return(0);
  }
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
//| Custom indicator iteration function                              |
//+SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS+
int start()
  {
//---- �������� ���������� ����� �� ������������� ��� �������
if (Bars < 11)return(0);
//----+ �������� ����� ���������� � ��������� ��� ������������ �����
//---- ������������ ��������� ���� ������������ � ������������ ����� ��� ����������� � ���������
int MaxBar,Tnew,bar,counted_bars=INDICATOR_COUNTED(0); INDICATOR_COUNTED(1);
//---- �������� �� ��������� ������
if (counted_bars<0){INDICATOR_COUNTED(-1);return(-1);}
//---- ��������� ������������ ��� ������ ���� ���������� 
if (counted_bars>0) counted_bars--;
//---- ����������� ������ ������ ������� ����, ������� � �������� ����� �������� �������� ����� �����
MaxBar=Bars-1-10;
bar=Bars-counted_bars-1; 
if (bar>MaxBar)bar=MaxBar;
//+--- �������������� �������� ���������� +================+
Tnew=Time[bar+1];
if (bar<MaxBar)
 if (Tnew==T2[1])p=MEMORY[1]; 
 else 
 if (Tnew==T2[0]){p=MEMORY[0];MEMORY[1]=MEMORY[0];}  
 else
  {
   if (Tnew>T2[1])Print("ERROR01");
   else Print("ERROR02");
   INDICATOR_COUNTED(-1);return(-1);  
  }
//+--- +===================================================+
while(bar>=0)    
      {
      Range  = iATR(NULL,0,f, bar)/ d;
      range1 = iATR(NULL,0,10,bar)* s;
      value2 = iStochastic(NULL,0,value11,value11,1,0,0,0,bar);
//---- 
      val1 = 0;
      val2 = 0;
      val3 = MathAbs(PriceSeries(Input_Price_Customs,bar) - PriceSeries(Input_Price_Customs,bar+2));
      if (value2 < x2 && val3 > Range&& p != 1 )
         {
         value3 = High[bar] + range1 / 4;
         val1 = value3;
         p = 1; 
         r = val1; 
         bs = r; 
         Ind_Buffer1[bar]=val1; 
         Ind_Buffer2[bar]=0;       
         }
      if (value2 > x1 && val3 > Range && p != 2 ) 
         {
         value3 = Low[bar] - range1/ 4;
         val2 = value3;
         p = 2;
         r = val2; 
         bs = r; 
         Ind_Buffer1[bar]=0;
         Ind_Buffer2[bar]=val2;   
         }
      value4 = High[bar] + range1;
      value5 = Low [bar] - range1;
      if (val1 == 0 && val2 == 0 && p == 1 )
         {
         if (value4 < r)
          {
          r = value4; 
          bs = r; 
          Ind_Buffer1[bar]=r; 
          Ind_Buffer2[bar]=0;
          }
         else
          {
        //r = r; 
          bs = r; 
          Ind_Buffer1[bar]=r; 
          Ind_Buffer2[bar]=0;}
          }
         if( val1 == 0 && val2 == 0 && p == 2 && value5 >  r)                   
          {
          r = value5; 
          bs = r; 
          Ind_Buffer1[bar]=0; 
          Ind_Buffer2[bar]=r;
          }
         if( val1 == 0 && val2 == 0 && p == 2 && value5 <= r)        
          {
        //r = r; 
          bs = r; 
          Ind_Buffer1[bar]=0; 
          Ind_Buffer2[bar]=r;
          } 
        //+--- ���������� �������� ���������� +====+
        if ((bar==2)||(bar==1))
          {
           MEMORY[bar-1]=p;
           T2[bar-1]=Time[bar];
          }
        //+---+====================================+     
          
         bar--;
        }
   
   if (EnableAlerts == 1 && bs > 0 && bs != bs1)
      {
      bs1 = bs;
      a1 = FileOpen("alert1" + SignalID,";");
      FileWrite(a1,"BT1Stop value for " , Symbol() , " is " , bs , " Current Close=" , PriceSeries(Input_Price_Customs,0) , " M" + Period());
      FileClose(a1);
      }
//----
   return(0);
  }
//+---------------------------------------------------------------------------------------------------------------------------+
//----+ �������� ������� INDICATOR_COUNTED (���� INDICATOR_COUNTED.mqh ������� �������� � ����� (����������): 
#include <INDICATOR_COUNTED.mqh> ////////////////////////////////                                  MetaTrader\experts\include)
//+---------------------------------------------------------------------------------------------------------------------------+
//----+ �������� ������� PriceSeries, ���� PriceSeries.mqh ������� �������� � ����� (����������): MetaTrader\experts\include
//----+ �������� ������� PriceSeriesAlert (�������������� ������� ����� PriceSeries.mqh)
#include <PriceSeries.mqh>
//+---------------------------------------------------------------------------------------------------------------------------+