#property copyright "Zarko Asenov"
#property link      "jaltoh.6x.to"

/* General functionality settings */
extern int          Slippage = 3;
extern double       LotSize = 0.12;
extern double       PipStep = 16.0; // For variable spread brokers keep it at the average guaranteed spread
extern bool         InitialBuyOrder = true;
extern int          MagicNumber =  1771611;
extern double       Close_All_Profit = 10.0;
extern bool         Clear_Stops = true;
extern double       Order_Lifetime_Days = 0.2;
extern bool         Add_Spread_to_PipStep = false; // For fixed spread brokers - be sure about this!
extern bool         Add_MinStop_to_PipStep = true;
extern double       Max_spread = 22.0;
extern double       Hours_Bring_Even = 0.0;
extern double       Multi_Coef = 1.0; // multiplies LotSize PipStep Close_All_Profit Min_Bands
extern bool         Debug_Print = false;
<<<<<<< HEAD
extern double       LTS_LotSize = 0.0; /* Leave 0 for no Long Term Strategy trading */
extern double       LTS_Close_All_Profit = 10.0;
extern int          Max_LTS_Positions = 1;
extern int          Grid_Base_Period = PERIOD_M5;
=======
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

/* Bollinger bands */
extern int          Bands_Days = 10;
extern int          Bands_Deviations = 4;
extern double       Min_Bands = 0.017;
extern int          Num_Bands = 2;

<<<<<<< HEAD
/* Stochastic oscillator */
extern bool         Check_Stoch = true;
extern int          Stoch_K = 6;
extern int          Stoch_D = 4;
extern int          Stoch_Slow = 3;
extern int          Stoch_num_pos = 2;
extern double       Stoch_border = 40.0;
extern int          Stochastic_Period = PERIOD_H4;
=======
/* MACD */
extern bool         Check_MACD = true;
extern int          MACD_fast = 5;
extern int          MACD_slow = 34;
extern int          MACD_signal = 12;
extern int          MACD_num_pos = 2;
extern double       MACD_floor = 0.0;
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

/* Average Directional Movement Index */
extern bool         Check_ADX = true;
extern int          ADX_positions_to_check = 2;
extern int          ADX_period_in_days = 4;
extern double       ADX_min = 37.0;

/* Average True Range */
extern double       ATR_Floor = 0.0055;
extern int          ATR_period_in_days = 35;
extern int          ATR_positions_to_check = 3;
<<<<<<< HEAD
extern int          ATR_Period = PERIOD_H4;
=======
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

/* Globals variables */
int longOrderCount = 0;
int shortOrderCount = 0;
double startPrice = 0.0;
int digits = 5;
bool initOrdersDone = false;
double spread = 0.0;
double pipstep_adjusted = 0.0;
double current_adx = 0.0;
<<<<<<< HEAD
double neg_adx_min = 0.0;
double neg_stoch_border = 0.0;
=======
double current_macd = 0.0;
double neg_adx_min = 0.0;
double neg_macd_floor = 0.0;
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
double current_atr = 0.0;
int order_lifetime = 0;
double time_bring_even = 0.0;
double bandwidth = 0.0;
double adjustedLotSize = 0.0;
double adjustedPipStep = 0.0;
double adjustedClose_All_Profit = 0.0;
double adjustedMin_Bands = 0.0;
double tick_value = 0.0;
double total_spread_loss = 0.0;
<<<<<<< HEAD
int prev_signal = 0;
int lts_magic_number = -1;
int lts_long_positions = 0;
int lts_short_positions = 0;
double current_stoch_k = 0.0;
double current_stoch_d = 0.0;


#define SIG_NO_TREND 0
#define SIG_IN_TREND 1
#define SIG_GO_BUY 2
#define SIG_GO_SELL 3
=======
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9


int init()
{
<<<<<<< HEAD
    if (Stoch_border > 100.0) Stoch_border = 100.0;
    
    neg_stoch_border = 100.0 - Stoch_border;
    neg_adx_min = -1.0 * ADX_min;
    
    order_lifetime = MathRound(Order_Lifetime_Days * 86400);
    time_bring_even = 3600 * Hours_Bring_Even;
    lts_magic_number = MagicNumber + 1;

    adjustedLotSize = LotSize * Multi_Coef;
    adjustedPipStep = PipStep;
    adjustedClose_All_Profit = Close_All_Profit * Multi_Coef;
    adjustedMin_Bands = Min_Bands;
=======
    neg_adx_min = -1.0 * ADX_min;
    neg_macd_floor = -1.0 * MACD_floor;
    order_lifetime = MathRound(Order_Lifetime_Days * 86400);
    time_bring_even = 3600 * Hours_Bring_Even;

    adjustedLotSize = LotSize * Multi_Coef;
    adjustedPipStep = PipStep * Multi_Coef;
    adjustedClose_All_Profit = Close_All_Profit * Multi_Coef;
    adjustedMin_Bands = Min_Bands * Multi_Coef;
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    
    tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
    digits = MarketInfo(Symbol(), MODE_DIGITS);
    
    Print("Digits: " + digits + " Negative ADX floor: " + neg_adx_min);
   
    return(0);
}

double get_minstop()
{
    return( MarketInfo(Symbol(), MODE_STOPLEVEL) + 1.0 );
}


double
get_adx(int index)
{
    double ADX1, ADX2;

<<<<<<< HEAD
    current_adx = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_MAIN, index);
    ADX1 = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_PLUSDI, index);
    ADX2 = iADX(NULL, PERIOD_D1, ADX_period_in_days, PRICE_CLOSE, MODE_MINUSDI, index);
=======
    current_adx = iADX(NULL, PERIOD_H4, ADX_period_in_days, PRICE_CLOSE, MODE_MAIN, index);
    ADX1 = iADX(NULL, PERIOD_H4, ADX_period_in_days, PRICE_CLOSE, MODE_PLUSDI, index);
    ADX2 = iADX(NULL, PERIOD_H4, ADX_period_in_days, PRICE_CLOSE, MODE_MINUSDI, index);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

    if(ADX1 >= ADX2)
        return(current_adx);
    else
        return(-1.0 * current_adx);
}

bool
adx_is_sell(int num_pos)
{
    if (Check_ADX == false) return (true);

    
    for (int i = 0; i < num_pos; i++) {
<<<<<<< HEAD
        if (get_adx(i) > neg_adx_min) return (false);
=======
        if (get_adx(i) > -ADX_min) return (false);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    }

    return (true);
}

bool
adx_is_buy(int num_pos)
{
    if (Check_ADX == false) return (true);

    for (int i = 0; i < num_pos; i++) {
        current_adx = get_adx(i);
        if (current_adx < ADX_min) return (false);
    }

    return (true);
}

<<<<<<< HEAD
bool
stoch_is_buy(int num_pos)
{
    if (Check_Stoch == false) return (true);
    
    for (int i = 0; i < num_pos; i++) {
          current_stoch_k = iStochastic(NULL, Stochastic_Period, 6, 4, 3, MODE_EMA, 0, MODE_MAIN, i);
          current_stoch_d = iStochastic(NULL, Stochastic_Period, 6, 4, 3, MODE_EMA, 0, MODE_SIGNAL, i);
          
          if (current_stoch_k > Stoch_border) return(false);
//          if (current_stoch_k < current_stoch_d) return (false);
=======
double Stoch_border = 40.0;
double neg_stoch_border = 60.0;
double current_stoch_k = 0.0;
double current_stoch_d = 0.0;

bool
macd_is_sell(int num_pos)
{
    if (Check_MACD == false) return (true);
    
//    for (int i = 0; i < MACD_num_pos; i++) {
//          current_macd = iMACD(NULL, PERIOD_H4, MACD_fast, MACD_slow, MACD_signal, PRICE_CLOSE, MODE_MAIN, i);
//          if (current_macd > neg_macd_floor) return(false);

    for (int i = 0; i < num_pos; i++) {
          current_stoch_k = iCustom(NULL, PERIOD_M15, "Ehlers Fisher transform histo", 12, 4, 5, i);
          current_stoch_d = iCustom(NULL, PERIOD_M15, "Ehlers Fisher transform histo", 12, 4, 4, i);
          if (current_stoch_k > 0.0 || current_stoch_d > 0.0) return (false);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    }
    
    return(true);
}
<<<<<<< HEAD
     
bool
stoch_is_sell(int num_pos)
{
    if (Check_Stoch == false) return (true);
    
    for (int i = 0; i < num_pos; i++) {
          current_stoch_k = iStochastic(NULL, Stochastic_Period, 6, 4, 3, MODE_EMA, 0, MODE_MAIN, i);
          current_stoch_d = iStochastic(NULL, Stochastic_Period, 6, 4, 3, MODE_EMA, 0, MODE_SIGNAL, i);
          
          if (current_stoch_k < neg_stoch_border) return(false);
//          if (current_stoch_k > current_stoch_d) return (false);
    }
    
=======

bool
macd_is_buy(int num_pos)
{
    if (Check_MACD == false) return (true);
/*    
    for (int i = 0; i < MACD_num_pos; i++) {
//          current_macd = iMACD(NULL, PERIOD_H4, MACD_fast, MACD_slow, MACD_signal, PRICE_CLOSE, MODE_MAIN, i);
//          if (current_macd < MACD_floor) return(false);
    }
*/

    for (int i = 0; i < num_pos; i++) {
          current_stoch_k = iCustom(NULL, PERIOD_M15, "Ehlers Fisher transform histo", 12, 4, 5, i);
          current_stoch_d = iCustom(NULL, PERIOD_M15, "Ehlers Fisher transform histo", 12, 4, 4, i);
          if (current_stoch_k < 0.0 || current_stoch_d < 0.0) return (false);
    }
    
     
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    return(true);
}

bool
atr_is_go(int num_pos)
{
    for (int i = 0; i < num_pos; i++) {
<<<<<<< HEAD
        current_atr = iATR(NULL, ATR_Period, ATR_period_in_days, i);
=======
        current_atr = iCustom(NULL, PERIOD_M15, "ATR ratio", 8, 35, 1.0, 1, i);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
        if (current_atr < ATR_Floor) return (false);
    }
        
   return (true);
}

double
calc_profit()
{
    double presult = 0.0;
    total_spread_loss = 0.0;
    tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
    
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if ((OrderSymbol() != Symbol() || OrderMagicNumber() != MagicNumber) && 
          OrderType() != OP_BUY && OrderType() != OP_SELL) continue;

        double spread_loss = tick_value * OrderLots() * spread;
        presult += (OrderProfit() - spread_loss);
        total_spread_loss += spread_loss;
    }

    return (presult);   
}

double
<<<<<<< HEAD
calc_profit_lts()
{
    if (LTS_LotSize == 0 || lts_magic_number == -1 || 
          lts_long_positions + lts_short_positions < 1) return;
     
    double presult = 0.0;
    total_spread_loss = 0.0;
    tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
    
    for (int i = 0; i < OrdersTotal(); i++) {
        OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
        
        if ((OrderSymbol() != Symbol() || OrderMagicNumber() != lts_magic_number) && 
          OrderType() != OP_BUY && OrderType() != OP_SELL) continue;

        double spread_loss = tick_value * OrderLots() * spread;
        presult += (OrderProfit() - spread_loss);
        total_spread_loss += spread_loss;
    }

    return (presult);   
}

double
getBandwidth(int pos)
{
    bandwidth = iBands(NULL, Grid_Base_Period, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_UPPER, pos) - 
          iBands(NULL, Grid_Base_Period, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_LOWER, pos);
=======
getBandwidth(int pos)
{
    bandwidth = iBands(NULL, PERIOD_M15, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_UPPER, pos) - 
     iBands(NULL, PERIOD_M15, Bands_Days, Bands_Deviations, 0, PRICE_CLOSE, MODE_LOWER, pos);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
   
    return (bandwidth);
}

bool
inBands()
{
    for (int i = 0; i < Num_Bands; i++)
        if (getBandwidth(i) < adjustedMin_Bands) 
           return (false);
  
    return (true);
}

bool
in_trend()
{
     return ( atr_is_go(ATR_positions_to_check) && inBands() );
}

bool
go_buy()
{
<<<<<<< HEAD
     return ( adx_is_buy(ADX_positions_to_check) && stoch_is_buy(Stoch_num_pos) );
=======
     return ( adx_is_buy(ADX_positions_to_check) && macd_is_buy(MACD_num_pos) );
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
}

bool
go_sell()
{
<<<<<<< HEAD
     return ( adx_is_sell(ADX_positions_to_check) && stoch_is_sell(Stoch_num_pos) );
}

int 
get_current_signal() {

     bool in_trend = in_trend();
          
     if ( in_trend && go_buy() ) 
          return(SIG_GO_BUY);
     else if ( in_trend && go_sell() ) 
          return(SIG_GO_SELL);
     else if ( in_trend == true) 
          return(SIG_IN_TREND);
     else 
          return(SIG_NO_TREND);
=======
     return ( adx_is_sell(ADX_positions_to_check) && macd_is_sell(MACD_num_pos) );
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
}

int 
start()
{
    if(startPrice == 0.0) {
<<<<<<< HEAD
        startPrice = iOpen(NULL, Grid_Base_Period, 0);
        if (Debug_Print) Print("startPrice: " + startPrice);
=======
        startPrice = MarketInfo(Symbol(), MODE_ASK);
        if (Debug_Print) 
          Print("startPrice: " + startPrice);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    }

    pipstep_adjusted = adjustedPipStep;
      
    if (Add_Spread_to_PipStep)
        pipstep_adjusted += spread / MarketInfo(Symbol(), MODE_POINT);

    if (Add_MinStop_to_PipStep)
        pipstep_adjusted += get_minstop();

    
    if ( !initOrdersDone && in_trend() ) {
           
        if (go_buy()) {
        
            placeBuyOrder();
            placeInitBuyOrder();
            
        } else if (go_sell()) {
        
            placeSellOrder();
            placeInitSellOrder();
            
        }
        
        initOrdersDone = true;
    }
<<<<<<< HEAD
    
    int current_signal = get_current_signal();
    
    if(iClose(NULL, Grid_Base_Period, 0) > startPrice + pipstep_adjusted * Point) {
    
        startPrice = iClose(NULL, Grid_Base_Period, 0);
=======

    if(MarketInfo(Symbol(), MODE_ASK) > startPrice + pipstep_adjusted * Point) {
    
        startPrice = MarketInfo(Symbol(), MODE_ASK);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
        
        if (Debug_Print) 
          Print("Go buy ,new startPrice:" + startPrice);
          
        // need to set the sl to BE of last order
        moveSellOrderToBE();
        
        moveHitBuyOrdersToBE();
        
        // place buy stops
<<<<<<< HEAD
        if ( current_signal == SIG_GO_BUY ) {
          placeBuyOrder();
        }          
        
    } else if (iClose(NULL, Grid_Base_Period, 0) < startPrice - pipstep_adjusted * Point) {
    
        startPrice = iClose(NULL, Grid_Base_Period, 0);
=======
        if ( in_trend() && go_buy() ) placeBuyOrder();
          
        
    } else if (MarketInfo(Symbol(), MODE_BID) < startPrice - pipstep_adjusted * Point) {
    
        startPrice = MarketInfo(Symbol(), MODE_BID);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
        
        if (Debug_Print) 
          Print("Go sell, new startPrice: " + startPrice);
          
        // need to set the sl to BE of last order
        moveBuyOrderToBE();  

        moveHitSellOrdersToBE();
        
        // place sell stops
<<<<<<< HEAD
        if ( current_signal == SIG_GO_SELL ) {
          placeSellOrder();
        }          
          
    }

    /* Long Term Strategy */
    if (LTS_LotSize != 0.0) {
        if (calc_profit_lts() > LTS_Close_All_Profit) close_all_lts();
        else if (current_signal == SIG_GO_SELL && prev_signal != SIG_GO_SELL) place_lts_sell();
        else if (current_signal == SIG_GO_BUY && prev_signal != SIG_GO_BUY) place_lts_buy();
        else if (current_signal == SIG_GO_BUY && lts_short_positions > 0) close_all_lts();
        else if (current_signal == SIG_GO_SELL && lts_long_positions > 0) close_all_lts();
        prev_signal = current_signal;
    }

=======
        if ( in_trend() && go_sell() ) placeSellOrder();
          
    }

>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    RefreshRates();       
    spread = Point * MarketInfo(Symbol(), MODE_SPREAD);
    double current_profit = calc_profit();

    if (Debug_Print) {
        Comment(
            "SpeedTrain Forte\nby DrZed 2011\n" +
            "Symbol: " + Symbol() + "\n" +
            "Spread: " + spread + "\n" +
            "Bandwidth: " + bandwidth + "\n" +
            "ADX difference: " + current_adx + "\n" +
<<<<<<< HEAD
            "Stoch base: " + current_stoch_k + "\n" +
            "Stoch signal: " + current_stoch_d + "\n" +
            "ATR value: " + current_atr + "\n" +
            "Pipstep adjusted: " + pipstep_adjusted + "\n" +
            "Last orders price: " + startPrice + "\n" +
            "Current close M15 price: " + iClose(NULL, Grid_Base_Period, 0) + "\n" +
=======
            "MACD base: " + current_stoch_k + "\n" +
            "ATR value: " + current_atr + "\n" +
            "Pipstep adjusted: " + pipstep_adjusted + "\n" +
            "Last orders price: " + startPrice + "\n" +
            "Current close M15 price: " + iClose(NULL, PERIOD_M15, 0) + "\n" +
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
            "Profit: " + current_profit + " USD\n" +
            "Total spread loss: " + total_spread_loss + " USD\n"
            );      
    }
   
    if (current_profit > adjustedClose_All_Profit && spread < Max_spread) {
    
        if (Debug_Print) 
            Print("Closing orders on profit " + current_profit + 
                " total spread loss: " + total_spread_loss + 
                " spread: " + spread + " pips.");
            
        for (int i = 0; i < OrdersTotal(); i++) {
        
            OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
            if (OrderSymbol() != Symbol() || OrderMagicNumber() != MagicNumber) continue;
         
            switch ( OrderType() ) {
            
               case OP_BUY:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Blue);
                    break;
                    
               case OP_SELL:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Red);
                    break;
                    
               case OP_SELLSTOP: 
               case OP_BUYSTOP:
                    OrderDelete( OrderTicket() );
                    break;
                    
               default:
                    if (Debug_Print) 
                         Print("Uknown order type " + OrderType() + " of order #" + OrderTicket() );
            }
        }
        
        shortOrderCount = 0;
        longOrderCount = 0;
    }
      
}

<<<<<<< HEAD
void
place_lts_buy()
{
    if (LTS_LotSize == 0.0 || lts_magic_number == -1 || 
          lts_long_positions + lts_short_positions > Max_LTS_Positions) {
        if (Debug_Print) Print("Long Term trading Stradegy disabled!");
        return;
    }
    
    double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
    if (Debug_Print) Print("LTS Placing buy order at " + ask);
         
    int  ticketlong = OrderSend(
                                Symbol(),
                                OP_BUY, 
                                LTS_LotSize,
                                NormalizeDouble(ask, digits),
                                Slippage, 
                                0,
                                0, 
                                "Long Speed Train LTS", 
                                lts_magic_number,
                                0, 
                                Yellow);

    if (ticketlong < 0) {
        if (Debug_Print) 
           Print("LTS Long OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Debug_Print) Print("LTS Buy order placed with ticket " + ticketlong + "  at " + ask);
          
    lts_long_positions++;
}

void
place_lts_sell()
{
    if (LTS_LotSize == 0.0 || lts_magic_number == -1 || 
          lts_long_positions + lts_short_positions > Max_LTS_Positions) {
        if (Debug_Print) Print("Long Term trading Stradegy disabled!");
        return;
    }

    double bid = MarketInfo(Symbol(), MODE_BID); 
    double sellSL = bid + pipstep_adjusted * Point;
          
    if (Debug_Print) Print("Placing LTS sell order at " + bid);
         
    int  ticketshort = OrderSend(
                                 Symbol(), 
                                 OP_SELL, 
                                 LTS_LotSize,
                                 NormalizeDouble(bid, digits), 
                                 Slippage, 
                                 0, 
                                 0, 
                                 "Short Speed Train LTS", 
                                 lts_magic_number, 
                                 0, 
                                 Red);
                                      
    if (ticketshort < 0) {
        if (Debug_Print) Print("Short OrderSend failed with error #", GetLastError());
          
        return;
    }
          
    if (Debug_Print) Print("Sell order placed with ticket " + ticketshort + "  at " + bid);
          
    lts_short_positions++;
}

void
close_all_lts()
{
        for (int i = 0; i < OrdersTotal(); i++) {
        
            OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            
            if (OrderSymbol() != Symbol() || OrderMagicNumber() != lts_magic_number) continue;
         
            switch ( OrderType() ) {
            
               case OP_BUY:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Blue);
                    break;
                    
               case OP_SELL:
                    OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Red);
                    break;
                    
               case OP_SELLSTOP: 
               case OP_BUYSTOP:
                    OrderDelete( OrderTicket() );
                    break;
                    
               default:
                    if (Debug_Print) Print("Uknown LTS order type " + OrderType() + " of order #" + OrderTicket() );
            }
        }
    
    lts_short_positions = 0;
    lts_long_positions = 0;
}

=======
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
void 
placeBuyOrder()
{
    double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
    double buyPrice = ask + pipstep_adjusted * Point;
    double buySL = ask;
          
    if (Debug_Print) 
        Print("Placing buy stop order at " + buyPrice+" with SL at " + buySL);
         
    int ticketlong = OrderSend(
                               Symbol(), 
                               OP_BUYSTOP, 
                               adjustedLotSize, 
                               NormalizeDouble(buyPrice, digits),
                               Slippage, 
                               NormalizeDouble(buySL, digits), 
                               0.0, 
                               "Long Speed Train",
                               MagicNumber, 
                               order_lifetime + TimeCurrent(), 
                               Blue);
                                      
    if (ticketlong < 0) {
        Print("Long OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Debug_Print) 
       Print("Buy stop order placed with ticket " + ticketlong + " at " + buyPrice);
          
    longOrderCount++;
}
    
void 
placeSellOrder()
{
    double bid = MarketInfo(Symbol(), MODE_BID) ; 
          
    double sellPrice = bid - pipstep_adjusted * Point;
    double sellSL = bid;
          
          
    if (Debug_Print) 
         Print("Placing sell stop order at " + sellPrice + " with SL at " + sellSL);
         
    int  ticketshort = OrderSend(
                                 Symbol(), 
                                 OP_SELLSTOP, 
                                 adjustedLotSize, 
                                 NormalizeDouble(sellPrice, digits),
                                 Slippage, 
                                 NormalizeDouble(sellSL, digits), 
                                 0.0, 
                                 "Short Speed Train", 
                                 MagicNumber, 
                                 order_lifetime + TimeCurrent(), 
                                 Red);
                                      
    if (ticketshort < 0) {
        Print("Short OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Debug_Print) 
         Print("Sell stop order placed with ticket " + ticketshort + " at " + sellPrice);
          
    shortOrderCount++;
}
      
void 
placeInitBuyOrder()
{
    double ask = MarketInfo(Symbol(), MODE_ASK) ; 
          
    double buySL = ask - pipstep_adjusted * Point;

    if (Debug_Print) 
<<<<<<< HEAD
         Print("Placing buy order at " + ask + " with SL at " + buySL);
=======
         Print("Placing buy stop order at " + ask + " with SL at " + buySL);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
         
    int  ticketlong = OrderSend(
                                Symbol(),
                                OP_BUY, 
                                adjustedLotSize, 
                                NormalizeDouble(ask, digits),
                                Slippage, 
                                NormalizeDouble(buySL, digits), 
                                0, 
                                "Long Speed Train", 
                                MagicNumber, 
                                0, 
                                Blue);
                                      
    if (ticketlong < 0) {
        if (Debug_Print) 
           Print("Long OrderSend failed with error #", GetLastError()); 
        return;
    }
          
    if (Debug_Print) 
<<<<<<< HEAD
          Print("Buy order placed with ticket " + ticketlong + "  at " + ask);
=======
          Print("Buy stop order placed with ticket " + ticketlong + "  at " + ask);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
          
    longOrderCount++;        
}
      
      
void 
placeInitSellOrder()
{
     
    double bid = MarketInfo(Symbol(), MODE_BID); 
    double sellSL = bid + pipstep_adjusted * Point;
          
    if (Debug_Print) 
<<<<<<< HEAD
        Print("Placing sell order at "+bid+" with SL at "+sellSL);
=======
        Print("Placing sell stop order at "+bid+" with SL at "+sellSL);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
         
    int  ticketshort = OrderSend(
                                 Symbol(), 
                                 OP_SELL, 
                                 adjustedLotSize, 
                                 NormalizeDouble(bid, digits), 
                                 Slippage, 
                                 NormalizeDouble(sellSL, digits), 
                                 0, 
                                 "Short Speed Train", 
                                 MagicNumber, 
                                 0, 
                                 Red);
                                      
    if (ticketshort < 0) {
        if (Debug_Print) 
          Print("Short OrderSend failed with error #", GetLastError());
          
        return;
    }
          
    if (Debug_Print) 
<<<<<<< HEAD
       Print("Sell order placed with ticket " + ticketshort + "  at " + bid);
=======
       Print("Sell stop order placed with ticket " + ticketshort + "  at " + bid);
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
          
    shortOrderCount++;        
}

<<<<<<< HEAD
=======
double Panic_Profit = -200.0;

>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
void 
moveHitBuyOrdersToBE()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if (OrderMagicNumber() == MagicNumber && OrderType() == OP_BUY &&
            TimeCurrent() - OrderOpenTime() > time_bring_even)
            
<<<<<<< HEAD
                OrderModify(
=======
                bool res = OrderModify(
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
                         OrderTicket(),                                
                         OrderOpenPrice(), 
                         OrderOpenPrice(), 
                         OrderTakeProfit(), 
                         OrderExpiration(), 
                         Blue);

<<<<<<< HEAD
=======

                if ( false) res = OrderModify(
                         OrderTicket(),                                
                         OrderOpenPrice(), 
                         OrderOpenPrice() - Point * get_minstop(), 
                         OrderTakeProfit(), 
                         OrderExpiration(), 
                         Blue);
                
                 if (OrderProfit() < Panic_Profit) OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Blue);

>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    }
}
      
void 
moveHitSellOrdersToBE()
{
    for(int j = 0; j < OrdersTotal(); j++)
    {
        OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
            
        if (OrderMagicNumber() == MagicNumber && OrderType() == OP_SELL &&
            TimeCurrent() - OrderOpenTime() > time_bring_even)

<<<<<<< HEAD
                OrderModify(
=======
                bool res = OrderModify(
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
                         OrderTicket(),                                
                         OrderOpenPrice(), 
                         OrderOpenPrice(), 
                         OrderTakeProfit(), 
                         OrderExpiration(), 
                         Red);   

<<<<<<< HEAD
=======
                if ( false) res = OrderModify(
                         OrderTicket(),                                
                         OrderOpenPrice(), 
                         OrderOpenPrice() + Point * get_minstop(), 
                         OrderTakeProfit(), 
                         OrderExpiration(), 
                         Red);
                         
                if (OrderProfit() < Panic_Profit) 
                         OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), Slippage, Red);

>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
    }
}

void 
moveBuyOrderToBE()
{
        int lastTicket = 0;
          
        for(int j = 0; j < OrdersTotal(); j++)
        {
            OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
<<<<<<< HEAD
            
            if (OrderMagicNumber() == MagicNumber && OrderType() == OP_BUYSTOP)
            {
               int ticketId = OrderTicket() ;
               if (ticketId > lastTicket) lastTicket = ticketId;
           }
       }
       
       if(lastTicket > 0) {
       
=======
            if (OrderMagicNumber() != MagicNumber || OrderType() != OP_BUYSTOP) continue;
            
               int ticketId = OrderTicket();
               if (ticketId > lastTicket) lastTicket = ticketId;
            
       }
       
       if(lastTicket > 0) {
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
            OrderSelect(lastTicket, SELECT_BY_TICKET, MODE_TRADES);
            
            if (Debug_Print) 
               Print("Moving long order number " + lastTicket + " to BE at " + OrderOpenPrice() );
       
<<<<<<< HEAD
            int ticketlong = OrderModify(
                                        lastTicket, 
                                        OrderOpenPrice(), 
                                        OrderOpenPrice(), 
                                        OrderTakeProfit(), 
                                        OrderExpiration(), 
                                        Blue);
          
                                      
            if (ticketlong < 0 && Debug_Print) 
                Print("Long SL2BE order failed with error #", GetLastError());
                 
=======
            bool res = OrderModify(
                                lastTicket, 
                                OrderOpenPrice(), 
                                OrderOpenPrice(), 
                                OrderTakeProfit(), 
                                OrderExpiration(), 
                                Blue);
          
                                      
            if (!res && Debug_Print) 
                Print("Long SL2BE order failed with error #", GetLastError());
          
            if (false)
                     res = OrderModify(
                                        lastTicket, 
                                        OrderOpenPrice(), 
                                        OrderOpenPrice() - Point * get_minstop(), 
                                        OrderTakeProfit(), 
                                        OrderExpiration(), 
                                        Blue);
                                        
                   
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
       }           
}
      
void moveSellOrderToBE()
{
     int lastTicket=0;
      
     for(int j = 0; j < OrdersTotal(); j++) {
          OrderSelect(j, SELECT_BY_POS, MODE_TRADES);
<<<<<<< HEAD

          if (OrderMagicNumber() == MagicNumber && OrderType() == OP_SELLSTOP)
          {
               int ticketId = OrderTicket();
               if(ticketId > lastTicket) lastTicket = ticketId;
          }
=======
          if (OrderMagicNumber() != MagicNumber || OrderType() != OP_SELLSTOP) continue;
          
               int ticketId = OrderTicket();
               if(ticketId > lastTicket) lastTicket = ticketId;
          
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9

     }
       
     if(lastTicket > 0) {
          OrderSelect(lastTicket, SELECT_BY_TICKET, MODE_TRADES);
       
          if (Debug_Print) 
              Print("Moving short order number " + lastTicket+ " to BE at " + OrderOpenPrice());
               
<<<<<<< HEAD
          int ticketshort = OrderModify(
=======
          bool res = OrderModify(
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
                                        lastTicket, 
                                        OrderOpenPrice(), 
                                        OrderOpenPrice(), 
                                        OrderTakeProfit(), 
                                        OrderExpiration(), 
                                        Red);
                                      
<<<<<<< HEAD
          if (ticketshort < 0)
                 if (Debug_Print) Print("Short SL2BE order failed with error #",GetLastError()); 
             
=======
          if (false) {
                 if (Debug_Print) Print("Short SL2BE order failed with error #",GetLastError()); 
                                    res = OrderModify(
                                        lastTicket, 
                                        OrderOpenPrice(), 
                                        OrderOpenPrice() + Point * get_minstop(), 
                                        OrderTakeProfit(), 
                                        OrderExpiration(), 
                                        Red);
              }
                         
>>>>>>> d1f8eca3054a866379156d71012f6feb4bddbde9
     }
     
}

